/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.T3AVDTHV35TestAtlanticOcean;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests the action {@link AnalyzeInputSourceAction}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 * TODO Faire repasser ces tests
 */
@Ignore
public class AnalyzeInputSourceActionAtlanticOceanIT extends AnalyzeInputSourceActionITSupport implements T3AVDTHV35TestAtlanticOcean {

    @Test
    @Override
    public void testExecute_OA_2000() throws Exception {
        testExecute(318, 36, 354, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2001() throws Exception {
        testExecute(298, 63, 361, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2002() throws Exception {
        testExecute(184, 133, 317, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2003() throws Exception {
        testExecute(218, 157, 375, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2004() throws Exception {
        testExecute(121, 61, 182, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2005() throws Exception {
        testExecute(93, 31, 124, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2006() throws Exception {
        testExecute(70, 27, 97, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2007() throws Exception {
        testExecute(53, 27, 80, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2008() throws Exception {
        testExecute(46, 14, 60, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2009() throws Exception {
        testExecute(64, 21, 85, 0);
    }

    @Test
    @Override
    public void testExecute_OA_2010() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_OA_2011() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_OA_2012() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_OA_2013() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_OA_2014() throws Exception {
        testExecute(75, 13, 80, 8);
    }

    @Test
    @Override
    public void testExecute_OA_2015() throws Exception {
        testExecute(75, 13, 80, 8);
    }

}
