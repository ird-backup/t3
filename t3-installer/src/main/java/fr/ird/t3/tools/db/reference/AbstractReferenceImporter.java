/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3ServiceSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Abstract reference importer class.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractReferenceImporter<E extends TopiaEntity> extends T3ServiceSupport implements TopiaTransactionAware {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractReferenceImporter.class);

    @Override
    public void setTransaction(TopiaContext transaction) {
    }

    public final void execute(File inputFile) throws Exception {

        // do inject in the tool
        newService(IOCService.class).injectExcept(this);

        before();

        List<E> universe = loadFile(inputFile);

        if (log.isInfoEnabled()) {
            log.info("Will add " + universe.size() + " entities.");
        }
        getTransaction().commitTransaction();
    }

    protected abstract void before() throws TopiaException;

    protected abstract List<E> loadFile(File inputFile) throws TopiaException, IOException;

    protected final int convertToInt(String[] cells,
                                     int lineNumber,
                                     int cellNumber) {
        String cell = cells[cellNumber];
        try {
            Integer result = Integer.valueOf(cell.trim());
            return result;
        } catch (NumberFormatException e) {
            throw new IllegalStateException(
                    "At line [" + lineNumber + "], cell [" + cellNumber +
                    "] could not format " + cell + " to integer");
        }
    }

    protected final int convertToIntTenTimes(String[] cells,
                                             int lineNumber,
                                             int cellNumber) {
        String cell = cells[cellNumber];
        try {

            int result = (int) (Float.valueOf(cell.trim()) * 10);
            return result;
        } catch (NumberFormatException e) {
            throw new IllegalStateException(
                    "At line [" + lineNumber + "], cell [" + cellNumber +
                    "] could not format " + cell + " to integer");
        }
    }

    protected final float convertToFloat(String[] cells,
                                         int lineNumber,
                                         int cellNumber) {
        String cell = cells[cellNumber];
        try {

            float result = Float.valueOf(cell.trim());
            return result;
        } catch (NumberFormatException e) {
            throw new IllegalStateException(
                    "At line [" + lineNumber + "], cell [" + cellNumber +
                    "] could not format " + cell + " to float");
        }
    }

    protected final Species getSpecie(Map<Integer, Species> specieByCode,
                                      int specieCode,
                                      int lineNumber) {

        Species species = specieByCode.get(specieCode);
        if (species == null) {
            throw new NullPointerException(
                    "At line [" + lineNumber + "], species with code [" +
                    specieCode + "] does not exists in db.");
        }
        return species;
    }

    protected final Ocean getOcean(Map<Integer, Ocean> oceanByCode,
                                   int oceanCode,
                                   int lineNumber) {

        Ocean ocean = oceanByCode.get(oceanCode);
        if (ocean == null) {
            throw new NullPointerException(
                    "At line [" + lineNumber + "], ocean with code [" +
                    oceanCode + "] does not exists in db.");
        }
        return ocean;
    }

    protected final Country getCountry(Map<Integer, Country> countryBycode,
                                       int countryCode,
                                       int lineNumber) {

        Country ocean = countryBycode.get(countryCode);
        if (ocean == null) {
            throw new NullPointerException(
                    "At line [" + lineNumber + "], country with code [" +
                    countryCode + "] does not exists in db.");
        }
        return ocean;
    }
}
