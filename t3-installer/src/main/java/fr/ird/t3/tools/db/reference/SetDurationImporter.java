/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SchoolTypeDAO;
import fr.ird.t3.entities.reference.SetDuration;
import fr.ird.t3.entities.reference.SetDurationDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A tool to import content of table {@link SetDuration} from a incoming file
 * named {@code tempscal.txt}.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SetDurationImporter extends AbstractReferenceImporter<SetDuration> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SetDurationImporter.class);

    @InjectDAO(entityType = SetDuration.class)
    protected SetDurationDAO dao;

    @InjectDAO(entityType = SchoolType.class)
    protected SchoolTypeDAO schoolTypeDAO;

    @InjectFromDAO(entityType = Ocean.class)
    protected List<Ocean> oceans;

    @InjectFromDAO(entityType = Country.class)
    protected List<Country> countries;

    protected SchoolType schoolTypeObject;

    protected SchoolType schoolTypeFree;

    protected Map<Integer, Ocean> oceanByCode;

    protected Map<Integer, Country> countryBycode;

    @Override
    protected void before() throws TopiaException {

        if (CollectionUtils.isEmpty(oceans)) {
            throw new IllegalStateException("No ocean found in db.");
        }


        if (CollectionUtils.isEmpty(countries)) {
            throw new IllegalStateException("No country found in db.");
        }

        schoolTypeObject = schoolTypeDAO.findByCode(1);
        schoolTypeFree = schoolTypeDAO.findByCode(2);
        long nb = dao.count();

        if (nb > 0) {
            throw new IllegalStateException(
                    "Some Setduration found in db.");
        }
        oceanByCode = T3EntityHelper.splitBycode(oceans);
        countryBycode = T3EntityHelper.splitBycode(countries);

    }

    @Override
    protected List<SetDuration> loadFile(File inputFile) throws TopiaException, IOException {

        List<SetDuration> result = new ArrayList<SetDuration>();

        LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
        try {

            String line;


            // first line is not useable
            reader.readLine();

            while ((line = reader.readLine()) != null) {

                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.trim().split("\\s+");

                // always check we have 103 cells
                if (cells.length != 9) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have 9 cells but had here " +
                            cells.length);
                }

                loadLine(result,
                         lineNumber,
                         cells
                );
            }
        } finally {
            reader.close();
        }

        return result;
    }


    protected void loadLine(List<SetDuration> result,
                            int lineNumber,
                            String[] cells) throws TopiaException {

        // first cell is ocean code
        int oceanCode = convertToInt(cells, lineNumber, 0);
        Ocean ocean = getOcean(oceanByCode, oceanCode, lineNumber);

        // second cell is species code
        int countryCode = convertToInt(cells, lineNumber, 1);
        Country country = getCountry(countryBycode, countryCode, lineNumber);

        int year = convertToInt(cells, lineNumber, 2);

        float nullSetValue;
        float a;
        float b;
        SetDuration setDuration;

        // 4/5/6 : cell is null set value / parameter A and parameter B for schoolTypeObject

        nullSetValue = convertToFloat(cells, lineNumber, 3);
        a = convertToFloat(cells, lineNumber, 4);
        b = convertToFloat(cells, lineNumber, 5);

        setDuration = dao.create(
                SetDuration.PROPERTY_OCEAN, ocean,
                SetDuration.PROPERTY_COUNTRY, country,
                SetDuration.PROPERTY_SCHOOL_TYPE, schoolTypeObject,
                SetDuration.PROPERTY_YEAR, year,
                SetDuration.PROPERTY_NULL_SET_VALUE, nullSetValue,
                SetDuration.PROPERTY_PARAMETER_A, a,
                SetDuration.PROPERTY_PARAMETER_B, b
        );
        result.add(setDuration);

        // 7/8/9 : cell is null set value / parameter A and parameter B for schoolTypefree

        nullSetValue = convertToFloat(cells, lineNumber, 6);
        a = convertToFloat(cells, lineNumber, 7);
        b = convertToFloat(cells, lineNumber, 8);

        setDuration = dao.create(
                SetDuration.PROPERTY_OCEAN, ocean,
                SetDuration.PROPERTY_COUNTRY, country,
                SetDuration.PROPERTY_SCHOOL_TYPE, schoolTypeFree,
                SetDuration.PROPERTY_YEAR, year,
                SetDuration.PROPERTY_NULL_SET_VALUE, nullSetValue,
                SetDuration.PROPERTY_PARAMETER_A, a,
                SetDuration.PROPERTY_PARAMETER_B, b
        );
        result.add(setDuration);
    }
}
