/*
 * #%L
 * T3 :: Business
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.reference.zone.Zone;
import fr.ird.t3.services.T3ServiceSupport;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaDAO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * abstract zone importer tool.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractZoneImporter<Z extends Zone, D extends TopiaDAO<Z>> extends T3ServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractZoneImporter.class);

    protected final String versionId;

    protected final String versionLibelle;

    protected final String versionStartDate;

    protected final String versionEndDate;

    protected final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");

    protected D dao;

    protected final int nbRequiedCell;

    public AbstractZoneImporter(String versionId,
                                String versionLibelle,
                                Date versionStartDate,
                                Date versionEndDate,
                                int nbRequiedCell) {
        this.versionId = versionId;
        this.versionLibelle = versionLibelle;
        this.versionStartDate = formatDate(versionStartDate);
        this.versionEndDate = formatDate(versionEndDate);
        this.nbRequiedCell = nbRequiedCell;
    }

    protected abstract D getDAO() throws TopiaException;

    public void execute(InputStream inputStream,
                        File outputFile) throws Exception {

        dao = getDAO();

        List<Z> zones = dao.findAll();

        if (CollectionUtils.isNotEmpty(zones)) {
            for (Z zone : zones) {
                dao.delete(zone);
            }
        }

        prepareExecute();

        BufferedWriter writer =
                new BufferedWriter(new FileWriter(outputFile, false));
        try {
            if (log.isInfoEnabled()) {
                log.info("Will create update sql file in " + outputFile);
            }
            List<Z> universe = loadFile(inputStream, writer);

            if (log.isInfoEnabled()) {
                log.info("Will add " + universe.size() +
                         " entities in ZoneET table.");
            }
        } finally {
            writer.close();
        }
        getTransaction().commitTransaction();
    }

    protected void prepareExecute() throws TopiaException {
    }

    private List<Z> loadFile(InputStream inputStream,
                             BufferedWriter writer) throws TopiaException, IOException {

        List<Z> result = Lists.newArrayList();

        LineNumberReader reader =
                new LineNumberReader(new InputStreamReader(inputStream, Charsets.UTF_8));

        try {

            String line;

            while ((line = reader.readLine()) != null) {

                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }
                if (line.startsWith("#") || StringUtils.isBlank(line)) {

                    // this is not a data line
                    continue;
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.split("\\|");

                // always check we have 103 cells
                if (cells.length != nbRequiedCell) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have " + nbRequiedCell + " cells but had here " +
                            cells.length);
                }

                String insertLine = loadLine(result,
                                             lineNumber,
                                             cells
                );
                writer.newLine();
                writer.append(insertLine);
            }
        } finally {
            reader.close();
        }
        return result;
    }

    protected abstract String loadLine(List<Z> result,
                                       int lineNumber,
                                       String[] cells) throws TopiaException;

    protected String formatDate(Date date) {
        String result;
        if (date == null) {
            result = "null";
        } else {
            String f = dateFormat.format(date);
            result = "TIMESTAMP '" + f + " 00:00:00.0'";
        }
        return result;
    }

    protected int convertToInt(String[] cells,
                               int lineNumber,
                               int cellNumber) {
        String cell = cells[cellNumber];
        try {
            Integer result = Integer.valueOf(cell.trim());
            return result;
        } catch (NumberFormatException e) {
            throw new IllegalStateException(
                    "At line [" + lineNumber + "], cell [" + cellNumber +
                    "] could not format " + cell + " to integer");
        }
    }

    protected String convertToString(String[] cells,
                                     int lineNumber,
                                     int cellNumber) {
        String cell = cells[cellNumber].trim();
        return cell;
    }

}
