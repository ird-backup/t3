/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import fr.ird.t3.T3Configuration;
import fr.ird.t3.T3SqlScriptsImporter;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3ScriptHelper;
import fr.ird.t3.entities.T3TopiaRootContextFactory;
import fr.ird.t3.entities.data.TripImpl;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.entities.user.JdbcConfigurationImpl;
import fr.ird.t3.services.DefaultT3ServiceContext;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaUtil;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * A tool to install the application (run all required scripts).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3ApplicationInstaller {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3ApplicationInstaller.class);

    public static void main(String... args) throws Exception {

        if (args.length != 4) {
            usage();
            System.exit(1);
        } else {

            File connectionFile = new File(args[0]);
            File postgisCreationScript = new File(args[1]);
            File referentielDataScript = new File(args[2]);
            File postgisDataScript = new File(args[3]);

            Preconditions.checkArgument(connectionFile.exists(), connectionFile + " was not found!");
            Preconditions.checkArgument(postgisCreationScript.exists(), postgisCreationScript + " was not found!");
            Preconditions.checkArgument(referentielDataScript.exists(), referentielDataScript + " was not found!");
            Preconditions.checkArgument(postgisDataScript.exists(), postgisDataScript + " was not found!");

            T3ApplicationInstaller installer = new T3ApplicationInstaller(
                    connectionFile,
                    postgisCreationScript,
                    referentielDataScript,
                    postgisDataScript
            );

            try {

                installer.run();

            } finally {
                installer.destroy();
            }
        }
    }

    public static void usage() {
        StringBuilder buffer = new StringBuilder(T3ApplicationInstaller.class.getSimpleName() + " requires 4 parameters :");
        buffer.append("\n").append("- databse connection properties file");
        buffer.append("\n").append("- postgis structure script creation");
        buffer.append("\n").append("- referential scripts directory");
        buffer.append("\n").append("- postgis data script").append("\n");
        System.out.println(buffer.toString());
    }

    protected final File connectionFile;

    protected final File[] postgisDataFiles;

    protected final File ddlScriptsDirectory;

    protected final File referentialScriptsDirectory;

    protected T3ServiceContext serviceContext;

    protected JdbcConfiguration jdbcConfiguration;

    protected T3ApplicationInstaller(File connectionFile,
                                     File ddlScriptsDirectory,
                                     File referentialScriptsDirectory,
                                     File postgisDataScript) {
        this.connectionFile = connectionFile;
        this.ddlScriptsDirectory = ddlScriptsDirectory;
        this.referentialScriptsDirectory = referentialScriptsDirectory;

        postgisDataFiles = postgisDataScript.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".zip");
            }
        });

    }

    public void run() throws Exception {
        if (log.isInfoEnabled()) {
            log.info("1/6 - Setup installer...");
        }
        setup();

        if (log.isInfoEnabled()) {
            log.info("2/6 - Database configuration verifier...");
        }
        checkDatabaseConnection();

        if (log.isInfoEnabled()) {
            log.info("3/6 - Create database schema...");
        }
        createDatabase();

        T3SqlScriptsImporter dllScriptsImporter = new T3SqlScriptsImporter(ddlScriptsDirectory);
        dllScriptsImporter.prepare();

        if (log.isInfoEnabled()) {
            log.info("4/6 - Loading ddl from " + dllScriptsImporter.getScriptsFile().size() + " scripts.");
        }

        dllScriptsImporter.importScripts(serviceContext, Predicates.<File>alwaysTrue());

        T3SqlScriptsImporter referentialScriptsImporter = new T3SqlScriptsImporter(referentialScriptsDirectory);
        referentialScriptsImporter.prepare();


        if (log.isInfoEnabled()) {
            log.info("5/6 - Load referential from " + referentialScriptsImporter.getScriptsFile().size() + " scripts.");
        }
        referentialScriptsImporter.importScripts(serviceContext, Predicates.<File>alwaysTrue());

        File unzipDirectory = createUnzipDirectory("postgis-data");

        if (log.isInfoEnabled()) {
            log.info("6/6 - Import postGis data from " + postgisDataFiles.length + " scripts.");
        }
        for (File postgisDataFile : postgisDataFiles) {

            if (log.isInfoEnabled()) {
                log.info(" o Unzip postGis data...(" + postgisDataFile + ")");
            }
            File unzipFile = unzipFile(unzipDirectory, postgisDataFile);

            if (log.isInfoEnabled()) {
                log.info(" o Load postGis data... from " + unzipFile + " script.");
            }
            loadScriptLineByLine(unzipFile);
        }
    }

    protected void setup() throws IOException {


        // initialize configuration
        T3Configuration configuration = new T3Configuration();
        configuration.init();

        serviceContext = DefaultT3ServiceContext.newContext(
                Locale.getDefault(),
                null,
                null,
                configuration,
                new T3ServiceFactory()
        );

        // attach configuration to servide factory
//        T3ServiceFactory.setConfiguration(configuration);
//        serviceFactory = T3ServiceFactory.newInstance();
        // init I18n
        DefaultI18nInitializer i18nInitializer =
                new DefaultI18nInitializer("t3-i18n");
        i18nInitializer.setMissingKeyReturnNull(true);
        I18n.init(i18nInitializer, Locale.getDefault());

        Properties p = new Properties();
        FileInputStream inputStream = new FileInputStream(connectionFile);
        try {
            p.load(inputStream);
        } finally {
            inputStream.close();
        }

        Preconditions.checkNotNull(p.getProperty("url"), "url property not found in file " + connectionFile);
        Preconditions.checkNotNull(p.getProperty("login"), "login property not found in file " + connectionFile);
        Preconditions.checkNotNull(p.getProperty("password"), "password property not found in file " + connectionFile);

        jdbcConfiguration = new JdbcConfigurationImpl();
        jdbcConfiguration.setUrl(p.getProperty("url"));
        jdbcConfiguration.setLogin(p.getProperty("login"));
        jdbcConfiguration.setPassword(p.getProperty("password"));
    }

    protected void checkDatabaseConnection() throws IOException {

        // check jdbc connection
        try {

            T3EntityHelper.checkJDBCConnection(jdbcConfiguration);

        } catch (Exception e) {
            // can not connect to database
            throw new IllegalStateException("Could not connect to db", e);
        }
    }

    protected void createDatabase() {


        T3TopiaRootContextFactory factory = new T3TopiaRootContextFactory();
        TopiaContext rootContext = factory.newDb(jdbcConfiguration);
        serviceContext.setTransaction(rootContext);

        try {
            boolean schemaFound = isSchemaCreated(rootContext);

            if (!schemaFound) {

                if (log.isInfoEnabled()) {
                    log.info("Will create schema for db (no schema found).");
                }
                // must create the schema
                rootContext.createSchema();

            }
        } catch (TopiaException e) {
            throw new IllegalStateException("could not start db", e);
        }
    }

    protected File unzipFile(File unzipDirectory, File script) throws IOException {

        List<String>[] lists = ZipUtil.scanAndExplodeZip(script, unzipDirectory, null);
        ZipUtil.uncompress(script, unzipDirectory);
        File result = new File(unzipDirectory, lists[0].get(0));
        result.deleteOnExit();
        return result;
    }

    protected File createUnzipDirectory(String name) throws IOException {
        String tmpPath = System.getProperty("java.io.tmpdir");
        File tmpDir = new File(tmpPath);
        File unzupDirectory = new File(tmpDir, name + "_" + System.nanoTime());
        unzupDirectory.deleteOnExit();
        FileUtil.createDirectoryIfNecessary(unzupDirectory);
        return unzupDirectory;
    }

    protected void loadScriptLineByLine(File script) throws TopiaException, IOException {

        TopiaContext tx = serviceContext.getTransaction().beginTransaction();

        try {
            T3ScriptHelper.loadScriptLineByLine(tx, script, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (log.isInfoEnabled()) {
                        log.info("Sql line " + evt.getNewValue() + " done.");
                    }
                }
            });
        } finally {
            tx.closeContext();
        }
    }

    protected boolean isSchemaCreated(TopiaContext tx) throws TopiaException {

        boolean schemaFound;

        schemaFound = TopiaUtil.isSchemaExist(
                ((TopiaContextImplementor) tx).getHibernateConfiguration(),
                TripImpl.class.getName());

        return schemaFound;
    }

    protected void destroy() {

        T3EntityHelper.releaseRootContext(serviceContext.getTransaction());
    }

}
