/*
 * #%L
 * T3 :: Business
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.zone.ZoneEE;
import fr.ird.t3.entities.reference.zone.ZoneEEDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;

/**
 * Tool to import zone ee from a csv file and produce an output sql file with
 * update statement to inject postgis data.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ZoneEEImporter extends AbstractZoneImporter<ZoneEE, ZoneEEDAO> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneEEImporter.class);

    public static final String INSERT_LINE_PATTERN =
            "INSERT INTO ZONEEE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, VERSIONID, " +
            "VERSIONLIBELLE, VERSIONSTARTDATE, VERSIONENDDATE, GID, CODE, LIBELLE, THE_GEOM) VALUES ('%s', %s, %s, '%s', '%s', %s, %s, %s, %s, '%s', '%s');";


    public ZoneEEImporter(String versionId,
                          String versionLibelle,
                          Date versionStartDate,
                          Date versionEndDate) {
        super(versionId,
              versionLibelle,
              versionStartDate,
              versionEndDate,
              4);
    }

    @Override
    protected ZoneEEDAO getDAO() throws TopiaException {
        return T3DAOHelper.getZoneEEDAO(getTransaction());
    }

    @Override
    protected String loadLine(List<ZoneEE> result,
                              int lineNumber,
                              String[] cells) throws TopiaException {

        // first cell is gid code
        int gid = convertToInt(cells, lineNumber, 0);

        // second cell is zone code
        int zoneCode = convertToInt(cells, lineNumber, 1);

        // third cell is libelle
        String libelle = convertToString(cells, lineNumber, 2);

        // fourth cell is postgis field
        String postgis = convertToString(cells, lineNumber, 3);

        ZoneEE zone = dao.create(
                ZoneEE.PROPERTY_GID, gid,
                ZoneEE.PROPERTY_LIBELLE, libelle,
                ZoneEE.PROPERTY_CODE, zoneCode
        );
        result.add(zone);
        String insertLine = String.format(INSERT_LINE_PATTERN,
                                          zone.getTopiaId(),
                                          zone.getTopiaVersion(),
                                          formatDate(zone.getTopiaCreateDate()),
                                          versionId,
                                          versionLibelle,
                                          versionStartDate,
                                          versionEndDate,
                                          zone.getGid(),
                                          zone.getCode(),
                                          zone.getLibelle().replaceAll("'", "''"),
                                          postgis
        );
        if (log.isDebugEnabled()) {
            log.debug(insertLine);
        }
        return insertLine;
    }
}
