/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.reference;

import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Tools to import species conversion length weight.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class LengthWeightConversionImporter extends AbstractReferenceImporter<LengthWeightConversion> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(LengthWeightConversionImporter.class);

    public static final String DEFAULT_TO_WEIGHT_RELATION = "a * Math.pow(L, b)";

    public static final String DEFAULT_TO_LENGTH_RELATION = "Math.pow(P/a, 1/b)";

    @InjectDAO(entityType = LengthWeightConversion.class)
    protected LengthWeightConversionDAO dao;

    @InjectFromDAO(entityType = Ocean.class)
    protected List<Ocean> oceans;

    @InjectFromDAO(entityType = Species.class)
    protected List<Species> species;

    protected Map<Integer, Ocean> oceanByCode;

    protected Map<String, Species> specieByCodeFAO;

    protected Date creationDate;

    @Override
    protected void before() throws TopiaException {

        if (CollectionUtils.isEmpty(oceans)) {
            throw new IllegalStateException("No ocean found in db.");
        }
        if (CollectionUtils.isEmpty(species)) {
            throw new IllegalStateException("No species found in db.");
        }

        oceanByCode = T3EntityHelper.splitBycode(oceans);
        specieByCodeFAO = T3EntityHelper.splitByCodeFAO(species);

        // use as begin date always First january of 1970
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.YEAR, 1970);
        calendar.set(Calendar.DAY_OF_YEAR, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        creationDate = calendar.getTime();
    }

    @Override
    protected List<LengthWeightConversion> loadFile(File inputFile) throws TopiaException, IOException {

        List<LengthWeightConversion> result = new ArrayList<LengthWeightConversion>();

        LineNumberReader reader = new LineNumberReader(new FileReader(inputFile));
        try {

            String line;

            // first line is not useable
            reader.readLine();

            while ((line = reader.readLine()) != null) {

                // there is a line to treat
                if (log.isTraceEnabled()) {
                    log.trace("Incoming line : " + line);
                }

                int lineNumber = reader.getLineNumber();

                if (log.isDebugEnabled()) {
                    log.debug("At Line [" + lineNumber +
                              "] Data line to treat : " + line);
                }

                String[] cells = line.split(";");

                // always check we have 4 cells
                if (cells.length != 3) {
                    throw new IllegalStateException(
                            "At line [" + lineNumber +
                            "], data line must have 4 cells but had here " +
                            cells.length);
                }

                loadLine(result,
                         lineNumber,
                         cells
                );
            }
        } finally {
            reader.close();
        }

        return result;
    }

    protected void loadLine(List<LengthWeightConversion> result,
                            int lineNumber,
                            String[] cells) throws TopiaException {

        // first cell is ocean code
        int oceanCode = convertToInt(cells, lineNumber, 0);

        Ocean ocean = getOcean(oceanByCode, oceanCode, lineNumber);

        // second cell is species code
        String specieCodeFAO = cells[1];
        Species species = specieByCodeFAO.get(specieCodeFAO);

        if (species != null) {

            long nb = dao.countByQuery("SELECT COUNT(*) FROM LengthWeightConversionImpl WHERE ocean = :ocean AND species = :species", "ocean", ocean, "species", species);

            if (nb == 0) {

                // coefficient parameters
                String coefficients = cells[2];

                LengthWeightConversion lengthWeightConversion = dao.create(
                        LengthWeightConversion.PROPERTY_OCEAN, ocean,
                        LengthWeightConversion.PROPERTY_SPECIES, species,
                        LengthWeightConversion.PROPERTY_COEFFICIENTS, coefficients,
                        LengthWeightConversion.PROPERTY_TO_LENGTH_RELATION, DEFAULT_TO_LENGTH_RELATION,
                        LengthWeightConversion.PROPERTY_TO_WEIGHT_RELATION, DEFAULT_TO_WEIGHT_RELATION,
                        LengthWeightConversion.PROPERTY_BEGIN_DATE, creationDate
                );
                result.add(lengthWeightConversion);
            }


        }

    }
}
