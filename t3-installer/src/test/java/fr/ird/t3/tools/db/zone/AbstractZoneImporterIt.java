/*
 * #%L
 * T3 :: Business
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.reference.zone.Zone;
import fr.ird.t3.tools.AbstracToolTest;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * abstract zone importer test.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractZoneImporterIt<Z extends Zone, T extends AbstractZoneImporter<Z, ?>> extends AbstracToolTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractZoneImporterIt.class);

    protected final String dataResourcePath;

    protected final String outputName;

    protected final String versionId;

    protected final String versionLibelle;

    protected final Date versionStartDate;

    protected final Date versionEndDate;

    protected final int expected;

    protected final Class<Z> zoneType;

    // csv data file to use for the test
    protected URL input;

    // where to write extra update sql commands
    protected File outputFile;

    public AbstractZoneImporterIt(
            Class<Z> zoneType,
            String dataResourcePath,
            String outputName,
            String versionId,
            String versionLibelle,
            Date versionStartDate,
            Date versionEndDate,
            int expected

    ) {
        this.zoneType = zoneType;
        this.dataResourcePath = dataResourcePath;
        this.outputName = outputName;
        this.versionId = versionId;
        this.versionLibelle = versionLibelle;
        this.versionStartDate = versionStartDate;
        this.versionEndDate = versionEndDate;
        this.expected = expected;
    }

    protected abstract T createTool();

    @Before
    public void setUp() throws Exception {

        super.setUp();

        if (log.isDebugEnabled()) {
            log.debug("Do test for db " + serviceContext.getTestDir());
        }

        input = getClass().getResource(dataResourcePath);
    }

    @Test
    public void testExecute() throws Exception {

        T tool = createTool();
        tool.setServiceContext(serviceContext);

        Assert.assertNotNull(
                "Could not find input file from " + dataResourcePath, input);

        File testDir = serviceContext.getTestDir();

        outputFile = new File(testDir, outputName);

        InputStream inputStream;
        if (input.toString().endsWith(".zip")) {

            // let's unzip it before using it

            File zipFile = new File(testDir, new File(input.getFile()).getName());

            zipFile.deleteOnExit();

            if (log.isInfoEnabled()) {
                log.info("Will copy zip file [" + input + "] to " + zipFile);
            }
            InputStream tmpStream = input.openStream();

            try {
                // copy it to a nice concrete file
                OutputStream outputStream = new FileOutputStream(zipFile);
                try {
                    IOUtils.copy(tmpStream, outputStream);
                } finally {
                    outputStream.close();
                }
            } finally {
                tmpStream.close();
            }

            if (log.isInfoEnabled()) {
                log.info("Will explode zip file [" + zipFile + "] to " + testDir);
            }
            List<String>[] lists = ZipUtil.scanAndExplodeZip(zipFile, testDir, null);

            // remove this when the superbe explode zip method will do something
            ZipUtil.uncompress(zipFile, testDir);
            File unzipFile;

            List<String> explodedFiles = lists[0];
            List<String> existingFiles = lists[1];
            if (!explodedFiles.isEmpty()) {

                // take the first file
                unzipFile = new File(testDir, explodedFiles.get(0));
            } else {

                // file was alreay here
                unzipFile = new File(testDir, existingFiles.get(0));
            }

            if (log.isInfoEnabled()) {
                log.info("Real file to use in test " + unzipFile);
            }
            unzipFile.deleteOnExit();

            inputStream = new FileInputStream(unzipFile);
        } else {

            inputStream = input.openStream();
        }
        try {
            // launch tools
            tool.execute(inputStream, outputFile);
        } finally {
            inputStream.close();
        }

        // count result
        assertCountEntities(serviceContext.getTransaction(),
                            zoneType,
                            expected);
    }
}
