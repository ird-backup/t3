/*
 * #%L
 * T3 :: Installer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.tools.db.zone;

import fr.ird.t3.entities.reference.zone.ZoneCWP;
import fr.ird.t3.entities.type.T3Date;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Test tools {@link ZoneEEImporter}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
@RunWith(value = Parameterized.class)
public class ZoneCWPImporterIT extends AbstractZoneImporterIt<ZoneCWP, ZoneCWPImporter> {

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {

        Collection<Object[]> result = new ArrayList<Object[]>();
        result.add(new Object[]{"/zone/zones_cwp-1x1.csv.zip", "zones-cwp-1x1.sql", "1x1-2011", "Version 1x1 2011", T3Date.newDate(1, 2011).toBeginDate(), null, 64800});
        result.add(new Object[]{"/zone/zones_cwp-5x5.csv.zip", "zones-cwp-5x5.sql", "5x5-2011", "Version 5x5 2011", T3Date.newDate(1, 2011).toBeginDate(), null, 2592});
        result.add(new Object[]{"/zone/zones_cwp-10x10.csv.zip", "zones-cwp-10x10.sql", "10x10-2011", "Version 10x10 2011", T3Date.newDate(1, 2011).toBeginDate(), null, 648});
        return result;
    }

    public ZoneCWPImporterIT(String dataResourcePath,
                             String outputName,
                             String versionId,
                             String versionLibelle,
                             Date versionStartDate,
                             Date versionEndDate,
                             int expected) {
        super(ZoneCWP.class,
              dataResourcePath,
              outputName,
              versionId,
              versionLibelle,
              versionStartDate,
              versionEndDate,
              expected
        );
    }

    @Override
    protected ZoneCWPImporter createTool() {
        ZoneCWPImporter tool = new ZoneCWPImporter(
                versionId,
                versionLibelle,
                versionStartDate,
                versionEndDate
        );
        return tool;
    }
}

