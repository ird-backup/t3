/*
 * #%L
 * T3 :: Input AVDTH v 33
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

/**
 * Contract to use for all tests on avdth database
 *
 * @author chemit <chemit@codelutin.com>
 * @since 2.0
 */
public interface T3AVDTHV36TestAtlanticOcean {

    void testExecute_OA_2000() throws Exception;

    void testExecute_OA_2001() throws Exception;

    void testExecute_OA_2002() throws Exception;

    void testExecute_OA_2003() throws Exception;

    void testExecute_OA_2004() throws Exception;

    void testExecute_OA_2005() throws Exception;

    void testExecute_OA_2006() throws Exception;

    void testExecute_OA_2007() throws Exception;

    void testExecute_OA_2008() throws Exception;

    void testExecute_OA_2009() throws Exception;

    void testExecute_OA_2010() throws Exception;

    void testExecute_OA_2011() throws Exception;

    void testExecute_OA_2012() throws Exception;

    void testExecute_OA_2013() throws Exception;

    void testExecute_OA_2014() throws Exception;

    void testExecute_OA_2015() throws Exception;

}
