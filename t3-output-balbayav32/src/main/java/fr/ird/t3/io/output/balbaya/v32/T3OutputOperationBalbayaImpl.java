/*
 * #%L
 * T3 :: Output Balbaya v 32
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output.balbaya.v32;

import fr.ird.t3.io.output.T3OutputOperation;

import java.sql.Connection;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;
import static org.nuiton.i18n.I18n.n_;

/**
 * Available operations for the balbaya output pilot.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public enum T3OutputOperationBalbayaImpl implements T3OutputOperation {

    /** To export trips and elementary landings. */
    TRIP_AND_LANDING(n_("t3.output.balbabya.operation.tripAndLanding"), 3) {
        @Override
        protected AbstractBalbayaOperationExecution newExecution(
                Connection connection) {
            return new BalbayaOperationExecutionTripImpl(connection);
        }
    },
    /** to export activities and catches. */
    ACTIVITY_AND_CATCHES(n_("t3.output.balbabya.operation.activityAndCatches"), 1) {
        @Override
        protected AbstractBalbayaOperationExecution newExecution(
                Connection connection) {
            return new BalbayaOperationExecutionActivityImpl(connection);
        }
    },
    /** To export samples. */
    SAMPLE(n_("t3.output.balbabya.operation.sample"), 2) {
        @Override
        protected AbstractBalbayaOperationExecution newExecution(
                Connection connection) {
            return new BalbayaOperationExecutionSampleImpl(connection);
        }
    };

    /** I18n key of the operation. */
    private final String i18nKey;

    /**
     * Balbaya treatment code of the operation.
     * <p/>
     * This is the {@code C_TYPE_D} value of the table {@code A_JEU_D}.
     */
    private final int treatmentId;

    T3OutputOperationBalbayaImpl(String i18nKey, int treatmentId) {
        this.i18nKey = i18nKey;
        this.treatmentId = treatmentId;
    }

    @Override
    public String getId() {
        return name();
    }

    @Override
    public String getLibelle(Locale locale) {
        return l_(locale, i18nKey);
    }

    public int getTreatmentId() {
        return treatmentId;
    }

    protected abstract AbstractBalbayaOperationExecution newExecution(Connection connection);
}
