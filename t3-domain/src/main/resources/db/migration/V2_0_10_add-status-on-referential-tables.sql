---
-- #%L
-- T3 :: Domain
-- %%
-- Copyright (C) 2010 - 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
ALTER TABLE country ADD COLUMN status BOOLEAN;
UPDATE country SET status = TRUE;
ALTER TABLE schoolType ADD COLUMN status BOOLEAN;
UPDATE schoolType SET status = TRUE;
ALTER TABLE fishingContext ADD COLUMN status BOOLEAN;
UPDATE fishingContext SET status = TRUE;
ALTER TABLE harbour ADD COLUMN status BOOLEAN;
UPDATE harbour SET status = TRUE;
ALTER TABLE ocean ADD COLUMN status BOOLEAN;
UPDATE ocean SET status = TRUE;
ALTER TABLE sampleQuality ADD COLUMN status BOOLEAN;
UPDATE sampleQuality SET status = TRUE;
ALTER TABLE sampleType ADD COLUMN status BOOLEAN;
UPDATE sampleType SET status = TRUE;
ALTER TABLE species ADD COLUMN status BOOLEAN;
UPDATE species SET status = TRUE;
ALTER TABLE vesselSizeCategory ADD COLUMN status BOOLEAN;
UPDATE vesselSizeCategory SET status = TRUE;
ALTER TABLE vesselSimpleType ADD COLUMN status BOOLEAN;
UPDATE vesselSimpleType SET status = TRUE;
ALTER TABLE vesselType ADD COLUMN status BOOLEAN;
UPDATE vesselType SET status = TRUE;
ALTER TABLE vessel ADD COLUMN status BOOLEAN;
UPDATE vessel SET status = TRUE;
ALTER TABLE vesselActivity ADD COLUMN status BOOLEAN;
UPDATE vesselActivity SET status = TRUE;
ALTER TABLE weightCategoryLanding ADD COLUMN status BOOLEAN;
UPDATE weightCategoryLanding SET status = TRUE;
ALTER TABLE weightCategoryLogbook ADD COLUMN status BOOLEAN;
UPDATE weightCategoryLogbook SET status = TRUE;
ALTER TABLE weightCategoryTreatment ADD COLUMN status BOOLEAN;
UPDATE weightCategoryTreatment SET status = TRUE;
ALTER TABLE weightCategoryWellPlan ADD COLUMN status BOOLEAN;
UPDATE weightCategoryWellPlan SET status = TRUE;
ALTER TABLE weightCategorySample ADD COLUMN status BOOLEAN;
UPDATE weightCategorySample SET status = TRUE;
ALTER TABLE wellDestiny ADD COLUMN status BOOLEAN;
UPDATE wellDestiny SET status = TRUE;
ALTER TABLE lengthWeightConversion ADD COLUMN status BOOLEAN;
UPDATE lengthWeightConversion SET status = TRUE;
ALTER TABLE speciesLengthStep ADD COLUMN status BOOLEAN;
UPDATE speciesLengthStep SET status = TRUE;
ALTER TABLE setDuration ADD COLUMN status BOOLEAN;
UPDATE setDuration SET status = TRUE;
ALTER TABLE rf1SpeciesForFleet ADD COLUMN status BOOLEAN;
UPDATE rf1SpeciesForFleet SET status = TRUE;
ALTER TABLE localMarketPackagingType ADD COLUMN status BOOLEAN;
UPDATE localMarketPackagingType SET status = TRUE;
ALTER TABLE localMarketPackaging ADD COLUMN status BOOLEAN;
UPDATE localMarketPackaging SET status = TRUE;