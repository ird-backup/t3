/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.decorator.Decorator;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * Fires the {@link InjectDecoratedBeans} annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectDecoratedBeans
 * @since 1.0
 */
public class InjectorDecoratedBeans extends AbstractInjector<InjectDecoratedBeans, LocaleProvider> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(InjectorDecoratedBeans.class);

    protected DecoratorService decoratorService;

    public InjectorDecoratedBeans() {
        super(InjectDecoratedBeans.class);
    }

    @Override
    public void init(T3ServiceContext serviceContext) {
        decoratorService = serviceContext.getServiceFactory().newService(DecoratorService.class, serviceContext);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      LocaleProvider bean,
                                      InjectDecoratedBeans annotation) throws Exception {

        // get object type
        Class<?> objectType = annotation.beanType();

        String dataContainerPath = annotation.dataContainerPath();

        Object fromObject;

        if (StringUtils.isEmpty(dataContainerPath)) {

            fromObject = bean;
        } else {
            fromObject = PropertyUtils.getProperty(bean, dataContainerPath);
        }

        Preconditions.checkNotNull(fromObject,
                                   "Could not find data container from " + bean
                                   + " with path " + dataContainerPath);

        // get param id where to find ids to load
        String path = annotation.path();
        if (StringUtils.isEmpty(path)) {

            // let's use the name of the field
            path = field.getName();
        }

        String decoratorContext = annotation.decoratorContext();
        if (StringUtils.isEmpty(decoratorContext)) {

            // no decorator context givne, then it is a null context
            decoratorContext = null;
        }

        Collection<?> entities = (Collection<?>)
                PropertyUtils.getProperty(fromObject, path);

        Preconditions.checkNotNull(entities,
                                   "Could not find entities from configuration " + fromObject
                                   + " with path " + path);

        Decorator<?> decorator =
                decoratorService.getDecorator(
                        bean.getLocale(),
                        objectType,
                        decoratorContext
                );

        Preconditions.checkNotNull(
                decorator,
                "Could not find decorator for type " +
                objectType + " and context " + decoratorContext);

        Collection<?> ids = null;

        if (annotation.filterById()) {

            boolean useSingleId = annotation.filterBySingleId();

            // let's find ids to keep
            String filterIds = annotation.pathIds();
            if (StringUtils.isEmpty(filterIds)) {

                // let's use the name of the field
                filterIds = field.getName();

                if (useSingleId) {
                    filterIds =
                            filterIds.substring(0, filterIds.length() - 1) + "Id";
                } else {
                    filterIds =
                            filterIds.substring(0, filterIds.length() - 1) + "Ids";
                }
            }
            if (useSingleId) {

                // using a single id
                ids = Arrays.asList(PropertyUtils.getProperty(fromObject, filterIds));
            } else {
                ids = (Collection<?>)
                        PropertyUtils.getProperty(fromObject, filterIds);
            }
        }

        Map<String, String> valueToInject = Maps.newLinkedHashMap();

        if (ids == null) {

            // take all entities
            for (Object entity : entities) {
                String id = getId(entity);
                String str = decorator.toString(entity);
                valueToInject.put(id, str);
            }
        } else {

            // do annotation filter by ids
            for (Object object : entities) {
                String id = getId(object);
                if (ids.contains(id)) {
                    String str = decorator.toString(object);
                    valueToInject.put(id, str);
                }
            }
        }

        if (log.isInfoEnabled()) {
            log.info("Will set " + entities.size() +
                     " entities of type [" + objectType.getName() +
                     "] to field " + field);
        }
        return valueToInject;
    }

    protected String getId(Object o) {
        if (o instanceof TopiaEntity) {
            TopiaEntity topiaEntity = (TopiaEntity) o;

            return topiaEntity.getTopiaId();
        }
        if (o instanceof Idable) {
            Idable idable = (Idable) o;
            return idable.getId();
        }
        throw new IllegalArgumentException("Can not find a way to get id of " + o);
    }
}
