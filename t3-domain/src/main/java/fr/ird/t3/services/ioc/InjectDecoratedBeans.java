/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import fr.ird.t3.entities.reference.Idable;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Map;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * To inject entities or {@link Idable} objects using a decorator on a
 * {@link Map} (keys are ids are values are the decoration).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface InjectDecoratedBeans {

    /**
     * Obtains the type of entities to load.
     *
     * @return the type of entities to load
     */
    Class<?> beanType();

    /**
     * Obtains the path to obtain all data.
     *
     * @return the path of the container of all data to obtain
     */
    String dataContainerPath() default "configuration";

    /**
     * Obtains the path where to find collection of entities to use.
     * <p/>
     * If not setted, then will use the name of the field.
     *
     * @return the optional path of the parameter containing entities
     */
    String path() default "";

    /**
     * @return {@code true} if can filter entities on some ids
     * @see #pathIds()
     */
    boolean filterById() default false;

    /**
     * @return {@code true} if can filter entities on a unique id
     * @see #pathIds()
     */
    boolean filterBySingleId() default false;


    /**
     * Obtains the path where to find selected ids to keep.
     * <p/>
     * If not setted, and if {@link #filterById()} will take the name of the
     * field minus the last 's' plus suffix by Id.
     *
     * @return the optional path of the parameter containing id of entities to keep
     */
    String pathIds() default "";

    /**
     * Obtains the optional decorator context to use to obtain the decorator.
     *
     * @return the optional decorator context
     */
    String decoratorContext() default "";
}
