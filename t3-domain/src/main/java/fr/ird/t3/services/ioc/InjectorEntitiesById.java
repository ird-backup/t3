/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Fires the {@link InjectEntitiesById} annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectEntitiesById
 * @since 1.0
 */
public class InjectorEntitiesById extends AbstractInjector<InjectEntitiesById, TopiaTransactionAware> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InjectorEntitiesById.class);

    public InjectorEntitiesById() {
        super(InjectEntitiesById.class);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      TopiaTransactionAware bean,
                                      InjectEntitiesById annotation) throws Exception {
        // get entity type
        Class<? extends TopiaEntity> entityType = annotation.entityType();

        // get param id where to find ids to load
        String paramIds = annotation.path();

        if (StringUtils.isEmpty(paramIds)) {

            // use default ids from the field name + Ids
            paramIds = field.getName();

            paramIds = "configuration." + paramIds.substring(0, paramIds.length() - 1) + "Ids";
        }

        Object ids = PropertyUtils.getProperty(bean, paramIds);

        // get dao
        TopiaDAO<?> dao = getDAO(bean, entityType);

        Collection<TopiaEntity> valueToInject = Lists.newArrayList();

        // load trips fro this list of ids
        for (Object id : (Collection<?>) ids) {
            TopiaEntity entity = dao.findByTopiaId((String) id);
            if (entity == null) {

                // valueToInject must all exists
                throw new IllegalStateException(
                        "Could not find entity " + id + " with dao " + dao);
            }
            valueToInject.add(entity);
        }

        if (log.isInfoEnabled()) {
            log.info("Will set " + valueToInject.size() +
                     " valueToInject of type [" + entityType.getName() +
                     "] to field " + field);
        }

        return valueToInject;
    }
}
