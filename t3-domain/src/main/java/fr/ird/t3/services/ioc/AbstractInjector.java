/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3UserDAOHelper;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Abstract injector with some useful logi.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractInjector<A extends Annotation, B> implements Injector<A, B> {

    private final Class<A> annotationType;

    protected AbstractInjector(Class<A> annotationType) {
        this.annotationType = annotationType;
    }

    protected abstract Object getValueToInject(Field field,
                                               B bean,
                                               A annotation) throws Exception;

    @Override
    public final Class<A> getAnnotationType() {
        return annotationType;
    }

    @Override
    public void init(T3ServiceContext serviceContext) {
    }

    @Override
    public final void processField(Field field,
                                   B bean) throws Exception {

        A annotation = field.getAnnotation(annotationType);

        Object valueToInject = getValueToInject(field, bean, annotation);

        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        field.set(bean, valueToInject);
    }

    protected TopiaDAO<?> getDAO(TopiaTransactionAware bean,
                                 Class<? extends TopiaEntity> entityType) throws TopiaException {

        // get transaction from bean
        TopiaContext tx = bean.getTransaction();

        TopiaDAO dao;

        // found the dao to populate
        if (T3EntityHelper.T3_USER_MODEL_CLASSES.contains(entityType)) {

            dao = T3UserDAOHelper.getDAO(tx, entityType);
        } else {
            dao = T3DAOHelper.getDAO(tx, entityType);
        }
        return dao;
    }

}
