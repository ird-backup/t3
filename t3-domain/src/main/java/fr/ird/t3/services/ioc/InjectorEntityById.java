/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;

/**
 * Fires the {@link InjectEntityById} annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectEntityById
 * @since 1.0
 */
public class InjectorEntityById extends AbstractInjector<InjectEntityById, TopiaTransactionAware> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InjectorEntityById.class);

    public InjectorEntityById() {
        super(InjectEntityById.class);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      TopiaTransactionAware bean,
                                      InjectEntityById annotation) throws Exception {
        // get entity type
        Class<? extends TopiaEntity> entityType = annotation.entityType();

        // get param id where to find ids to load
        String paramIds = annotation.path();

        if (StringUtils.isEmpty(paramIds)) {

            // use default ids from the field name + Id
            paramIds = "configuration." + field.getName() + "Id";
        }

        String id = (String) PropertyUtils.getProperty(bean, paramIds);

        // get dao
        TopiaDAO<?> dao = getDAO(bean, entityType);

        // load entity from the id
        TopiaEntity valueToInject = dao.findByTopiaId(id);

        if (log.isInfoEnabled()) {
            log.info("Will set entity of type [" + entityType.getName() +
                     "] to field " + field);
        }

        return valueToInject;
    }


}
