/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services.ioc;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Fires the {@link InjectFromDAO} annotation.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see InjectFromDAO
 * @since 1.0
 */
public class InjectorFromDAO extends AbstractInjector<InjectFromDAO, TopiaTransactionAware> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(InjectorFromDAO.class);

    public InjectorFromDAO() {
        super(InjectFromDAO.class);
    }

    @Override
    protected Object getValueToInject(Field field,
                                      TopiaTransactionAware bean,
                                      InjectFromDAO annotation) throws TopiaException, InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        // get entity type
        Class<? extends TopiaEntity> entityType = annotation.entityType();

        // get dao
        TopiaDAO<?> dao = getDAO(bean, entityType);

        String methodName = annotation.method();

        Object result = MethodUtils.invokeMethod(dao, methodName, null);

        if (List.class.equals(field.getType()) &&
            result instanceof Iterable<?> &&
            !(result instanceof List)) {

            // let's box it in a list
            result = Lists.newArrayList((Iterable<?>) result);
        }
        if (log.isInfoEnabled()) {
            log.info("Will set " + result +
                     " entities of type [" + entityType.getName() +
                     "] to field " + field);
        }
        return result;
    }
}
