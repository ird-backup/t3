/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.collect.Sets;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ServiceLoader;
import java.util.Set;

/**
 * Service to deal with zoneStratums.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see ZoneStratumAwareMeta
 * @since 1.0
 */
public class ZoneStratumService extends T3ServiceSupport implements T3ServiceSingleton {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneStratumService.class);

    /**
     * Store of found zone metas.
     * <p/>
     * This store is loaded using the {@link ServiceLoader} mecanism.
     */
    protected Set<ZoneStratumAwareMeta> metas;

    /**
     * Obtain the store of all metas available.
     * <p/>
     * <strong>Note: </strong>the store is created by lazy
     * instanciation.
     *
     * @return all zone metas available.
     */
    public Set<ZoneStratumAwareMeta> getZoneStratumAwareMetas() {
        if (metas == null) {
            ServiceLoader<ZoneStratumAwareMeta> loader =
                    ServiceLoader.load(ZoneStratumAwareMeta.class);

            metas = Sets.newHashSet();
            for (ZoneStratumAwareMeta zoneMeta : loader) {
                if (log.isInfoEnabled()) {
                    log.info("Register zone meta : " +
                             zoneMeta.getType().getName());
                }
                metas.add(zoneMeta);
            }
        }
        return metas;
    }

    public ZoneStratumAwareMeta getZoneMetaByType(Class<?> type) {
        for (ZoneStratumAwareMeta zoneMeta : getZoneStratumAwareMetas()) {
            if (type.equals(zoneMeta.getType())) {
                return zoneMeta;
            }
        }
        throw new IllegalStateException(
                "Could not find a zone meta for type " + type.getName());
    }

    public ZoneStratumAwareMeta getZoneMetaById(String id) {
        for (ZoneStratumAwareMeta zoneMeta : getZoneStratumAwareMetas()) {
            if (id.equals(zoneMeta.getId())) {
                return zoneMeta;
            }
        }
        throw new IllegalStateException(
                "Could not find a zone meta for id  " + id);
    }
}
