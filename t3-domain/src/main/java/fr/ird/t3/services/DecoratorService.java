/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.Well;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SampleQuality;
import fr.ird.t3.entities.reference.SampleType;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.WeightCategory;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.zone.ZoneCWP;
import fr.ird.t3.entities.reference.zone.ZoneEE;
import fr.ird.t3.entities.reference.zone.ZoneET;
import fr.ird.t3.entities.reference.zone.ZoneFAO;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.user.UserOutputDatabase;
import fr.ird.t3.entities.user.UserT3Database;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.decorator.Decorator;
import org.nuiton.util.decorator.DecoratorMulti18nProvider;
import org.nuiton.util.decorator.DecoratorUtil;
import org.nuiton.util.decorator.JXPathDecorator;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.l_;

/**
 * Service for all decorations (and sort by decorations).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class DecoratorService extends T3ServiceSupport implements T3ServiceSingleton {

    public static final String WITH_ID = "withId";

    protected final DecoratorMulti18nProvider decoratorProvider;

    public DecoratorService() {
        decoratorProvider = new T3DecoratorProvider();
    }

    public <O> Decorator<O> getDecorator(Locale locale,
                                         Class<O> type,
                                         String context) {
        Preconditions.checkNotNull(type, "Decorator type can not be null!");
        Decorator<O> decorator = decoratorProvider.getDecoratorByType(locale, type, context);
        Preconditions.checkNotNull(
                decorator,
                "Could not find decorator for type " + type +
                " and context " + context
        );
        return decorator;
    }

    public String decorate(Locale locale, Object o, String context) {
        Decorator<?> decorator = getDecorator(locale, o.getClass(), context);
        String result = decorator.toString(o);
        return result;
    }

    public <O> List<O> sortToList(Locale locale, Collection<O> beans, String context) {
        Preconditions.checkNotNull(beans);
        List<O> list = Lists.newArrayList(beans);
        getDecoratorAndSort(locale, context, list);
        return list;
    }

    public <E extends TopiaEntity> Map<String, String> sortAndDecorate(Locale locale,
                                                                       Collection<E> beans,
                                                                       String context) {
        Preconditions.checkNotNull(beans);
        List<E> list = Lists.newArrayList(beans);

        Decorator<E> decorator = getDecoratorAndSort(locale, context, list);
        Map<String, String> result = Maps.newLinkedHashMap();
        for (E bean : list) {
            result.put(bean.getTopiaId(), decorator.toString(bean));
        }
        return result;
    }

    public <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Locale locale,
                                                                         Collection<E> beans,
                                                                         String context) {

        Preconditions.checkNotNull(beans);
        List<E> list = Lists.newArrayList(beans);

        Decorator<E> decorator = getDecoratorAndSort(locale, context, list);
        Map<String, String> result = Maps.newLinkedHashMap();
        for (E bean : list) {
            result.put(bean.getId(), decorator.toString(bean));
        }
        return result;
    }

    protected <O> Decorator<O> getDecoratorAndSort(Locale locale,
                                                   String context,
                                                   List<O> list) {
        Decorator<O> decorator = null;
        if (CollectionUtils.isNotEmpty(list)) {
            O object = list.get(0);
            Preconditions.checkNotNull(object);
            decorator = decoratorProvider.getDecorator(locale, object, context);
            Preconditions.checkNotNull(
                    decorator,
                    "Could not find decorator for type " + object.getClass() +
                    " and context " + context
            );
            DecoratorUtil.sort((JXPathDecorator<O>) decorator, list, 0);
        }
        return decorator;
    }

    static class T3DecoratorProvider extends DecoratorMulti18nProvider {

        @Override
        protected void loadDecorators(Locale locale) {

            // trip decorator
            registerJXPathDecorator(locale, Trip.class, "${vesselLabel}$s - ${landingDate}$td/%2$tm/%2$tY");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, Trip.class, WITH_ID, "${vesselLabel}$s - ${landingDate}$td/%2$tm/%2$tY [${topiaId}$s]");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, CompleteTrip.class, "${landingTrip/vesselLabel}$s - ${landingTrip/landingDate}$td/%2$tm/%2$tY (nb trip(s) ${nbTrips}$s)");

            // trip (with topiaid) decorator
            registerJXPathDecorator(locale, CompleteTrip.class, WITH_ID, "${landingTrip/vesselLabel}$s - ${landingTrip/landingDate}$td/%2$tm/%2$tY [${landingTrip/topiaId}$s] (nb trip(s) ${nbTrips}$s)");

            // well (with topiaid) decorator
            registerJXPathDecorator(locale, Well.class, "Number ${wellNumber}$s - Position ${wellPosition}$s [${topiaId}$s]");

            // sample decorator
            registerJXPathDecorator(locale, Sample.class, "${sampleNumber}$s");

            // sample (with topiaid) decorator
            registerJXPathDecorator(locale, Sample.class, WITH_ID, "${sampleNumber}$s [${topiaId}$s]");

            // activity  decorator
            registerJXPathDecorator(locale, Activity.class, "${date}$td/%1$tm/%1$tY - (${longitude}$s, ${latitude}$s)");

            // activity  decorator
            registerJXPathDecorator(locale, Activity.class,WITH_ID,  "${date}$td/%1$tm/%1$tY - (${longitude}$s, ${latitude}$s) [${topiaId}$s]");

            // vessel decorator
            registerJXPathDecorator(locale, Vessel.class, "${code}$d - ${libelle}$s - ${fleetCountry/libelle}$s");

            // vessel simple type decorator
            registerJXPathDecorator(locale, VesselSimpleType.class, "${libelle}$s");

            // schoolType decorator
            registerJXPathDecorator(locale, SchoolType.class, "${libelle}$s");

            // zoneEE decorator
            registerJXPathDecorator(locale, ZoneEE.class, "${libelle}$s");

            // zoneET decorator
            registerJXPathDecorator(locale, ZoneET.class, "${libelle}$s");

            // zoneFAO decorator
            registerJXPathDecorator(locale, ZoneFAO.class, "${libelle}$s");

            // zoneCWP decorator
            registerJXPathDecorator(locale, ZoneCWP.class, "${libelle}$s");

            // ocean decorator
            registerJXPathDecorator(locale, Ocean.class, "${libelle}$s");

            // country decorator
            registerJXPathDecorator(locale, Country.class, "${libelle}$s");

            // harbour decorator
            registerJXPathDecorator(locale, Harbour.class, "${libelle}$s");

            // sample quality decorator
            registerJXPathDecorator(locale, SampleQuality.class, "${libelle}$s");

            // sample type decorator
            registerJXPathDecorator(locale, SampleType.class, "${libelle}$s");

            // species decorator
            registerJXPathDecorator(locale, Species.class, "${code3L}$s - ${libelle}$s");

            // weightCategoryTreatment decorator
            registerJXPathDecorator(locale, WeightCategoryTreatment.class, "${libelle}$s - [${min}$s - ${max}$s]");

            // weightCategory decorator
            registerJXPathDecorator(locale, WeightCategory.class, "${libelle}$s - [${min}$s - ${max}$s]");

            // zoneVersion decorator
            registerJXPathDecorator(locale, ZoneVersion.class, "${versionLibelle}$s");

            // userT3Database decorator
            registerJXPathDecorator(locale, UserT3Database.class, "${description}$s");

            // userOutputDatabase decorator
            registerJXPathDecorator(locale, UserOutputDatabase.class, "${description}$s");

            // zone meta decorator
            String expression = "${i18nKey}$s";
            JXPathDecorator.Context<ZoneStratumAwareMeta> context = DecoratorUtil.createJXPathContext(expression);
            registerDecorator(locale, new ZoneStratumAwareMetaJXPathDecorator(expression, context, locale));
        }
    }

    private static class ZoneStratumAwareMetaJXPathDecorator extends JXPathDecorator<ZoneStratumAwareMeta> {

        private static final long serialVersionUID = 1L;

        private final Locale locale;

        public ZoneStratumAwareMetaJXPathDecorator(String expression,
                                                   Context<ZoneStratumAwareMeta> context,
                                                   Locale locale) throws IllegalArgumentException, NullPointerException {
            super(ZoneStratumAwareMeta.class, expression, context);
            this.locale = locale;
        }

        @SuppressWarnings({"RawUseOfParameterizedType", "RedundantCast", "unchecked"})
        @Override
        protected Comparable<Comparable<?>> getTokenValue(JXPathContext jxcontext, String token) {
            Comparable tokenValue = super.getTokenValue(jxcontext, token);
            return (Comparable) l_(locale, (String) tokenValue);
        }
    }
}
