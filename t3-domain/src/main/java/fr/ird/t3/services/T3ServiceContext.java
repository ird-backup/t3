/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Configuration;
import org.nuiton.topia.TopiaContext;

import java.util.Date;
import java.util.Locale;

/**
 * This contract represents objects you must provide when asking for a service.
 * Objects provided may be injected in services returned by
 * {@link T3ServiceFactory#newService(Class, T3ServiceContext)}
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public interface T3ServiceContext {

    TopiaContext getInternalTransaction();

    TopiaContext getTransaction();

    Locale getLocale();

    T3Configuration getApplicationConfiguration();

    T3ServiceFactory getServiceFactory();

    <E extends T3Service> E newService(Class<E> clazz);

    void setInternalTransaction(TopiaContext transaction);

    void setTransaction(TopiaContext transaction);

    void setLocale(Locale locale);

    Date getCurrentDate();
}
