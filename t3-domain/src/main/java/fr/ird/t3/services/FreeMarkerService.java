/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.base.Preconditions;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

/**
 * To use freeMarker to render action resume.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class FreeMarkerService extends T3ServiceSupport implements T3ServiceSingleton {

    /**
     * Shared freemarker configuration.
     * <p/>
     * <b>Note: </b> The configuration is auto loading as needed.
     */
    protected Configuration freemarkerConfiguration;

    public String renderTemplate(String templateName,
                                 Locale locale,
                                 Map<String, Object> parameters) throws Exception {
        Template template =
                getFreemarkerConfiguration().getTemplate(templateName, locale);
        Preconditions.checkNotNull(
                template,
                "Could not find template '" + templateName + "'");
        StringWriter out = new StringWriter();
        template.process(parameters, out);
        String processResult = out.toString();
        return processResult;
    }

    protected Configuration getFreemarkerConfiguration() {
        if (freemarkerConfiguration == null) {
            freemarkerConfiguration = new Configuration();
            TemplateLoader loader = new ClassTemplateLoader(getClass(), "/");
            freemarkerConfiguration.setTemplateLoader(loader);
            BeansWrapper objectWrapper = new DefaultObjectWrapper();
            freemarkerConfiguration.setObjectWrapper(objectWrapper);
        }
        return freemarkerConfiguration;
    }
}
