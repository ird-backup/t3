/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.base.Preconditions;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserDAO;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.entities.user.UserOutputDatabase;
import fr.ird.t3.entities.user.UserOutputDatabaseDAO;
import fr.ird.t3.entities.user.UserT3Database;
import fr.ird.t3.entities.user.UserT3DatabaseDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.util.StringUtil;

import java.util.List;

/**
 * Default {@link UserService} implementation.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class UserService extends T3ServiceSupport implements TopiaTransactionAware, T3ServiceInjectable {

//    protected TopiaContext transaction;

    @InjectDAO(entityType = T3User.class)
    protected T3UserDAO userDAO;

    @InjectDAO(entityType = UserT3Database.class)
    protected UserT3DatabaseDAO userT3DatabaseDAODAO;

    @InjectDAO(entityType = UserOutputDatabase.class)
    protected UserOutputDatabaseDAO userOutputDatabaseDAO;

    @Override
    public TopiaContext getTransaction() {
        // use the internal transaction as db transaction.
        return getInternalTransaction();
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
//        this.transaction = transaction;
    }

    public T3User getUserByLogin(String login) throws Exception {
        T3User user = userDAO.findByLogin(login);
        return user;
    }

    public T3User getUserById(String userId) throws Exception {
        T3User user = userDAO.findByTopiaId(userId);
        return user;
    }

    public List<T3User> getUsers() throws Exception {
        return userDAO.findAll();
    }

    public void createUser(T3User user) throws Exception {
        String newPassword = encodePassword(user.getPassword());
        user.setPassword(newPassword);
        T3User userToSave = userDAO.create(user);
        userDAO.update(userToSave);
        getTransaction().commitTransaction();
    }

    public void updateUser(T3User user) throws Exception {
        String userId = user.getTopiaId();
        T3User userToSave = getUserById(userId);
        Preconditions.checkNotNull(userToSave, "Could not find user with id : " + userId);

        userToSave.setLogin(user.getLogin());
        userToSave.setAdmin(user.isAdmin());
        if (!StringUtils.isEmpty(user.getPassword())) {

            // only change password when it is not blank
            userToSave.setPassword(encodePassword(user.getPassword()));
        }
        userDAO.update(userToSave);
        getTransaction().commitTransaction();
    }

    public void deleteUser(String userId) throws Exception {
        T3User user = getUserById(userId);
        if (user == null) {
            throw new IllegalArgumentException("Could not find user with id : " + userId);
        }
        userDAO.delete(user);
        getTransaction().commitTransaction();
    }

    public void addUserT3Database(String userId,
                                  UserT3Database dbConfiguration) throws Exception {
        T3User user = getUserById(userId);
        Preconditions.checkNotNull(user,
                                   "Could not find user with id : " + userId);
        UserT3Database dbConfigurationtoSave = userT3DatabaseDAODAO.create(dbConfiguration);
        user.addUserT3Database(dbConfigurationtoSave);
        userDAO.update(user);
        getTransaction().commitTransaction();
    }

    public void updateUserT3Database(UserT3Database dbConfiguration) throws Exception {

        String dbconfigurationId = dbConfiguration.getTopiaId();
        UserT3Database dbConfigurationtoSave = getUserT3Database(dbconfigurationId);
        updateDatabaseConfiguration(dbConfiguration, dbConfigurationtoSave);
        userT3DatabaseDAODAO.update(dbConfigurationtoSave);
        getTransaction().commitTransaction();
    }

    public void removeUserT3Database(String userId,
                                     String dbconfigurationId) throws Exception {
        T3User user = getUserById(userId);
        Preconditions.checkNotNull(user,
                                   "Could not find user with id : " + userId);

        UserT3Database dbConfigurationtoSave =
                getUserT3Database(dbconfigurationId);
        user.removeUserT3Database(dbConfigurationtoSave);
        userDAO.update(user);
        getTransaction().commitTransaction();
    }

    public void addOutputDatabase(String userId,
                                  UserOutputDatabase dbConfiguration) throws Exception {
        T3User user = getUserById(userId);
        Preconditions.checkNotNull(user,
                                   "Could not find user with id : " + userId);
        UserOutputDatabase dbConfigurationtoSave =
                userOutputDatabaseDAO.create(dbConfiguration);
        user.addUserOutputDatabase(dbConfigurationtoSave);
        userDAO.update(user);
        getTransaction().commitTransaction();
    }

    public void updateUserOuputDatabase(
            UserOutputDatabase dbConfiguration) throws Exception {

        String dbconfigurationId = dbConfiguration.getTopiaId();
        UserOutputDatabase dbConfigurationtoSave =
                getUserOutputDatabase(dbconfigurationId);
        updateDatabaseConfiguration(dbConfiguration, dbConfigurationtoSave);
        userOutputDatabaseDAO.update(dbConfigurationtoSave);
        getTransaction().commitTransaction();
    }

    public void removeOutputDatabaseConfiguration(String userId,
                                                  String dbconfigurationId) throws Exception {
        T3User user = getUserById(userId);
        Preconditions.checkNotNull(user,
                                   "Could not find user with id : " + userId);

        UserOutputDatabase dbConfigurationtoSave =
                getUserOutputDatabase(dbconfigurationId);
        user.removeUserOutputDatabase(dbConfigurationtoSave);
        userDAO.update(user);

        getTransaction().commitTransaction();
    }

    public UserT3Database getUserT3Database(String dbconfigurationId) throws TopiaException {
        UserT3Database dbConfigurationtoSave =
                userT3DatabaseDAODAO.findByTopiaId(dbconfigurationId);
        Preconditions.checkNotNull(
                dbconfigurationId,
                "Could not find t3 database configuration  with id : " +
                dbconfigurationId);
        return dbConfigurationtoSave;
    }

    public UserOutputDatabase getUserOutputDatabase(String dbconfigurationId) throws TopiaException {
        UserOutputDatabase dbConfigurationtoSave =
                userOutputDatabaseDAO.findByTopiaId(dbconfigurationId);
        Preconditions.checkNotNull(
                dbconfigurationId,
                "Could not find t3 database configuration  with id : " +
                dbconfigurationId);
        return dbConfigurationtoSave;
    }

    public boolean checkPassword(T3User user,
                                 String password) throws Exception {
        String s = encodePassword(password);
        return s.equals(user.getPassword());
    }

    public static String encodePassword(String password) {
        String encodedPassword = StringUtil.encodeMD5(password);
        return encodedPassword;
    }

    protected void updateDatabaseConfiguration(UserDatabase dbConfiguration,
                                               UserDatabase dbConfigurationtoSave) throws TopiaException {
        dbConfigurationtoSave.setDescription(dbConfiguration.getDescription());
        dbConfigurationtoSave.setUrl(dbConfiguration.getUrl());
        dbConfigurationtoSave.setLogin(dbConfiguration.getLogin());
    }

}
