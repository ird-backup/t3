/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import com.google.common.collect.Sets;
import fr.ird.t3.io.input.T3InputProvider;

import java.util.ServiceLoader;
import java.util.Set;

/**
 * to deal with input pilots.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3InputService extends T3ServiceSupport implements T3ServiceSingleton {

    protected T3InputProvider[] providers;

    public T3InputProvider[] getProviders() {
        if (providers == null) {
            Set<T3InputProvider> result = Sets.newHashSet();
            for (T3InputProvider provider :
                    ServiceLoader.load(T3InputProvider.class)) {
                result.add(provider);
            }
            providers = result.toArray(new T3InputProvider[result.size()]);
        }
        return providers;
    }

    public T3InputProvider getProvider(String inputProviderId) {
        T3InputProvider result = null;
        for (T3InputProvider provider : getProviders()) {
            if (provider.getId().equals(inputProviderId)) {
                result = provider;
                break;
            }
        }
        return result;
    }
}
