/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.services;

import fr.ird.t3.T3Configuration;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.util.TimeLog;

import java.util.Locale;

/**
 * Service support.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3ServiceSupport implements T3Service {

    protected TimeLog timeLog;

    protected T3ServiceContext serviceContext;

    @Override
    public void setServiceContext(T3ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public TopiaContext getInternalTransaction() {
        return serviceContext.getInternalTransaction();
    }

    public TopiaContext getTransaction() {
        return serviceContext.getTransaction();
    }

    public final Locale getLocale() {
        return serviceContext.getLocale();
    }

    public T3Configuration getApplicationConfiguration() {
        return serviceContext.getApplicationConfiguration();
    }

    public T3ServiceFactory getServiceFactory() {
        return serviceContext.getServiceFactory();
    }

    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceContext.newService(clazz);
    }

    protected void flushTransaction(String message) throws TopiaException {
        long s0 = TimeLog.getTime();

        ((TopiaContextImplementor) getTransaction()).getHibernate().flush();

        getTimeLog().log(s0, message);
    }

    protected void flushTransaction(String message, String classifier) throws TopiaException {
        long s0 = TimeLog.getTime();

        ((TopiaContextImplementor) getTransaction()).getHibernate().flush();

        getTimeLog().log(s0, message, classifier);
    }

    protected TimeLog getTimeLog() {
        if (timeLog == null) {
            timeLog = new TimeLog(getClass());
        }
        return timeLog;
    }
}
