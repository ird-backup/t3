/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.collect.ImmutableSet;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.T3ServiceContext;
import org.nuiton.topia.TopiaException;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * base class for any stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see CatchStratum
 * @since 1.0
 */
public abstract class Stratum<C extends LevelConfigurationWithStratum, A extends T3Action<C>> implements Closeable, Iterable<Map.Entry<Activity, Integer>> {

    /**
     * All activities selected for this stratum, with for catch satratum the
     * number of zones where the activty should be used.
     * <p/>
     * see http://forge.codelutin.com/issues/1935
     * <p/>
     * <strong>Note:</strong> This data are available after invocation of method
     * {@link Stratum#init(T3ServiceContext, List, T3Action}.
     */
    private Map<Activity, Integer> activities;

    /**
     * Stratum configuration of this stratum.
     *
     * @see StratumConfiguration
     */
    private final StratumConfiguration<C> configuration;

    /**
     * Selected set of species to fix.
     *
     * @since 1.3.1
     */
    private final Set<Species> speciesToFix;

    /**
     * Initialize the data of the stratum.
     *
     * @param serviceContext   service context
     * @param weightCategories available weight categories usables for this stratum
     * @param messager         messager object to send messages to action
     * @throws TopiaException if any error while loading data from database
     */
    public abstract void init(T3ServiceContext serviceContext,
                              List<WeightCategoryTreatment> weightCategories,
                              A messager) throws Exception;

    @Override
    public Iterator<Map.Entry<Activity, Integer>> iterator() {
        return activities.entrySet().iterator();
    }

    protected Stratum(StratumConfiguration<C> configuration,
                      Collection<Species> speciesToFix) {
        this.configuration = configuration;
        this.speciesToFix = ImmutableSet.copyOf(speciesToFix);
    }

    public final StratumConfiguration<C> getConfiguration() {
        return configuration;
    }

    public final Set<Species> getSpeciesToFix() {
        return speciesToFix;
    }

    public final C getLevelConfiguration() {
        return configuration.getConfiguration();
    }

    /**
     * Obtain the activities selected for this stratum with the number of zones
     * they are involved in.
     * <p/>
     * The number of zones (see http://forge.codelutin.com/issues/1935) is only
     * used for catch stratum, for sample stratum always used a single zone.
     * <p/>
     * <strong>Note: </strong> to invoke this method, you need first to invoke
     * the {@link Stratum#init(T3ServiceContext, List, T3Action}
     *
     * @return the set of activities found for this stratum.
     */
    public final Map<Activity, Integer> getActivities() {
        return activities;
    }

    public void setActivities(Map<Activity, Integer> activities) {
        this.activities = activities;
    }

    @Override
    public void close() throws IOException {
        activities.clear();
    }

    protected void checkInitMethodInvoked(Object data) {
        if (data == null) {
            throw new IllegalStateException(
                    "You must invoke the #init(tx) method before accessing " +
                    "stratum data.");
        }
    }
}
