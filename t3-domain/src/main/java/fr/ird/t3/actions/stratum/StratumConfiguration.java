/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.cache.LoadingCache;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.type.T3Date;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Configuration of a stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class StratumConfiguration<C extends LevelConfigurationWithStratum> {

    /**
     * Obtain a new stratum configuration given all his parameters.
     *
     * @param conf                        the level action configuration
     * @param zoneMeta                    type of zone to use
     * @param zone                        the zone of the stratum
     * @param schoolType                  the school type of the stratum
     * @param startDate                   the start date of the stratum
     * @param endDate                     the end date of the stratum
     * @param zones                       all the zones useables by the level
     *                                    action which used this stratum (can
     *                                    be used for sample stratum substitution)
     * @param possibleCatchVessels        set of possible vessels to use when
     *                                    selecting data in the catch stratum
     * @param possibleSampleVessels       set of possible vessels to use when
     *                                    selecting data in the sample stratum
     * @param stratumMinimumCountBySpecie minimum sample count needed by species
     *                                    for sample stratum substitution
     * @param <C>                         type of configuration
     * @return the new instanciated stratum configuration
     */
    public static <C extends LevelConfigurationWithStratum> StratumConfiguration<C> newStratumConfiguration(
            C conf,
            ZoneStratumAwareMeta zoneMeta,
            ZoneStratumAware zone,
            SchoolType schoolType,
            T3Date startDate,
            T3Date endDate,
            Collection<ZoneStratumAware> zones,
            Set<Vessel> possibleCatchVessels,
            Set<Vessel> possibleSampleVessels,
            Map<String, Integer> stratumMinimumCountBySpecie,
            LoadingCache<String, Activity> activityCache) {

        StratumConfiguration<C> stratumConfiguration = new StratumConfiguration<C>(
                conf,
                zoneMeta,
                zone,
                schoolType,
                startDate,
                endDate,
                zones,
                possibleCatchVessels,
                possibleSampleVessels,
                stratumMinimumCountBySpecie,
                activityCache);
        return stratumConfiguration;
    }

    private final C configuration;

    private final ZoneStratumAwareMeta zoneMeta;

    private final ZoneStratumAware zone;

    private final Collection<ZoneStratumAware> zones;

    private String[] zoneIds;

    private final SchoolType schoolType;

    private final T3Date beginDate;

    private final T3Date endDate;

    private final Set<Vessel> possibleCatchVessels;

    private final Set<Vessel> possibleSampleVessels;

    private final Map<String, Integer> stratumMinimumCountBySpecie;

    private final LoadingCache<String, Activity> activityCache;

    protected StratumConfiguration(C configuration,
                                   ZoneStratumAwareMeta zoneMeta,
                                   ZoneStratumAware zone,
                                   SchoolType schoolType,
                                   T3Date beginDate,
                                   T3Date endDate,
                                   Collection<ZoneStratumAware> zones,
                                   Set<Vessel> possibleCatchVessels,
                                   Set<Vessel> possibleSampleVessels,
                                   Map<String, Integer> stratumMinimumCountBySpecie,
                                   LoadingCache<String, Activity> activityCache) {
        this.configuration = configuration;
        this.zoneMeta = zoneMeta;
        this.zone = zone;
        this.schoolType = schoolType;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.zones = zones;
        this.possibleCatchVessels = possibleCatchVessels;
        this.possibleSampleVessels = possibleSampleVessels;
        this.stratumMinimumCountBySpecie = stratumMinimumCountBySpecie;
        this.activityCache = activityCache;
    }

    public C getConfiguration() {
        return configuration;
    }

    public ZoneStratumAwareMeta getZoneMeta() {
        return zoneMeta;
    }

    public String getZoneTableName() {
        String tableName = getZoneMeta().getTableName();
        return tableName;
    }

    public ZoneStratumAware getZone() {
        return zone;
    }

    public SchoolType getSchoolType() {
        return schoolType;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public Collection<ZoneStratumAware> getZones() {
        return zones;
    }

    public Set<Vessel> getPossibleCatchVessels() {
        return possibleCatchVessels;
    }

    public Set<Vessel> getPossibleSampleVessels() {
        return possibleSampleVessels;
    }

    public Map<String, Integer> getStratumMinimumCountBySpecie() {
        return stratumMinimumCountBySpecie;
    }

    public String[] getZoneIds() {
        if (zoneIds == null) {
            Collection<ZoneStratumAware> allZones = getZones();
            Iterator<ZoneStratumAware> itr = getZones().iterator();
            int size = allZones.size();
            zoneIds = new String[size];
            for (int i = 0; i < size; i++) {
                zoneIds[i] = itr.next().getTopiaId();
            }
        }
        return zoneIds;
    }

    public Activity getActivity(String activityId) {
        try {
            return activityCache.get(activityId);
        } catch (ExecutionException e) {
            throw new RuntimeException("Could not obtain activity " + activityId, e);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " [schooltype: " + schoolType.getLibelle() +
               ", zone: " + zone.getTopiaId() + ", startDate:" +
               beginDate + ", endDate:" + endDate + "]";
    }
}
