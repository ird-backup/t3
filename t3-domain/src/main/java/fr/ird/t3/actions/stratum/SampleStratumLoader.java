/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * To loader a catch stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see CatchStratum
 * @since 1.3
 */
public abstract class SampleStratumLoader<C extends LevelConfigurationWithStratum, A extends T3Action<C>, S extends SampleStratum<C, A, S>> implements TopiaTransactionAware {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SampleStratumLoader.class);

    protected TopiaContext transaction;

    /**
     * The level of substitution used.
     * <p/>
     * If there is no need of substitution then level is {@code 0}.
     */
    private int substitutionLevel;

    /** The sample stratum to load. */
    private final S sampleStratum;

    protected final C levelConfiguration;

    protected SampleStratumLoader(S sampleStratum) {
        this.sampleStratum = sampleStratum;
        levelConfiguration = sampleStratum.getConfiguration().getConfiguration();
    }

    /**
     * For a given {@code level} of stratum substitution, find the usable
     * activities.
     * <p/>
     * These activities will after be filtered using the stratum configuration.
     *
     * @param level the substitution level to use which begins at 0
     * @return the set of activity ids usable
     * @throws TopiaException if any database problem while loading data
     */
    protected abstract Set<String> findActivityIds(int level) throws TopiaException;

    protected abstract Set<String> findActivityIds(String schoolTypeId,
                                                   T3Date beginDate,
                                                   T3Date endDate,
                                                   String... zoneIds) throws TopiaException;

    /**
     * From a set of activity ids, filter the one that can be use in this
     * stratum. This mainly means to filter some of them using the stratum
     * configuration.
     *
     * @param activityIds ids of activities to filter
     * @return the set of usable activites after having
     * @throws TopiaException if any database problem while loading data
     */
    protected abstract Set<Activity> filterActivities(Set<String> activityIds) throws TopiaException;

    protected abstract boolean isStratumOk();

    @Override
    public TopiaContext getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        this.transaction = transaction;
    }

    public Locale getLocale() {
        return levelConfiguration.getLocale();
    }

    public final Map<Activity, Integer> loadData(T3ServiceContext serviceContext,
                                                 A messager) throws TopiaException {

        substitutionLevel = 1;

        boolean needAnotherRound = true;

        Set<Activity> finalActivityIds = Sets.newHashSet();

        // to keep ids of all loaded activities (to avoid to reload them twice)
        Set<String> selectedActivitiesIds = Sets.newHashSet();

        while (needAnotherRound) {

            if (log.isInfoEnabled()) {
                log.info("Try to load level " + substitutionLevel);
            }
            Set<String> activityIds = findActivityIds(substitutionLevel);

            if (activityIds == null) {

                // limit case : last level but still no data...

                substitutionLevel = -1;

                break;
            }

            // remove all activities ids already grabbed
            activityIds.removeAll(selectedActivitiesIds);

            if (log.isInfoEnabled()) {
                log.info("Found " + activityIds.size() +
                         " new activities in this " +
                         "stratum at level " + substitutionLevel);
            }

            Set<Activity> activities = filterActivities(activityIds);

            if (log.isInfoEnabled()) {
                log.info("Found " + activityIds.size() +
                         " new activities (after filter) in this " +
                         "stratum at level " + substitutionLevel);
            }

            finalActivityIds.addAll(activities);

            // these activities are now loaded
            selectedActivitiesIds.addAll(activityIds);

            // merge activites into the stratum
            sampleStratum.mergeNewActivities(serviceContext, activities);

            // log sample stratum new composition
            String message = sampleStratum.logSampleStratumLevel(
                    substitutionLevel, messager);

            if (log.isInfoEnabled()) {
                log.info(message);
            }
            messager.addInfoMessage(message);

            needAnotherRound = !isStratumOk();

            if (needAnotherRound) {

                // go to next level
                if (log.isInfoEnabled()) {
                    log.info("Need another round");
                }
                substitutionLevel++;
            }
        }
        Map<Activity, Integer> result = Maps.newHashMap();
        for (Activity activityId : finalActivityIds) {
            result.put(activityId, 1);
        }
        return result;
    }

    public int getSubstitutionLevel() {
        return substitutionLevel;
    }

    public S getSampleStratum() {
        return sampleStratum;
    }

    public int getTimeStep() {
        return levelConfiguration.getTimeStep();
    }

    protected float computeSampleWeight(Collection<SetSpeciesCatWeight> newDatas,
                                        Set<String> specieIds) {

        float weightCount = 0;

        for (SetSpeciesCatWeight newData : newDatas) {

            Species species = newData.getSpecies();

            if (!specieIds.contains(species.getTopiaId())) {

                // not on one of the species to fix
                continue;
            }

            weightCount += newData.getWeight();
        }
        return weightCount;
    }
}
