/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.LocaleProvider;
import fr.ird.t3.T3Configuration;
import fr.ird.t3.entities.T3Messager;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import org.nuiton.topia.TopiaContext;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * generic data action context which contains three things :
 * <p/>
 * <ul>
 * <li>action configuration {@link #configuration}</li>
 * <li>action generated {@link #messages}</li>
 * <li>action result {@link #result}</li>
 * <p/>
 * </ul>
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3ActionContext<C extends T3ActionConfiguration> implements T3Messager, LocaleProvider, T3ServiceContext, T3Service {

    enum MessageLevel {
        INFO,
        WARNING,
        ERROR
    }

    private Class<? extends T3Action<C>> actionType;

    /** Configuration of action. */
    private C configuration;

    /** Map of results to be filled while the execute method. */
    private Map<String, Object> result;

    /** Map of messages to be filled while the execute method. */
    private ArrayListMultimap<MessageLevel, String> messages;

    private int nbSteps;

    private float stepIncrement;

    private float progression;

    private long totalTime;

    private String resume;

    /** Delegate service context. */
    private T3ServiceContext serviceContext;

    public T3ActionContext() {
        messages = ArrayListMultimap.create();
        result = Maps.newTreeMap();
    }

    public T3ServiceContext getServiceContext() {
        return serviceContext;
    }

    public String getActionName() {
        return getConfiguration().getName(getLocale());
    }

    @Override
    public void setServiceContext(T3ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public C getConfiguration() {
        return configuration;
    }

    @Override
    public Locale getLocale() {
        return serviceContext.getLocale();
    }

    @Override
    public T3Configuration getApplicationConfiguration() {
        return serviceContext.getApplicationConfiguration();
    }

    @Override
    public T3ServiceFactory getServiceFactory() {
        return serviceContext.getServiceFactory();
    }

    @Override
    public <E extends T3Service> E newService(Class<E> clazz) {
        return serviceContext.newService(clazz);
    }

    @Override
    public void setInternalTransaction(TopiaContext transaction) {
        serviceContext.setInternalTransaction(transaction);
    }

    @Override
    public void setTransaction(TopiaContext transaction) {
        serviceContext.setTransaction(transaction);
    }

    @Override
    public void setLocale(Locale locale) {
        serviceContext.setLocale(locale);
    }

    @Override
    public Date getCurrentDate() {
        return serviceContext.getCurrentDate();
    }

    @Override
    public TopiaContext getInternalTransaction() {
        return serviceContext.getInternalTransaction();
    }

    @Override
    public TopiaContext getTransaction() {
        return serviceContext.getTransaction();
    }

    public Class<? extends T3Action<C>> getActionType() {
        return actionType;
    }

    public void setActionType(Class<? extends T3Action<C>> actionType) {
        this.actionType = actionType;
    }

    public void setConfiguration(C configuration) {
        this.configuration = configuration;
    }

    @Override
    public void addInfoMessage(String message) {
        messages.get(MessageLevel.INFO).add(message);
    }

    @Override
    public void addWarningMessage(String message) {
        messages.get(MessageLevel.WARNING).add(message);
    }

    @Override
    public void addErrorMessage(String message) {
        messages.get(MessageLevel.ERROR).add(message);
    }

    public String getMessagesAsStr(MessageLevel level) {
        StringBuilder sb = new StringBuilder();
        for (String s : messages.get(level)) {
            sb.append(s).append('\n');
        }
        return sb.toString();
    }

    public String getInfoMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.INFO);
    }

    public String getWarnMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.WARNING);
    }

    public String getErrorMessagesAsStr() {
        return getMessagesAsStr(MessageLevel.ERROR);
    }

    @Override
    public List<String> getInfoMessages() {
        return messages.get(MessageLevel.INFO);
    }

    @Override
    public List<String> getWarnMessages() {
        return messages.get(MessageLevel.WARNING);
    }

    @Override
    public List<String> getErrorMessages() {
        return messages.get(MessageLevel.ERROR);
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public void clearMessages() {
        for (MessageLevel level : MessageLevel.values()) {
            messages.get(level).clear();
        }
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public int getNbSteps() {
        return nbSteps;
    }

    public void setNbSteps(int nbSteps) {
        this.nbSteps = nbSteps;
        stepIncrement = 100f / nbSteps;
    }

    public float getStepIncrement() {
        return stepIncrement;
    }

    public int getProgression() {
        return (int) progression;
    }

    public void setProgression(float progression) {
        this.progression = progression;
    }

    public void incrementsProgression() {
        setProgression(progression + stepIncrement);
    }


    public Object getResult(String paramKey) {
        Object o = result.get(paramKey);
        return o;
    }

    public void putResult(String key, Object value) {
        result.put(key, value);
    }

    public <V> V getResult(String paramKey, Class<V> valueType) {
        V o = getValue(result, paramKey, valueType);
        return o;
    }

    public <V> Set<V> getResultAsSet(String paramKey, Class<V> valueType) {
        Set<V> o = getValueAsSet(result, paramKey);
        return o;
    }

    public <V> List<V> getResultAsList(String paramKey, Class<V> valueType) {
        List<V> o = getValueAsList(result, paramKey);
        return o;
    }

    public <K, V> Map<K, V> getResultAsMap(String paramKey) {
        Map<K, V> o = getValueAsMap(result, paramKey);
        return o;
    }

    public <K, V> Multimap<K, V> getResultAsMultimap(String paramKey) {
        Multimap<K, V> o = getValueAsMultimap(result, paramKey);
        return o;
    }

    private static <T> T getValue(Map<String, Object> context,
                                  String paramKey,
                                  Class<T> type) {
        Object value = context.get(paramKey);
        if (value != null) {
            // can make a strict check of type
            checkParam(context, paramKey, type);
        }
        return (T) value;
    }

    private static <K, V> Map<K, V> getValueAsMap(Map<String, Object> context,
                                                  String paramKey) {
        Map<K, V> value = getValue(context, paramKey, Map.class);
        return value;
    }

    private static <K, V> Multimap<K, V> getValueAsMultimap(Map<String, Object> context,
                                                            String paramKey) {
        Multimap<K, V> value = getValue(context, paramKey, Multimap.class);
        return value;
    }

    private static <V> List<V> getValueAsList(Map<String, Object> context,
                                              String paramKey) {
        List<V> value = getValue(context, paramKey, List.class);
        return value;
    }

    private static <V> Set<V> getValueAsSet(Map<String, Object> context,
                                            String paramKey) {
        Set<V> value = getValue(context, paramKey, Set.class);
        return value;
    }

    private static void checkParam(Map<String, Object> context,
                                   String paramKey,
                                   Class<?> type) {
        if (!context.containsKey(paramKey)) {
            throw new IllegalArgumentException(
                    " action requires a parameter named '" + paramKey + "'");
        }

        Object value = context.get(paramKey);
        if (!type.isInstance(value)) {
            throw new IllegalArgumentException(
                    " action requires a parameter named '" + paramKey +
                    "', typed as '" + type + "', but was '" +
                    value.getClass() + "'"
            );
        }
    }
}
