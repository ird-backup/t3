/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import java.io.Closeable;

/**
 * Result for a stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public abstract class StratumResult<C extends LevelConfigurationWithStratum> implements Closeable {

    private final StratumConfiguration<C> configuration;

    private final String libelle;

    private int substitutionLevel;

    private int nbActivities;

    private int nbActivitiesWithSample;

    protected StratumResult(StratumConfiguration<C> configuration,
                            String libelle) {
        this.configuration = configuration;
        this.libelle = libelle;
    }

    public StratumConfiguration<C> getConfiguration() {
        return configuration;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getSubstitutionLevel() {
        return substitutionLevel;
    }

    public int getNbActivities() {
        return nbActivities;
    }

    public int getNbActivitiesWithSample() {
        return nbActivitiesWithSample;
    }

    public int getNbActivitiesWithoutSample() {
        return nbActivities - nbActivitiesWithSample;
    }

    public void setSubstitutionLevel(int substitutionLevel) {
        this.substitutionLevel = substitutionLevel;
    }

    public void setNbActivities(int nbActivities) {
        this.nbActivities = nbActivities;
    }

    public void setNbActivitiesWithSample(int nbActivitiesWithSample) {
        this.nbActivitiesWithSample = nbActivitiesWithSample;
    }

    public void addNbActivities(int nbActivities) {
        this.nbActivities += nbActivities;
    }

    public void addNbActivitiesWithSample(int nbActivitiesWithSample) {
        this.nbActivitiesWithSample += nbActivitiesWithSample;
    }
}
