/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.stratum;

import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.ioc.InjectDAO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Base catch stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public abstract class CatchStratum<C extends LevelConfigurationWithStratum, A extends T3Action<C>> extends Stratum<C, A> {

    /**
     * Gets a new loader of catch stratum.
     *
     * @return new instance of a catch stratum loader
     */
    protected abstract CatchStratumLoader<C> newLoader();

    protected CatchStratum(StratumConfiguration<C> configuration,
                           Collection<Species> speciesToFix) {
        super(configuration, speciesToFix);
    }

    @Override
    public void init(T3ServiceContext serviceContext,
                     List<WeightCategoryTreatment> weightCategories,
                     A messager) throws Exception {

        CatchStratumLoader<C> stratumLoader = newLoader();

        // inject transaction in loader
        stratumLoader.setTransaction(serviceContext.getTransaction());

        // inject daos in loader
        serviceContext.newService(IOCService.class).injectOnly(stratumLoader, InjectDAO.class);

        // get all catches usable in this stratum grouped by their owing activity
        Map<Activity, Integer> activities = stratumLoader.loadData(getConfiguration());
        setActivities(activities);
    }

    /**
     * Obtain the number of activities found for this catch stratum.
     *
     * @return the number of activities found for this catch stratum
     */
    public final int getNbActivities() {
        return getActivities().size();
    }
}
