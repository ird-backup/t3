/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3;

import org.nuiton.util.Version;
import org.nuiton.util.config.ConfigOptionDef;

import java.io.File;

import static org.nuiton.i18n.I18n.n_;

/**
 * All T3 configuration options.
 *
 * @since 1.0
 */
public enum T3ConfigurationOption implements ConfigOptionDef {

    /** Main directory where to put t3 data (logs, and others...). */
    DATA_DIRECTORY(
            "data.directory",
            n_("t3.config.data.directory.description"),
            "/var/local/t3",
            File.class),
    INTERNAL_DB_DIRECTORY(
            "internal.db.directory",
            n_("t3.config.internal.db.directory.description"),
            "${data.directory}/db",
            File.class),

    PARAMETER_PROFILE_DIRECTORY(
            "parameterProfiles.storage.directory",
            n_("t3.config.parameterProfiles.storage.directory.description"),
            "${data.directory}/parameter-profiles",
            File.class),

    USER_LOG_DIRECTORY(
            "user.log.directory",
            n_("t3.user.log.directory.description"),
            "${data.directory}/logs",
            File.class),

    TREATMENT_WORKING_DIRECTORY(
            "treatment.working.directory",
            n_("t3.config.treatment.working.directory.description"),
            "${data.directory}/treatments",
            File.class),
    STRATUM_WEIGHT_RATIO(
            "stratum.weightRatio",
            n_("t3.config.stratum.weightRatio.description"),
            "250",
            Float.class),
    RF1_MINIMUM_RATE(
            "rf1.minimumrate",
            n_("t3.config.rf1.minimumrate.description"),
            "0.8",
            Float.class),
    RF1_MAXIMUM_RATE(
            "rf1.maximumrate",
            n_("t3.config.rf1.maximumrate.description"),
            "1.2",
            Float.class),
    RF_TOT_MAX(
            "rfTot.maximum",
            n_("t3.config.rfTot.maximum.description"),
            "250",
            Float.class),
    RF_MINUS10_MAX(
            "rfMinus10.maximum",
            n_("t3.config.rfMinus10.maximum.description"),
            "500",
            Float.class),
    RF_PLUS10_MAX(
            "rfPlus10.maximum",
            n_("t3.config.rfPlus10.maximum.description"),
            "500",
            Float.class),
    RF_MINUS10_MIN_NUMBER(
            "rfMinus10.minNumber",
            n_("t3.config.rfMinus10.minNumber.description"),
            "75",
            Integer.class),
    RF_PLUS10_MIN_NUMBER(
            "rfPlus10.minNumber",
            n_("t3.config.rfPlus10.minNumber.description"),
            "75",
            Integer.class),
    WEIGHTED_SET_WEIGHT(
            "level0.weightedSetWeight",
            n_("t3.config.level0.weightedSetWeight.description"),
            "40",
            Float.class),
    LEVEL2_DEFAULT_SPECIES(
            "level2.defaultSpecies",
            n_("t3.config.level2.defaultSpecies.description"),
            "1,2,3", // YFT, SKJ, BET
            int[].class),
    LEVEL3_DEFAULT_SPECIES(
            "level3.defaultSpecies",
            n_("t3.config.level3.defaultSpecies.description"),
            "1,2,3,4,5,6", // YFT, SKJ, BET, ALB, LTA, FRI
            int[].class),
    T3_DATA_VERSION(
            "data.version",
            n_("t3.config.data.version.description"),
            "",
            Version.class),;

    /** Configuration key. */
    protected final String key;

    /** I18n key of option description */
    protected final String description;

    /** Type of option */
    protected final Class<?> type;

    /** Default value of option. */
    protected String defaultValue;

    T3ConfigurationOption(String key,
                          String description,
                          String defaultValue,
                          Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return true;
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    @Override
    public void setFinal(boolean newValue) {
        // not used
    }
}
