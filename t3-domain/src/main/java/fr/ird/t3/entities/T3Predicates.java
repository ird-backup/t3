/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Predicate;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SpeciesAware;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Store usefull predicates on entities.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1
 */
public class T3Predicates {

    /**
     * Predicates that trip has no data computed.
     *
     * @see Trip#isNoDataComputed()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_WITH_NO_DATA_COMPUTED = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            return input.isNoDataComputed();
        }
    };

    /**
     * Predicates that trip has some data computed.
     *
     * @see Trip#isSomeDataComputed()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_WITH_SOME_DATA_COMPUTED = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            return input.isSomeDataComputed();
        }
    };

    /**
     * Predicates that trip has all his data computed.
     *
     * @see Trip#isAllDataComputed() ()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_WITH_ALL_DATA_COMPUTED = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            return input.isAllDataComputed();
        }
    };

    /**
     * Predicates that trip has a log book.
     *
     * @see Trip#getLogBookAvailability()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_WITH_LOG_BOOK = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            return input.getLogBookAvailability() == 1;
        }
    };

    /**
     * Predicates that trip ends a complete trip.
     *
     * @see Trip#getFishHoldEmpty()
     * @since 1.1
     */
    public static final Predicate<Trip> TRIP_ENDS_A_COMPLETE_TRIP = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            return input.getFishHoldEmpty() == 1;
        }
    };


    /**
     * Predicates that trip is inside a complete trip (as a simple trip or a
     * partial complete trip).
     * <p/>
     * Notes that a such trip can be used in level 0, and 1.
     *
     * @see Trip#getCompletionStatus()
     * @since 1.2
     */
    public static final Predicate<Trip> TRIP_INSIDE_A_COMPLETE_TRIP = new Predicate<Trip>() {
        @Override
        public boolean apply(Trip input) {
            Integer completionStatus = input.getCompletionStatus();
            return completionStatus != null && completionStatus > 0;
        }
    };

    /**
     * Predicates that trip is inside a complete trip (as a simple trip or a
     * partial complete trip).
     * <p/>
     * Notes that a such trip can be used in level 0, and 1.
     *
     * @see Trip#getCompletionStatus()
     * @since 1.2
     */
    public static final Predicate<Activity> ACTIVITY_WITH_SET_SAMPLE = new Predicate<Activity>() {
        @Override
        public boolean apply(Activity input) {
            boolean result = !input.isSetSpeciesCatWeightEmpty();
            return result;
        }
    };

    public static Predicate<SpeciesAware> newSpeciesFilter(final Species species) {

        return new Predicate<SpeciesAware>() {
            @Override
            public boolean apply(SpeciesAware input) {

                return species.equals(input.getSpecies());
            }
        };
    }

    public static <E extends SpeciesAware> Predicate<E> newSpeciesContainsFilter(final Collection<Species> species) {

        return new Predicate<E>() {
            @Override
            public boolean apply(E input) {

                return species.contains(input.getSpecies());
            }
        };
    }

    public static final Predicate<LengthWeightConversion> LENGTH_WEIGHT_CONVERSION_BY_COEFFICIENT = new Predicate<LengthWeightConversion>() {
        @Override
        public boolean apply(LengthWeightConversion input) {
            boolean result = true;
            Double a = input.getCoefficientValue(LengthWeightConversionDAO.COEFFICIENT_A);
            if (a == null || a == 0) {

                result = false;
            }

            if (result) {
                Double b = input.getCoefficientValue(LengthWeightConversionDAO.COEFFICIENT_B);
                // on autorise d'avoir b à 0 (mais cela ne permet plus de calculer la taille à partir du poids)
                if (b == null) {
                    result = false;
                }
            }

            return result;
        }
    };

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForOcean(final Ocean ocean) {

        return new Predicate<LengthWeightConversion>() {
            @Override
            public boolean apply(LengthWeightConversion input) {
                boolean result;
                Ocean inputOcean = input.getOcean();
                if (ocean == null) {
                    result = inputOcean == null;
                } else {
                    result = ocean.equals(inputOcean);
                }
                return result;
            }
        };
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForSexe(final int sexe) {

        return new Predicate<LengthWeightConversion>() {
            @Override
            public boolean apply(LengthWeightConversion input) {
                return sexe == input.getSexe();
            }
        };
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForDatedebut(final Date dateDebut) {

        return new Predicate<LengthWeightConversion>() {
            @Override
            public boolean apply(LengthWeightConversion input) {
                return input.getBeginDate().before(dateDebut) ||
                       input.getBeginDate().equals(dateDebut);
            }
        };
    }

    public static Predicate<LengthWeightConversion> newLengthWeightConversionForDateFin(final Date dateFin) {

        return new Predicate<LengthWeightConversion>() {
            @Override
            public boolean apply(LengthWeightConversion input) {
                Date date = input.getEndDate();
                boolean result;
                if (dateFin == null) {
                    // on n'accepte que les parametrages selon les critères suivants :
                    // - sans date de fin (i.e en cours de validite)

                    result = date == null;
                } else {

                    // on n'accepte que les parametrages selon les critères suivants :
                    // - sans date de fin (i.e en cours de validite)
                    // - ceux dont la date de fin est avant la date de fin donnée
                    result = date == null ||
                             date.after(dateFin) ||
                             date.equals(dateFin);
                }
                return result;
            }
        };
    }

    /**
     * Predicates that the trips vessel is one of the given vessels.
     *
     * @param vessels usable vessels
     * @return {@code true} if the trip  vessel is one of the given one, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingVessel(final Collection<Vessel> vessels) {
        return new Predicate<Trip>() {
            @Override
            public boolean apply(Trip input) {
                return vessels.contains(input.getVessel());
            }
        };
    }

    /**
     * Predicates that the trips harbour is one of the given harbours.
     *
     * @param harbours usable harbours
     * @return {@code true} if the trip harbour is one of the given one, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingHarbour(final Collection<Harbour> harbours) {
        return new Predicate<Trip>() {
            @Override
            public boolean apply(Trip input) {
                return harbours.contains(input.getLandingHarbour());
            }
        };
    }

    /**
     * Precidates that trip has at least one activity using one of the given oceans.
     *
     * @param oceans ocean to filter
     * @return {@code true} if trip has at least one activity using one of the given trip, {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingOcean(final Collection<Ocean> oceans) {
        return new Predicate<Trip>() {
            @Override
            public boolean apply(Trip input) {
                boolean result = false;

                if (!input.isActivityEmpty()) {

                    Set<Ocean> allOceans = input.getAllOceans();
                    for (Ocean ocean : allOceans) {
                        if (oceans.contains(ocean)) {
                            result = true;
                            break;
                        }
                    }
                }
                return result;
            }
        };
    }

    /**
     * Predicates that the trips departure year is one of the given years.
     *
     * @param years usable departure year
     * @return {@code true} if the trip departure year is one of the given one,
     *         {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Trip> tripUsingDepartureYear(final Collection<Integer> years) {
        return new Predicate<Trip>() {
            @Override
            public boolean apply(Trip input) {
                Integer year = T3Functions.TRIP_TO_DEPARTURE_YEAR.apply(input);
                return years.contains(year);
            }
        };
    }

    /**
     * Predicates that the vessel simple type is one of the given vessel simple types.
     *
     * @param vesselSimpleTypes usable vessel simple types
     * @return {@code true} if the vessel simple type is one of the given one,
     *         {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingVesselType(final Collection<VesselSimpleType> vesselSimpleTypes) {
        return new Predicate<Vessel>() {
            @Override
            public boolean apply(Vessel input) {
                return vesselSimpleTypes.contains(input.getVesselType().getVesselSimpleType());
            }
        };
    }

    /**
     * Predicates that the vessel fleet country is one of the given countries.
     *
     * @param fleetCountries usable fleet countries
     * @return {@code true} if the vessel fleet country is one of the given one,
     *         {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingFleetCountry(final Collection<Country> fleetCountries) {
        return new Predicate<Vessel>() {
            @Override
            public boolean apply(Vessel input) {
                return fleetCountries.contains(input.getFleetCountry());
            }
        };
    }

    /**
     * Predicates that the vessel flag country is one of the given countries.
     *
     * @param flagCountries usable flag countries
     * @return {@code true} if the vessel flag country is one of the given one,
     *         {@code false} otherwise.
     * @since 1.1
     */
    public static Predicate<Vessel> vesselUsingFlagCountry(final Collection<Country> flagCountries) {
        return new Predicate<Vessel>() {
            @Override
            public boolean apply(Vessel input) {
                return flagCountries.contains(input.getFlagCountry());
            }
        };
    }

    /**
     * Precidates that a entity is equals to the given same topia id.
     *
     * @param id topia id to test
     * @return {@code true} if entity has same topia id as the given one, {@code false} otherwise.
     * @since 1.1.2
     */
    public static <E extends TopiaEntity> Predicate<E> equalsTopiaEntity(final String id) {
        return new Predicate<E>() {
            @Override
            public boolean apply(E input) {
                boolean result = id.equals(input.getTopiaId());

                return result;
            }
        };
    }
}
