/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.data.Trip;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Set;

/**
 * {@link Ocean} dao user operations.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class OceanDAOImpl<E extends Ocean> extends OceanDAOAbstract<E> {

    /**
     * Obtains all oceans used in any activities in the database.
     *
     * @return the set of used ocean in any activities in database
     * @throws TopiaException if any problem while querying database
     */
    public Set<E> findAllUsedInActivity() throws TopiaException {

        // select distinct(o) from ocean o, activity a where a.ocean = o.topiaid;

//        TopiaQuery query = createQuery("o")
//                .addFrom(Activity.class, "a")
//                .addDistinct()
//                .addWhere("a.ocean = o.id");

        String hql = "SELECT DISTINCT(o) FROM OceanImpl o, ActivityImpl a WHERE a.ocean = o.id";

        return T3EntityHelper.querytoSet(hql, this);
    }

    public static Set<Ocean> getAllOcean(Collection<Trip> trips) {

        Set<Ocean> result = Sets.newHashSet();
        for (Trip trip : trips) {
            result.addAll(trip.getAllOceans());
        }
        return result;
    }
}
