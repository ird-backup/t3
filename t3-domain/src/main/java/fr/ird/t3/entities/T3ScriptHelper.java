/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Usefull method to deal with sql scripts.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3ScriptHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3ScriptHelper.class);

    public static void exportDatabase(TopiaContext rootTx,
                                      File outputFile) throws TopiaException {

        if (log.isInfoEnabled()) {
            log.info("Export database to file " + outputFile);
        }
        TopiaContext tx = rootTx.beginTransaction();

        try {
            tx.backup(outputFile, false);
        } finally {
            tx.closeContext();
        }
    }

    public static void loadReferentiel(TopiaContext tx,
                                       String location) throws IOException, TopiaException {
        InputStream resourceAsStream =
                T3EntityHelper.class.getResourceAsStream(location);

        try {
            loadScript(tx, resourceAsStream);
        } finally {
            resourceAsStream.close();
        }
    }

    public static void loadScript(TopiaContext tx,
                                  InputStream resourceAsStream) throws IOException, TopiaException {

        String script = IOUtils.toString(resourceAsStream, Charsets.UTF_8.name());

        TopiaContextImplementor newTx = (TopiaContextImplementor) tx.beginTransaction();

        try {
            newTx.executeSQL(script);
            newTx.commitTransaction();
        } finally {
            newTx.closeContext();
        }
    }

    public static void loadScript(TopiaContext tx,
                                  File file) throws IOException, TopiaException {

        InputStream resourceAsStream = new FileInputStream(file);

        try {
            loadScript(tx, resourceAsStream);
        } finally {
            resourceAsStream.close();
        }
    }

    public static void loadScriptLineByLine(TopiaContext tx,
                                            File script,
                                            PropertyChangeListener listener) throws TopiaException, IOException {

        TopiaContextImplementor newTx = (TopiaContextImplementor) tx.beginTransaction();

        int lineCount = 0;
        try {
            BufferedReader stream =
                    new BufferedReader(new InputStreamReader(new FileInputStream(script), Charsets.UTF_8));
            try {


                String line;

                while ((line = stream.readLine()) != null) {

                    if (StringUtils.isBlank(line)) {

                        // empty line
                        continue;
                    }

                    line = line.trim();

                    if (line.startsWith("--")) {

                        // comment line
                        continue;
                    }

                    newTx.executeSQL(line);
                    if (log.isDebugEnabled()) {
                        log.debug("Inject line : " + line);
                    }
                    listener.propertyChange(new PropertyChangeEvent(script, "newLine", null, lineCount++));
                }
            } finally {
                stream.close();
            }
            newTx.commitTransaction();
        } finally {
            newTx.closeContext();
        }
    }

    protected T3ScriptHelper() {
        // hide helper constructor
    }
}
