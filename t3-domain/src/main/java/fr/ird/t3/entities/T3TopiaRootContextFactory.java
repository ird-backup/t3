/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Charsets;
import fr.ird.t3.T3Configuration;
import fr.ird.t3.T3ConfigurationOption;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.user.JdbcConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.util.RecursiveProperties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

/**
 * Factory of t3 root contexts.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3TopiaRootContextFactory {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3TopiaRootContextFactory.class);

    public static final String T3_EMBEDDED_DB_PROPERTIES =
            "/t3-TopiaContextImpl.properties";

    public static final String T3_USER_DB_PROPERTIES = "/t3-datadb.properties";

    public static final String T3_INTERNAL_DB_PROPERTIES = "/t3-internaldb.properties";

    /**
     * Open a new topia root context from given configuration.
     *
     * @param configuration topia configuration
     * @return the new fresh root context
     */
    public TopiaContext newDb(Properties configuration) {
        try {
            if (log.isInfoEnabled()) {
                log.info("Starts a db at : " +
                         configuration.get(TopiaContextFactory.CONFIG_URL));
            }
            TopiaContext result = TopiaContextFactory.getContext(configuration);
            return result;
        } catch (TopiaNotFoundException e) {
            throw new TopiaRuntimeException("Could not init db", e);
        }
    }

    /**
     * Open a new topia root context from the given jdbc configuration.
     *
     * @param jdbcConfiguration jdbc configuration
     * @return the new fresh root context
     */
    public TopiaContext newDb(JdbcConfiguration jdbcConfiguration) {

        URL dbConfigFile = T3TopiaRootContextFactory.class.getResource(
                T3_USER_DB_PROPERTIES);

        Properties result = null;
        try {
            InputStreamReader reader =
                    new InputStreamReader(dbConfigFile.openStream(),
                                          Charsets.UTF_8);
            try {
                result = new RecursiveProperties();

                result.setProperty("configuration.url", jdbcConfiguration.getUrl());
                result.setProperty("configuration.login", jdbcConfiguration.getLogin());
                result.setProperty("configuration.password", jdbcConfiguration.getPassword());
                result.load(reader);

                T3IOUtil.reloadProperty(result, TopiaContextFactory.CONFIG_URL);
                T3IOUtil.reloadProperty(result, TopiaContextFactory.CONFIG_USER);
                T3IOUtil.reloadProperty(result, TopiaContextFactory.CONFIG_PASS);

                result.remove("configuration.url");
                result.remove("configuration.login");
                result.remove("configuration.password");
            } finally {
                reader.close();
            }
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Could not load database configuration", e);
        }

        // add entities mapping
        result.put(
                TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                T3DAOHelper.getImplementationClassesAsString()
        );

        return newDb(result);
    }

    /**
     * Open a new topia root context for the internal db of t3 (this should be
     * a h2 db used only for security).
     *
     * @param configuration application configuration where to find db directory
     * @return the new fresh root context of the internal db
     */
    public TopiaContext newInternalDb(T3Configuration configuration) {

        URL dbConfigFile = getClass().getResource(T3_INTERNAL_DB_PROPERTIES);

        File internalDbDirectory = configuration.getInternalDbDirectory();
        Properties result = null;

        try {
            InputStreamReader reader =
                    new InputStreamReader(dbConfigFile.openStream(),
                                          Charsets.UTF_8);
            try {
                result = new RecursiveProperties();

                String key =
                        T3ConfigurationOption.INTERNAL_DB_DIRECTORY.getKey();

                result.setProperty(key, internalDbDirectory.getAbsolutePath());
                result.load(reader);
                T3IOUtil.reloadProperty(result, TopiaContextFactory.CONFIG_URL);
                result.remove(key);
            } finally {
                reader.close();
            }
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Could not load internal database configuration", e);
        }

        // add entities mapping
        result.put(TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                   T3UserDAOHelper.getImplementationClassesAsString()
        );
        return newDb(result);
    }

    /**
     * Open a new topia root context for the user t3 db using a embedded db (
     * this should be a h2 db).
     * <p/>
     * This is mainly used by tests.
     *
     * @param dbLocation file where to store the db.
     * @return the new fresh root context of the internal db
     */
    public TopiaContext newEmbeddedDb(File dbLocation) {
        Properties configuration = new Properties();

        InputStream stream = getClass().getResourceAsStream(
                T3_EMBEDDED_DB_PROPERTIES);
        try {
            try {
                configuration.load(stream);
            } finally {
                stream.close();
            }
        } catch (IOException e) {
            throw new TopiaRuntimeException("Could not load input file " +
                                            T3_EMBEDDED_DB_PROPERTIES, e);
        }
        configuration.setProperty(
                TopiaContextFactory.CONFIG_PERSISTENCE_CLASSES,
                T3DAOHelper.getImplementationClassesAsString());

        // make sure we always use a different directory

        if (log.isDebugEnabled()) {
            log.debug("dbPath = " + dbLocation);
        }
        configuration.setProperty(TopiaContextFactory.CONFIG_URL,
                                  "jdbc:h2:file:" + dbLocation
        );
        TopiaContext result = newDb(configuration);
        return result;
    }
}
