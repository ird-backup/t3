package fr.ird.t3.entities.data;

/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2013 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * A complete trip is a set of trips.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.5
 */
public class CompleteTrip implements Iterable<Trip> {

    protected final List<Trip> trips;

    public CompleteTrip(List<Trip> trips) {
        // can't create a complete trip with no trips
        Preconditions.checkNotNull(trips);
        Preconditions.checkState(CollectionUtils.isNotEmpty(trips));
        // check that all trips have same vessel
        Vessel vessel = null;
        for (Trip trip : trips) {
            if (vessel == null) {
                vessel = trip.getVessel();
                continue;
            }
            Preconditions.checkState(vessel.equals(trip.getVessel()));
        }
        this.trips = trips;
    }

    public int getNbTrips() {
        return trips.size();
    }

    public Harbour getLandingHarbour() {
        Harbour result = getLandingTrip().getLandingHarbour();

        return result;
    }

    public Trip getDepartureTrip() {
        return trips.get(0);
    }

    public Trip getLandingTrip() {
        return trips.get(trips.size() - 1);
    }

    public Date getLandingDate() {
        Date result = getLandingTrip().getLandingDate();
        return result;
    }

    public Vessel getVessel() {
        Vessel result = getLandingTrip().getVessel();
        return result;
    }

    public Float getRf1() {
        Float result = getLandingTrip().getRf1();
        return result;
    }

    public float getElementaryCatchTotalWeight(Collection<Species> filtredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryCatchTotalWeight(filtredSpecies);
        }
        return result;
    }

    public float getElementaryLandingTotalWeight(Collection<Species> filtredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryLandingTotalWeight(filtredSpecies);
        }
        return result;
    }

    public float getElementaryCatchTotalWeightRf1(Collection<Species> filtredSpecies) {
        float result = 0;
        for (Trip trip : this) {
            result += trip.getElementaryCatchTotalWeightRf1(filtredSpecies);
        }
        return result;
    }

    public void applyRf2(Float rf2) {
        for (Trip trip : this) {
            trip.applyRf2(rf2);
        }
    }

    @Override
    public Iterator<Trip> iterator() {
        return trips.iterator();
    }

    public void removeTrips(Collection<Trip> tripList) {
        tripList.removeAll(trips);
    }
}
