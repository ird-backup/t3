/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract log book convertor.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractWeightCategoryLogBookConvertor implements WeightCategoryLogBookConvertor {

    protected final int oceanCode;

    protected final int schoolTypeCode;

    protected Map<WeightCategoryTreatment, Map<Integer, Float>> distributions;

    protected final WeightCategoryTreatment minus10Category;

    protected final WeightCategoryTreatment unknownCategory;

    protected AbstractWeightCategoryLogBookConvertor(int oceanCode,
                                                     int schoolTypeCode,
                                                     WeightCategoryTreatment minus10Category,
                                                     WeightCategoryTreatment unknownCategory) {
        this.oceanCode = oceanCode;
        this.schoolTypeCode = schoolTypeCode;
        this.minus10Category = minus10Category;
        this.unknownCategory = unknownCategory;
    }

    @Override
    public final boolean accept(Ocean ocean, SchoolType schoolType) {
        return ocean.getCode() == oceanCode &&
               schoolType.getCode() == schoolTypeCode;
    }

    @Override
    public String toString() {
        return super.toString() + ", ocean  " + oceanCode +
               ", schoolType " + schoolTypeCode;
    }

    /**
     * Obtains the total weight of given {@code catches} which are in
     * {@code unknown} weight category (with a code 9).
     *
     * @param catches catches to use
     * @return the total weight of given catches which uses the unknown weight
     *         category
     */
    protected float getUnknownWeight(Collection<ElementaryCatch> catches) {
        float result = 0;
        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                if (aCatch.getWeightCategoryLogBook().getCode() == 9) {
                    result += aCatch.getCatchWeightRf2();
                }
            }
        }
        return result;
    }

    /**
     * Obtains the total weight of given {@code catches} for all weight
     * categories.
     *
     * @param catches catches to use
     * @return the total weight of the the given weight for any weight
     *         categories
     */
    protected float getTotalWeight(Collection<ElementaryCatch> catches) {
        float result = 0;
        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                result += aCatch.getCatchWeightRf2();
            }
        }
        return result;
    }

    /**
     * Given the {@code distribution} of weight categories (keys are log book
     * categories code and values are ratio to apply to weight) and the given
     * {@code catches}, computes the weight.
     *
     * @param distribution distribution of
     * @param catches      catches to use
     * @return the weight computed using the distribution
     */
    protected float distribute(Map<Integer, Float> distribution,
                               Collection<ElementaryCatch> catches) {
        float result = 0;

        if (CollectionUtils.isNotEmpty(catches)) {
            for (ElementaryCatch aCatch : catches) {
                Integer weightCategoryCode =
                        aCatch.getWeightCategoryLogBook().getCode();
                Float ratio = distribution.get(weightCategoryCode);
                if (ratio != null) {
                    result += ratio * aCatch.getCatchWeightRf2();
                }
            }
        }
        return result;
    }

    /**
     * Gets the distributions used while conversion for each weight category of
     * t3.
     * <p/>
     * <strong>Note: </string>
     *
     * @return the distributions
     */
    public Map<WeightCategoryTreatment, Map<Integer, Float>> getDistributions() {
        if (distributions == null) {
            distributions = buildDistributions();
        }
        return distributions;
    }

    @Override
    public final WeightCategoryTreatment getMinus10Category() {
        return minus10Category;
    }

    @Override
    public final WeightCategoryTreatment getUnkownCategory() {
        return unknownCategory;
    }

    protected abstract Map<WeightCategoryTreatment, Map<Integer, Float>> buildDistributions();

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForSpecie1or3or4(
            Collection<ElementaryCatch> catches,
            Map<WeightCategoryTreatment, Map<Integer, Float>> distributions,
            float unknownWeight) {

        Map<WeightCategoryTreatment, Float> unknownResult = Maps.newHashMap();

        float totalWeight = 0f;

        for (Map.Entry<WeightCategoryTreatment, Map<Integer, Float>> e :
                distributions.entrySet()) {

            WeightCategoryTreatment category = e.getKey();

            Map<Integer, Float> distribution = e.getValue();

            float weight = distribute(distribution, catches);

            unknownResult.put(category, weight);

            // sum total weight
            totalWeight += weight;
        }

        Map<WeightCategoryTreatment, Float> result;

        if (unknownWeight == 0) {

            // no unknown weight to redistribute, can keep current result
            result = unknownResult;
        } else {

            // there is some unknown weight to redistribute
            result = Maps.newHashMap();

            if (totalWeight == 0) {

                // distribute unknown weight in all weight categories
                float weight = unknownWeight / unknownResult.keySet().size();

                for (WeightCategoryTreatment e : unknownResult.keySet()) {
                    // store new weight
                    result.put(e, weight);
                }
            } else {
                for (Map.Entry<WeightCategoryTreatment, Float> e : unknownResult.entrySet()) {
                    WeightCategoryTreatment weightCategory = e.getKey();
                    // weight (with no unknown category)
                    float weight = e.getValue();
                    // rate to distribute
                    float rate = weight / totalWeight;
                    // weight redistributed
                    float unknownWeightToAdd = unknownWeight * rate;
                    // store new weight
                    result.put(weightCategory, weight + unknownWeightToAdd);
                }
            }
        }
        return result;
    }

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForSpecie2(
            float totalWeight) {

        Map<WeightCategoryTreatment, Float> result = Maps.newHashMap();

        // for species SKJ (code 2) : all goes to -10 kg categorie
        result.put(getMinus10Category(), totalWeight);
        return result;
    }

    protected final Map<WeightCategoryTreatment, Float> defaultDistributeForOtherSpecie(
            float unknownWeight) {

        Map<WeightCategoryTreatment, Float> result = Maps.newHashMap();

        // only keep unknown categorie weight
        result.put(getUnkownCategory(), unknownWeight);
        return result;
    }
}
