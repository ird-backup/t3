/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;


import fr.ird.t3.entities.T3EntityHelper;
import org.nuiton.topia.TopiaException;

import java.util.Set;

/**
 * {@link VesselSimpleType} user dao operations.
 *
 * @author tchemit <chemit@codelutin.com
 * @since 1.0
 */
public class VesselSimpleTypeDAOImpl<E extends VesselSimpleType> extends VesselSimpleTypeDAOAbstract<E> {

    /**
     * Obtains all vessel simple types used by all trips in the database.
     *
     * @return the set of used vessel simple types in trips in the database
     * @throws TopiaException if any problem while querying the database
     */
    public Set<E> findAllUsedInTrip() throws TopiaException {

//        TopiaQuery query = createQuery("vst")
//                .addFrom(Trip.class, "t")
//                .addDistinct()
//                .addWhere("t." + Trip.PROPERTY_VESSEL + "." +
//                          Vessel.PROPERTY_VESSEL_TYPE + "." +
//                          VesselType.PROPERTY_VESSEL_SIMPLE_TYPE + " = vst.id");

        String hql = "SELECT DISTINCT(vst) FROM VesselSimpleTypeImpl vst, TripImpl t WHERE t.vessel.vesselType.vesselSimpleType = vst.id";

        return T3EntityHelper.querytoSet(hql, this);
    }
}
