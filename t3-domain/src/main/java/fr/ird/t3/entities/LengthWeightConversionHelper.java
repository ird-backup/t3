/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;

/**
 * Helper to do conversion length &lt;-&gt; weight of species.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class LengthWeightConversionHelper {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(LengthWeightConversionHelper.class);

    public static final MathContext MC_WEIGHT = new MathContext(3);

    /** Each thread gets his own script engine. */
    protected static final ThreadLocal<ScriptEngine> SCRIPT_ENGINE =
            new ThreadLocal<ScriptEngine>() {
                @Override
                protected ScriptEngine initialValue() {
                    ScriptEngineManager factory = new ScriptEngineManager();
                    return factory.getEngineByExtension("js");
                }
            };

    public static ScriptEngine getScriptEngine() {
        return SCRIPT_ENGINE.get();
    }

    /** variable poids à utiliser dans la relation taille */
    public static final String VARIABLE_POIDS = "P";

    /** variable taille à utiliser dans la relation poids */
    public static final String VARIABLE_TAILLE = "L";

    public static Float computeWeight(LengthWeightConversion parametrage,
                                      float taille) {

        Float o = computeValue(parametrage,
                               parametrage.getToWeightRelation(),
                               VARIABLE_TAILLE,
                               taille
        );

        if (o != null) {
            o = roundWeight(o);
        }
        return o;
    }

    public static Float computeLength(LengthWeightConversion parametrage,
                                      float poids) {
        Double b = parametrage.getCoefficientValue(LengthWeightConversionDAO.COEFFICIENT_B);
        if (b == 0) {

            // ce cas limite ne permet pas de calculer la taille a partir du poids
            return null;
        }
        Float o = computeValue(parametrage,
                               parametrage.getToLengthRelation(),
                               VARIABLE_POIDS,
                               poids
        );

        if (o != null) {
            o = roundLength(o);
        }
        return o;
    }

    protected static Float computeValue(LengthWeightConversion parametrage,
                                        String relation,
                                        String variable,
                                        float taille) {
        Map<String, Double> coeffs = parametrage.getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);

            if (log.isDebugEnabled()) {
                log.debug("add constant " + key + '=' + value);
            }
        }
        bindings.put(variable, taille);
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Double o = null;
        try {
            o = (Double) engine.eval(relation);
        } catch (ScriptException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not compute value from " + relation, e);
            }
        }
        return o == null ? null : o.floatValue();
    }

    protected static Float roundWeight(Float number) {
        return round(number, MC_WEIGHT);
    }

    protected static Float roundLength(Float number) {
        return (float) number.intValue();
    }

    protected static Float round(Float number, MathContext mc) {
        float old = number;
        float partieEntier = (int) old;
        float digit = old - (int) old;
        Float result = partieEntier + new BigDecimal(digit).round(mc).floatValue();
        if (log.isDebugEnabled()) {
            log.debug("round " + old + " to " + result + " with mc = " + mc);
        }
        return result;
    }


    protected LengthWeightConversionHelper() {
        // hide helper constructor
    }
}
