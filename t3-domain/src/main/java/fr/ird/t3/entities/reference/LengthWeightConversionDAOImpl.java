/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.T3Predicates;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;

public class LengthWeightConversionDAOImpl<E extends LengthWeightConversion> extends LengthWeightConversionDAOAbstract<E> {

    public static final String COEFFICIENT_A = "a";

    public static final String COEFFICIENT_B = "b";

    public LengthWeightConversionHelper newConversionHelper() {
        return new LengthWeightConversionHelper((LengthWeightConversionDAO) this);
    }

    public LengthWeightConversionHelper newConversionHelper(Ocean ocean, int sex, Date date) {
        return new LengthWeightConversionHelper((LengthWeightConversionDAO) this, ocean, sex, date);
    }

    public E findLengthWeightConversion(Species species,
                                        Ocean ocean,
                                        int sexe,
                                        Date date) throws TopiaException {

        List<E> list = findByEspece(species);

        if (CollectionUtils.isEmpty(list)) {

            // aucun parametrage pour le type donne
            return null;
        }

        // filtrage par ocean
        list = filterByOcean(list, ocean);

        if (CollectionUtils.isEmpty(list) && ocean != null) {

            // filtre par ocean null (car non trouvé par ocean)
            list = filterByOcean(list, null);
        }

        if (CollectionUtils.isEmpty(list)) {

            // pas d'ocean adequate
            return null;
        }

        // filtrage par sexe
        list = filterBySexe(list, sexe);

        if (CollectionUtils.isEmpty(list) && sexe != 0) {

            // filtrage par sexe indetermine
            list = filterBySexe(list, 0);
        }

        if (CollectionUtils.isEmpty(list)) {

            // pas de sexe adequate
            return null;
        }

        // filtrage par dateDebut de validite
        list = filterByBeginDate(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de debut adequate
            return null;
        }

        // filtrage par dateFin de validite
        list = filterByEndDate(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de fin adequate
            return null;
        }

        // au final il ne devrait en rester qu'un

        if (list.size() > 1) {
            throw new IllegalStateException(
                    "Should have only one conversion line for species " +
                    species.getLibelle() + ", ocean " + ocean + ", sexe " +
                    sexe + " at date " + date);
        }

        E result = list.get(0);
        return result;
    }

    protected List<E> findByEspece(Species taillePoidsAble) throws TopiaException {

        List<E> list = findAllBySpecies(taillePoidsAble);

        Predicate<LengthWeightConversion> predicate =
                T3Predicates.LENGTH_WEIGHT_CONVERSION_BY_COEFFICIENT;
        List<E> result = Lists.newArrayList(Iterables.filter(list, predicate));
        return result;

    }

    protected List<E> filterByOcean(List<E> list, Ocean ocean) {

        Predicate<LengthWeightConversion> predicate =
                T3Predicates.newLengthWeightConversionForOcean(ocean);
        List<E> result = Lists.newArrayList(Iterables.filter(list, predicate));
        return result;
    }

    protected List<E> filterBySexe(List<E> list, int sexe) {

        Predicate<LengthWeightConversion> predicate =
                T3Predicates.newLengthWeightConversionForSexe(sexe);
        List<E> result = Lists.newArrayList(Iterables.filter(list, predicate));
        return result;
    }

    protected List<E> filterByBeginDate(List<E> list, Date dateDebut) {

        Predicate<LengthWeightConversion> predicate =
                T3Predicates.newLengthWeightConversionForDatedebut(dateDebut);
        List<E> result = Lists.newArrayList(Iterables.filter(list, predicate));
        return result;
    }

    protected List<E> filterByEndDate(List<E> list, Date dateFin) {

        Predicate<LengthWeightConversion> predicate =
                T3Predicates.newLengthWeightConversionForDateFin(dateFin);
        List<E> result = Lists.newArrayList(Iterables.filter(list, predicate));
        return result;
    }

}
