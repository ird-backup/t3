/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.Sets;
import org.nuiton.topia.TopiaException;

import java.util.Set;

public class SchoolTypeDAOImpl<E extends SchoolType> extends SchoolTypeDAOAbstract<E> {

    /**
     * Obtain the two school types usabel while doing level 2 and 3 : says the
     * free school type (code = 1) and the object school type.
     *
     * @return the two school types
     * @throws TopiaException if any problem while loading data form database
     */
    public Set<E> findAllForStratum() throws TopiaException {
        Set<E> result = Sets.newHashSet();
        result.add(findByCode(1));
        result.add(findByCode(2));
        return result;
    }
}
