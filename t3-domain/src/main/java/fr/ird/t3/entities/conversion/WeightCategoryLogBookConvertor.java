/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.conversion;

import fr.ird.t3.entities.data.ElementaryCatch;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;

import java.util.Collection;
import java.util.Map;

/**
 * Contract of a converter of log book weight category to weight category
 * treatment.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface WeightCategoryLogBookConvertor {

    boolean accept(Ocean ocean, SchoolType schoolType);

    Map<WeightCategoryTreatment, Float> distribute(Species species,
                                                   Collection<ElementaryCatch> catches);

    WeightCategoryTreatment getMinus10Category();

    WeightCategoryTreatment getUnkownCategory();
}
