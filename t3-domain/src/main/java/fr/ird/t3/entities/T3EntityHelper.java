/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.entities.user.JdbcConfigurationImpl;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserOutputDatabase;
import fr.ird.t3.entities.user.UserT3Database;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Transaction;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Helper for entities (to sort them and other stuff).
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3EntityHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3EntityHelper.class);

    public static final Set<Class<? extends TopiaEntity>> T3_USER_MODEL_CLASSES = Collections.unmodifiableSet(
            Sets.newHashSet(T3User.class,
                            UserT3Database.class,
                            UserOutputDatabase.class)
    );

//    /**
//     * Set of none importable types of data from ms-access : this types are
//     * computed directly in t3 application.
//     *
//     * @since 1.0
//     */
//    public static final Set<T3EntityEnum> NONE_IMPORTABLE_DATA_TYPES =
//            Collections.unmodifiableSet(
//                    EnumSet.of(
//                            T3EntityEnum.RaisingFactor2,
//                            T3EntityEnum.SampleSetSpeciesFrequency,
//                            T3EntityEnum.StandardiseSampleSpecies,
//                            T3EntityEnum.StandardiseSampleSpeciesFrequency,
//                            T3EntityEnum.SetSpeciesCatWeight,
//                            T3EntityEnum.SetSpeciesFrequency,
//                            T3EntityEnum.WellSetAllSpecies
//
//                    )
//            );

//    /**
//     * Set of all importable data types from ms-access.
//     *
//     * @since 1.0
//     */
//    public static final Set<T3EntityEnum> IMPORTABLE_DATA_TYPES =
//            Collections.unmodifiableSet(
//                    EnumSet.of(
//                            T3EntityEnum.Activity,
//                            T3EntityEnum.ActivityFishingContext,
//                            T3EntityEnum.ElementaryCatch,
//                            T3EntityEnum.ElementaryLanding,
//                            T3EntityEnum.Sample,
//                            T3EntityEnum.SampleWell,
//                            T3EntityEnum.SampleSpecies,
//                            T3EntityEnum.SampleSpeciesFrequency,
//                            T3EntityEnum.Trip,
//                            T3EntityEnum.Well,
//                            T3EntityEnum.WellPlan
//                    )
//            );

//    /**
//     * Set of all importable reference types from ms-access.
//     *
//     * @since 1.0
//     */
//    public static final Set<T3EntityEnum> IMPORTABLE_REFERENCE_TYPES =
//            Collections.unmodifiableSet(
//                    EnumSet.of(
//                            T3EntityEnum.Country,
//                            T3EntityEnum.FishingContext,
//                            T3EntityEnum.Harbour,
//                            T3EntityEnum.Ocean,
//                            T3EntityEnum.SampleQuality,
//                            T3EntityEnum.SampleType,
//                            T3EntityEnum.SchoolType,
//                            T3EntityEnum.Species,
//                            T3EntityEnum.Vessel,
//                            T3EntityEnum.VesselActivity,
//                            T3EntityEnum.VesselSizeCategory,
//                            T3EntityEnum.VesselType,
//                            T3EntityEnum.WeightCategoryLanding,
//                            T3EntityEnum.WeightCategoryLogBook,
//                            T3EntityEnum.WeightCategoryWellPlan,
//                            T3EntityEnum.WellDestiny
//                    )
//            );

//    /**
//     * Set of all none importable reference types from ms-access.
//     *
//     * @since 1.0
//     */
//    public static final Set<T3EntityEnum> NONE_IMPORTABLE_REFERENCE_TYPES =
//            Collections.unmodifiableSet(
//                    EnumSet.of(
//                            T3EntityEnum.LengthWeightConversion,
//                            T3EntityEnum.RF1SpeciesForFleet,
//                            T3EntityEnum.SetDuration,
//                            T3EntityEnum.SpeciesLengthStep,
//                            T3EntityEnum.VesselSimpleType,
//                            T3EntityEnum.WeightCategoryTreatment,
//                            T3EntityEnum.ZoneCWP,
//                            T3EntityEnum.ZoneEE,
//                            T3EntityEnum.ZoneET,
//                            T3EntityEnum.ZoneFAO
//                    )
//            );

//    public static final String DELETE_BY_ACTIVITY =
//            "DELETE FROM %s WHERE " + ActivityAware.PROPERTY_ACTIVITY +
//            "." + TopiaEntity.TOPIA_ID + " = :activityId";

    public static <T extends T3ReferenceEntity> List<String> selectIdsByCodes(Collection<T> universe,
                                                                              Integer... codes) {

        List<String> ids = Lists.newArrayList();
        Map<Integer, T> bycode = splitBycode(universe);

        for (Integer code : codes) {
            T t = bycode.get(code);
            if (t != null) {
                ids.add(t.getTopiaId());
            }
        }
        return ids;
    }

//    public static Set<Class<? extends TopiaEntity>> getT3ImportableDataTypes() {
//        Set<Class<? extends TopiaEntity>> result = Sets.newHashSet();
//        for (T3EntityEnum anEnum : IMPORTABLE_DATA_TYPES) {
//            Class<? extends TopiaEntity> contract = anEnum.getContract();
//            if (T3DataEntity.class.isAssignableFrom(contract)) {
//                result.add(contract);
//            }
//        }
//        return result;
//    }

    public static <T extends T3ReferenceEntity> Map<Integer, T> splitBycode(Collection<T> entities) {
        Map<Integer, T> result = Maps.uniqueIndex(entities, new Function<T, Integer>() {
            @Override
            public Integer apply(T input) {
                return input.getCode();
            }
        });
        return result;
    }

    public static Map<String, Species> splitByCodeFAO(Collection<Species> entities) {
        Map<String, Species> result = Maps.uniqueIndex(entities, new Function<Species, String>() {
            @Override
            public String apply(Species input) {
                String faoId = input.getFaoId();
                return faoId == null ? "Undefined" + System.nanoTime() : faoId;
            }
        });
        return result;
    }

    public static <T extends TopiaEntity> Map<String, T> splitByTopiaId(Collection<T> entities) {
        Map<String, T> result = Maps.uniqueIndex(entities, new Function<T, String>() {
            @Override
            public String apply(T input) {
                return input.getTopiaId();
            }
        });
        return result;
    }

    public static TopiaContext newTransactionFromRootContext(TopiaContext otherTx) throws TopiaException {
        TopiaContext tx = ((TopiaContextImplementor) otherTx).getRootContext().beginTransaction();
        return tx;
    }

//    public static void deleteByActivity(TopiaContext tx,
//                                        Class<?> entityType,
//                                        Activity activity) throws TopiaException {
//        String hql = String.format(DELETE_BY_ACTIVITY, entityType.getName());
//
//        tx.execute(hql, "activityId", activity.getTopiaId());
//    }

    public static <T extends Enum<T>> EnumSet<T> allBefore(Class<T> type, T step, T[] values) {
        EnumSet<T> result = EnumSet.noneOf(type);
        int maximumOrdinal = step.ordinal();
        for (T level0Step : values) {
            if (maximumOrdinal >= level0Step.ordinal()) {
                result.add(level0Step);
            }
        }
        return result;
    }

    public static <T extends Enum<T>> EnumSet<T> allAfter(Class<T> type, T step, T[] values) {
        EnumSet<T> result = EnumSet.noneOf(type);
        int minimumOrdinal = step.ordinal();
        for (T level0Step : values) {
            if (minimumOrdinal < level0Step.ordinal()) {
                result.add(level0Step);
            }
        }
        return result;
    }

    protected T3EntityHelper() {
        // hide helper constructor
    }

//    public static <E extends TopiaEntity> Set<E> querytoSet(TopiaQuery query,
//                                                            TopiaDAO<E> dao) throws TopiaException {
//        List<E> resultList =
//                query.executeToEntityList(dao.getContext(), dao.getEntityClass());
//        Set<E> result = Sets.newHashSet(resultList);
//        return result;
//    }

    public static <E extends TopiaEntity> Set<E> querytoSet(String query,
                                                            TopiaDAO<E> dao,
                                                            Object... params) throws TopiaException {
        List<E> resultList = dao.findAllByQuery(query, params);
//                query.executeToEntityList(dao.getContext(), dao.getEntityClass());
        Set<E> result = Sets.newHashSet(resultList);
        return result;
    }

    public static Connection newJDBCConnection(JdbcConfiguration configuration) throws SQLException {

        String jdbcUrl = configuration.getUrl();
        String login = configuration.getLogin();
        String password = configuration.getPassword();

        Connection conn = DriverManager.getConnection(jdbcUrl, login, password);

        if (log.isDebugEnabled()) {
            log.debug("connexion reussie pour l'utilisateur " + login +
                      " at  [" + jdbcUrl + ']');
        }

        return conn;
    }

    public static void releaseH2RootContext(TopiaContext context) throws SQLException {

        TopiaContextImplementor rootContext = ((TopiaContextImplementor) context).getRootContext();

        String url = (String) rootContext.getConfig().get(TopiaContextFactory.CONFIG_URL);
        String login = (String) rootContext.getConfig().get(TopiaContextFactory.CONFIG_USER);
        String password = (String) rootContext.getConfig().get(TopiaContextFactory.CONFIG_PASS);

        releaseRootContext(context);

        Connection conn = DriverManager.getConnection(url, login, password);

        try {
            Statement statement = conn.createStatement();
            statement.execute("SHUTDOWN;");
            statement.close();
            conn.close();
        } finally {
            conn.close();
        }
    }

    public static void releaseRootContext(TopiaContext context) {

        TopiaContextImplementor rootContext = ((TopiaContextImplementor) context).getRootContext();

        if (log.isInfoEnabled()) {
            log.info("release database " +
                     rootContext.getConfig().get(TopiaContextFactory.CONFIG_URL));
        }
        try {
            releaseContext(rootContext);
        } catch (TopiaException e) {
            // we don't want this to throw an exception, wants to close
            // all possible opened contexts
            if (log.isErrorEnabled()) {
                log.error("Could not close context " + rootContext, e);
            }
        }
    }

    public static void releaseContext(TopiaContext rootContext) throws TopiaException {
        if (rootContext != null && !rootContext.isClosed()) {
            rootContext.closeContext();
        }
    }

    public static void checkJDBCConnection(JdbcConfiguration configuration) throws SQLException {

        Connection conn = null;
        try {

            conn = newJDBCConnection(configuration);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not close properly connection to "
                                  + configuration.getUrl(), e);
                    }
                }
            }
        }
    }

    public static JdbcConfiguration newJdbcConfiguration(Properties dbConf) {

        JdbcConfiguration result = new JdbcConfigurationImpl();
        result.setUrl(dbConf.getProperty(TopiaContextFactory.CONFIG_URL));
        result.setLogin(dbConf.getProperty(TopiaContextFactory.CONFIG_USER));
        result.setPassword(dbConf.getProperty(TopiaContextFactory.CONFIG_PASS));
        return result;
    }

    public static <E> float getTotal(Collection<E> data,
                                     Function<E, Float> function) {
        return getTotal(data, function, Predicates.<E>alwaysTrue());
    }

    public static <E> float getTotal(Collection<E> data,
                                     Function<E, Float> function,
                                     Predicate<E> predicate) {
        float result = 0;
        for (E aCatch : data) {
            if (predicate.apply(aCatch))
                result += function.apply(aCatch);
        }
        return result;
    }

    public static <E> float getTotal(Collection<E> data,
                                     Function<E, Float> function,
                                     Predicate<E> predicate,
                                     Supplier<String> errorMessage) {
        float result = 0;
        for (E aCatch : data) {
            if (predicate.apply(aCatch)) {
                Float apply = function.apply(aCatch);
                Preconditions.checkNotNull(apply, errorMessage.get() + " on data: " + aCatch);
                result += apply;
            }
        }
        return result;
    }

    public static void closeConnection(TopiaContext transaction) {
        if (transaction == null) {
            if (log.isTraceEnabled()) {
                log.trace("no transaction to close");
            }
        } else if (transaction.isClosed()) {
            if (log.isTraceEnabled()) {
                log.trace("transaction " + transaction + " is already closed");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("closing transaction " + transaction);
            }

            try {
                Transaction tx = ((TopiaContextImplementor) transaction).getHibernate().getTransaction();
                if (!tx.wasCommitted() && !tx.wasRolledBack()) {
                    if (log.isDebugEnabled()) {
                        log.debug("rollback transaction!");
                    }
                    tx.rollback();
                }
                transaction.closeContext();
            } catch (TopiaException e) {
                throw new TopiaRuntimeException(e);
            }
        }
    }
}
