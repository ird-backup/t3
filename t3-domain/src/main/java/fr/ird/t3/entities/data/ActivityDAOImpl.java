/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;


import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.framework.TopiaSQLQuery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ActivityDAOImpl<E extends Activity> extends ActivityDAOAbstract<E> {

    public boolean isActivityWithSample(Activity activity) throws TopiaException {
        SampleWellDAO sampleSetDAO = T3DAOHelper.getSampleWellDAO(getContext());
        List<SampleWell> allByActivity = sampleSetDAO.findAllByActivity(activity);
        boolean withSample = CollectionUtils.isNotEmpty(allByActivity);
        return withSample;
    }

    public Map<String, Integer> findAllActivityIdsForCatchStratum(String zoneTableName,
                                                                  String zoneId,
                                                                  String schoolTypeId,
                                                                  T3Date beginDate,
                                                                  T3Date endDate) throws TopiaException {

        // ---
        // Get activities strictly inside the zone
        // ---

        TopiaSQLQuery<String> query = new GetActivityIdsInZoneForCatchStratumQuery(
                zoneTableName,
                zoneId,
                schoolTypeId,
                beginDate,
                endDate
        );

        List<String> activityIds = query.findMultipleResult(getContext());

        Map<String, Integer> result = Maps.newHashMap();
        for (String activityId : activityIds) {

            // activity strictly inside a zone are exactly in one zone
            result.put(activityId, 1);
        }

        // ---
        // Get activities on the border of the zone
        // ---

        query = new GetActivityIdsInBorderForCatchStratumQuery(
                zoneTableName,
                zoneId,
                schoolTypeId,
                beginDate,
                endDate
        );

        activityIds = query.findMultipleResult(getContext());

        // ---
        // Get nb zones of such activities
        // ---

        GetActivityIdsAndNbZonesQuery nbZoneQuery =
                new GetActivityIdsAndNbZonesQuery(zoneTableName);

        for (String activityId : activityIds) {

            // get all zones for this activity
            Integer nbzones = nbZoneQuery.getNbZones(context, activityId);
            result.put(activityId, nbzones);
        }
        return result;
    }

    public List<String> findAllActivityIdsForSampleStratum(String zoneTableName,
                                                           String zoneId,
                                                           String schoolTypeId,
                                                           T3Date beginDate,
                                                           T3Date endDate) throws TopiaException {

        TopiaSQLQuery<String> query;

        if (schoolTypeId == null) {

            // for any school type
            query = new GetActivityIdsForSampleStratumWithAnySchoolTypeQuery(
                    zoneTableName,
                    zoneId,
                    beginDate,
                    endDate
            );
        } else {

            // for a specific school type
            query = new GetActivityIdsForSampleStratumQuery(
                    zoneTableName,
                    zoneId,
                    schoolTypeId,
                    beginDate,
                    endDate
            );
        }

        List<String> activityIds = query.findMultipleResult(getContext());
        return activityIds;
    }

    public static Multimap<Long, Activity> groupActivitiesByDay(Collection<Activity> activities) {
        Multimap<Long, Activity> result =
                Multimaps.index(activities, T3Functions.ACTIVITY_BY_LONG_DAY);

        return result;
    }

    public static <E extends ActivityAware> Multimap<Activity, E> groupByActivity(Collection<E> activityAwares) {

        Multimap<Activity, E> result;

        if (CollectionUtils.isEmpty(activityAwares)) {

            // creates empty result
            result = ArrayListMultimap.create();
        } else {
            result = Multimaps.index(activityAwares,
                                     T3Functions.ACTIVITY_AWARE_BY_ACTIVITY);
        }
        return result;
    }

    public static Set<Activity> getAllActivitiesFromSampleWell(Collection<Sample> samples) {

        Set<Activity> result = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(samples)) {

            // there is some samples

            for (Sample sample : samples) {

                if (!sample.isSampleWellEmpty()) {

                    // there is some sample result
                    result.addAll(Collections2.transform(
                            sample.getSampleWell(), T3Functions.ACTIVITY_AWARE_BY_ACTIVITY
                    ));
                }
            }
        }
        return result;
    }

    public static <X extends WeightCategoryTreatmentAware> Multimap<WeightCategoryTreatment, X> groupByWeightCategoryTreatment(Collection<X> activity) {
        Multimap<WeightCategoryTreatment, X> index;
        if (CollectionUtils.isEmpty(activity)) {
            index = ArrayListMultimap.create();
        } else {
            index = Multimaps.index(activity, T3Functions.BY_WEIGHT_CATEGORY_TREATMENT);
        }
        return index;
    }

    public static <X extends WeightCategorySampleAware> Multimap<WeightCategorySample, X> groupByWeightCategorySample(Collection<X> activity) {
        Multimap<WeightCategorySample, X> index;
        if (CollectionUtils.isEmpty(activity)) {
            index = ArrayListMultimap.create();
        } else {
            index = Multimaps.index(activity, T3Functions.BY_WEIGHT_CATEGORY_SAMPLE);
        }
        return index;
    }

    public Set<Species> getAllSpeciesFromCorrectedCatches(Activity activity) {
        Set<Species> result = Sets.newHashSet();
        for (CorrectedElementaryCatch aCatch : activity.getCorrectedElementaryCatch()) {
            result.add(aCatch.getSpecies());
        }
        return result;
    }

    public Set<Species> getAllSpeciesFromSetSpeciesFrequencies(Activity activity) {
        Set<Species> result = Sets.newHashSet();
        for (SetSpeciesFrequency frequency : activity.getSetSpeciesFrequency()) {
            result.add(frequency.getSpecies());
        }
        return result;
    }

    public static void fillWeightsFromSetSpeciesCatWeight(Activity activity,
                                                          Map<WeightCategorySample, Map<Species, Float>> weights,
                                                          WeightCompositionAggregateModel model) {

        if (weights == null) {
            weights = Maps.newHashMap();
        }
        Multimap<WeightCategorySample, SetSpeciesCatWeight> data =
                ActivityDAO.groupByWeightCategorySample(activity.getSetSpeciesCatWeight());

        T3IOUtil.fillMapWithDefaultValue(
                weights,
                data.keySet(),
                T3Suppliers.MAP_SPECIES_FLOAT_SUPPLIER);

        for (WeightCategorySample weightCategory : data.keySet()) {

            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);

            Collection<SetSpeciesCatWeight> setSpeciesCatWeights = data.get(weightCategory);

            fillWeights(weightCategory,
                        setSpeciesCatWeights,
                        speciesFloatMap,
                        T3Functions.SET_SPECIES_CAT_WEIGHT_TO_WEIGHT,
                        model
            );
        }
    }

    public static void fillWeightsFromCatchesWeight(Activity activity,
                                                    WeightCompositionAggregateModel model,
                                                    int dividerFactor) {

        Map<WeightCategoryTreatment, Map<Species, Float>> weights = Maps.newHashMap();
        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> data =
                ActivityDAO.groupByWeightCategoryTreatment(activity.getCorrectedElementaryCatch());

        T3IOUtil.fillMapWithDefaultValue(
                weights, data.keySet(), T3Suppliers.MAP_SPECIES_FLOAT_SUPPLIER);

        for (WeightCategoryTreatment weightCategory : data.keySet()) {

            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);

            Collection<CorrectedElementaryCatch> correctedElementaryCatches = data.get(weightCategory);

            fillWeights(weightCategory,
                        correctedElementaryCatches,
                        speciesFloatMap,
                        T3Functions.CORRECTED_ELEMENTARY_CATCH_TO_CATCH_WEIGHT,
                        model,
                        dividerFactor
            );
        }
    }

//    public static void fillWeightsFromCorrectedCatchesWeight(Activity activity,
//                                                             Map<WeightCategoryTreatment, Map<Species, Float>> weights,
//                                                             WeightCompositionAggregateModel model) {
//
//        if (weights == null) {
//            weights = Maps.newHashMap();
//        }
//        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> data =
//                ActivityDAO.groupByWeightCategoryTreatment(activity.getCorrectedElementaryCatch());
//
//        T3IOUtil.fillMapWithDefaultValue(
//                weights, data.keySet(), T3Suppliers.MAP_SPECIES_FLOAT_SUPPLIER);
//
//        for (WeightCategoryTreatment weightCategory : data.keySet()) {
//
//            Map<Species, Float> speciesFloatMap = weights.get(weightCategory);
//
//            Collection<CorrectedElementaryCatch> correctedElementaryCatches = data.get(weightCategory);
//
//            fillWeights(weightCategory,
//                        correctedElementaryCatches,
//                        speciesFloatMap,
//                        T3Functions.CORRECTED_ELEMENTARY_CATCH_TO_CORRECTED_CATCH_WEIGHT,
//                        model
//            );
//        }
//    }

    public static <X extends WeightCategorySampleAware> void fillWeights(
            WeightCategorySample weightCategory,
            Collection<X> data,
            Map<Species, Float> weights,
            Function<X, Float> function,
            WeightCompositionAggregateModel model) {

        if (weights == null) {
            weights = Maps.newHashMap();
        }

        for (X setSpeciesCatWeight : data) {
            Species species = setSpeciesCatWeight.getSpecies();
            Float weight = weights.get(species);
            if (weight == null) {
                weight = 0f;
            }
            weight += function.apply(setSpeciesCatWeight);
            weights.put(species, weight);
        }

        if (model != null) {
            model.addModel(weightCategory, weights);
        }


    }

    public static <X extends WeightCategoryTreatmentAware> void fillWeights(
            WeightCategoryTreatment weightCategory,
            Collection<X> data,
            Map<Species, Float> weights,
            Function<X, Float> function,
            WeightCompositionAggregateModel model,
            int divideFactor) {

        if (weights == null) {
            weights = Maps.newHashMap();
        }

        for (X setSpeciesCatWeight : data) {
            Species species = setSpeciesCatWeight.getSpecies();
            Float weight = weights.get(species);
            if (weight == null) {
                weight = 0f;
            }
            weight += function.apply(setSpeciesCatWeight) / divideFactor;
            weights.put(species, weight);
        }

        if (model != null) {
            model.addModel(weightCategory, weights);
        }
    }

    public static <X extends SpeciesAware> Set<Species> getSpecies(Collection<X> correctedElementaryCatch) {
        Set<Species> result = Sets.newHashSet();
        Iterable<Species> transform = Iterables.transform(correctedElementaryCatch, T3Functions.SPECIES_AWARE_BY_SPECIES);
        Iterables.addAll(result, transform);
        return result;
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * catch stratum.
     *
     * @since 1.0
     */
    public static class GetActivityIdsInZoneForCatchStratumQuery extends TopiaSQLQuery<String> {

        private final String zoneTableName;

        private final String zoneId;

        private final String schoolTypeId;

        private final T3Date beginDate;

        private final T3Date endDate;

        public GetActivityIdsInZoneForCatchStratumQuery(String zoneTableName,
                                                        String zoneId,
                                                        String schoolTypeId,
                                                        T3Date beginDate,
                                                        T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeId = schoolTypeId;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, activity a, " +
                    zoneTableName + " z WHERE " +
                    "a.trip = t.topiaId " +
                    "AND t." + Trip.PROPERTY_SAMPLES_ONLY + " = false " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 " +
                    "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                    "AND z.topiaid = ? " +
                    "AND a.expertflag != 0 " +
                    "AND a.schooltype = ? " +
                    "AND a.date::date >= ? " +
                    "AND a.date::date <= ? " +
                    "AND (SELECT COUNT(*) FROM CorrectedElementaryCatch c WHERE c.activity = a.topiaid) > 0"
            );

            ps.setString(1, zoneId);
            ps.setString(2, schoolTypeId);
            ps.setDate(3, beginDate.toBeginSqlDate());
            ps.setDate(4, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        protected String prepareResult(ResultSet set) throws SQLException {
            String activityId = set.getString(1);
            return activityId;
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * catch stratum.
     *
     * @since 1.0
     */
    public static class GetActivityIdsInBorderForCatchStratumQuery extends TopiaSQLQuery<String> {

        private final String zoneTableName;

        private final String zoneId;

        private final String schoolTypeId;

        private final T3Date beginDate;

        private final T3Date endDate;

        public GetActivityIdsInBorderForCatchStratumQuery(String zoneTableName,
                                                          String zoneId,
                                                          String schoolTypeId,
                                                          T3Date beginDate,
                                                          T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeId = schoolTypeId;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, activity a, " +
                    zoneTableName + " z WHERE " +
                    "a.trip = t.topiaId " +
                    "AND t." + Trip.PROPERTY_SAMPLES_ONLY + " = false " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 " +
                    "AND ST_INTERSECTS(a.the_geom, z.the_geom) " +
                    "AND NOT ST_WITHIN(a.the_geom, z.the_geom) " +
                    "AND z.topiaid = ? " +
                    "AND a.expertflag != 0 " +
                    "AND a.schooltype = ? " +
                    "AND a.date::date >= ? " +
                    "AND a.date::date <= ? " +
                    "AND (SELECT COUNT(*) FROM CorrectedElementaryCatch c WHERE c.activity = a.topiaid) > 0"
            );

            ps.setString(1, zoneId);
            ps.setString(2, schoolTypeId);
            ps.setDate(3, beginDate.toBeginSqlDate());
            ps.setDate(4, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        protected String prepareResult(ResultSet set) throws SQLException {
            String activityId = set.getString(1);
            return activityId;
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * sample stratum.
     *
     * @since 1.0
     */
    public static class GetActivityIdsForSampleStratumQuery extends TopiaSQLQuery<String> {

        private final String zoneTableName;

        private final String zoneId;

        private final String schoolTypeId;

        private final T3Date beginDate;

        private final T3Date endDate;

        public GetActivityIdsForSampleStratumQuery(String zoneTableName,
                                                   String zoneId,
                                                   String schoolTypeId,
                                                   T3Date beginDate,
                                                   T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.schoolTypeId = schoolTypeId;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, activity a, " +
                    zoneTableName + " z WHERE " +
                    "a.trip = t.topiaId " +
                    "AND (t." + Trip.PROPERTY_SAMPLES_ONLY + " = true OR (" +
                    "t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 ))" +
                    "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                    "AND z.topiaid = ? " +
                    "AND a.schooltype = ?" +
                    "AND a.date::date >= ? " +
                    "AND a.date::date <= ? " +
                    "AND (SELECT COUNT(*) FROM SetSpeciesCatWeight c WHERE c.activity = a.topiaid) > 0"
            );
            ps.setString(1, zoneId);
            ps.setString(2, schoolTypeId);
            ps.setDate(3, beginDate.toBeginSqlDate());
            ps.setDate(4, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        protected String prepareResult(ResultSet set) throws SQLException {
            String activityId = set.getString(1);
            return activityId;
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * sample stratum for any school type (used by level 3 last substitution
     * level).
     *
     * @since 1.4
     */
    public static class GetActivityIdsForSampleStratumWithAnySchoolTypeQuery extends TopiaSQLQuery<String> {

        private final String zoneTableName;

        private final String zoneId;

        private final T3Date beginDate;

        private final T3Date endDate;

        public GetActivityIdsForSampleStratumWithAnySchoolTypeQuery(String zoneTableName,
                                                                    String zoneId,
                                                                    T3Date beginDate,
                                                                    T3Date endDate) {
            this.zoneTableName = zoneTableName;
            this.zoneId = zoneId;
            this.beginDate = beginDate;
            this.endDate = endDate;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT a.topiaid FROM trip t, activity a, " +
                    zoneTableName + " z WHERE " +
                    "a.trip = t.topiaId " +
                    "AND (t." + Trip.PROPERTY_SAMPLES_ONLY + " = true OR (" +
                    "t." + Trip.PROPERTY_COMPLETION_STATUS + " IS NOT NULL " +
                    "AND t." + Trip.PROPERTY_COMPLETION_STATUS + " > 0 ))" +
                    "AND ST_WITHIN(a.the_geom, z.the_geom) " +
                    "AND z.topiaid = ? " +
                    "AND a.date::date >= ? " +
                    "AND a.date::date <= ? " +
                    "AND (SELECT COUNT(*) FROM SetSpeciesCatWeight c WHERE c.activity = a.topiaid) > 0"
            );
            ps.setString(1, zoneId);
            ps.setDate(2, beginDate.toBeginSqlDate());
            ps.setDate(3, endDate.toEndSqlDate());
            return ps;
        }

        @Override
        protected String prepareResult(ResultSet set) throws SQLException {
            String activityId = set.getString(1);
            return activityId;
        }
    }

    /**
     * Query to obtain from the given configuration, all the activity ids of a
     * sample stratum.
     *
     * @since 1.4
     */
    public static class GetActivityIdsAndNbZonesQuery extends TopiaSQLQuery<Integer> {

        private final String zoneTableName;

        private String activityId;

        public GetActivityIdsAndNbZonesQuery(String zoneTableName) {
            this.zoneTableName = zoneTableName;
        }

        @Override
        protected PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT count(*) " +
                    "FROM activity a, " + zoneTableName + " z " +
                    "WHERE a.topiaId  = ? " +
                    "AND ST_INTERSECTS(a.the_geom, z.the_geom) " +
                    "AND z.schoolType = a.schoolType"
            );
            ps.setString(1, activityId);
            return ps;
        }

        @Override
        protected Integer prepareResult(ResultSet set) throws SQLException {
            int nbZones = set.getInt(1);
            return nbZones;
        }

        public int getNbZones(TopiaContextImplementor context, String activityId) throws TopiaException {
            this.activityId = activityId;
            return findSingleResult(context);
        }

    }
}
