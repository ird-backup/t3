/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.entities.reference.T3ReferenceEntity;
import org.nuiton.topia.persistence.util.TopiaEntityMap;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * A map of reference entities associated with their type ({@link T3EntityEnum}).
 *
 * @author chemit <chemit@codelutin.com>
 * @see T3EntityEnum
 * @see T3ReferenceEntity
 * @since 1.0
 */
public class ReferenceEntityMap extends TopiaEntityMap<T3EntityEnum, T3ReferenceEntity> {

    private static final long serialVersionUID = 1L;

    public ReferenceEntityMap(EnumMap<T3EntityEnum, ? extends List<? extends T3ReferenceEntity>> m) {
        super(m);
    }

    public ReferenceEntityMap(Map<T3EntityEnum, ? extends List<? extends T3ReferenceEntity>> m) {
        super(m);
    }

    public ReferenceEntityMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public ReferenceEntityMap(int initialCapacity) {
        super(initialCapacity);
    }

    public ReferenceEntityMap() {
    }

    @Override
    protected T3EntityEnum getType(Class<?> e) {
        T3EntityEnum type = T3EntityEnum.valueOf(e);
        return type;
    }

}
