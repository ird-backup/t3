/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentDAO;
import fr.ird.t3.services.DecoratorService;
import org.nuiton.util.decorator.Decorator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Helper to build and render {@link LengthCompositionModel} and
 * {@link LengthCompositionAggregateModel}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthCompositionModelHelper {

    private LengthCompositionModelHelper() {
        // helper with no instance
    }

    public static String decorateModel(DecoratorService decoratorService,
                                       String title,
                                       String valueTitle,
                                       Map<Species, LengthCompositionAggregateModel> models) {

        StringBuilder inResume = new StringBuilder();

        Locale locale = decoratorService.getLocale();

        String lengthClassLibelle = l_(locale, "t3.common.lengthClass");

        int lengthClassColumnLength = lengthClassLibelle.length();
        int valueColumnLength = 15;
        int rateColumnLength = 15;

        Decorator<Species> decorator =
                decoratorService.getDecorator(locale, Species.class, null);

        Decorator<WeightCategoryTreatment> decorator2 =
                decoratorService.getDecorator(locale, WeightCategoryTreatment.class, null);

        List<Species> speciesList = Lists.newArrayList(models.keySet());
        Collections.sort(speciesList, SPECIES_COMPARATOR);
        int origMaxLength = 10 + (lengthClassColumnLength) + (valueColumnLength + rateColumnLength);
        int maxLength = origMaxLength;

        maxLength = Math.max(maxLength, l_(locale, "t3.common.forAllWeightCategories").length() + 4);

        for (Species species : speciesList) {
            int l = decorator.toString(species).length() + 4;
            maxLength = Math.max(maxLength, l);
            LengthCompositionAggregateModel model = models.get(species);

            Set<WeightCategoryTreatment> weightCategories = model.getWeightCategories();
            for (WeightCategoryTreatment weightCategory : weightCategories) {
                int l2 =
                        l_(locale, "t3.common.weightCategory",
                           decorator2.toString(weightCategory)).length() + 4;

                maxLength = Math.max(maxLength, l2);
            }
        }

        if (maxLength > origMaxLength) {
            lengthClassColumnLength += maxLength - origMaxLength;
        }

        CompositionTableModel header = new CompositionTableModel();
        String lineFormat = "| %1$-" + lengthClassColumnLength + "s | %2$-" +
                            valueColumnLength + "s - %3$-" + rateColumnLength
                            + "s |\n";

        header.setLineFormat(lineFormat);
        header.setHeader(
                String.format("| %1$-" + lengthClassColumnLength + "s | %2$-" +
                              (valueColumnLength + rateColumnLength + 3) + "s |\n",
                              lengthClassLibelle,
                              valueTitle));

        header.setSeparatorFormat3(
                Strings.padEnd("|", lengthClassColumnLength + 3, '-') +
                Strings.padEnd("|", (valueColumnLength + rateColumnLength + 6), '-') + "|\n");

        header.setCategoryFormat("| %1$-" + (maxLength - 4) + "s |\n");
        header.setSeparatorFormat(Strings.padEnd("|", maxLength - 1, '-') + "|\n");
        header.setSeparatorFormat2(Strings.padEnd("|", maxLength - 1, '=') + "|\n");
        header.setTopSeparatorFormat(Strings.padEnd("=", maxLength, '=') + "\n");
        header.setBottomSeparatorFormat(Strings.padEnd("=", maxLength, '=') + "\n");

        inResume.append(title).append('\n');
        inResume.append(header.getTopSeparatorFormat());
        inResume.append(header.getHeader());

        for (Species species : speciesList) {

            inResume.append(header.getSeparatorFormat2());
            inResume.append(String.format(header.getCategoryFormat(),
                                          decorator.toString(species)));

            LengthCompositionAggregateModel model = models.get(species);

            List<WeightCategoryTreatment> weightCategories = Lists.newArrayList(model.getWeightCategories());
            Collections.sort(weightCategories, WeightCategoryTreatmentDAO.newComparator());

            for (WeightCategoryTreatment weightCategory : weightCategories) {
                LengthCompositionModel lengthCompositionModel = model.getModel(weightCategory);
                if (!lengthCompositionModel.isEmpty()) {
                    inResume.append(header.getSeparatorFormat());
                    decorateModel(
                            header,
                            decoratorService,
                            model,
                            weightCategory,
                            inResume
                    );
                }

            }
        }

        inResume.append(header.getBottomSeparatorFormat());

        return inResume.toString();
    }

    private static void decorateModel(CompositionTableModel header,
                                      DecoratorService decoratorService,
                                      LengthCompositionAggregateModel model,
                                      WeightCategoryTreatment weightCategory,
                                      StringBuilder inResume) {

        Decorator<WeightCategoryTreatment> decorator2 =
                decoratorService.getDecorator(decoratorService.getLocale(), WeightCategoryTreatment.class, null);

        LengthCompositionModel m;

        String categoryLibelle;

        Locale locale = decoratorService.getLocale();

        String totalLibelle = l_(locale, "t3.common.total");
        String averageLengthLibelle = l_(locale, "t3.common.averageLength");

        if (weightCategory == null) {

            m = model.getTotalModel();
            categoryLibelle = l_(locale, "t3.common.forAllWeightCategories");
        } else {
            m = model.getModel(weightCategory);

            categoryLibelle =
                    l_(locale, "t3.common.weightCategory",
                       decorator2.toString(weightCategory));
        }

        if (!m.isEmpty()) {

            inResume.append(String.format(header.getCategoryFormat(), categoryLibelle));
            inResume.append(header.getSeparatorFormat());

            String lineFormat = header.getLineFormat();

            List<Integer> lengthClasses = Lists.newArrayList(m.getLengthClasses());
            Collections.sort(lengthClasses);

            float totalRate = 0;
            float totalCount = 0;

            float totalLength = 0;

            for (Integer lengthClass : lengthClasses) {

                float weightRate1 = m.getRate(lengthClass);
                totalRate += weightRate1;
                float value = m.getValue(lengthClass);
                inResume.append(String.format(
                        lineFormat,
                        lengthClass,
                        value,
                        (100 * weightRate1) + "%"));
                totalCount += value;
                totalLength += lengthClass * value;
            }

            float averageLength = totalLength / totalCount;

            inResume.append(header.getSeparatorFormat3());

            inResume.append(String.format(
                    lineFormat,
                    totalLibelle,
                    totalCount,
                    (100 * totalRate) + "%"));

            if (weightCategory != null) {
                inResume.append(String.format(
                        lineFormat,
                        averageLengthLibelle,
                        averageLength,
                        "-"));
            }

        }
    }

    public static final Comparator<Species> SPECIES_COMPARATOR = new Comparator<Species>() {
        @Override
        public int compare(Species o1, Species o2) {
            return o1.getCode() - o2.getCode();
        }
    };
}
