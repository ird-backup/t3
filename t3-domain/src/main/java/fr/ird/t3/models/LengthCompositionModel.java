/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import org.apache.commons.collections.MapUtils;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Model of a by length class composition model.
 * <p/>
 * This contains for some values for each length class  and also
 * his rate amoung all species values.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthCompositionModel implements Closeable {

    /** The weight cateogry involved. */
    protected final WeightCategoryTreatment weightCategory;

    /** The species involved. */
    protected final Species species;

    /** Weight for each length class. */
    protected final Map<Integer, Float> value;

    /** Rate for each length class. */
    protected final Map<Integer, Float> rate;

    /** Total value for length class. */
    protected float totalValue;

    /** Extra value to set (for a species/weightCategory context). */
    protected float extraValue;

    public LengthCompositionModel(WeightCategoryTreatment weightCategory) {
        this(weightCategory, null);
    }

    public LengthCompositionModel() {
        this(null, null);
    }

    public LengthCompositionModel(WeightCategoryTreatment weightCategory,
                                  Species species) {
        this.weightCategory = weightCategory;
        this.species = species;
        value = Maps.newHashMap();
        rate = Maps.newHashMap();
    }

    public LengthCompositionModel(WeightCategoryTreatment weightCategory,
                                  Species species,
                                  Map<Integer, Float> values) {
        this(weightCategory, species);
        addValues(values);
    }

    public boolean isEmpty() {
        return value.isEmpty();
    }

    public void addValues(Map<Integer, Float> values) {

        for (Map.Entry<Integer, Float> e : values.entrySet()) {
            Integer lengthClass = e.getKey();
            Float lengthClassValue = e.getValue();
            if (lengthClassValue == 0) {

                // do not add empty value
                continue;
            }
            Float aFloat = value.get(lengthClass);
            if (aFloat == null) {
                aFloat = lengthClassValue;

            } else {
                aFloat += lengthClassValue;
            }
            value.put(lengthClass, aFloat);
        }

        // rebuild totalWeight
        totalValue = 0;
        for (Float aFloat : value.values()) {
            totalValue += aFloat;
        }

        boolean nullTotalValue = totalValue == 0;
        if (nullTotalValue) {

            // to be sure to have no NaN
            totalValue = 1;
        }

        // rebuild weightRate
        rate.clear();
        for (Map.Entry<Integer, Float> e : value.entrySet()) {
            Integer lengthClass = e.getKey();
            Float w = e.getValue();
            rate.put(lengthClass, w / totalValue);
        }

        if (nullTotalValue) {

            // push back total weight to 0
            totalValue = 0;
        }
    }

    public void addValues(LengthCompositionModel model) {
        addValues(model.value);
    }

    public LengthCompositionModel extractForLengthClasses(Collection<Integer> lengthClasses) {
        Map<Integer, Float> newValues = Maps.newHashMap();
        for (Integer lengthClass : lengthClasses) {
            float value1;
            if (value.containsKey(lengthClass)) {
                value1 = getValue(lengthClass);
            } else {
                value1 = 0;
            }
            newValues.put(lengthClass, value1);
        }

        LengthCompositionModel result;
        if (MapUtils.isEmpty(newValues)) {
            result = null;
        } else {
            result = new LengthCompositionModel(weightCategory, species, newValues);
        }
        return result;
    }

    public WeightCategoryTreatment getWeightCategory() {
        return weightCategory;
    }

    public Species getSpecies() {
        return species;
    }

    public float getRate(int lengthClass) {
        float result = rate.get(lengthClass);
        return result;
    }

    public float getValue(int lengthClass) {
        float result = value.get(lengthClass);
        return result;
    }

    public float getTotalValue() {
        return totalValue;
    }

    public float getExtraValue() {
        return extraValue;
    }

    public void setExtraValue(float extraValue) {
        this.extraValue = extraValue;
    }

    public Set<Integer> getLengthClasses() {
        return value.keySet();
    }

    @Override
    public void close() throws IOException {
        value.clear();
        rate.clear();
    }
}
