/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.models;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyDAO;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import org.nuiton.topia.TopiaException;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To aggregate some {@link LengthCompositionModel} models.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthCompositionAggregateModel implements Closeable {

    protected final Map<WeightCategoryTreatment, LengthCompositionModel> model;

    protected final LengthCompositionModel totalModel;

    public LengthCompositionAggregateModel() {
        model = Maps.newHashMap();
        totalModel = new LengthCompositionModel();
    }

    public void addModel(WeightCategoryTreatment weightCategory,
                         Species species,
                         Map<Integer, Float> weights) {
        LengthCompositionModel m;
        m = getModel(weightCategory);
        if (m == null) {

            m = new LengthCompositionModel(weightCategory, species, weights);
            model.put(weightCategory, m);
        } else {

            // add weights to existing model
            m.addValues(weights);
        }

        // add also to total model
        totalModel.addValues(weights);
    }

    public void addModel(LengthCompositionModel incomingModel) {

        WeightCategoryTreatment weightCategory =
                incomingModel.getWeightCategory();

        LengthCompositionModel m = getModel(weightCategory);
        if (m == null) {
            m = new LengthCompositionModel(weightCategory);

            // new model to store
            model.put(weightCategory, m);
        }

        // add weights to model
        m.addValues(incomingModel);

        // add also to total model
        totalModel.addValues(incomingModel);
    }

    public void addModel(LengthCompositionAggregateModel modelToMerge) {
        for (LengthCompositionModel compositionModel :
                modelToMerge.getModel().values()) {
            addModel(compositionModel);
        }
    }

    public LengthCompositionAggregateModel extractForLengthClasses(Collection<Integer> lengthClasses) {
        LengthCompositionAggregateModel result = new LengthCompositionAggregateModel();
        for (LengthCompositionModel e : model.values()) {
            LengthCompositionModel newModel = e.extractForLengthClasses(lengthClasses);
            if (newModel != null) {
                result.addModel(newModel);
            }
        }
        return result;
    }

    public LengthCompositionModel getModel(WeightCategoryTreatment weightCategory) {
        return model.get(weightCategory);
    }

    public LengthCompositionModel getTotalModel() {
        return totalModel;
    }

    protected Map<WeightCategoryTreatment, LengthCompositionModel> getModel() {
        return model;
    }

    public Set<WeightCategoryTreatment> getWeightCategories() {
        return model.keySet();
    }

    @Override
    public void close() throws IOException {
        try {

            totalModel.close();

            for (LengthCompositionModel weightCompositionModel : model.values()) {
                weightCompositionModel.close();
            }
        } finally {
            model.clear();
        }
    }

    public static void close(Map<?, LengthCompositionAggregateModel> map) throws IOException {
        try {
            for (LengthCompositionAggregateModel model : map.values()) {
                model.close();
            }
        } finally {
            map.clear();
        }
    }

    public static void buildCompositionModel(Multimap<Species, SetSpeciesFrequency> setSpeciesFrequenciesBySpecies,
                                             Map<Species, LengthCompositionAggregateModel> models,
                                             List<WeightCategoryTreatment> weightCategories,
                                             LengthWeightConversionHelper conversionHelper) throws TopiaException {

        for (Species species : setSpeciesFrequenciesBySpecies.keySet()) {

            LengthWeightConversion conversions =
                    conversionHelper.getConversions(species);

            Collection<SetSpeciesFrequency> setSpeciesFrequencies =
                    setSpeciesFrequenciesBySpecies.get(species);

            // collect counts
            Map<Integer, Float> counts =
                    SetSpeciesFrequencyDAO.collectSampleCount(setSpeciesFrequencies);

            // get all length classes
            List<Integer> lengthClasses = Lists.newArrayList(counts.keySet());

            // sort them
            Collections.sort(lengthClasses);

            // distribute then to the weightCategories
            Map<Integer, WeightCategoryTreatment> weightCategoriesDistribution =
                    conversionHelper.getWeightCategoriesDistribution(
                            conversions, weightCategories, lengthClasses);

            LengthCompositionAggregateModel model =
                    new LengthCompositionAggregateModel();

            models.put(species, model);

            // compute

            for (WeightCategoryTreatment weightCategory : weightCategories) {

                // compute weights
                Map<Integer, Float> weights = Maps.newHashMap();

                for (Map.Entry<Integer, WeightCategoryTreatment> entry : weightCategoriesDistribution.entrySet()) {
                    if (weightCategory.equals(entry.getValue())) {

                        // length class
                        int lengthClass = entry.getKey();

                        // fishes count
                        float count = counts.get(lengthClass);

                        // unit fish weight (from length class)
                        float unitWeight = conversions.computeWeightFromLFLengthClass(lengthClass);

                        // total weight for this lenght class
                        float totalWeight = count * unitWeight;

                        // keep it
                        weights.put(lengthClass, totalWeight);
                    }
                }

                model.addModel(weightCategory, species, weights);
            }

        }
    }
}
