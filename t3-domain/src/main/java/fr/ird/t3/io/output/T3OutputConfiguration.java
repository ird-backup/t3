/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.output;

import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.entities.user.JdbcConfigurationImpl;

import java.util.List;

import static org.nuiton.i18n.I18n.n_;

/**
 * Configuration of a output pilot.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3OutputConfiguration extends JdbcConfigurationImpl {

    static {
        n_("t3.output.operations");
        n_("t3.output.outputProvider");
    }

    private static final long serialVersionUID = 1L;

    /** List of operation ids selected by user. */
    protected List<String> operationIds;

    /** Begin date to use. */
    protected T3Date beginDate;

    /** End date to use. */
    protected T3Date endDate;

    /** Id of the selected fleet to use. */
    protected String fleetId;

    /** Id of the selected ocean to use. */
    protected String oceanId;

    public List<String> getOperationIds() {
        return operationIds;
    }

    public void setOperationIds(List<String> operationIds) {
        this.operationIds = operationIds;
    }

    public T3Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(T3Date beginDate) {
        this.beginDate = beginDate;
    }

    public T3Date getEndDate() {
        return endDate;
    }

    public void setEndDate(T3Date endDate) {
        this.endDate = endDate;
    }

    public String getFleetId() {
        return fleetId;
    }

    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }

    public String getOceanId() {
        return oceanId;
    }

    public void setOceanId(String oceanId) {
        this.oceanId = oceanId;
    }

}
