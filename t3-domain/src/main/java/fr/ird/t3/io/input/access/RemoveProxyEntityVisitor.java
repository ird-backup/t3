/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import com.google.common.collect.Lists;
import fr.ird.msaccess.importer.AccessEntityVisitor;
import fr.ird.t3.entities.T3EntityEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

import java.util.List;

/**
 * Visitor to remove all proxy instance for a given entity.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class RemoveProxyEntityVisitor extends AccessEntityVisitor<T3EntityEnum, T3AccessEntityMeta, T3AccessEntity> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(RemoveProxyEntityVisitor.class);

    public <E extends TopiaEntity> E doVisit(E entity) throws TopiaException {

        if (!(entity instanceof T3AccessEntity)) {
            throw new IllegalStateException("Can not use this visitor with not a T3AccessEntity");
        }

        T3AccessEntity t3AccessEntity = (T3AccessEntity) entity;

        if (log.isDebugEnabled()) {
            log.debug("Will visit " + AbstractT3EntityVisitor.toString(t3AccessEntity));
        }
        acceptEntity(t3AccessEntity);

        return (E) t3AccessEntity.getTopiaEntity();
    }

    @Override
    public void onStart(T3AccessEntity entity, T3AccessEntityMeta meta) {

        if (log.isDebugEnabled()) {

            log.debug(AbstractT3EntityVisitor.toString(entity));
        }
    }

    @Override
    public void onEnd(T3AccessEntity entity, T3AccessEntityMeta meta) {

        if (log.isDebugEnabled()) {

            log.debug(AbstractT3EntityVisitor.toString(entity));
        }
    }

    @Override
    public void onVisitReverseAssociation(String propertyName,
                                          T3AccessEntity entity,
                                          T3AccessEntityMeta meta) {

        T3AccessEntity reverse = (T3AccessEntity)
                entity.getProperty(propertyName);

        if (reverse != null) {
            entity.setProperty(propertyName, reverse.getTopiaEntity());
        }
    }

    @Override
    public void onVisitComposition(String propertyName,
                                   Class<?> type,
                                   T3AccessEntity entity,
                                   T3AccessEntityMeta meta) {

        TopiaEntity entityProperty = (TopiaEntity) entity.getProperty(propertyName);

        if (!(entityProperty instanceof T3AccessEntity)) {

            // this entity is no more a access entity, nothing to do
            return;
        }

        T3AccessEntity reverse = (T3AccessEntity) entityProperty;

        acceptEntity(reverse);

        entity.setProperty(propertyName, reverse.getTopiaEntity());
    }

    @Override
    public void onVisitAssociation(String propertyName,
                                   Class<?> type,
                                   T3AccessEntity entity,
                                   T3AccessEntityMeta meta) {

        List<? extends T3AccessEntity> childs =
                (List<T3AccessEntity>) entity.getProperty(propertyName);

        if (!CollectionUtils.isEmpty(childs)) {
            List<TopiaEntity> newChilds = Lists.newArrayList();

            for (T3AccessEntity child : childs) {

                acceptEntity(child);

                newChilds.add(child.getTopiaEntity());
            }

            entity.setAssociationProperty(propertyName, newChilds);
        }
    }

    @Override
    public void onVisitSimpleProperty(String propertyName,
                                      Class<?> type,
                                      T3AccessEntity entity,
                                      T3AccessEntityMeta meta) {
    }

    protected void acceptEntity(T3AccessEntity child) {
        if (child.getTopiaId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("Skip already treated entity " +
                          AbstractT3EntityVisitor.toString(child));
            }
            return;
        }
        child.setTopiaId(TopiaId.create(child.getMeta().getType().getContract()));
        try {
            child.accept(this);
        } catch (TopiaException e) {
            // on ne devrait pas avoir de tel exception (on utilise pas topia)
            throw new IllegalStateException("Could not accept " + child, e);
        }
    }

    @Override
    public void clear() {
    }

}
