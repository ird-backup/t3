/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input.access;

import com.google.common.collect.Lists;
import fr.ird.t3.entities.ReferenceEntityMap;
import fr.ird.t3.entities.T3EntityEnum;
import fr.ird.t3.entities.reference.T3ReferenceEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;

/**
 * Helper to use ms-access.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class T3MSAccessHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3MSAccessHelper.class);

    protected T3MSAccessHelper() {
        // hide helper constructor
    }

    public static void loadReferentiel(T3ReferentielEntityVisitor visitor,
                                       T3AccessDataSource dataSource,
                                       ReferenceEntityMap references,
                                       T3EntityEnum constant,
                                       boolean deep) throws TopiaException {

        T3AccessEntityMeta meta = dataSource.getMeta(constant);

        if (log.isInfoEnabled()) {
            log.info("Load referentiel " + constant);
        }
        List<? extends T3ReferenceEntity> entities = dataSource.loadEntities(meta);

        for (T3ReferenceEntity entity : entities) {
            visitor.doVisit(entity, deep);
        }
        references.put(constant, entities);
        if (log.isInfoEnabled()) {
            log.info("Loaded entity " + constant + " with " +
                     entities.size() + " entries");
        }
    }

    public static <E extends TopiaEntity> List<E> removeProxies(RemoveProxyEntityVisitor visitor,
                                                                List<E> entities) throws TopiaException {

        List<E> entitiesToStore = Lists.newArrayList();
        for (E entity : entities) {
            E e = visitor.doVisit(entity);
            entitiesToStore.add(e);
        }
        return entitiesToStore;
    }
}
