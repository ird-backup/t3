/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.io.input;

import java.io.File;

/**
 * Configuration of a T3Input.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class T3InputConfiguration {

    protected boolean useWells;

    protected boolean canCreateVessel;

    protected boolean createVirtualVessel;

    protected boolean samplesOnly;

    protected File inputFile;

    public boolean isUseWells() {
        return useWells;
    }

    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    public boolean isSamplesOnly() {
        return samplesOnly;
    }

    public void setSamplesOnly(boolean samplesOnly) {
        this.samplesOnly = samplesOnly;
    }

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }
}
