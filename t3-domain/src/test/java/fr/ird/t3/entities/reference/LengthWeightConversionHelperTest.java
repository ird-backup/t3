/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.T3ScriptHelper;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * tests the {@link LengthWeightConversionHelper}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class LengthWeightConversionHelperTest extends AbstractDatabaseTest {

    @Before
    public void setUp() throws Exception {

        TopiaContext tx = beginTransaction();
        try {
            T3ScriptHelper.loadReferentiel(
                    tx, "/db/LengthWeightConversionHelperTest.sql");
        } finally {
            tx.closeContext();
        }
    }

    @Test
    public void testGetConversionsAtlantique() throws Exception {

        TopiaContext tx = beginTransaction();

        Ocean ocean = T3DAOHelper.getOceanDAO(tx).findByCode(1);
        Assert.assertNotNull(ocean);
        testConversionForOcean(tx, ocean);

    }

    @Test
    public void testGetConversionsIndian() throws Exception {

        TopiaContext tx = beginTransaction();

        Ocean ocean = T3DAOHelper.getOceanDAO(tx).findByCode(2);
        Assert.assertNotNull(ocean);
        testConversionForOcean(tx, ocean);

    }

    protected void testConversionForOcean(TopiaContext tx,
                                          Ocean ocean) throws TopiaException {
        Date date = T3Date.newDate(1, 2010).toBeginDate();

        LengthWeightConversionHelper conversionHelper =
                new LengthWeightConversionHelper(
                        T3DAOHelper.getLengthWeightConversionDAO(tx),
                        ocean,
                        0,
                        date
                );
        Assert.assertNotNull(conversionHelper);

        SpeciesDAO speciesDAO = T3DAOHelper.getSpeciesDAO(tx);
        List<Species> allSpecies = speciesDAO.findAll();
        for (Species species : allSpecies) {

            LengthWeightConversion conversion =
                    conversionHelper.getConversions(species);

            // only species with code in [1-6] have a convertor
            if (species.getCode() > 6) {

                // no convertor
                Assert.assertNull(conversion);
            } else {

                Assert.assertNotNull(conversion);

                // try to convert

                conversion.computeLength(1);
                conversion.computeWeight(100);

                WeightCategoryTreatmentDAO weightCategoryTreatmentDAO =
                        T3DAOHelper.getWeightCategoryTreatmentDAO(tx);

                List<SchoolType> schoolTypes = Lists.newArrayList();
                schoolTypes.add(T3DAOHelper.getSchoolTypeDAO(tx).findByCode(1));
                schoolTypes.add(T3DAOHelper.getSchoolTypeDAO(tx).findByCode(2));

                for (SchoolType schoolType : schoolTypes) {

                    List<WeightCategoryTreatment> allCategories =
                            weightCategoryTreatmentDAO.findAllByProperties(
                                    WeightCategoryTreatment.PROPERTY_OCEAN, ocean,
                                    WeightCategoryTreatment.PROPERTY_SCHOOL_TYPE, schoolType
                            );

                    Assert.assertNotNull(allCategories);
                    Assert.assertFalse(allCategories.isEmpty());

                    for (WeightCategoryTreatment category : allCategories) {
                        conversionHelper.getSpecieHighestLengthClass(conversion, category);
                    }

                    Range<Integer> open = Range.open(0, 1000);
                    List<Integer> lenghtClasses = Lists.newArrayList(open.asSet(DiscreteDomain.integers()));

                    Map<Integer, WeightCategoryTreatment> distribution =
                            conversionHelper.getWeightCategoriesDistribution(conversion, allCategories, lenghtClasses);
                    Assert.assertNotNull(distribution);
                    Set<WeightCategoryTreatment> usedweithCategories = Sets.newHashSet(distribution.values());
                    // all categories must be used (except the undefined one
                    Assert.assertEquals(allCategories.size() - 1, usedweithCategories.size());
                }
            }

        }
    }
}
