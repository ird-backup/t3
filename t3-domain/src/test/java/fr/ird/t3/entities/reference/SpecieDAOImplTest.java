/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Set;

/**
 * Test the user dao {@link SpeciesDAOImpl}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SpecieDAOImplTest extends AbstractDatabaseTest {

    @Test
    public void findAllSpecieUsedInCatch() throws Exception {

        TopiaContext tx = beginTransaction();

        SpeciesDAO dao = T3DAOHelper.getSpeciesDAO(tx);

        Species species = dao.create(Species.PROPERTY_CODE, 0);
        Species species2 = dao.create(Species.PROPERTY_CODE, 1);

        Trip trip = T3DAOHelper.getTripDAO(tx).create();

        // create another trip without any activity (so withou also any corrected catches)
        Trip trip2 = T3DAOHelper.getTripDAO(tx).create();

        Activity activity = T3DAOHelper.getActivityDAO(tx).create(
                Activity.PROPERTY_TRIP, trip
        );
        trip.addActivity(activity);

        CorrectedElementaryCatch catche =
                T3DAOHelper.getCorrectedElementaryCatchDAO(tx).create(CorrectedElementaryCatch.PROPERTY_SPECIES, species);
        activity.addCorrectedElementaryCatch(catche);

        Set<Species> result = dao.findAllSpeciesUsedInCatch();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(species, result.iterator().next());
    }

}
