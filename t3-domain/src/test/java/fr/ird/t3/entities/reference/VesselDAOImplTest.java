/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Set;

/**
 * Test the user dao {@link VesselDAOImpl}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class VesselDAOImplTest extends AbstractDatabaseTest {

    @Test
    public void findAllUsedInTrip() throws Exception {

        TopiaContext tx = beginTransaction();

        VesselDAO dao = T3DAOHelper.getVesselDAO(tx);
        Vessel vessel = dao.create(Vessel.PROPERTY_CODE, 1);
        Vessel vessel2 = dao.create(Vessel.PROPERTY_CODE, 2);

        Trip trip = T3DAOHelper.getTripDAO(tx).create(
                Trip.PROPERTY_VESSEL, vessel
        );
        Activity activity = T3DAOHelper.getActivityDAO(tx).create(
                Activity.PROPERTY_TRIP, trip
        );
        trip.addActivity(activity);
        Set<Vessel> result = dao.findAllUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(vessel, result.iterator().next());
    }

}
