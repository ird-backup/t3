/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.reference;

import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.Trip;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Set;

/**
 * Test the user dao {@link VesselSimpleTypeDAOImpl}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class VesselSimpleTypeDAOImplTest extends AbstractDatabaseTest {

    @Test
    public void findAllUsedInTrip() throws Exception {

        TopiaContext tx = beginTransaction();

        VesselSimpleTypeDAO dao = T3DAOHelper.getVesselSimpleTypeDAO(tx);
        VesselSimpleType vesselSimpleType = dao.create(VesselSimpleType.PROPERTY_CODE, 1);
        VesselSimpleType vesselSimpleType2 = dao.create(VesselSimpleType.PROPERTY_CODE, 2);

        VesselTypeDAO vesselTypeDAO = T3DAOHelper.getVesselTypeDAO(tx);
        VesselType vesselType = vesselTypeDAO.create(VesselType.PROPERTY_CODE, 1, VesselType.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType);
        VesselType vesselType2 = vesselTypeDAO.create(VesselType.PROPERTY_CODE, 2, VesselType.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType2);

        VesselDAO vesselDAO = T3DAOHelper.getVesselDAO(tx);
        Vessel vessel = vesselDAO.create(Vessel.PROPERTY_CODE, 1, Vessel.PROPERTY_VESSEL_TYPE, vesselType);
        Vessel vessel2 = vesselDAO.create(Vessel.PROPERTY_CODE, 2, Vessel.PROPERTY_VESSEL_TYPE, vesselType2);

        Trip trip = T3DAOHelper.getTripDAO(tx).create(
                Trip.PROPERTY_VESSEL, vessel
        );
        Set<VesselSimpleType> result = dao.findAllUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(vesselSimpleType, result.iterator().next());
    }

}
