/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities.data;

import com.google.common.collect.Multimap;
import fr.ird.t3.entities.AbstractDatabaseTest;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanDAO;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Tests the trip dao {@link TripDAO}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class TripDAOImplTest extends AbstractDatabaseTest {

    TopiaContext tx;

    TripDAO dao;

    OceanDAO oceanDAO;

    ActivityDAO activitDAO;

    @Before
    public void setUp() throws Exception {

        tx = beginTransaction();
        dao = T3DAOHelper.getTripDAO(tx);
        oceanDAO = T3DAOHelper.getOceanDAO(tx);
        activitDAO = T3DAOHelper.getActivityDAO(tx);
    }

    @Test
    public void findAllBetweenLandingDate() throws Exception {

        T3Date startDate = T3Date.newDate(1, 2010);
        T3Date endDate = T3Date.newDate(1, 2011);

        Trip trip = dao.create(Trip.PROPERTY_CODE, 0,
                               Trip.PROPERTY_LANDING_DATE, startDate.toBeginDate(),
                               Trip.PROPERTY_SAMPLES_ONLY, false
        );
        Trip trip1 = dao.create(Trip.PROPERTY_CODE, 1,
                                Trip.PROPERTY_LANDING_DATE, endDate.toBeginDate(),
                                Trip.PROPERTY_SAMPLES_ONLY, true);

        List<Trip> result;

        result = dao.findAllBetweenLandingDate(startDate, endDate, null, false);
        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(trip, result.get(0));
        Assert.assertEquals(trip1, result.get(1));

        result = dao.findAllBetweenLandingDate(startDate, endDate, false, false);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(trip, result.get(0));

        result = dao.findAllBetweenLandingDate(startDate, endDate, true, false);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(trip1, result.get(0));

        result = dao.findAllBetweenLandingDate(startDate, startDate, null, false);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(trip, result.get(0));

        result = dao.findAllBetweenLandingDate(endDate, endDate, null, false);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(trip1, result.get(0));
    }

    @Test
    public void getFirstLandingDate() throws Exception {

        T3Date startDate = T3Date.newDate(1, 2010);
        T3Date endDate = T3Date.newDate(1, 2011);

        T3Date result;

        dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, endDate.toBeginDate());

        result = dao.getFirstLandingDate();
        Assert.assertNotNull(result);
        Assert.assertEquals(endDate, result);

        dao.create(Trip.PROPERTY_CODE, 0, Trip.PROPERTY_LANDING_DATE, startDate.toBeginDate());

        result = dao.getFirstLandingDate();
        Assert.assertNotNull(result);
        Assert.assertEquals(startDate, result);
    }

    @Test
    public void getLastLandingDate() throws Exception {

        T3Date startDate = T3Date.newDate(1, 2010);
        T3Date endDate = T3Date.newDate(1, 2011);

        T3Date result;

        dao.create(Trip.PROPERTY_CODE, 0, Trip.PROPERTY_LANDING_DATE, startDate.toBeginDate());

        result = dao.getLastLandingDate();
        Assert.assertNotNull(result);
        Assert.assertEquals(startDate, result);

        dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, endDate.toBeginDate());

        result = dao.getLastLandingDate();
        Assert.assertNotNull(result);
        Assert.assertEquals(endDate, result);
    }

    @Test
    public void findAllYearsUsedInTrip() throws Exception {

        T3Date startDate = T3Date.newDate(1, 2010);
        T3Date endDate = T3Date.newDate(1, 2011);

        List<Integer> result;

        dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, endDate.toBeginDate());

        result = dao.findAllYearsUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(Arrays.asList(2011), result);

        dao.create(Trip.PROPERTY_CODE, 0, Trip.PROPERTY_LANDING_DATE, startDate.toBeginDate());

        result = dao.findAllYearsUsedInTrip();
        Assert.assertNotNull(result);
        Assert.assertEquals(Arrays.asList(2010, 2011), result);
    }

//    @Test
//    public void findAllByOcean() throws Exception {
//
//        Ocean ocean1 = oceanDAO.create(Ocean.PROPERTY_CODE, 1);
//        Ocean ocean2 = oceanDAO.create(Ocean.PROPERTY_CODE, 2);
//
//
//        Trip trip1 = dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, new Date());
//        Activity activity1 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean1);
//        trip1.addActivity(activity1);
//        Activity activity11 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
//        trip1.addActivity(activity11);
//
//
//        Trip trip2 = dao.create(Trip.PROPERTY_CODE, 2, Trip.PROPERTY_LANDING_DATE, new Date());
//        Activity activity2 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
//        trip2.addActivity(activity2);
//
//        Multimap<Ocean, Trip> result = dao.findAllByOcean();
//
//        Assert.assertNotNull(result);
//        Assert.assertEquals(3, result.size());
//
//        Collection<Trip> trips = result.get(ocean1);
//        Assert.assertNotNull(trips);
//        Assert.assertEquals(1, trips.size());
//        Assert.assertTrue(trips.contains(trip1));
//
//        trips = result.get(ocean2);
//        Assert.assertNotNull(trips);
//        Assert.assertEquals(2, trips.size());
//        Assert.assertTrue(trips.contains(trip1));
//        Assert.assertTrue(trips.contains(trip2));
//    }
//
//    @Test
//    public void findAllByOcean2() throws Exception {
//
//        Ocean ocean1 = oceanDAO.create(Ocean.PROPERTY_CODE, 1);
//        Ocean ocean2 = oceanDAO.create(Ocean.PROPERTY_CODE, 2);
//
//
//        Trip trip1 = dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, new Date());
//        Activity activity1 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean1);
//        trip1.addActivity(activity1);
//        Activity activity11 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
//        trip1.addActivity(activity11);
//
//        Trip trip2 = dao.create(Trip.PROPERTY_CODE, 2, Trip.PROPERTY_LANDING_DATE, new Date());
//        Activity activity2 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
//        trip2.addActivity(activity2);
//
//        Collection<Trip> trips = dao.findAllByOcean(ocean1);
//        Assert.assertNotNull(trips);
//        Assert.assertEquals(1, trips.size());
//        Assert.assertTrue(trips.contains(trip1));
//
//        trips = dao.findAllByOcean(ocean2);
//        Assert.assertNotNull(trips);
//        Assert.assertEquals(2, trips.size());
//        Assert.assertTrue(trips.contains(trip1));
//        Assert.assertTrue(trips.contains(trip2));
//    }

    @Test
    public void findAllIdsByOcean() throws Exception {

        Ocean ocean1 = oceanDAO.create(Ocean.PROPERTY_CODE, 1);
        Ocean ocean2 = oceanDAO.create(Ocean.PROPERTY_CODE, 2);

        Trip trip1 = dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, new Date());
        Activity activity1 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean1);
        trip1.addActivity(activity1);
        Activity activity11 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
        trip1.addActivity(activity11);

        Trip trip2 = dao.create(Trip.PROPERTY_CODE, 2, Trip.PROPERTY_LANDING_DATE, new Date());
        Activity activity2 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
        trip2.addActivity(activity2);

        Trip trip3 = dao.create(Trip.PROPERTY_CODE, 3, Trip.PROPERTY_LANDING_DATE, new Date());

        Multimap<Ocean, String> result = dao.findAllIdsByOcean();

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());

        Collection<String> trips = result.get(ocean1);
        Assert.assertNotNull(trips);
        Assert.assertEquals(1, trips.size());
        Assert.assertTrue(trips.contains(trip1.getTopiaId()));

        trips = result.get(ocean2);
        Assert.assertNotNull(trips);
        Assert.assertEquals(2, trips.size());
        Assert.assertTrue(trips.contains(trip1.getTopiaId()));
        Assert.assertTrue(trips.contains(trip2.getTopiaId()));

        trips = result.get(null);
        Assert.assertNotNull(trips);
        Assert.assertEquals(1, trips.size());
        Assert.assertTrue(trips.contains(trip3.getTopiaId()));

        List<String> allIdsByOcean;

        allIdsByOcean = dao.findAllIdsByOcean(ocean1);
        Assert.assertNotNull(allIdsByOcean);
        Assert.assertEquals(1, allIdsByOcean.size());
        Assert.assertTrue(allIdsByOcean.contains(trip1.getTopiaId()));

        allIdsByOcean = dao.findAllIdsByOcean(ocean2);
        Assert.assertNotNull(allIdsByOcean);
        Assert.assertEquals(2, allIdsByOcean.size());
        Assert.assertTrue(allIdsByOcean.contains(trip1.getTopiaId()));
        Assert.assertTrue(allIdsByOcean.contains(trip2.getTopiaId()));
    }

    @Test
    public void findAllByIds() throws Exception {

        Ocean ocean1 = oceanDAO.create(Ocean.PROPERTY_CODE, 1);
        Ocean ocean2 = oceanDAO.create(Ocean.PROPERTY_CODE, 2);


        Trip trip1 = dao.create(Trip.PROPERTY_CODE, 1, Trip.PROPERTY_LANDING_DATE, new Date());
        Activity activity1 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean1);
        trip1.addActivity(activity1);
        Activity activity11 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
        trip1.addActivity(activity11);

        Trip trip2 = dao.create(Trip.PROPERTY_CODE, 2, Trip.PROPERTY_LANDING_DATE, new Date());
        Activity activity2 = activitDAO.create(Activity.PROPERTY_OCEAN, ocean2);
        trip2.addActivity(activity2);

        List<Trip> result;

        result = dao.findAllByIds(Arrays.asList(trip1.getTopiaId()));

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.contains(trip1));

        result = dao.findAllByIds(Arrays.asList(trip1.getTopiaId(), trip2.getTopiaId()));

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(trip1));
        Assert.assertTrue(result.contains(trip2));
    }

//    @Test
//    public void getDaysOfActivities() throws Exception {
//
//        ActivityDAO activityDAO = T3DAOHelper.getActivityDAO(tx);
//
//        Calendar cal = Calendar.getInstance();
//
//        cal.setTimeInMillis(0);
//        cal.set(Calendar.YEAR, 2000);
////        cal.set(Calendar.HOUR_OF_DAY, 0);
////        cal.set(Calendar.MINUTE, 0);
////        cal.set(Calendar.SECOND, 0);
////        cal.set(Calendar.MILLISECOND, 0);
//
//        Trip trip = dao.create(Trip.PROPERTY_CODE, 0);
//        Trip trip2 = dao.create(Trip.PROPERTY_CODE, 1);
//
//        for (int i = 0; i < 100; i++) {
//            cal.set(Calendar.DAY_OF_YEAR, 100 + i);
//            Date d = cal.getTime();
//            Activity activity;
//            activity = activityDAO.create(Activity.PROPERTY_DATE, d,
//                                                   Activity.PROPERTY_TRIP, trip);
//            trip.addActivity(activity);
//
//            activity = activityDAO.create(Activity.PROPERTY_DATE, d,
//                                                   Activity.PROPERTY_TRIP, trip);
//            trip.addActivity(activity);
//        }
//
//        List<Date> result;
//
//        result = dao.getDaysOfActivities(trip);
//
//        Assert.assertNotNull(result);
//        Assert.assertEquals(100, result.size());
//        cal.set(Calendar.DAY_OF_YEAR, 100);
//        Date d = cal.getTime();
//        Assert.assertEquals(d, result.get(0));
//
//        cal.set(Calendar.DAY_OF_YEAR, 100 + 99);
//        d = cal.getTime();
//        Assert.assertEquals(d, result.get(99));
//
//        result = dao.getDaysOfActivities(trip2);
//
//        Assert.assertNotNull(result);
//        Assert.assertEquals(0, result.size());
//    }
}
