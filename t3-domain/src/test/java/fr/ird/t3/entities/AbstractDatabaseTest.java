/*
 * #%L
 * T3 :: Domain
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.entities;

import fr.ird.t3.T3IOUtil;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.io.File;

public abstract class AbstractDatabaseTest {

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public T3Database db = new T3Database();

    public T3Database getDb() {
        return db;
    }

    public TopiaContext beginTransaction() throws TopiaException {
        return db.beginTransaction();
    }

    /**
     * A new database created for each test.
     *
     * @author tchemit <chemit@codelutin.com>
     * @since 1.0
     */
    public static class T3Database extends TestWatcher {

        private File testBasedir;

        private TopiaContext rootCtxt;

        @Override
        protected void starting(Description description) {

            testBasedir = T3IOUtil.getTestSpecificDirectory(
                    description.getTestClass(),
                    description.getMethodName());

            // create the database
            rootCtxt = new T3TopiaRootContextFactory().newEmbeddedDb(
                    testBasedir
            );
        }

        @Override
        public void finished(Description description) {

            T3EntityHelper.releaseRootContext(rootCtxt);
        }

        public File getTestBasedir() {
            return testBasedir;
        }

        public TopiaContext getRootCtxt() {
            return rootCtxt;
        }

        public TopiaContext beginTransaction() throws TopiaException {
            return rootCtxt.beginTransaction();
        }
    }
}
