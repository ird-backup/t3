.. -
.. * #%L
.. * T3
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

===========================
Scripts postgis disponibles
===========================

:Author: Tony Chemit <chemit@codelutin.com>

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2

Présentation
------------

Cette page permet de télécharger les scripts de remplissage des zones
optionnelles lors d'une installation de T3+.

- `Zones FAO`_ : Zone FAO version 2011
- `Zones EE`_  : Zone EE détaillée 2011
- `Zones CWP 1x1`_  : Zone CWP de carée 1 2011
- `Zones CWP 5x5`_  : Zone CWP de carée 5 2011
- `Zones CWP 10x10`_  : Zone CWP de carée 10 2011

Veuillez placer les archives dans le répertoire **scripts/postigs-data** avant
de lancer l'installeur.

.. _Zones FAO: postgis/zones-fao-2011.sql.zip
.. _Zones EE: postgis/zones-ee-detail-2011.sql.zip
.. _Zones CWP 1x1: postgis/zones-cwp-1x1-2011.sql.zip
.. _Zones CWP 5x5: postgis/zones-cwp-5x5-2011.sql.zip
.. _Zones CWP 10x10: postgis/zones-cwp-10x10-2011.sql.zip
