.. -
.. * #%L
.. * T3
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Affero General Public License as published by
.. * the Free Software Foundation, either version 3 of the License, or
.. * (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU Affero General Public License
.. * along with this program.  If not, see <http://www.gnu.org/licenses/>.
.. * #L%
.. -

============
Modélisation
============

:Date: 24/02/2011

.. contents:: Table des matières
   :depth: 2

.. sectnum::
   :start: 1
   :depth: 2

Présentation
------------

On donne ici des explications sur le modèle de T3.

Le modèle conçu sous argoUML est téléchargeable `ici`_ .

Référentiel
-----------

- `Toutes les entités du référentiel`_.
- `Bateau`_.
- `Catégories poids`_.
- `Espece`_.
- `Ocean`_.
- `Port`_.
- `Pays`_.
- `Zone`_.

Données thématiques
-------------------

- `Toutes les données thématiques (sans calcul)`_
- `Marée`_
- `Activité (sans calcul)`_
- `Activité N0`_
- `Activité N1`_
- `Activité N3`_
- `Echantillon (sans calcul)`_
- `Echantillon N1`_
- `Cuve (sans calcul)`_
- `Cuve N1`_
- `Raising Factor 2`_

.. _ici: model/t3-persistence.zargo

.. _Toutes les entités du référentiel: model/ReferencesAll.png
.. _Espece: model/ReferencesSpecies.png
.. _Bateau: model/ReferencesVessel.png
.. _Catégories poids: model/ReferencesWeightCategories.png
.. _Pays: model/ReferencesCountry.png
.. _Ocean: model/ReferencesOcean.png
.. _Port: model/ReferencesHarbour.png
.. _Zone: model/ReferencesZones.png

.. _Toutes les données thématiques (sans calcul): model/DataAll.png
.. _Marée: model/DataTrip.png
.. _Activité (sans calcul): model/DataActivity.png
.. _Activité N0: model/DataActivityN0.png
.. _Activité N1: model/DataActivityN1.png
.. _Activité N3: model/DataActivityN3.png
.. _Echantillon (sans calcul): model/DataSample.png
.. _Echantillon N1: model/DataSampleN1.png
.. _Cuve (sans calcul): model/DataWell.png
.. _Cuve N1: model/DataWellN1.png
.. _Raising Factor 2: model/DataRaisingFactor2.png


