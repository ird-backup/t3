/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.output;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.io.output.fake.v0.T3OutputOperationFakeImpl;
import fr.ird.t3.io.output.fake.v0.T3OutputProviderFakeImpl;
import fr.ird.t3.services.T3OutputService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To test render of action {@link ExportAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ExportActionResumeTest extends AbstractActionResumeTest<ExportConfiguration, ExportAction> {

    public ExportActionResumeTest() {
        super(ExportConfiguration.class, ExportAction.class);
    }

    @Override
    protected void prepareConfiguration(ExportConfiguration configuration) {

        T3OutputService t3OutputService = serviceContext.newService(T3OutputService.class);
        T3OutputProvider<?, ?> outputProvider = t3OutputService.getProvider(T3OutputProviderFakeImpl.ID);

        configuration.setOutputProvider(outputProvider);
        List<String> operationIds = new ArrayList<String>();
        for (T3OutputOperation operation : outputProvider.getOperations()) {
            operationIds.add(operation.getId());
        }
        configuration.setOperationIds(operationIds);
        configuration.setBeginDate(T3Date.newDate(10, 2011));
        configuration.setEndDate(T3Date.newDate(10, 2012));

        configuration.setOceanId(String.valueOf(fixtures.oceanAtlantic().getCode()));
        configuration.setFleetId(String.valueOf(fixtures.frenchCountry().getCode()));

        configuration.setUrl("jdbc://localhost/Fake");
        configuration.setLogin("login");
        configuration.setPassword("***");
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> oceans = new TreeMap<String, String>();

        putInMap(oceans, fixtures.oceanAtlantic());
        parameters.put("oceans", oceans);

        Map<String, String> fleets = new TreeMap<String, String>();
        putInMap(fleets, fixtures.frenchCountry());
        parameters.put("fleets", fleets);
        return parameters;
    }

    @Override
    protected void prepareAction(ExportAction action, Locale locale) {
        super.prepareAction(action, locale);
        action.operationSummaries = new LinkedHashMap<T3OutputOperation, String>();
        action.operationSummaries.put(T3OutputOperationFakeImpl.TRIP_AND_LANDING, "summary for export of trip and landing...");
        action.operationSummaries.put(T3OutputOperationFakeImpl.ACTIVITY_AND_CATCHES, "summary for export of activities and catches...");
        action.operationSummaries.put(T3OutputOperationFakeImpl.SAMPLE, "summary for export of samples...");
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }

}
