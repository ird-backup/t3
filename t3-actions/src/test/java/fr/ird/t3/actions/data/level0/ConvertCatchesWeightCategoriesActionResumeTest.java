/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesImpl;
import fr.ird.t3.entities.type.T3Date;
import org.junit.Test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * To test the action {@link ComputeRF1Action}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConvertCatchesWeightCategoriesActionResumeTest extends AbstractActionResumeTest<ConvertCatchesWeightCategoriesConfiguration, ConvertCatchesWeightCategoriesAction> {

    public ConvertCatchesWeightCategoriesActionResumeTest() {
        super(ConvertCatchesWeightCategoriesConfiguration.class,
              ConvertCatchesWeightCategoriesAction.class);
    }

    @Override
    protected void prepareConfiguration(ConvertCatchesWeightCategoriesConfiguration conf) {
        super.prepareConfiguration(conf);
        configuration.setBeginDate(T3Date.newDate(1, 2011));
        configuration.setEndDate(T3Date.newDate(1, 2012));
    }

    @Override
    protected void prepareAction(ConvertCatchesWeightCategoriesAction action, Locale locale) {
        super.prepareAction(action, locale);

        action.nbTrips = 10;

        Map<Species, ConvertCatchesWeightCategoriesAction.CatchWeightResult> map = new HashMap<Species, ConvertCatchesWeightCategoriesAction.CatchWeightResult>();
        Species species = new SpeciesImpl();
        species.setLibelle("Albaroce");
        species.setCode3L("ABT");
        ConvertCatchesWeightCategoriesAction.CatchWeightResult result = new ConvertCatchesWeightCategoriesAction.CatchWeightResult();
        result.logBookTotalWeight = 10;
        result.treatmentTotalWeight = 10.2f;
        map.put(species, result);
        action.specieWeightResult = map;
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        Map<String, String> vesselSimpleTypes = new TreeMap<String, String>();
        putInMap(vesselSimpleTypes,
                 fixtures.vesselSimpleTypeCanneur(),
                 fixtures.vesselSimpleTypeSenneur());
        parameters.put("vesselSimpleTypes", vesselSimpleTypes);

        Map<String, String> fleets = new TreeMap<String, String>();
        putInMap(fleets, fixtures.frenchCountry());
        parameters.put("fleets", fleets);

        parameters.put("speciesDecorator", getDecorator(Species.class));
        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
