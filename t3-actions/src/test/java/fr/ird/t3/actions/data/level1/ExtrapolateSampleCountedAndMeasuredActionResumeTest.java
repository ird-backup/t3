/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * To test the resume generation of action
 * {@link ExtrapolateSampleCountedAndMeasuredAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ExtrapolateSampleCountedAndMeasuredActionResumeTest extends AbstractLevel1ActionResumeTest<ExtrapolateSampleCountedAndMeasuredAction> {

    public ExtrapolateSampleCountedAndMeasuredActionResumeTest() {
        super(ExtrapolateSampleCountedAndMeasuredAction.class);
    }

    @Override
    protected void prepareAction(ExtrapolateSampleCountedAndMeasuredAction action, Locale locale) {
        super.prepareAction(action, locale);
        List<ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel> speciesModel = new ArrayList<ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel>();
        Species s = new SpeciesImpl();
        s.setTopiaId("topiaid:1");
        s.setLibelle("species 1");
        s.setCode3L("code3L 1");
        ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel sM = new ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel(s);
        sM.addTotalCount(10);
        sM.addMeasuredCount(5);
        speciesModel.add(sM);
        s = new SpeciesImpl();
        s.setTopiaId("topiaid:2");
        s.setLibelle("species 2");
        s.setCode3L("code3L 2");
        sM = new ExtrapolateSampleCountedAndMeasuredAction.SpeciesCountAndMeasuredModel(s);
        sM.addTotalCount(20);
        sM.addMeasuredCount(15);
        speciesModel.add(sM);
        action.putResult(ExtrapolateSampleCountedAndMeasuredAction.RESULT_SPECIES_MODEL, speciesModel);
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        parameters.put("speciesDecorator", getDecorator(Species.class));

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
