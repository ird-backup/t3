/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.AbstractActionResumeTest;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.T3InputService;
import org.junit.Test;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * To test render of action {@link AnalyzeInputSourceAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ImportInputSourceActionResumeTest extends AbstractActionResumeTest<ImportInputSourceConfiguration, ImportInputSourceAction> {

    public ImportInputSourceActionResumeTest() {
        super(ImportInputSourceConfiguration.class, ImportInputSourceAction.class);
    }

    @Override
    protected void prepareConfiguration(ImportInputSourceConfiguration conf) {
        configuration.setInputFile(new File("inputSource"));
        configuration.setUseWells(true);
        configuration.setInputProvider(serviceContext.newService(T3InputService.class).getProviders()[0]);
        configuration.setCanCreateVessel(true);
        configuration.setCreateVirtualVessel(true);

        Set<Trip> safeTripList = new HashSet<Trip>();

        safeTripList.add(fixtures.trip1());
        safeTripList.add(fixtures.trip1());

        Set<Trip> unsafeTripList = new HashSet<Trip>();
        unsafeTripList.add(fixtures.trip3());
        unsafeTripList.add(fixtures.trip3());

        configuration.setTripsToImport(safeTripList);
        configuration.setTripsToDelete(unsafeTripList);
    }

    @Override
    protected Map<String, Object> createTemplateParameters() {

        Map<String, Object> parameters = super.createTemplateParameters();

        parameters.put("tripDecorator",
                       getDecorator(Trip.class, DecoratorService.WITH_ID));

        return parameters;
    }

    @Test
    public void testRender() throws Exception {
        doTestRender();
    }
}
