<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header_en.ftl"/>

Begin date:    ${configuration.beginDate}
End date:      ${configuration.endDate}
Seuil rf1 min: ${configuration.minimumRate}
Seuil rf1 max: ${configuration.maximumRate}

<#list vesselSimpleTypes?values as vesselSimpleType>
Selected simple vessel type: ${vesselSimpleType}
</#list>

<#list fleets?values as fleet>
Selected fleet: ${fleet}
</#list>

Indicators
----------

- Number of vessels:                                   ${action.nbVessels}
- Number of trips:                                     ${action.nbTrips}
- Number of rejected trips (without logbook):          ${action.nbRejectedTrips}
- Number of accepted trips (with rf1 computation):     ${action.nbAcceptedTrips}
- Number of complete trips:                            ${action.nbCompleteTrips}
- Number of rejected complete trips (without logbook): ${action.nbCompleteRejectedTrips}
- Number of accepted complete trips (rf1 computation): ${action.nbCompleteAcceptedTrips}
- Number of complete trip with strange rf1:            ${action.nbCompleteAcceptedTripsWithBadRF1}
- Total catch weight:                                  ${action.totalCatchWeightRF1}
- total landing weight:                                ${action.totalLandingWeight}

<#include "/ftl/showMessages_en.ftl"/>



