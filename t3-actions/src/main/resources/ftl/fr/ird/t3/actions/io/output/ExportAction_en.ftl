<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#assign locale = action.locale/>
<#assign outputProvider = configuration.outputProvider/>
<#include "/ftl/header_en.ftl"/>

Output Pilot: ${outputProvider.libelle}

Selected operations:
<#list configuration.operationIds as operationId>
<#assign operation = outputProvider.getOperation(operationId)/>
- ${operation.getLibelle(locale)}
</#list>

Begin date:     ${configuration.beginDate}
End date:       ${configuration.endDate}
<#list oceans?values as ocean>
Selected ocean: ${ocean}
</#list>
<#list fleets?values as fleet>
Selected fleet: ${fleet}
</#list>

Summaries of operations
-----------------------

<#list configuration.operationIds as operationId>
<#assign operation = outputProvider.getOperation(operationId)/>
Operation ${operation.getLibelle(locale)} :
${action.getOperationSummary(operation)}

</#list>

<#include "/ftl/showMessages_en.ftl"/>


