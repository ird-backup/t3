<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#include "/ftl/header.ftl"/>

Pilote d'acquisition de données : ${configuration.inputProvider.libelle}
Source de données               : ${configuration.inputFile.name}
Utilisation des plans de cuves  : ${configuration.useWells?string}
Base échantillon seulement      : ${configuration.samplesOnly?string}
Autoriser à créer des bateaux   : ${configuration.canCreateVessel?string}
<#if configuration.canCreateVessel>
Créer des bateaux virtuels      : ${configuration.createVirtualVessel?string}
</#if>

Indicateurs
-----------

<#if safeTrips?size &gt; 0>
- Nombre de marées importables                           : ${safeTrips?size}
</#if>

<#if unsafeTrips?size &gt; 0>
- Nombre de marées non importables                       : ${unsafeTrips?size}
</#if>
<#if tripsToReplace?size &gt; 0>
- Nombre de marées importables à remplacer               : ${tripsToReplace?size}
</#if>

<#if unsafeTrips?size &gt; 0>

L'import n'est pas possible car il existe des marées non importables (voir les messages d'erreur).

<#list unsafeTrips as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

<#if safeTrips?size &gt; 0>
Liste des marées importables :

<#list safeTrips as trip>
- ${tripDecorator.toString(trip)}
</#list>
</#if>

<#if tripsToReplace?size &gt; 0>
Il existe des marées importables mais qui existe déjà en base dont voici la liste :

<#list tripsToReplace?keys as trip>
  <#assign value = action.getTripToReplace(trip)/>
- ${tripDecorator2.toString(trip)} remplacé par ${tripDecorator.toString(value)}
</#list>
</#if>

<#include "/ftl/showMessages.ftl"/>


