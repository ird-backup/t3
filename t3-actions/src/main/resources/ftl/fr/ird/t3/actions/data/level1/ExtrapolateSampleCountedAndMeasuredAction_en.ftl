<#--
 #%L
 T3 :: Actions
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #L%
-->
<#function tableFormat arg0, arg1, arg2, arg3>
<#return statics['java.lang.String'].format("| %1$-50s | %2$-15s | %3$-15s | %4$-10s |", arg0, arg1, arg2, arg3)>
</#function>
<#include "/ftl/header_en.ftl"/>

Begin date: ${configuration.beginDate}
End date:   ${configuration.endDate}

<#list oceans?values as ocean>
Selected ocean: ${ocean}
</#list>

<#list fleets?values as fleet>
Selected fleet: ${fleet}
</#list>

<#list sampleQualities?values as sampleQualitity>
Selected sample quality: ${sampleQualitity}
</#list>

<#list sampleTypes?values as sampleType>
Selected sample type: ${sampleType}
</#list>

Indicators
----------

Number of treated trips:     ${action.nbTripsTreated}
Number of treated samples:   ${action.nbSamplesTreated}
Number of untreated samples: ${action.nbSamplesNotTreated}

${tableFormat("Species","Total count", "Measured count", "RF0" )}
<#list action.resultSpeciesModel as specieModel>
  <#assign speciesLabel = speciesDecorator.toString(specieModel.species)/>
${tableFormat(speciesLabel,specieModel.totalCount?string, specieModel.measuredCount?string, specieModel.rf0?string )}
</#list>
<#assign totalModel = action.totalSpeciesModel/>
${tableFormat("Total",totalModel.totalCount?string, totalModel.measuredCount?string, totalModel.rf0?string )}

<#include "/ftl/showMessages_en.ftl"/>
