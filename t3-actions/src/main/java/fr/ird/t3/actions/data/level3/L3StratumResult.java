/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.actions.stratum.StratumResult;
import fr.ird.t3.models.SpeciesCountAggregateModel;

import java.io.IOException;

/**
 * Result of a level 3 Stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class L3StratumResult extends StratumResult<Level3Configuration> {

    /**
     * Global total fishes count per species and length classes for all stratums
     * before N3 and after N3.
     *
     * @since 1.3.1
     */
    protected SpeciesCountAggregateModel totalFishesCount;

    public L3StratumResult(
            StratumConfiguration<Level3Configuration> configuration,
            String libelle) {
        super(configuration, libelle);
        totalFishesCount = new SpeciesCountAggregateModel();
    }

    public SpeciesCountAggregateModel getTotalFishesCount() {
        return totalFishesCount;
    }

    @Override
    public void close() throws IOException {
        totalFishesCount.close();
    }


}
