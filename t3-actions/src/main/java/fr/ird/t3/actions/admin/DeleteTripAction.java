/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.admin;

import com.google.common.base.Preconditions;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.decorator.Decorator;

import java.util.Collection;

import static org.nuiton.i18n.I18n.l_;

/**
 * Action to delete trips and or trip computed data.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class DeleteTripAction extends T3Action<DeleteTripConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DeleteTripAction.class);

    @InjectDAO(entityType = Trip.class)
    protected TripDAO tripDAO;

    protected Decorator<Trip> tripDecorator;

    public int nbTrips;

    public int nbActivities;

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        tripDecorator = getDecoratorService().getDecorator(
                locale,
                Trip.class,
                DecoratorService.WITH_ID);
    }

    @Override
    protected void deletePreviousData() {
        // nothing to do here
    }

    @Override
    protected boolean executeAction() throws Exception {

        Collection<String> ids = getConfiguration().getTripIds();

        boolean result = false;

        if (CollectionUtils.isNotEmpty(ids)) {

            setNbSteps(ids.size());

            if (getConfiguration().isDeleteTrip()) {

                deleteTrips(ids);
            } else {

                deleteTripsComputedDatas(ids);
            }
            result = true;
        }
        return result;
    }

    public int getNbTrips() {
        return nbTrips;
    }

    public int getNbActivities() {
        return nbActivities;
    }

    protected void deleteTrips(Collection<String> tripIds) throws Exception {
        for (String tripId : tripIds) {

            Trip trip = getTripId(tripId);
            String message = l_(locale, "t3.admin.deleteTrip",
                                tripDecorator.toString(trip));
            addInfoMessage(message);
            if (log.isInfoEnabled()) {
                log.info(message);
            }

            // delete all computed data attaches to activities
//            deleteComputedDataForActivities(trip);

            // then delete trips
            tripDAO.delete(trip);
        }
        getTransaction().commitTransaction();
    }

    protected void deleteTripsComputedDatas(Collection<String> tripIds) throws Exception {

        for (String tripId : tripIds) {

            Trip trip = getTripId(tripId);

            String message = l_(locale, "t3.admin.deleteTripDatas",
                                tripDecorator.toString(trip));
            addInfoMessage(message);
            if (log.isInfoEnabled()) {
                log.info(message);
            }

            // then delete all computed data attached to trips
            trip.deleteComputedData();
        }
        getTransaction().commitTransaction();
    }

    protected Trip getTripId(String tripId) throws TopiaException {
        incrementsProgression();
        Trip trip = tripDAO.findByTopiaId(tripId);
        Preconditions.checkNotNull(trip, "Could not find with id " + tripId);
        nbActivities += trip.sizeActivity();
        nbTrips++;
        return trip;
    }
}
