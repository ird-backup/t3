/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.SampleWellDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.WellSetAllSpecies;
import fr.ird.t3.entities.data.WellSetAllSpeciesDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import org.nuiton.topia.TopiaException;

import java.util.Collection;

import static org.nuiton.i18n.I18n.l_;

/**
 * Compute weight for categories in sets.
 * <p/>
 * This operation make 3 things in facts :
 * <ul>
 * <li>Get weights for each weight categories in WellPlan without the species parameter : no now done in level 0</li>
 * <li>Compute sample set weight ratio from the complete set</li>
 * <li>Transmit weight from sample to set</li>
 * </lu>
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeWeightOfCategoriesForSetAction extends AbstractLevel1Action {

    @InjectDAO(entityType = WellSetAllSpecies.class)
    protected WellSetAllSpeciesDAO wellSetAllSpeciesDAO;

    @InjectDAO(entityType = SampleWell.class)
    protected SampleWellDAO sampleWellDAO;

    protected int nbSampleWithoutWell;

    public ComputeWeightOfCategoriesForSetAction() {
        super(Level1Step.COMPUTE_WEIGHT_OF_CATEGORIES_FOR_SET);
    }

    public int getNbSampleWithoutWell() {
        return nbSampleWithoutWell;
    }

    public int getNbSampleWithWell() {
        return getNbSamplesTreated() - getNbSampleWithoutWell();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 1 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        setNbSteps(2 * samplesByTrip.size());

        for (Trip trip : samplesByTrip.keySet()) {
            Collection<Sample> samples = samplesByTrip.get(trip);

            logTreatedAndNotSamplesforATrip(trip, samples);

            for (Sample sample : samples) {
                doExecuteSample(trip, sample);

                // mark sample as treated for this step of level 1 treatment
                markAsTreated(sample);
            }

            // mar trip as treated for this level 1 step treatment
            markAsTreated(trip);
        }
        return true;
    }

    protected void doExecuteSample(Trip trip, Sample sample) throws TopiaException {

        String tripStr = decorate(trip);

        addInfoMessage(l_(locale, "t3.level1.computeWeightOfCategoriesForSet.treat.sample",
                          tripStr,
                          sample.getSampleNumber(),
                          sample.sizeSampleWell()
        ));

        transmitWeight(sample);
        incrementsProgression();

        computePropWeightedWeight(sample);
        incrementsProgression();
    }

    protected void transmitWeight(Sample sample) {

        float minus10Weight = sample.getMinus10Weight();
        float plus10Weight = sample.getPlus10Weight();
        float globalWeight = sample.getGlobalWeight();

        float totalWeight = minus10Weight + plus10Weight;

        String sampleStr = decorate(sample);

        if (totalWeight == 0) {

            // can not  compute propMinus10Weight and propPlus10Weight

            String message =
                    l_(locale, "t3.level1.computeWeightOfCategoriesForSet.sample.noComputePropWeight"
                    );
            addInfoMessage(message);

        } else {

            // can compute propMinus10Weight and propPlus10Weight

            if (globalWeight > 0) {

                // this case should never happens, data are not so good

                addWarningMessage(
                        l_(locale, "t3.level1.computeWeightOfCategoriesForSet.warning.globalWeightAndMinusAndPlus10Weight", sampleStr));
            }

            float propMinus10Weight = minus10Weight / totalWeight;
            float propPlus10Weight = plus10Weight / totalWeight;

            sample.setPropMinus10Weight(propMinus10Weight);
            sample.setPropPlus10Weight(propPlus10Weight);

            String message =
                    l_(locale, "t3.level1.computeWeightOfCategoriesForSet.sample.computePropWeight",
                       sample.getPropMinus10Weight(),
                       sample.getPropPlus10Weight()
                    );
            addInfoMessage(message);

            if (!sample.isSampleWellEmpty()) {

                // transmit propMinus10Weight and propPlus10Weight to sample set

                for (SampleWell sampleWell : sample.getSampleWell()) {

                    float weightedWeight = sampleWell.getWeightedWeight();
                    float weightedWeightMinus10 = weightedWeight * propMinus10Weight;
                    float weightedWeightPlus10 = weightedWeight * propPlus10Weight;
                    sampleWell.setWeightedWeightMinus10(weightedWeightMinus10);
                    sampleWell.setWeightedWeightPlus10(weightedWeightPlus10);
                }
            }
        }
    }

    protected void computePropWeightedWeight(Sample sample) throws TopiaException {

        if (!sample.isSampleWellEmpty()) {

            Collection<SampleWell> sampleWells = sample.getSampleWell();

            // compute the total weight
            float totalWeight = 0f;
            for (SampleWell sampleWell : sampleWells) {
                totalWeight += sampleWell.getWeightedWeight();
            }

            for (SampleWell sampleWell : sampleWells) {

                float prop = sampleWell.getWeightedWeight() / totalWeight;
                addInfoMessage(
                        l_(locale, "t3.level1.computeWeightOfCategoriesForSet.resume.for.sampleSet",
                           sampleWell.getWeightedWeight(), prop));
                sampleWell.setPropWeightedWeight(prop);

                //TODO - Check if this is the good value to store
                //sampleWell.setTotalSampleWeight(totalWeight);
            }
        }
    }

}
