/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.RF1SpeciesForFleet;
import fr.ird.t3.entities.reference.RF1SpeciesForFleetDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Action to compute Raising Factor 1 on each trip.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeRF1Action extends AbstractLevel0Action<ComputeRF1Configuration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeRF1Action.class);

    @InjectDAO(entityType = RF1SpeciesForFleet.class)
    protected RF1SpeciesForFleetDAO rF1SpecieForFleetDAO;

    /** usable trips group by vessel */
    protected Multimap<Vessel, Trip> tripsByVessel;

    /** usable species group by country */
    protected Multimap<Country, Species> speciesByCountry;

    /** total catch weight for trips with rf1 != null. */
    protected float totalCatchWeightRF1;

    /** total landing weight for trips with rf1 != null. */
    protected float totalLandingWeight;

    /** Number of trips rejected (with no logbook). */
    protected int nbRejectedTrips;

    /** Number of complete trips. */
    protected int nbCompleteTrips;

    /** Number of complete trips rejected (with no logbook). */
    protected int nbCompleteRejectedTrips;

    /** Number of accepted trips (says with a rf1 computation). */
    protected int nbAcceptedTrips;

    /** Number of accepted complete trips (says with a rf1 computation). */
    protected int nbCompleteAcceptedTrips;

    /** Number of complete accepted trips (says with a rf1 computation, but not in defined bound). */
    protected int nbCompleteAcceptedTripsWithBadRF1;

    public ComputeRF1Action() {
        super(Level0Step.COMPUTE_RF1);
    }

    public int getNbCompleteAcceptedTrips() {
        return nbCompleteAcceptedTrips;
    }

    public int getNbCompleteAcceptedTripsWithBadRF1() {
        return nbCompleteAcceptedTripsWithBadRF1;
    }

    public float getTotalCatchWeightRF1() {
        return totalCatchWeightRF1;
    }

    public float getTotalLandingWeight() {
        return totalLandingWeight;
    }

    public int getNbRejectedTrips() {
        return nbRejectedTrips;
    }

    public int getNbCompleteRejectedTrips() {
        return nbCompleteRejectedTrips;
    }

    public int getNbAcceptedTrips() {
        return nbAcceptedTrips;
    }

    public int getNbCompleteTrips() {
        return nbCompleteTrips;
    }

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();

        // get all species usable for rf1 for all selected fleet countries
        Multimap<Country, Species> result =
                rF1SpecieForFleetDAO.getSpeciesForCountry(fleets);
        setSpeciesByCountry(result);

        List<Trip> tripList = getUsableTrips(null, false);
        setTrips(tripList);

        // get all trips group by the vessel
        setTripsByVessel(TripDAO.groupByVessel(tripList));
    }

    @Override
    protected void deletePreviousData() {

        for (Trip trip : trips) {

            // remove level0 data
            trip.deleteComputedDataLevel0();

            // remove level 1 data
            trip.deleteComputedDataLevel1();

            // remove level 2 data
            trip.deleteComputedDataLevel2();

            // remove level 3 data
            trip.deleteComputedDataLevel3();
        }
    }

    @Override
    protected boolean executeAction() throws Exception {

        Set<Vessel> allVessel = tripsByVessel.keySet();

        boolean result = false;

        if (CollectionUtils.isNotEmpty(allVessel)) {

            setNbSteps(allVessel.size());

            for (Vessel vessel : allVessel) {

                if (log.isDebugEnabled()) {
                    log.debug("Treat vessel " + vessel.getLibelle());
                }

                List<Trip> tripsForVessel =
                        Lists.newArrayList(tripsByVessel.get(vessel));
                TripDAO.sortTrips(tripsForVessel);
                result |= doExecuteForVessel(vessel, tripsForVessel);
            }

            addInfoMessage(l_(locale, "t3.level0.computeRF1.totalCatchWeightRF1",
                              totalCatchWeightRF1));
            addInfoMessage(l_(locale, "t3.level0.computeRF1.totalLandingWeight",
                              totalLandingWeight));
        }

        return result;
    }

    protected boolean doExecuteForVessel(Vessel vessel,
                                         List<Trip> tripList) throws Exception {

        incrementsProgression();

        String vesselStr = decorate(vessel);
        addInfoMessage(l_(locale, "t3.level0.computeRF1.treat.vessel",
                          vesselStr));

        if (CollectionUtils.isEmpty(tripList)) {

            // no trip for vessel
            // nothing to commit
            return false;
        }

        addInfoMessage(l_(locale, "t3.level0.computeRF1.trips.to.use.for.vessel",
                          tripList.size(), vesselStr));

        // get country of the vessel (always use the fleet country)
        Country country = vessel.getFleetCountry();

        // get species useable for this country
        Collection<Species> species = speciesByCountry.get(country);

        if (CollectionUtils.isEmpty(species)) {

            // means no rf1SpecieForFleet found, no rf1 to compute (use a rf1.0)
            String message =
                    l_(locale, "t3.level0.computeRF1.warning.no.species.usable.for.country",
                       country.getLibelle());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            addWarningMessage(message);
        } else {

            // there is some species to use for rf1 computation
            StringBuilder sb = new StringBuilder();
            for (Species specy : species) {
                sb.append(", ").append(decorate(specy));
            }

            String message = l_(locale, "t3.level0.computeRF1.species.to.use",
                                species.size(), sb.substring(2)
            );
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);
        }

        float minimumRate = getConfiguration().getMinimumRate();
        float maximumRate = getConfiguration().getMaximumRate();

        // split trips by complete trips
        List<CompleteTrip> completeTrips = tripDAO.toCompleteTrip(tripList);

        addInfoMessage(
                l_(locale, "t3.level0.computeRF1.complete.trips.to.use.for.vessel",
                   completeTrips.size(), vesselStr));

        nbCompleteTrips += completeTrips.size();

        for (CompleteTrip completeTrip : completeTrips) {

            // consume the completeTrip
            treatCompleteTrip(completeTrip,
                              species,
                              minimumRate,
                              maximumRate
            );

            // complete trip was consumed
            completeTrip.removeTrips(tripList);
//            tripList.removeAll(completeTrip);
        }

        if (CollectionUtils.isNotEmpty(tripList)) {

            // still some trips not complete

            for (Trip trip : tripList) {

                String message =
                        l_(locale, "t3.level0.computeRF1.warning.trip.is.not.complete",
                           decorate(trip, DecoratorService.WITH_ID));
                if (log.isWarnEnabled()) {
                    log.warn(message);
                }
                addWarningMessage(message);
            }

            // set completionStatus to 0
            applyCompletionStatus(tripList, 0);
        }
        return true;
    }

    private void treatCompleteTrip(CompleteTrip completeTrip,
                                   Collection<Species> species,
                                   float minimumRate,
                                   float maximumRate) {

        int completionStatus;

        if (completeTrip.getNbTrips() == 1) {

            // simple trip
            completionStatus = 1;
        } else {

            // partial trip
            completionStatus = 2;
        }
        applyCompletionStatus(completeTrip, completionStatus);

        boolean allwithLogBook = TripDAO.isTripsAllWithLogBook(completeTrip);

        if (!allwithLogBook) {

            // there is some trips with no log book
            // reject all the complete trip
            nbRejectedTrips += completeTrip.getNbTrips();

            nbCompleteRejectedTrips += 1;

            // update totalWeights
            updateTotalWeights(completeTrip, species);

            // apply on them a null rf1
            applyRF1(null, completeTrip, null);

        } else {

            // complete trip safe, can compute rf1 of it

            // one more complete trip accepted
            nbCompleteAcceptedTrips++;

            // size of the comple trips accepted
            nbAcceptedTrips += completeTrip.getNbTrips();

            // compute rf1 for the complete trip
            float rf1;

            if (species == null) {

                // means there is so species for this complete trip so
                // rf1 = 1.0
                rf1 = 1.0f;
            } else {
                rf1 = computeRF1ForCompleteTrip(completeTrip, species);

                if (rf1 < minimumRate) {

                    String warnMessage =
                            l_(locale, "t3.level0.computeRF1.warning.too.low.rf1",
                               decorate(completeTrip.getDepartureTrip()),
                               decorate(completeTrip.getLandingTrip()),
                               rf1,
                               minimumRate
                            );

                    addWarningMessage(warnMessage);

                    nbCompleteAcceptedTripsWithBadRF1++;
                }

                if (rf1 > maximumRate) {

                    String warnMessage =
                            l_(locale, "t3.level0.computeRF1.warning.too.high.rf1",
                               decorate(completeTrip.getDepartureTrip()),
                               decorate(completeTrip.getLandingTrip()),
                               rf1,
                               maximumRate
                            );
                    addWarningMessage(warnMessage);
                    nbCompleteAcceptedTripsWithBadRF1++;
                }
            }

            applyRF1(rf1, completeTrip, species);

            // update totalWeights
            updateTotalWeights(completeTrip, species);
        }
    }

    private void applyCompletionStatus(Iterable<Trip> trips,
                                       int completionStatus) {

        for (Trip trip : trips) {

            trip.setCompletionStatus(completionStatus);
        }
    }

    private void applyRF1(Float rf1,
                          CompleteTrip trips,
                          Collection<Species> species) {

        for (Trip trip : trips) {

            String tripStr = decorate(trip);

            Float rf1ToUse = rf1;

            if (trip.getLogBookAvailability() != 1) {

                // means can not apply any rf1 : rf1 must stay at null...
                rf1ToUse = null;
                addWarningMessage(
                        l_(locale, "t3.level0.computeRF1.warning.no.logbook",
                           tripStr)
                );
            }

            trip.applyRf1(rf1ToUse, species);
            markTripAsTreated(trip);
            addInfoMessage(
                    l_(locale, "t3.level0.computeRF1.resume.rf1.for.trip",
                       tripStr, rf1ToUse)
            );
        }
    }

    protected void updateTotalWeights(CompleteTrip completeTrip,
                                      Collection<Species> species) {


        float tripTotalCatchWeightRf1 =
                completeTrip.getElementaryCatchTotalWeightRf1(species);
        totalCatchWeightRF1 += tripTotalCatchWeightRf1;

        float tripTotalLandingWeight =
                completeTrip.getElementaryLandingTotalWeight(species);
        totalLandingWeight += tripTotalLandingWeight;
        if (log.isInfoEnabled()) {

            log.info("After trip " + decorate(completeTrip.getLandingTrip()) +
                     " tripTotalCatchWeightRf1 = " + tripTotalCatchWeightRf1 +
                     " tripTotalLandingWeight = " + tripTotalLandingWeight);
        }
        if (log.isDebugEnabled()) {
            log.debug("After trip " + decorate(completeTrip.getLandingTrip()) +
                      " totalCatchWeightRF1 = " + totalCatchWeightRF1 +
                      " totalLandingWeight = " + totalLandingWeight);
        }
//        for (Trip trip : completeTrip) {
//
//            float tripTotalCatchWeightRf1 =
//                    trip.getElementaryCatchTotalWeightRf1(species);
//            totalCatchWeightRF1 += tripTotalCatchWeightRf1;
//
//            float tripTotalLandingWeight =
//                    trip.getElementaryLandingTotalWeight(species);
//            totalLandingWeight += tripTotalLandingWeight;
//
//            if (log.isInfoEnabled()) {
//
//                log.info("After trip " + decorate(trip) +
//                         " tripTotalCatchWeightRf1 = " + tripTotalCatchWeightRf1 +
//                         " tripTotalLandingWeight = " + tripTotalLandingWeight);
//            }
//            if (log.isDebugEnabled()) {
//                log.debug("After trip " + decorate(trip) +
//                          " totalCatchWeightRF1 = " + totalCatchWeightRF1 +
//                          " totalLandingWeight = " + totalLandingWeight);
//            }
//        }
    }

    protected float computeRF1ForCompleteTrip(Iterable<Trip> completeTrip,
                                              Collection<Species> speciesList) {

        // do the computation of rf1 for all trips of the complete trip
        float sumLanding = 0;
        float sumCatch = 0;

        for (Trip trip : completeTrip) {

            String tripStr = decorate(trip, DecoratorService.WITH_ID);
            if (log.isDebugEnabled()) {
                log.debug("Start count for trip " +
                          tripStr);
            }

            float elementaryLandingTotalWeight =
                    trip.getElementaryLandingTotalWeight(speciesList);
            float elementaryCatchTotalWeight =
                    trip.getElementaryCatchTotalWeight(speciesList);

            if (elementaryCatchTotalWeight == 0 &&
                elementaryLandingTotalWeight > 0) {

                // Add a warning, seems not possible to have no catches and landing :
                // the logBookAvaibility flag should be setted to 0.

                addWarningMessage(
                        l_(locale, "t3.level0.computeRF1.warning.no.catches.but.some.landings",
                           tripStr,
                           elementaryCatchTotalWeight)
                );
            }

            if (elementaryCatchTotalWeight > 0 && elementaryLandingTotalWeight == 0) {

                // Special case : no landing

                // do not take account of the catch weight
                elementaryCatchTotalWeight = 0;

                addWarningMessage(
                        l_(locale, "t3.level0.computeRF1.warning.no.landings.but.some.catches",
                           tripStr,
                           elementaryLandingTotalWeight)
                );
            }

            addInfoMessage(l_(locale, "t3.level0.computeRF1.resume.for.trip",
                              tripStr, elementaryCatchTotalWeight,
                              elementaryLandingTotalWeight)
            );

            if (log.isDebugEnabled()) {
                log.debug("elementaryLandingTotalWeight = " +
                          elementaryLandingTotalWeight);
                log.debug("elementaryCatchTotalWeight = " +
                          elementaryCatchTotalWeight);
            }

            sumLanding += elementaryLandingTotalWeight;
            sumCatch += elementaryCatchTotalWeight;
        }

        float rf1 = 1;
        if (sumCatch > 0) {
            rf1 = sumLanding / sumCatch;
        }
        if (log.isDebugEnabled()) {
            log.debug("Computed rf1 " + rf1);
        }

        addInfoMessage(
                l_(locale, "t3.level0.computeRF1.resume.for.complete.trip",
                   sumCatch, sumLanding, rf1)
        );

        return rf1;
    }

    public void setSpeciesByCountry(Multimap<Country, Species> speciesByCountry) {
        this.speciesByCountry = speciesByCountry;
    }

    public void setTripsByVessel(Multimap<Vessel, Trip> tripsByVessel) {
        this.tripsByVessel = tripsByVessel;
    }
}
