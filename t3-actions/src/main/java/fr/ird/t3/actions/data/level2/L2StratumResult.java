/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.actions.stratum.StratumResult;

import java.io.IOException;

/**
 * Result of a level 2 Stratum.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3.1
 */
public class L2StratumResult extends StratumResult<Level2Configuration> {

    public L2StratumResult(
            StratumConfiguration<Level2Configuration> configuration,
            String libelle) {
        super(configuration, libelle);
    }

    @Override
    public void close() throws IOException {
    }
}
