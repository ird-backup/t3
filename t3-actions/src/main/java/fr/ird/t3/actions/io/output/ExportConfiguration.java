/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.output;

import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.io.output.T3OutputConfiguration;
import fr.ird.t3.io.output.T3OutputProvider;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.l_;

/**
 * Configuration of action {@link ExportAction}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ExportConfiguration extends JdbcConfiguration implements T3ActionConfiguration {

    private static final long serialVersionUID = 1L;

    protected T3OutputProvider<?, ?> outputProvider;

    protected T3OutputConfiguration outputConfiguration;

    private List<Ocean> oceans;

    private List<Country> fleets;

    /** min begin date to use. */
    protected T3Date minDate;

    /** max end date to use. */
    protected T3Date maxDate;

    /** Id of selected InputProvider */
    protected String outputProviderId;

    public T3OutputProvider<?, ?> getOutputProvider() {
        return outputProvider;
    }

    public void setOutputProvider(T3OutputProvider<?, ?> outputProvider) {
        this.outputProvider = outputProvider;
    }

    public T3OutputConfiguration getOutputConfiguration() {
        if (outputConfiguration == null) {
            outputConfiguration = new T3OutputConfiguration();
        }
        return outputConfiguration;
    }

    @Override
    public String getUrl() {
        return getOutputConfiguration().getUrl();
    }

    @Override
    public String getLogin() {
        return getOutputConfiguration().getLogin();
    }

    @Override
    public String getPassword() {
        return getOutputConfiguration().getPassword();
    }

    public void setUrl(String url) {
        getOutputConfiguration().setUrl(url);
    }

    public void setLogin(String login) {
        getOutputConfiguration().setLogin(login);
    }

    public void setPassword(String password) {
        getOutputConfiguration().setPassword(password);
    }

    public T3Date getBeginDate() {
        return getOutputConfiguration().getBeginDate();
    }

    public void setBeginDate(T3Date beginDate) {
        getOutputConfiguration().setBeginDate(beginDate);
    }

    public T3Date getEndDate() {
        return getOutputConfiguration().getEndDate();
    }

    public void setEndDate(T3Date endDate) {
        getOutputConfiguration().setEndDate(endDate);
    }

    public String getFleetId() {
        return getOutputConfiguration().getFleetId();
    }

    public void setFleetId(String fleetId) {
        getOutputConfiguration().setFleetId(fleetId);
    }

    public List<String> getOperationIds() {
        return getOutputConfiguration().getOperationIds();
    }

    public void setOperationIds(List<String> operationIds) {
        getOutputConfiguration().setOperationIds(operationIds);
    }

    public String getOceanId() {
        return getOutputConfiguration().getOceanId();
    }

    public void setOceanId(String oceanId) {
        getOutputConfiguration().setOceanId(oceanId);
    }

    public List<Ocean> getOceans() {
        return oceans;
    }

    public void setOceans(List<Ocean> oceans) {
        this.oceans = oceans;
    }

    public List<Country> getFleets() {
        return fleets;
    }

    public void setFleets(List<Country> fleets) {
        this.fleets = fleets;
    }

    public String getOutputProviderId() {
        return outputProviderId;
    }

    public void setOutputProviderId(String outputProviderId) {
        if (ObjectUtils.notEqual(this.outputProviderId, outputProviderId)) {

            // changing provider means reset selected operation ids
            setOperationIds(null);
        }
        this.outputProviderId = outputProviderId;
    }

    public T3Date getMinDate() {
        return minDate;
    }

    public void setMinDate(T3Date minDate) {
        this.minDate = minDate;
    }

    public T3Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(T3Date maxDate) {
        this.maxDate = maxDate;
    }

    @Override
    public String getName(Locale locale) {
        return l_(locale, "t3.output.Export");
    }


}
