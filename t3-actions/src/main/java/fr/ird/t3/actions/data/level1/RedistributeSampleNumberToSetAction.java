/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.Maps;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequency;
import fr.ird.t3.entities.data.SampleSetSpeciesFrequencyDAO;
import fr.ird.t3.entities.data.SampleWell;
import fr.ird.t3.entities.data.StandardiseSampleSpecies;
import fr.ird.t3.entities.data.StandardiseSampleSpeciesFrequency;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Redistribute number of fishes from sample to his owning set.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class RedistributeSampleNumberToSetAction extends AbstractLevel1Action {

    @InjectDAO(entityType = SampleSetSpeciesFrequency.class)
    protected SampleSetSpeciesFrequencyDAO sampleSetSpeciesFrequencyDAO;

    public RedistributeSampleNumberToSetAction() {
        super(Level1Step.REDISTRIBUTE_SAMPLE_SET_TO_SET);
    }

    public Set<Species> getSpeciesOfSample(Sample sample) {
        Set<Species> result = SpeciesDAO.getAllSpecies(
                sample.getStandardiseSampleSpecies());
        return result;
    }

    public SpeciesModel getResultSpeciesModel(Sample sample, Species species) {
        SpeciesModel result = new SpeciesModel(species);

        // get the sum of number for the sample (for this given species)
        float sum = sample.getTotalStandardiseSampleSpeciesFrequencyNumber(species);
        result.setSampleSum(sum);

        for (SampleWell sampleWell : sample.getSampleWell()) {

            float sampleWellSum = sampleWell.getTotalSampleSetSpeciesFrequencyNumber(species);
            result.addSampleWellSum(sampleWell, sampleWellSum);
        }
        return result;
    }

    @Override
    protected void deletePreviousData() {

        // do not delete data here (done for all level 1 at first step)

        for (Sample sample : samplesByTrip.values()) {
            for (SampleWell sampleWell : sample.getSampleWell()) {
                sampleWell.clearSampleSetSpeciesFrequency();
            }
        }
    }

    @Override
    protected boolean executeAction() throws Exception {

        setNbSteps(2 * samplesByTrip.size());

        for (Trip trip : samplesByTrip.keySet()) {
            Collection<Sample> samples = samplesByTrip.get(trip);

            logTreatedAndNotSamplesforATrip(trip, samples);

            for (Sample sample : samples) {

                // treat for current sample
                doExecuteSample(sample);

                // mark sample as treated for this step of level 1 treatment
                markAsTreated(sample);
            }

            // mar trip as treated for this level 1 step
            markAsTreated(trip);
        }
        return true;
    }

    protected void doExecuteSample(Sample sample) throws TopiaException {

        addInfoMessage(l_(locale, "t3.level1.redistributeSampleNumberToSet.treat.sample",
                          sample.getSampleNumber()));

        Map<SampleWell, Float> frequenciesSum = Maps.newHashMap();

        // remove all previous computed data for the sample
        for (SampleWell sampleWell : sample.getSampleWell()) {
            frequenciesSum.put(sampleWell, 0f);
        }
        incrementsProgression();

        long sampleTotalNumber = 0l;
        long sampleWellTotalNumber = 0l;

        for (StandardiseSampleSpecies sampleSpecies :
                sample.getStandardiseSampleSpecies()) {

            Species species = sampleSpecies.getSpecies();

            for (StandardiseSampleSpeciesFrequency sampleSpeciesFrequency :
                    sampleSpecies.getStandardiseSampleSpeciesFrequency()) {

                float number = sampleSpeciesFrequency.getNumber();

                sampleTotalNumber += number;

                int lfLengthClass = sampleSpeciesFrequency.getLfLengthClass();

                for (SampleWell sampleWell : sample.getSampleWell()) {

                    float setNumber = number * sampleWell.getPropWeightedWeight();

                    Float sum = frequenciesSum.get(sampleWell);
                    sum += setNumber;
                    frequenciesSum.put(sampleWell, sum);

                    SampleSetSpeciesFrequency sampleSetSpeciesFrequency =
                            sampleSetSpeciesFrequencyDAO.create(
                                    SampleSetSpeciesFrequency.PROPERTY_LF_LENGTH_CLASS, lfLengthClass,
                                    SampleSetSpeciesFrequency.PROPERTY_SPECIES, species,
                                    SampleSetSpeciesFrequency.PROPERTY_NUMBER, setNumber
                            );

                    sampleWell.addSampleSetSpeciesFrequency(
                            sampleSetSpeciesFrequency);

                }
            }
        }
        incrementsProgression();

        for (SampleWell sampleWell : sample.getSampleWell()) {
            Float sum = frequenciesSum.get(sampleWell);
            sampleWellTotalNumber += sum;
        }

        String message = l_(locale, "t3.level1.redistributeSampleNumberToSet.resume.for.sample",
                            decorate(sample),
                            sampleTotalNumber,
                            sampleWellTotalNumber
        );
        addInfoMessage(message);

        for (SampleWell sampleWell : sample.getSampleWell()) {
            Float sum = frequenciesSum.get(sampleWell);
            sampleWellTotalNumber += sum;
            message = l_(locale, "t3.level1.redistributeSampleNumberToSet.resume.for.sampleWell",
                         sampleWell.getTopiaId(),
                         sampleWell.getPropWeightedWeight(),
                         sum
            );
            addInfoMessage(message);
        }
    }

    public static class SpeciesModel {

        protected Species species;

        protected float sampleSum;

        protected Map<SampleWell, Float> sampleWellSum;

        public SpeciesModel(Species species) {
            this.species = species;
            sampleWellSum = Maps.newHashMap();
        }

        public Species getSpecies() {
            return species;
        }

        public float getSampleSum() {
            return sampleSum;
        }

        public Map<SampleWell, Float> getSampleWellSum() {
            return sampleWellSum;
        }

        public void addSampleWellSum(SampleWell sampleWell, float sum) {
            sampleWellSum.put(sampleWell, sum);
        }

        public float getSampleWellSum(SampleWell sampleWell) {
            return sampleWellSum.get(sampleWell);
        }

        public void setSampleSum(float sampleSum) {
            this.sampleSum = sampleSum;
        }
    }
}
