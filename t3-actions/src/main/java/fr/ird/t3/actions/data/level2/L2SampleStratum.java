/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.stratum.SampleStratum;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategorySample;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModelHelper;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.IOException;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Sample stratum for level 2 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class L2SampleStratum extends SampleStratum<Level2Configuration, Level2Action, L2SampleStratum> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(L2SampleStratum.class);

    /**
     * Total weight of the catch stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    protected final float catchStratumTotalWeight;

    /** Weight composition for all species found in catches. */
    private final WeightCompositionAggregateModel modelsForAllSpecies;

    /** Weight composition model for only species to fix. */
    private WeightCompositionAggregateModel modelsForSpeciesToFix;

    /**
     * Current total count of sample.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private int sampleStratumTotalCount;

    /**
     * Current sample total weight of the stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    private float sampleStratumTotalWeight;


    @Override
    public void close() throws IOException {
        super.close();
        modelsForAllSpecies.close();
        modelsForSpeciesToFix.close();
    }

    public L2SampleStratum(StratumConfiguration<Level2Configuration> configuration,
                           Collection<Species> speciesToFix,
                           float catchStratumTotalWeight) {
        super(configuration, speciesToFix);
        this.catchStratumTotalWeight = catchStratumTotalWeight;
        modelsForAllSpecies = new WeightCompositionAggregateModel();
    }

    @Override
    protected L2SampleStratumLoader newLoader() {

        int oceanCode = getConfiguration().getZone().getOcean().getCode();

        L2SampleStratumLoader result;
        switch (oceanCode) {
            case 1:
                result = new L2SampleStratumLoaderAtlantic(this);
                break;
            case 2:
                result = new L2SampleStratumLoaderIndian(this);
                break;
            default:
                throw new IllegalStateException(
                        "Not implemented for ocean with code " + oceanCode);
        }
        return result;
    }

    public WeightCompositionAggregateModel getModelsForSpeciesToFix() {
        return modelsForSpeciesToFix;
    }

    public float getCatchStratumTotalWeight() {
        return catchStratumTotalWeight;
    }

    public int getSampleStratumTotalCount() {
        return sampleStratumTotalCount;
    }

    public float getSampleStratumTotalWeight() {
        return sampleStratumTotalWeight;
    }

    @Override
    protected void mergeNewActivities(T3ServiceContext serviceContext, Set<Activity> activities) throws TopiaException {

        Set<Species> species = getSpeciesToFix();

        // all weights (for each species) for all weight categories found
        Map<WeightCategorySample, Map<Species, Float>> weights =
                Maps.newHashMap();

        for (Activity activity : activities) {

            // split cat weight by weight category
            ActivityDAO.fillWeightsFromSetSpeciesCatWeight(
                    activity, weights, null);

            // obtain the set species frequencies for the current activity
            Collection<SetSpeciesFrequency> setSpeciesFrequencies =
                    activity.getSetSpeciesFrequency();

            if (CollectionUtils.isNotEmpty(setSpeciesFrequencies)) {

                // compute sample count for this activity
                int newCount = computeSampleCount(setSpeciesFrequencies,
                                                  species);

                // merge it with final total count
                sampleStratumTotalCount += newCount;
            }
        }

        // add all weights to model
        for (WeightCategorySample weightCategoryTreatment : weights.keySet()) {
            Map<Species, Float> speciesFloatMap = weights.get(weightCategoryTreatment);
            modelsForAllSpecies.addModel(weightCategoryTreatment, speciesFloatMap);
        }

        // recompute the weight model for species to fix
        modelsForSpeciesToFix = modelsForAllSpecies.extractForSpecies(species);

        // recompute the total sample weight (for species to fix)
        sampleStratumTotalWeight = modelsForSpeciesToFix.getTotalModel().getTotalWeight();

        addMergedActivitesCount(activities.size());

        if (log.isInfoEnabled()) {
            log.info("sampleStratumTotalCount  = " + getSampleStratumTotalCount());
            log.info("sampleStratumTotalWeight = " + getSampleStratumTotalWeight());
        }
    }

    protected int computeSampleCount(Collection<SetSpeciesFrequency> newDatas,
                                     Collection<Species> speciesToFix) {

        int newCount = 0;

        for (SetSpeciesFrequency newData : newDatas) {

            Species species = newData.getSpecies();

            if (speciesToFix.contains(species)) {

                newCount += newData.getNumber();
            }

        }
        return newCount;
    }

    @Override
    public String logSampleStratumLevel(int substitutionLevel,
                                        Level2Action messager) {

        Locale l = messager.getLocale();

        String title = l_(l, "t3.level2.sampleStratum.resume.for.level",
                          substitutionLevel,
                          // Do not use #getNbActivites() since it is perharp not yet setted.
                          // Use this internal state instead of the #getNbActivities() since activites can still not be setted in the stratum
                          // Fix http://forge.codelutin.com/issues/1907
                          getNbMergedActivities(),
                          getSampleStratumTotalCount(),
                          getSampleStratumTotalWeight()
        );

        String message = WeightCompositionModelHelper.decorateModel(
                messager.getDecoratorService(),
                title,
                modelsForAllSpecies,
                modelsForSpeciesToFix
        );
        return message;
    }

}
