/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.SetDuration;
import fr.ird.t3.entities.reference.SetDurationDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.TimeLog;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Compute for the all activities of selected trips, the set duration and the
 * positive set count.
 * <p/>
 * <strong>Note:</strong> The rf2 msut have been computed on each trip.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeSetDurationAndPositiveSetCountAction extends AbstractLevel0Action<ComputeSetDurationAndPositiveSetCountConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(
            ComputeSetDurationAndPositiveSetCountAction.class);

    @InjectDAO(entityType = SetDuration.class)
    protected SetDurationDAO setDurationDAO;

    /** Count of treated activites. */
    protected int nbActivities;

    /** Count of positive activites found. */
    protected int nbPositiveActivities;

    /**
     * Cache of setDuration (improve a lot performance!).
     *
     * @since 1.3
     */
    protected Map<String, SetDuration> setDurations;

    public ComputeSetDurationAndPositiveSetCountAction() {
        super(Level0Step.COMPUTE_SET_DURATION_AND_POSITIVE_SET_COUNT);
    }

    public int getNbActivities() {
        return nbActivities;
    }

    public int getNbPositiveActivities() {
        return nbPositiveActivities;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);

        for (Trip trip : tripList) {
            nbActivities += trip.sizeActivity();
        }

        setDurations = Maps.newTreeMap();
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        boolean result = false;

        if (CollectionUtils.isNotEmpty(trips)) {

            setNbSteps(2 * nbActivities);

            // do action for each data using the prepared action context

            for (Trip trip : trips) {

                result |= executeForTrip(trip);
            }
        }

        return result;
    }

    protected boolean executeForTrip(Trip trip) throws TopiaException {

        long s0 = TimeLog.getTime();

        String tripStr = decorate(trip, DecoratorService.WITH_ID);

        if (!trip.isActivityEmpty()) {

            int year = T3Functions.TRIP_TO_LANDING_YEAR.apply(trip);

            Set<Species> species = trip.getElementaryCatchSpecies();

            for (Activity activity : trip.getActivity()) {

                String activityStr = "Activity " + tripStr + " - " +
                                     decorate(activity);

                incrementsProgression();

                float totalCatchesWeight =
                        activity.getElementaryCatchTotalWeightRf2(species);

                if (log.isDebugEnabled()) {
                    log.debug("Total catches " + totalCatchesWeight +
                              " for " + activityStr);
                }

                computePositiveSetCount(activityStr, totalCatchesWeight, activity);

                incrementsProgression();
                computeSetDuration(year,
                                   totalCatchesWeight, activity);

                String message = l_(locale, "t3.level0.computeActivitySetDurationAndPositiveSetCount",
                                    activityStr,
                                    activity.getSetDuration(),
                                    activity.getPositiveSetCount()
                );
                addInfoMessage(message);
                if (log.isDebugEnabled()) {
                    log.debug(message);
                }
            }
        }

        markTripAsTreated(trip);

        getTimeLog().log(s0, "executeForTrip for " + tripStr);

        // always commit since trip is always modified
        return true;
    }

    protected void computePositiveSetCount(String activityStr,
                                           float totalCatchesWeight,
                                           Activity activity) {

        int positiveSetCount = 0;

        if (totalCatchesWeight > 0) {

            // ok some catches found on activity, it means
            // positive set

            positiveSetCount = activity.getSetCount();

        } else {
            if (log.isDebugEnabled()) {
                log.debug("Catches with no catches! for " + activityStr);
            }
        }

        if (log.isDebugEnabled()) {
            String message = "Computed positive Set Count = " +
                             positiveSetCount + " for " + activityStr;
            log.debug(message);
        }
        activity.setPositiveSetCount(positiveSetCount);

        if (positiveSetCount > 0) {
            nbPositiveActivities++;
        }
    }

    protected void computeSetDuration(int year,
                                      float totalCatchesWeight,
                                      Activity activity) throws TopiaException {

        // get the correct setDuration
        SetDuration setDuration = getSetDuration(activity, year);

        if (setDuration == null) {

            activity.setSetDuration(0f);

        } else {

            Float setTime;

            if (activity.isSetNull()) {

                // use setDuration nullSetValue
                setTime = setDuration.getNullSetValue();
                if (log.isDebugEnabled()) {
                    log.debug("Computed set duration (set is null) = " +
                              setTime);
                }
            } else if (activity.isWithSetDuration()) {

                // use setDuration computedValue
                setTime = setDuration.getParameterB() +
                          setDuration.getParameterA() * totalCatchesWeight;
                if (log.isDebugEnabled()) {
                    log.debug("Computed set duration = " + setTime);
                }
            } else {

                // no setDuration
                setTime = null;
                if (log.isDebugEnabled()) {
                    log.debug("Use null set duration (set is null) ");
                }
            }

            activity.setSetDuration(setTime);
        }
    }

    protected SetDuration getSetDuration(Activity activity, int year) throws TopiaException {
        Ocean ocean = activity.getOcean();
        Country fleetCountry = activity.getTrip().getVessel().getFleetCountry();
        SchoolType schoolType = activity.getSchoolType();
        String key = ocean.getCode() + ":" +
                     fleetCountry.getCode() + ":" +
                     schoolType.getCode() + ":" +
                     year;

        SetDuration result = setDurations.get(key);
        if (result == null) {

            // no result found in cache

            if (!setDurations.containsKey(key)) {

                // try to load it

                result = setDurationDAO.findByActivityAndYear(
                        activity,
                        year
                );

                if (result == null) {
                    addWarningMessage(
                            l_(getLocale(), "t3.level0.computeSetDurationAndPositiveSetCount.error.noSetDurationFound",
                               decorate(ocean),
                               decorate(fleetCountry),
                               decorate(schoolType),
                               year));
                }
            }

            // store it once for all (even if result is null)
            setDurations.put(key, result);
        }

        return result;
    }

}
