/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.CorrectedElementaryCatch;
import fr.ird.t3.entities.data.CorrectedElementaryCatchDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.entities.reference.WeightCategoryTreatmentDAO;
import fr.ird.t3.entities.reference.zone.ZoneStratumAware;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.models.WeightCompositionAggregateModel;
import fr.ird.t3.models.WeightCompositionModel;
import fr.ird.t3.models.WeightCompositionModelHelper;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ZoneStratumService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import fr.ird.t3.services.ioc.InjectEntityById;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Level 2 treatment action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class Level2Action extends T3Action<Level2Configuration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Level2Action.class);

    public static final String PARAM_LEVEL_CONFIGURATION = "levelConfiguration";

    @InjectDAO(entityType = Activity.class)
    protected ActivityDAO activityDAO;

    @InjectDAO(entityType = WeightCategoryTreatment.class)
    protected WeightCategoryTreatmentDAO weightCategoryTreatmentDAO;

    @InjectDAO(entityType = CorrectedElementaryCatch.class)
    protected CorrectedElementaryCatchDAO correctedElementaryCatchDAO;

    @InjectDAO(entityType = Vessel.class)
    protected VesselDAO vesselDAO;

    protected ZoneStratumAwareMeta zoneMeta;

    protected ZoneVersion zoneVersion;

    @InjectFromDAO(entityType = SchoolType.class, method = "findAllForStratum")
    protected Set<SchoolType> schoolTypes;

    protected Multimap<SchoolType, ZoneStratumAware> zoneBySchoolType;

    @InjectEntityById(entityType = Ocean.class)
    protected Ocean ocean;

    @InjectEntityById(entityType = Country.class)
    protected Country catchFleet;

    @InjectEntitiesById(entityType = Species.class, path = "configuration.speciesIds")
    protected Collection<Species> species;

    @InjectEntitiesById(entityType = Country.class)
    protected Collection<Country> sampleFleets;

    @InjectEntitiesById(entityType = Country.class)
    protected Collection<Country> sampleFlags;

    protected Set<T3Date> startDates;

    protected Multimap<SchoolType, WeightCategoryTreatment>
            weightCategoriesBySchoolType;

    protected Set<Vessel> possibleCatchVessels;

    protected Set<Vessel> possibleSampleVessels;

    protected int nbStratums;

    protected long totalCatchWeightForSpeciesFoFix;

    protected long totalCatchWeight;

    protected long totalCatchActivities;

    protected long totalCatchActivitiesWithSample;

    /**
     * Input Weight composition
     * (based on {@link CorrectedElementaryCatch#getCatchWeight()}) for all
     * species found in catches.
     *
     * @since 1.3
     */
    protected WeightCompositionAggregateModel inputCatchModelForAllSpecies;

    /**
     * Output Weight composition
     * (based on {@link CorrectedElementaryCatch#getCorrectedCatchWeight()}) for all
     * species found in catches.
     *
     * @since 1.3
     */
    protected WeightCompositionAggregateModel outputCatchModelForAllSpecies;

    /**
     * For each stratum gets his result.
     *
     * @since 1.3
     */
    protected Collection<L2StratumResult> stratumsResult;

    /**
     * Cache of activity (to avoid to reload them for catch and sample stratum).
     *
     * @since 1.4
     */
    protected final LoadingCache<String, Activity> activityCache;

    public Level2Action() {
        activityCache = CacheBuilder.newBuilder().build(
                new CacheLoader<String, Activity>() {
                    @Override
                    public Activity load(String key) throws Exception {
                        return activityDAO.findByTopiaId(key);
                    }
                }
        );
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        Level2Configuration configuration = getConfiguration();

        configuration.setLocale(getLocale());

        // get zones type meta to use
        setZoneMeta(
                newService(ZoneStratumService.class).getZoneMetaById(
                        configuration.getZoneTypeId()));

        setZoneVersion(zoneMeta.getZoneVersion(
                configuration.getZoneVersionId(), getTransaction()));

        // get all zones by ocean and shcool types
        setZoneBySchoolType(
                zoneMeta.getZones(ocean,
                                  schoolTypes,
                                  zoneVersion,
                                  getTransaction()));

        // get start dates to use
        setStartDates(T3Date.getStartDates(
                configuration.getBeginDate(),
                configuration.getEndDate(),
                configuration.getTimeStep()));

        // get weight categories to use (group by school type)
        setWeightCategoriesBySchoolType(
                weightCategoryTreatmentDAO.getWeightCategories(
                        ocean,
                        schoolTypes));

        // get possible vessels for catch stratum
        setPossibleCatchVessels(vesselDAO.getPossibleCatchVessels(catchFleet));

        // get possible vessels for sample stratum
        setPossibleSampleVessels(
                vesselDAO.getPossibleSampleVessels(sampleFleets, sampleFlags));

        stratumsResult = Sets.newLinkedHashSet();
        inputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
        outputCatchModelForAllSpecies = new WeightCompositionAggregateModel();
    }

    @Override
    protected void deletePreviousData() {
        // data will be removed from selected activities
    }

    @Override
    protected void finalizeAction() throws TopiaException, IOException {

        activityCache.invalidateAll();

        super.finalizeAction();
    }

    @Override
    protected boolean executeAction() throws Exception {

        Level2Configuration configuration = getConfiguration();

        // get time step
        int timeStep = configuration.getTimeStep();

        nbStratums = zoneBySchoolType.size() * startDates.size();
        setNbSteps(3 * nbStratums);

        // keep a track of alreay used activity ids (to remove previous level 2 data)
        Set<String> usedActivityIds = Sets.newHashSet();

        int stratumIndex = 1;
        for (SchoolType schoolType : schoolTypes) {

            // get all weight categories for this school type
            List<WeightCategoryTreatment> weightCategories =
                    Lists.newArrayList(weightCategoriesBySchoolType.get(schoolType));

            weightCategoryTreatmentDAO.sort(weightCategories);

            // get all zones for this school type
            Collection<ZoneStratumAware> zones =
                    zoneBySchoolType.get(schoolType);

            for (ZoneStratumAware zone : zones) {

                for (T3Date startDate : startDates) {

                    // get end date (only increments on timseStep - 1 to have
                    // exactly timstep month from beginDate.toStartDate() to
                    // endDate.toEndDate()
                    T3Date endDate = startDate.incrementsMonths(timeStep - 1);

                    StratumConfiguration<Level2Configuration>
                            stratumConfiguration =
                            StratumConfiguration.newStratumConfiguration(
                                    configuration,
                                    zoneMeta,
                                    zone,
                                    schoolType,
                                    startDate,
                                    endDate,
                                    zones,
                                    possibleCatchVessels,
                                    possibleSampleVessels,
                                    null,
                                    activityCache
                            );

                    try {
                        L2StratumResult result = doExecuteStratum(
                                stratumConfiguration,
                                weightCategories,
                                stratumIndex,
                                usedActivityIds
                        );

                        stratumsResult.add(result);
                    } finally {
                        flushTransaction("After stratum " + stratumIndex);
                    }

                    stratumIndex++;
                }
            }
        }
        return true;
    }

    protected L2StratumResult doExecuteStratum(
            StratumConfiguration<Level2Configuration> stratumConfiguration,
            List<WeightCategoryTreatment> weightCategories,
            int stratumIndex,
            Set<String> usedActivityIds) throws Exception {

        incrementsProgression();

        String stratumPrefix =
                l_(locale, "t3.level2.stratumLibelle",
                   stratumIndex, nbStratums,
                   decorate(stratumConfiguration.getSchoolType()),
                   decorate(stratumConfiguration.getZone()),
                   stratumConfiguration.getBeginDate(),
                   stratumConfiguration.getEndDate()
                );

        L2StratumResult result =
                new L2StratumResult(stratumConfiguration, stratumPrefix);

        String message =
                l_(locale, "t3.level2.message.start.stratum", stratumPrefix);

        if (log.isInfoEnabled()) {
            log.info(message);
        }

        addInfoMessage("==============================================================================================");
        addInfoMessage(message);
        addInfoMessage("==============================================================================================");

        L2CatchStratum catchStratum = null;
        L2SampleStratum sampleStratum = null;

        boolean noError = true;
        try {

            // compute the catch stratum
            catchStratum = newCatchStratum(stratumConfiguration,
                                           weightCategories);

            incrementsProgression();

            if (catchStratum == null) {

                // no catch in this stratum

                // mark as a 0 substitution level
                result.setSubstitutionLevel(0);

                incrementsProgression();

            } else {

                // compute sample stratum

                sampleStratum = newSampleStratum(
                        stratumConfiguration,
                        weightCategories,
                        catchStratum.getTotalCatchWeightForSpeciesToFix());


                incrementsProgression();

                // get the substitution level for the sample stratum
                Integer level = sampleStratum.getSubstitutionLevel();

                if (log.isInfoEnabled()) {
                    log.info("Sample stratum substitution level " + level);
                }

                if (level == -1) {

                    // sample stratum could not be found

                    // log sample stratum new composition
                    message =
                            l_(getLocale(), "t3.level2.warning.missing.data.for.stratum",
                               result.getLibelle());

                    if (log.isWarnEnabled()) {
                        log.warn(message);
                    }
                    addWarningMessage(message);

                    // mark it as 999 level to make it appear at top of a reverse list
                    result.setSubstitutionLevel(999);

                    // Fill back  CorrectedElementaryCatch.CatchWeight to CorrectedElementaryCatch.CorrectedCatchWeight

                    int activityIndex = 1;
                    int nbActivites = catchStratum.getNbActivities();
                    for (Map.Entry<Activity, Integer> e : catchStratum) {

                        Activity activity = e.getKey();

                        int nbZones = e.getValue();

                        // is activity was already treated ?
                        boolean newActivity = usedActivityIds.add(activity.getTopiaId());

                        doExecuteActivityInCatchStratum(catchStratum,
                                                        activity,
                                                        activityIndex++,
                                                        nbActivites,
                                                        newActivity);

                        // keep the stratum substitution level in the activity
                        activity.setStratumLevelN2(999);
                        activity.setUseMeanStratumCompositionN2(false);

                    }

                } else {

                    // can use this sampleStratum

                    int activityIndex = 1;
                    int nbActivites = catchStratum.getNbActivities();
                    for (Map.Entry<Activity, Integer> e : catchStratum) {

                        Activity activity = e.getKey();

                        int nbZones = e.getValue();

                        // is activity was already treated ?
                        boolean newActivity =
                                usedActivityIds.add(activity.getTopiaId());

                        doExecuteActivityInCatchStratum(catchStratum,
                                                        sampleStratum,
                                                        activity,
                                                        nbZones,
                                                        activityIndex++,
                                                        nbActivites,
                                                        newActivity);

                        // keep the stratum substitution level in the activity
                        activity.setStratumLevelN2(
                                sampleStratum.getSubstitutionLevel());
                        activity.setUseMeanStratumCompositionN2(true);
                    }

                    // usable catch stratum

                    // --------------------------------------------------
                    // --- Report results
                    // --------------------------------------------------

                    result.setSubstitutionLevel(sampleStratum.getSubstitutionLevel());
                    result.addNbActivities(catchStratum.getNbActivities());
                    result.addNbActivitiesWithSample(catchStratum.getNbActivitiesWithSample());

                    // merge input and output model from catch stratum models

                    catchStratum.mergeGlobalCompositionModels(inputCatchModelForAllSpecies,
                                                              outputCatchModelForAllSpecies);

                    totalCatchWeight += catchStratum.getTotalCatchWeightForAllSpecies();
                    totalCatchWeightForSpeciesFoFix += catchStratum.getTotalCatchWeightForSpeciesToFix();

                    totalCatchActivities += result.getNbActivities();
                    totalCatchActivitiesWithSample += result.getNbActivitiesWithSample();
                }
            }

        } finally {
            IOUtils.closeQuietly(catchStratum);
            IOUtils.closeQuietly(sampleStratum);
        }
        return result;
    }

    protected L2CatchStratum newCatchStratum(StratumConfiguration<Level2Configuration> stratumConfiguration,
                                             List<WeightCategoryTreatment> weightCategories) throws Exception {
        L2CatchStratum catchStratum =
                new L2CatchStratum(stratumConfiguration, species);
        catchStratum.init(serviceContext, weightCategories, this);

        // get the total weight of the catch stratum
        float catchStratumWeight =
                catchStratum.getTotalCatchWeightForSpeciesToFix();

        if (catchStratumWeight == 0) {

            // no catch in this stratum, skip it
            String message = l_(locale, "t3.level2.message.noCatch.in.stratum");
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);

            // let's nullify the catch stratum (make it no more available)
            catchStratum = null;
        } else {

            // log it
            String message = catchStratum.logCatchStratum(getDecoratorService());
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);
        }
        return catchStratum;
    }

    protected L2SampleStratum newSampleStratum(StratumConfiguration<Level2Configuration> stratumConfiguration,
                                               List<WeightCategoryTreatment> weightCategories,
                                               float totalCatchWeight) throws Exception {
        L2SampleStratum sampleStratum =
                new L2SampleStratum(stratumConfiguration,
                                    species,
                                    totalCatchWeight);
        sampleStratum.init(serviceContext, weightCategories, this);
        return sampleStratum;
    }

    protected void doExecuteActivityInCatchStratum(L2CatchStratum catchStratum,
                                                   L2SampleStratum sampleStratum,
                                                   Activity activity,
                                                   int nbZones,
                                                   int activityIndex,
                                                   int nbActivites,
                                                   boolean deleteOldData) throws TopiaException, IOException {

        String activityStr = decorate(activity) + " (" +
                             decorate(activity.getTrip(),
                                      DecoratorService.WITH_ID) + ")";

        String message = l_(locale, "t3.level2.message.start.activity",
                            activityIndex, nbActivites, activityStr, nbZones);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        if (deleteOldData) {

            // delete old data for this activity
            if (log.isInfoEnabled()) {
                log.info("Delete previous level2 data of " + activityStr);
            }
            activity.deleteComputedDataLevel2();
        }

        // build a model for getting total of each weight category
        WeightCompositionAggregateModel catchWeightModelForAllSpecies =
                new WeightCompositionAggregateModel();

        ActivityDAO.fillWeightsFromCatchesWeight(activity,
                                                 catchWeightModelForAllSpecies,
                                                 nbZones);

        WeightCompositionAggregateModel catchWeightModelForSpeciesToFix =
                catchWeightModelForAllSpecies.extractForSpecies(species);


        // log catches weight
        message = logCatchWeight(getDecoratorService(),
                                 catchWeightModelForAllSpecies,
                                 catchWeightModelForSpeciesToFix);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        // obtain the sample composition model to use for the
        // activity (can come from catch if it has some samples,
        // otherwise use the catch stratum one)

        WeightCompositionAggregateModel compositionModel;

        boolean activityWithSample = catchStratum.isActivityWithSample(activity);

        boolean useAllSamplesOfStratum = getConfiguration().isUseAllSamplesOfStratum();

        if (activityWithSample && !useAllSamplesOfStratum) {

            // activity has some samples, compute his specific composition model
            // and configuration does not ask to treat such catches with all stratum sample

            WeightCompositionAggregateModel model =
                    new WeightCompositionAggregateModel();

            ActivityDAO.fillWeightsFromSetSpeciesCatWeight(
                    activity, null, model);

            compositionModel = model.extractForSpecies(species);

            String activityResume = logActivityCatchStratum(
                    model,
                    compositionModel,
                    getDecoratorService());

            message = l_(locale, "t3.level2.message.activity.with.sample.useOwn.composition", activityResume);

            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);

        } else {

            // activity with no sample, use sample stratum composition
            // or configuration ask to always use stratum sample
            compositionModel = sampleStratum.getModelsForSpeciesToFix();

            if (activityWithSample) {
                message =
                        l_(locale, "t3.level2.message.activity.with.sample.useSampleStratum.composition", activityStr);
            } else {
                message =
                        l_(locale, "t3.level2.message.activity.with.no.sample.useSampleStratum.composition", activityStr);
            }

            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);
        }

        // apply composition model to activity catches
        applySampleSpecificComposition(catchStratum,
                                       activity,
                                       nbZones,
                                       compositionModel,
                                       catchWeightModelForSpeciesToFix);

        catchWeightModelForAllSpecies.close();
        catchWeightModelForSpeciesToFix.close();

        if (activityWithSample && !useAllSamplesOfStratum) {
            compositionModel.close();
        }

    }

    protected void doExecuteActivityInCatchStratum(L2CatchStratum catchStratum,
                                                   Activity activity,
                                                   int activityIndex,
                                                   int nbActivites,
                                                   boolean deleteOldData) throws TopiaException, IOException {

        String activityStr = decorate(activity) + " (" +
                             decorate(activity.getTrip(),
                                      DecoratorService.WITH_ID) + ")";

        String message = l_(locale, "t3.level2.message.start.activity",
                            activityIndex, nbActivites, activityStr, 1);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        if (deleteOldData) {

            // delete old data for this activity
            if (log.isInfoEnabled()) {
                log.info("Delete previous level2 data of " + activityStr);
            }
            activity.deleteComputedDataLevel2();
        }

        // build a model for getting total of each weight category
        WeightCompositionAggregateModel catchWeightModelForAllSpecies =
                new WeightCompositionAggregateModel();

        ActivityDAO.fillWeightsFromCatchesWeight(activity, catchWeightModelForAllSpecies, 1);

        WeightCompositionAggregateModel catchWeightModelForSpeciesToFix =
                catchWeightModelForAllSpecies.extractForSpecies(species);


        // log catches weight
        message = logCatchWeight(getDecoratorService(), catchWeightModelForAllSpecies, catchWeightModelForSpeciesToFix);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        WeightCompositionAggregateModel model = new WeightCompositionAggregateModel();

        ActivityDAO.fillWeightsFromSetSpeciesCatWeight(activity, null, model);

        WeightCompositionAggregateModel compositionModel = model.extractForSpecies(species);

        String activityResume = logActivityCatchStratum(model, compositionModel, getDecoratorService());

        message = l_(locale, "t3.level2.message.activity.with.sample.useOwn.composition", activityResume);

        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        // apply composition model to activity catches
        applySampleSpecificComposition(catchStratum, activity);

        catchWeightModelForAllSpecies.close();
        catchWeightModelForSpeciesToFix.close();

        compositionModel.close();

    }

    protected void applySampleSpecificComposition(L2CatchStratum catchStratum, Activity activity) throws TopiaException {

        WeightCompositionAggregateModel correctedCatchWeightModel = new WeightCompositionAggregateModel();

        // Get all corrected catch group by weight category
        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> correctedElementaryCatches =
                ActivityDAO.groupByWeightCategoryTreatment(activity.getCorrectedElementaryCatch());

        for (WeightCategoryTreatment weightCategory : correctedElementaryCatches.keySet()) {

            Map<Species, Float> weights = Maps.newHashMap();

            // Apply on all existing corrected catch weight

            for (CorrectedElementaryCatch aCatch : correctedElementaryCatches.get(weightCategory)) {

                Species speciesToUse = aCatch.getSpecies();

                // the corrected catch weight to add to the row
                float correctedCatchWeight = aCatch.getCatchWeight();

                // hold added value (for logs)
                weights.put(speciesToUse, correctedCatchWeight);

                aCatch.setCorrectedCatchWeight(correctedCatchWeight);
                aCatch.setCorrectedFlag(false);

            }

            // add corrected weights used for this category
            correctedCatchWeightModel.addModel(weightCategory, weights);
        }

        // log corrected catches weight
        String message = logCorrectedCatchWeight(correctedCatchWeightModel, getDecoratorService());
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        // add corrected catch weight done for this activity in stratum output model
        catchStratum.addActivityOutputModel(correctedCatchWeightModel);

    }

    protected void applySampleSpecificComposition(L2CatchStratum catchStratum,
                                                  Activity activity,
                                                  int nbZones,
                                                  WeightCompositionAggregateModel sampleCompositionModel,
                                                  WeightCompositionAggregateModel correctedCatchesForSpeciesToFix) throws TopiaException {

        WeightCompositionAggregateModel correctedCatchWeightModel = new WeightCompositionAggregateModel();

        // Get all corrected catch group by weight category
        Multimap<WeightCategoryTreatment, CorrectedElementaryCatch> correctedElementaryCatches =
                ActivityDAO.groupByWeightCategoryTreatment(
                        activity.getCorrectedElementaryCatch());

        for (WeightCategoryTreatment weightCategory : correctedElementaryCatches.keySet()) {

            Map<Species, Float> weights = Maps.newHashMap();

            // get composition model to use
            WeightCompositionModel model =
                    sampleCompositionModel.getModel(weightCategory);

            // set of species still to fix
            Set<Species> speciesToFix;

            // set of species still to fix
            Set<Species> speciesStillToFix;

            // get total weight for species to fix from correctedCatches
            float totalWeight;

            if (model == null) {

                // this means there is no specific composition for this weight
                // category to apply for species to fix

                // says there is no species to fix (so all catches will be
                // copied to correctedCatchWeight (with no fixedFlag)

                speciesToFix = speciesStillToFix = Collections.emptySet();

                totalWeight = 0f;
            } else {

                // found a specific composition to apply for this weight category

                speciesToFix = model.getSpecies();

                speciesStillToFix = Sets.newHashSet(speciesToFix);

                totalWeight = correctedCatchesForSpeciesToFix.getModel(
                        weightCategory).getTotalWeight();
            }

            // divide the total weight to used by the number of zones of this
            // activity
            totalWeight = totalWeight / nbZones;


            // Apply on all existing corrected catch weight

            for (CorrectedElementaryCatch aCatch :
                    correctedElementaryCatches.get(weightCategory)) {

                Species speciesToUse = aCatch.getSpecies();

                // flag (was fixed or not)
                boolean toFix;

                // the corrected catch weight to add to the row
                float correctedCatchWeight;

                if (speciesToFix.contains(speciesToUse)) {

                    // species to fix
                    toFix = true;

                    float weightRate = model.getWeightRate(speciesToUse);

                    // new corrected catch weight
                    correctedCatchWeight = totalWeight * weightRate;

                    // species no more to treat
                    speciesStillToFix.remove(speciesToUse);
                } else {

                    // nothing to fix, just propagate old value
                    toFix = false;
                    correctedCatchWeight = aCatch.getCatchWeight();
                }

                // hold added value (for logs)
                weights.put(speciesToUse, correctedCatchWeight);

                // Add to old value (if any)
                Float oldCorrectedCatchWeight = aCatch.getCorrectedCatchWeight();
                if (oldCorrectedCatchWeight == null) {
                    oldCorrectedCatchWeight = 0f;
                }
                aCatch.setCorrectedCatchWeight(oldCorrectedCatchWeight + correctedCatchWeight);
                aCatch.setCorrectedFlag(toFix);
            }

            // Creates new corrected catch weight records for all species
            // still to fix
            // Means they are in samples but not in catches

            for (Species speciesToUse : speciesStillToFix) {

                // weight rate of the species
                float weightRate = model.getWeightRate(speciesToUse);

                // corrected catch weight
                float correctedCatchWeight = totalWeight * weightRate;

                // create new record
                CorrectedElementaryCatch aCatch =
                        correctedElementaryCatchDAO.create();
                aCatch.setSpecies(speciesToUse);
                aCatch.setWeightCategoryTreatment(weightCategory);
                aCatch.setCatchWeight(0f);
                aCatch.setCorrectedFlag(true);
                aCatch.setCorrectedCatchWeight(correctedCatchWeight);

                // hold added value (for logs)
                weights.put(speciesToUse, correctedCatchWeight);

                // add it to activity
                activity.addCorrectedElementaryCatch(aCatch);
            }

            // add corrected weights used for this category
            correctedCatchWeightModel.addModel(weightCategory, weights);
        }

        // log corrected catches weight
        String message = logCorrectedCatchWeight(correctedCatchWeightModel,
                                                 getDecoratorService());
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        addInfoMessage(message);

        // add corrected catch weight done for this activity in stratum output model
        catchStratum.addActivityOutputModel(correctedCatchWeightModel);
    }

    public void setZoneMeta(ZoneStratumAwareMeta zoneMeta) {
        this.zoneMeta = zoneMeta;
    }

    public void setSchoolTypes(Set<SchoolType> schoolTypes) {
        this.schoolTypes = schoolTypes;
    }

    public void setZoneBySchoolType(Multimap<SchoolType, ZoneStratumAware> zoneBySchoolType) {
        this.zoneBySchoolType = zoneBySchoolType;
    }

    public void setSpecies(Set<Species> species) {
        this.species = species;
    }

    public void setStartDates(Set<T3Date> startDates) {
        this.startDates = startDates;
    }

    public void setWeightCategoriesBySchoolType(Multimap<SchoolType, WeightCategoryTreatment> weightCategoriesBySchoolType) {
        this.weightCategoriesBySchoolType = weightCategoriesBySchoolType;
    }

    public void setPossibleCatchVessels(Set<Vessel> possibleCatchVessels) {
        this.possibleCatchVessels = possibleCatchVessels;
    }

    public void setPossibleSampleVessels(Set<Vessel> possibleSampleVessels) {
        this.possibleSampleVessels = possibleSampleVessels;
    }

    public void setZoneVersion(ZoneVersion zoneVersion) {
        this.zoneVersion = zoneVersion;
    }

    // -------------------------------------------------------------------------
    // --- Results -------------------------------------------------------------
    // -------------------------------------------------------------------------

    public long getTotalCatchActivities() {
        return totalCatchActivities;
    }

    public int getNbStratums() {
        return nbStratums;
    }

    public long getTotalCatchWeightForSpeciesFoFix() {
        return totalCatchWeightForSpeciesFoFix;
    }

    public long getTotalCatchWeight() {
        return totalCatchWeight;
    }

    public int getNbStratumsFixed() {
        return stratumsResult.size();
    }

    public Integer[] getAllSubstitutionLevels() {
        Set<Integer> levels = Sets.newHashSet();
        for (L2StratumResult stratumResult : stratumsResult) {
            levels.add(stratumResult.getSubstitutionLevel());
        }
        List<Integer> result = Lists.newArrayList(levels);
        Collections.reverse(result);
        return result.toArray(new Integer[result.size()]);
    }

    public long getTotalCatchActivitiesWithSample() {
        return totalCatchActivitiesWithSample;
    }

    public Collection<L2StratumResult> getStratumResult(int level) {
        Set<L2StratumResult> singleResult = Sets.newLinkedHashSet();
        for (L2StratumResult stratumResult : stratumsResult) {
            if (level == stratumResult.getSubstitutionLevel()) {
                singleResult.add(stratumResult);
            }
        }
        return singleResult;
    }

    public Map<Integer, Collection<L2StratumResult>> getStratumResults() {
        Multimap<Integer, L2StratumResult> result = LinkedHashMultimap.create();
        Integer[] levels = getAllSubstitutionLevels();
        for (Integer level : levels) {
            Set<L2StratumResult> singleResult = Sets.newLinkedHashSet();
            for (L2StratumResult stratumResult : stratumsResult) {
                if (level == stratumResult.getSubstitutionLevel()) {
                    singleResult.add(stratumResult);
                }
            }
            result.putAll(level, singleResult);
        }
        return result.asMap();
    }

    public String getInputCatchStratumLog() {

        WeightCompositionAggregateModel inputCatchModelForSpeciesToFix =
                inputCatchModelForAllSpecies.extractForSpecies(species);

        String title =
                l_(locale, "t3.level2.message.strateInputGlobalComposition.resume");

        String message = WeightCompositionModelHelper.decorateModel(
                getDecoratorService(),
                title,
                inputCatchModelForAllSpecies,
                inputCatchModelForSpeciesToFix
        );
        return message;
    }

    public String getOutputCatchStratumLog() {

        WeightCompositionAggregateModel outputCatchModelForSpeciesToFix =
                outputCatchModelForAllSpecies.extractForSpecies(species);

        String title = l_(locale, "t3.level2.message.strateOutputGlobalComposition.resume");

        String message = WeightCompositionModelHelper.decorateModel(
                getDecoratorService(),
                title,
                outputCatchModelForAllSpecies,
                outputCatchModelForSpeciesToFix
        );
        return message;
    }

    public int getMaximumSizeForStratum() {
        int result = 0;
        for (L2StratumResult stratumResult : stratumsResult) {
            result = Math.max(result, stratumResult.getLibelle().length() + 1);
        }
        return result;
    }

    protected String logCatchWeight(DecoratorService decoratorService,
                                    WeightCompositionAggregateModel modelForAllSpecies,
                                    WeightCompositionAggregateModel modelForSpeciesToFix) {
//
//        WeightCompositionAggregateModel modelForAllSpecies = catchesWeightForAllSpecies.get(activity);
//        WeightCompositionAggregateModel modelForSpeciesToFix =
//                getCatchesWeightForSpeciesToFix(activity);

        String title = l_(locale, "t3.level2.message.activityCatchWeight.resume",
                          modelForAllSpecies.getTotalModel().getTotalWeight(),
                          modelForSpeciesToFix.getTotalModel().getTotalWeight()
        );

        String message = WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                modelForAllSpecies,
                modelForSpeciesToFix
        );
        return message;
    }

    public String logCorrectedCatchWeight(WeightCompositionAggregateModel correctedCatchWeightModel,
                                          DecoratorService decoratorService) {

        WeightCompositionAggregateModel modelForSpeciesToFix =
                correctedCatchWeightModel.extractForSpecies(species);


        String title = l_(locale, "t3.level2.message.activityCorrectedCatchWeight.resume",
                          correctedCatchWeightModel.getTotalModel().getTotalWeight(),
                          modelForSpeciesToFix.getTotalModel().getTotalWeight()
        );

        String message = WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                correctedCatchWeightModel,
                modelForSpeciesToFix
        );
        return message;
    }

    protected String logActivityCatchStratum(WeightCompositionAggregateModel modelForAllSpecies,
                                             WeightCompositionAggregateModel modelForSpeciestoFix,
                                             DecoratorService decoratorService) {

//        WeightCompositionAggregateModel modelForAllSpecies = specificActivityForAllSpeciesModels.get(activity);
//
//        WeightCompositionAggregateModel modelForSpeciestoFix =
//                getSpecificCompositionForSpeciesToFix(activity);

        String title = l_(locale, "t3.level2.message.activityComposition.resume",
                          modelForAllSpecies.getTotalModel().getTotalWeight(),
                          modelForSpeciestoFix.getTotalModel().getTotalWeight()
        );

        String message = WeightCompositionModelHelper.decorateModel(
                decoratorService,
                title,
                modelForAllSpecies,
                modelForSpeciestoFix
        );
        return message;
    }

}
