/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level1;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.data.N1ResultState;
import fr.ird.t3.entities.data.Sample;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntityById;
import org.apache.commons.collections.CollectionUtils;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Abstract level1 action.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractLevel1Action extends T3Action<Level1Configuration> {

    protected final Level1Step step;

    protected final Set<Level1Step> higherSteps;

    @InjectDAO(entityType = Trip.class)
    protected TripDAO tripDAO;

    @InjectEntityById(entityType = Ocean.class)
    protected Ocean ocean;

    /** For a trip, selected samples to treat. */
    protected Multimap<Trip, Sample> samplesByTrip;

    /** for a trip, not selected samples (can not be trat from configuration). */
    protected Multimap<Trip, Sample> notTreatedSamplesByTrip;

    protected AbstractLevel1Action(Level1Step step) {
        this.step = step;
        higherSteps = Level1Step.allAfter(step);
    }

    public Ocean getOcean() {
        return ocean;
    }

    public Multimap<Trip, Sample> getSamplesByTrip() {
        return samplesByTrip;
    }

    public Collection<Trip> getTrips() {
        return samplesByTrip.keySet();
    }

    public Collection<Sample> getSamples(Trip trip) {
        return getSamplesByTrip().get(trip);
    }

    public Multimap<Trip, Sample> getNotTreatedSamplesByTrip() {
        return notTreatedSamplesByTrip;
    }

    public int getNbTripsTreated() {
        int size = samplesByTrip.keySet().size();
        return size;
    }

    public int getNbSamplesTreated() {
        int size = samplesByTrip.size();
        return size;
    }

    public int getNbSamplesNotTreated() {
        int size = notTreatedSamplesByTrip.size();
        return size;
    }

    public Level1Step getStep() {
        return step;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        Level1Configuration config = getConfiguration();
        config.setStep(step);

        Multimap<Trip, Sample> samples = loadMatchingTrips(
                config.getSampleIdsByTripId());
        setSamplesByTrip(samples);

        Multimap<Trip, Sample> notTreatedSamples =
                buildSamplesNotTreated(samples);
        setNotTreatedSamplesByTrip(notTreatedSamples);
    }

    protected void markAsTreated(N1ResultState entity) {

        // let's validate this step
        step.setState(entity, true);

        // and invalidate all other following steps
        Level1Step.invalidate(entity, higherSteps);
    }

    protected void logTreatedAndNotSamplesforATrip(Trip trip, Collection<Sample> samples) {

        String tripStr = decorate(trip, DecoratorService.WITH_ID);

        String message = l_(locale, "t3.level1.info.treat.trip", tripStr,
                            samples.size());
        addInfoMessage(message);
        Collection<Sample> notTreatedSamples =
                getNotTreatedSamplesByTrip().get(trip);
        if (CollectionUtils.isNotEmpty(notTreatedSamples)) {
            message = l_(locale, "t3.level1.warning.skip.samples",
                         notTreatedSamples.size(), tripStr);
            for (Sample sample : notTreatedSamples) {
                String reason = getConfiguration().getNotTreatedSampleReason(sample);
                message += "\n" + l_(locale, "t3.level1.warning.skip.sample",
                                     sample.getSampleNumber(), sample.getTopiaId(), reason);
            }
            addWarningMessage(message);
        }
    }

    public void setSamplesByTrip(Multimap<Trip, Sample> samplesByTrip) {
        this.samplesByTrip = samplesByTrip;
    }

    public void setNotTreatedSamplesByTrip(Multimap<Trip, Sample> notTreatedSamplesByTrip) {
        this.notTreatedSamplesByTrip = notTreatedSamplesByTrip;
    }

    protected Multimap<Trip, Sample> buildSamplesNotTreated(Multimap<Trip, Sample> samplesByTrip) {
        Multimap<Trip, Sample> result = ArrayListMultimap.create();

        for (Trip trip : samplesByTrip.keySet()) {

            Collection<Sample> allSamples = Lists.newArrayList(trip.getSample());
            allSamples.removeAll(samplesByTrip.get(trip));
            result.putAll(trip, allSamples);
        }
        return result;
    }

    protected Multimap<Trip, Sample> loadMatchingTrips(Multimap<String, String> ids) throws TopiaException {

        Multimap<Trip, Sample> result = ArrayListMultimap.create();

        for (String tripId : ids.keySet()) {
            Collection<String> sampleIds = ids.get(tripId);
            Trip trip = tripDAO.findByTopiaId(tripId);
            for (Sample sample : trip.getSample()) {
                if (sampleIds.contains(sample.getTopiaId())) {
                    result.put(trip, sample);
                }
            }
        }
        return result;
    }
}
