/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import fr.ird.t3.actions.T3ActionConfiguration;
import fr.ird.t3.io.input.T3InputProvider;

import java.io.File;

/**
 * Common Input source action configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface InputSourceConfiguration extends T3ActionConfiguration {

    boolean isUseWells();

    void setUseWells(boolean useWells);

    boolean isSamplesOnly();

    void setSamplesOnly(boolean samplesOnly);

    boolean isCanCreateVessel();

    void setCanCreateVessel(boolean canCreateVessel);

    boolean isCreateVirtualVessel();

    void setCreateVirtualVessel(boolean createVirtualVessel);

    T3InputProvider getInputProvider();

    void setInputProvider(T3InputProvider inputProvider);

    File getInputFile();

    void setInputFile(File inputFile);

}
