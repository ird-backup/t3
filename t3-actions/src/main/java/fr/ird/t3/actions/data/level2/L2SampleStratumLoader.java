/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level2;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ird.t3.actions.stratum.SampleStratumLoader;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.SchoolType;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.List;
import java.util.Set;

/**
 * Base level 2 sample stratum loader.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public abstract class L2SampleStratumLoader extends SampleStratumLoader<Level2Configuration, Level2Action, L2SampleStratum> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(L2SampleStratumLoader.class);

    /**
     * The minimum sample total weight to obtain the correct substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration
     * and catch stratum total weight.
     *
     * @since 1.4
     */
    private final float minimumSampleTotalWeight;

    /**
     * The minimum sample count required to obtain the correct
     * substitution level.
     * <p/>
     * <strong>Note: </strong>This value comes from the stratum configuration.
     */
    private final int minimumSampleCount;

    @InjectDAO(entityType = Activity.class)
    protected ActivityDAO activityDAO;

    public L2SampleStratumLoader(L2SampleStratum sampleStratum) {
        super(sampleStratum);


        Level2Configuration configuration =
                sampleStratum.getLevelConfiguration();

        // compute minimumSampleTotalWeight

        float catchStratumTotalWeight = sampleStratum.getCatchStratumTotalWeight();
        float maximumWeightRatio = configuration.getStratumWeightRatio();

        minimumSampleTotalWeight = catchStratumTotalWeight / maximumWeightRatio;

        SchoolType schoolType =
                sampleStratum.getConfiguration().getSchoolType();

        int code = schoolType.getCode();
        switch (code) {
            case 1:

                // object School type
                minimumSampleCount =
                        configuration.getStratumMinimumSampleCountObjectSchoolType();
                break;
            case 2:

                // free school type
                minimumSampleCount =
                        configuration.getStratumMinimumSampleCountFreeSchoolType();
                break;
            default:
                throw new IllegalStateException(
                        "Can not deal with school type with a code = " + code);
        }
    }

    @Override
    protected Set<String> findActivityIds(String schoolTypeId,
                                          T3Date beginDate,
                                          T3Date endDate,
                                          String... zoneIds) throws TopiaException {

        StratumConfiguration<Level2Configuration> configuration =
                getSampleStratum().getConfiguration();

        Set<String> result = Sets.newHashSet();

        for (String zoneId : zoneIds) {

            // on commence par récupérer les activités :
            // - dans la zone ET
            // - avec le bon type de banc
            // - dans la bonne période de temps

            List<String> activityIds = activityDAO.findAllActivityIdsForSampleStratum(
                    configuration.getZoneTableName(),
                    zoneId,
                    schoolTypeId,
                    beginDate,
                    endDate
            );

            result.addAll(activityIds);
        }
        return result;
    }

    @Override
    protected Set<Activity> filterActivities(Set<String> activityIds) throws TopiaException {

        Set<Activity> result = Sets.newHashSet();

        StratumConfiguration<Level2Configuration> configuration =
                getSampleStratum().getConfiguration();

        Set<Vessel> possibleVessels =
                configuration.getPossibleSampleVessels();

        for (String activityId : activityIds) {
            Activity activity = configuration.getActivity(activityId);

            // recheck activity have some samples.
            Preconditions.checkState(
                    !activity.isSetSpeciesCatWeightEmpty(),
                    "Can not accept an activity (" + activity.getTopiaId() +
                    ") with no sample");

            // get his trip
            Trip trip = activity.getTrip();
            if (!possibleVessels.contains(trip.getVessel())) {

                // not a matching boat
                continue;
            }

            result.add(activity);
        }
        return result;
    }

    @Override
    public boolean isStratumOk() {

        // test if we have enough sample weight

        L2SampleStratum sampleStratum = getSampleStratum();

        float sampleStratumTotalWeight = sampleStratum.getSampleStratumTotalWeight();
        if (sampleStratumTotalWeight < minimumSampleTotalWeight) {

            // need more sample weight
            if (log.isInfoEnabled()) {
                log.info("Missing some weight (got: " + sampleStratumTotalWeight + ", but required minimum: " + minimumSampleTotalWeight + ").");
            }
            return false;
        }

        // test if we have enough sample count

        int sampleStratumTotalCount = sampleStratum.getSampleStratumTotalCount();
        if (sampleStratumTotalCount < minimumSampleCount) {

            // need more fishes count
            if (log.isInfoEnabled()) {
                log.info("Missing fishes count (got: " + sampleStratumTotalCount + ", but required minimum: " + minimumSampleCount + ").");
            }
            return false;
        }

        // ok the stratum is fine
        return true;
    }
}