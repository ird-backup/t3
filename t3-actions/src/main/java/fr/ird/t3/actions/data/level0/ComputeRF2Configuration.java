/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import fr.ird.t3.entities.reference.Harbour;

import java.util.List;

/**
 * Configuration of a compute RF2 action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see ComputeRF2Action
 * @since 1.0
 */
public class ComputeRF2Configuration extends AbstractLevel0Configuration {

    private static final long serialVersionUID = 1L;

    /**
     * All landing harbours used in trips (index are ids, values are string
     * representation of a landing harbour.
     */
    protected List<Harbour> landingHarbours;

    /** Selected landingHarbour ids. */
    protected List<String> landingHarbourIds;

    public ComputeRF2Configuration() {
        super(Level0Step.COMPUTE_RF2);
    }

    public boolean isConfigurationEmpty() {

        return (landingHarbourIds == null || landingHarbourIds.isEmpty())
               && (vesselSimpleTypeIds == null || vesselSimpleTypeIds.isEmpty())
               && (fleetIds == null || fleetIds.isEmpty());

    }

    public List<Harbour> getLandingHarbours() {
        return landingHarbours;
    }

    public void setLandingHarbours(List<Harbour> landingHarbours) {
        this.landingHarbours = landingHarbours;
    }

    public List<String> getLandingHarbourIds() {
        return landingHarbourIds;
    }

    public void setLandingHarbourIds(List<String> landingHarbourIds) {
        this.landingHarbourIds = landingHarbourIds;
    }

}
