/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ird.t3.entities.data.CompleteTrip;
import fr.ird.t3.entities.data.RaisingFactor2;
import fr.ird.t3.entities.data.RaisingFactor2DAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.Harbour;
import fr.ird.t3.entities.reference.HarbourDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselSimpleType;
import fr.ird.t3.entities.reference.VesselSimpleTypeDAO;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectEntitiesById;
import fr.ird.t3.services.ioc.InjectFromDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Action to compute Raising Factor 2 on each trip and fill the
 * {@link RaisingFactor2} table.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeRF2Action extends AbstractLevel0Action<ComputeRF2Configuration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeRF2Action.class);

    @InjectDAO(entityType = RaisingFactor2.class)
    protected RaisingFactor2DAO raisingFactor2DAO;

    @InjectEntitiesById(entityType = Harbour.class)
    protected List<Harbour> landingHarbours;

    @InjectFromDAO(entityType = Species.class)
    protected List<Species> species;

    @InjectDAO(entityType = Harbour.class)
    protected HarbourDAO harbourDAO;

    @InjectDAO(entityType = Country.class)
    protected CountryDAO countryDAO;

    @InjectDAO(entityType = VesselSimpleType.class)
    protected VesselSimpleTypeDAO vesselSimpleTypeDAO;

    /** usable complete trips group by vessel */
    protected ListMultimap<Vessel, CompleteTrip> completeTripsByVessel;

    protected int nbStratums;

    protected int nbTripsWithRF2;

    public ComputeRF2Action() {
        super(Level0Step.COMPUTE_RF2);
    }

    public int getNbStratums() {
        return nbStratums;
    }

    public int getNbTripsWithRF2() {
        return nbTripsWithRF2;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        boolean configurationEmpty = getConfiguration().isConfigurationEmpty();

        if (configurationEmpty) {

            // use all landingHarbours
            landingHarbours = harbourDAO.findAll();

            // use all vesselSimpleTypes
            vesselSimpleTypes = vesselSimpleTypeDAO.findAll();

            // use all fleets
            fleets = countryDAO.findAll();
        }

        List<Trip> tripList = getUsableTrips(landingHarbours, true);
        setTrips(tripList);

        completeTripsByVessel = ArrayListMultimap.create();

        // get all trips group by the vessel
        ListMultimap<Vessel, Trip> tripsByVessel = TripDAO.groupByVessel(tripList);

        // compute for each vessel list of complete trip
        for (Vessel vessel : tripsByVessel.keys()) {

            List<Trip> tripsForVessel =
                    Lists.newArrayList(tripsByVessel.get(vessel));
            TripDAO.sortTrips(tripsForVessel);

            // get all complete trips
            List<CompleteTrip> completeTrips = tripDAO.toCompleteTrip(tripsForVessel);
            completeTripsByVessel.putAll(vessel, completeTrips);
        }

    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        boolean result = false;

        if (CollectionUtils.isNotEmpty(trips)) {

            setNbSteps(landingHarbours.size() *
                       fleets.size() *
                       vesselSimpleTypes.size()
            );

            boolean computeRf2 = !getConfiguration().isConfigurationEmpty();

            for (Harbour harbour : landingHarbours) {

                String harbourStr = harbour.getLibelle();
                for (Country fleet : fleets) {
                    String countryStr = fleet.getLibelle();
                    for (VesselSimpleType vesselSimpleType : vesselSimpleTypes) {

                        incrementsProgression();

                        // get all trip usables for this stratum
                        Collection<Vessel> stratumVessels =
                                getStratumVessel(fleet, vesselSimpleType);

                        String vesselSimpleTypeStr = vesselSimpleType.getLibelle();

                        // obtain all trips for harbour / fleet / vesselSimpleType

                        List<CompleteTrip> completeTrips = getStratumCompleteTrips(
                                stratumVessels, harbour);

                        if (log.isDebugEnabled()) {
                            log.debug("For [" + harbourStr + "/" + countryStr +
                                      "/" + vesselSimpleTypeStr + "] nb trips = " +
                                      completeTrips.size());
                        }
                        if (completeTrips.isEmpty()) {

                            // no trip for this stratum

                            continue;
                        }
                        Multimap<T3Date, CompleteTrip> tripsByMonth =
                                TripDAO.splitTripsByMonth(completeTrips);

                        if (log.isDebugEnabled()) {
                            log.debug("found " + tripsByMonth.size() + " months.");
                        }

                        for (T3Date month : tripsByMonth.keySet()) {
                            Collection<CompleteTrip> strateTrips = tripsByMonth.get(month);

                            nbStratums++;

                            String message = l_(locale, "t3.level0.computeRF2.nbTrips.for.stratum",
                                                strateTrips.size(),
                                                nbStratums,
                                                harbourStr,
                                                countryStr,
                                                vesselSimpleTypeStr,
                                                month
                            );
                            if (log.isInfoEnabled()) {
                                log.info(message);
                            }
                            addInfoMessage(message);

                            executeForStrate(harbour,
                                             fleet,
                                             vesselSimpleType,
                                             month,
                                             species,
                                             strateTrips,
                                             computeRf2
                            );
                        }
                    }
                }
            }

            result = true;
        }

        return result;
    }

    protected Collection<Vessel> getStratumVessel(Country fleet,
                                                  VesselSimpleType vesselSimpleType) {
        Set<Vessel> result = Sets.newHashSet();
        for (Vessel vessel : completeTripsByVessel.keys()) {
            if (vesselSimpleType.equals(vessel.getVesselType().getVesselSimpleType()) &&
                fleet.equals(vessel.getFleetCountry())) {
                result.add(vessel);
            }
        }
        return result;
    }

    protected List<CompleteTrip> getStratumCompleteTrips(Collection<Vessel> vessels,
                                                         Harbour harbour) {

        List<CompleteTrip> result = Lists.newArrayList();

        for (Vessel vessel : vessels) {

            // get all complete trips
            List<CompleteTrip> completeTrips = completeTripsByVessel.get(vessel);
            Iterator<CompleteTrip> itr = completeTrips.iterator();
            while (itr.hasNext()) {
                CompleteTrip completeTrip = itr.next();
                Trip trip = completeTrip.getLandingTrip();
                if (harbour.equals(trip.getLandingHarbour())) {

                    // keep it for this stratum
                    result.add(completeTrip);

                    // complete trip no more usable for another stratum
                    itr.remove();
                }
            }
        }
        return result;
    }

    protected void executeForStrate(Harbour harbour,
                                    Country fleet,
                                    VesselSimpleType vesselSimpleType,
                                    T3Date month,
                                    List<Species> species,
                                    Collection<CompleteTrip> trips,
                                    boolean computeRf2) throws TopiaException {

        Date date = month.toBeginDate();

        // get rf2 (if already exists)
        RaisingFactor2 raisingFactor = raisingFactor2DAO.findByProperties(
                RaisingFactor2.PROPERTY_COUNTRY, fleet,
                RaisingFactor2.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType,
                RaisingFactor2.PROPERTY_HARBOUR, harbour,
                RaisingFactor2.PROPERTY_MONTH, date
        );

        boolean rFCreated = false;

        if (raisingFactor == null) {

            // creates it, beacause was not already found
            raisingFactor = raisingFactor2DAO.create(
                    RaisingFactor2.PROPERTY_COUNTRY, fleet,
                    RaisingFactor2.PROPERTY_VESSEL_SIMPLE_TYPE, vesselSimpleType,
                    RaisingFactor2.PROPERTY_HARBOUR, harbour,
                    RaisingFactor2.PROPERTY_MONTH, date
            );

            rFCreated = true;
        }

        // do the rf2 strate computation
        float rf2 = computeRf2 ? computeRF2ForStrate(trips, species) : 1f;

        addInfoMessage(
                l_(locale, "t3.level0.computeRF2.computed.rf2.for.stratum", rf2)
        );

        // assign rf2 value to the entity
        raisingFactor.setRaisingFactorValue(rf2);

        if (!rFCreated) {

            // must update it
            raisingFactor2DAO.update(raisingFactor);
        }

        if (rf2 != 1) {

            nbTripsWithRF2 += trips.size();
        }

        // set rf2 to all trips
        for (CompleteTrip trip : trips) {
            String tripStr = decorate(trip);
            String message = l_(locale, "t3.level0.computeRF1.resume.rf2.for.trip",
                                tripStr, rf2);
            if (log.isInfoEnabled()) {
                log.info(message);
            }
            addInfoMessage(message);

            trip.applyRf2(rf2);
            markTripAsTreated(trip);
        }
    }

    protected float computeRF2ForStrate(Collection<CompleteTrip> trips,
                                        List<Species> species) {

        float totalLandingWeight = 0f;
        float totalCatchWeigth = 0f;

        for (CompleteTrip trip : trips) {

            float tripLandingWeight = trip.getElementaryLandingTotalWeight(species);
            totalLandingWeight += tripLandingWeight;

            String tripStr = decorate(trip, DecoratorService.WITH_ID);

            if (tripLandingWeight > 0) {

                // there is some landing
                float tripCatchWeight;
                if (trip.getRf1() == null) {
                    tripCatchWeight = trip.getElementaryCatchTotalWeight(species);
                } else {
                    tripCatchWeight = trip.getElementaryCatchTotalWeightRf1(species);
                }
                totalCatchWeigth += tripCatchWeight;

                addInfoMessage(l_(locale, "t3.level0.computeRF2.resume.rf1.for.trip",
                                  tripStr, tripCatchWeight, tripLandingWeight));
            } else {

                // this trip does not have any landing catches for given species
                addWarningMessage(
                        l_(locale, "t3.level0.computeRF2.resume.skip.for.trip",
                           tripStr));
            }
        }
        addInfoMessage(l_(locale, "t3.level0.computeRF2.resume.total.rf1",
                          totalCatchWeigth, totalLandingWeight));

        float rf2 = 1;
        if (totalCatchWeigth > 0) {
            rf2 = totalLandingWeight / totalCatchWeigth;
        }

        if (log.isDebugEnabled()) {
            log.debug("Computed rf2 " + rf2);
        }
        return rf2;
    }

    protected void markTripAsTreated(CompleteTrip trip) {

        for (Trip trip1 : trip) {
            markTripAsTreated(trip1);
        }
    }

}
