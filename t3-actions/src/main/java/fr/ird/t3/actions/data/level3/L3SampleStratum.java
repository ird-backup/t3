/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level3;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import fr.ird.t3.T3IOUtil;
import fr.ird.t3.actions.stratum.SampleStratum;
import fr.ird.t3.actions.stratum.StratumConfiguration;
import fr.ird.t3.entities.T3EntityHelper;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.T3Suppliers;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.SetSpeciesCatWeight;
import fr.ird.t3.entities.data.SetSpeciesFrequency;
import fr.ird.t3.entities.data.SetSpeciesFrequencyDAOImpl;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.WeightCategoryTreatment;
import fr.ird.t3.models.LengthCompositionAggregateModel;
import fr.ird.t3.services.T3ServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.decorator.Decorator;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l_;

/**
 * Sample stratum for level 3 treatment.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.2
 */
public class L3SampleStratum extends SampleStratum<Level3Configuration, Level3Action, L3SampleStratum> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(L3SampleStratum.class);

    /**
     * Total weight of the catch stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    protected final float catchStratumTotalWeight;

    /**
     * Total weight of the sample stratum.
     * <p/>
     * This is needed to test sample stratum quality when loading data.
     */
    protected float sampleStratumTotalWeight;

    /**
     * Keep the count of fishes for all species of the stratum.
     *
     * @since 1.3.1
     */
    private final Map<Species, Float> speciesCount;

    /**
     * Contains for each species his composition to apply if a set does not
     * have any sample.
     *
     * @since 1.3.1
     */
    private final Map<Species, LengthCompositionAggregateModel> compositionModel;

    /**
     * Contains for all activities of the stratum their setspeciesFrequencies
     * grouped by species.
     *
     * @since 1.3.1
     */
    private final Multimap<Species, SetSpeciesFrequency> allSetSpeciesFrequenciesBySpecies;

    /**
     * Conversion helper.
     *
     * @since 1.3.1
     */
    private final LengthWeightConversionHelper conversionHelper;

    /**
     * Predicate to filter only species selected in configuration.
     *
     * @see #getSpeciesToFix()
     * @since 1.4
     */
    private final Predicate<SetSpeciesCatWeight> speciesToFixFilter;

    public L3SampleStratum(StratumConfiguration<Level3Configuration> configuration,
                           Collection<Species> speciesToFix,
                           float catchStratumTotalWeight,
                           LengthWeightConversionHelper conversionHelper,
                           Predicate speciesToFixFilter) {
        super(configuration, speciesToFix);
        this.catchStratumTotalWeight = catchStratumTotalWeight;
        this.conversionHelper = conversionHelper;
        speciesCount = Maps.newHashMap();
        compositionModel = Maps.newHashMap();
        allSetSpeciesFrequenciesBySpecies = ArrayListMultimap.create();
        this.speciesToFixFilter = speciesToFixFilter;
    }

    @Override
    public void close() throws IOException {
        super.close();
        LengthCompositionAggregateModel.close(compositionModel);
        speciesCount.clear();
        compositionModel.clear();
        allSetSpeciesFrequenciesBySpecies.clear();
    }

    @Override
    protected L3SampleStratumLoader newLoader() {

        int oceanCode = getConfiguration().getZone().getOcean().getCode();

        L3SampleStratumLoader result;
        switch (oceanCode) {
            case 1:
                result = new L3SampleStratumLoaderAtlantic(this);
                break;
            case 2:
                result = new L3SampleStratumLoaderIndian(this);
                break;
            default:
                throw new IllegalStateException(
                        "Not implemented for ocean with code " + oceanCode);
        }
        return result;
    }

    @Override
    public void mergeNewActivities(T3ServiceContext serviceContext,
                                   Set<Activity> activities) throws TopiaException {

        for (Activity activity : activities) {

            // add total weight of samples of the activity (only for selected species)
            float totalWeight = T3EntityHelper.getTotal(
                    activity.getSetSpeciesCatWeight(),
                    T3Functions.SET_SPECIES_CAT_WEIGHT_TO_WEIGHT,
                    speciesToFixFilter,
                    T3Suppliers.newActivityDecorateSupplier(
                            serviceContext,
                            activity,
                            "Found a trip %s - activity %s with a null SetSpeciesCatWeight#weight"));

            sampleStratumTotalWeight += totalWeight;

            // split activity frequencies by species
            Multimap<Species, SetSpeciesFrequency> setSpeciesFrequenciesBySpecies =
                    Multimaps.index(activity.getSetSpeciesFrequency(),
                                    T3Functions.SPECIES_AWARE_BY_SPECIES);

            // get species found (and only the one to use)
            Set<Species> speciesFound = Sets.newHashSet(setSpeciesFrequenciesBySpecies.keySet());
            speciesFound.retainAll(getSpeciesToFix());

            // add missing empty model
            T3IOUtil.fillMapWithDefaultValue(
                    speciesCount, speciesFound,
                    T3Suppliers.FLOAT_DEFAULT_VALUE
            );

            // store it in stratum (to be reused to compute composition model)
            // for each species compute his total count for any length class (used by level substitution)

            for (Species species : speciesFound) {

                Collection<SetSpeciesFrequency> frequencies =
                        setSpeciesFrequenciesBySpecies.get(species);

                allSetSpeciesFrequenciesBySpecies.putAll(species, frequencies);

                float newCount = SetSpeciesFrequencyDAOImpl.collectSimpleSampleCount(frequencies);
                Float oldCount = speciesCount.get(species);
                speciesCount.put(species, oldCount + newCount);
            }
        }

        addMergedActivitesCount(activities.size());

        if (log.isInfoEnabled()) {
            log.info("sampleStratumTotalWeight = " +
                     getSampleStratumTotalWeight());
        }
    }

    @Override
    public void init(T3ServiceContext serviceContext,
                     List<WeightCategoryTreatment> weightCategories,
                     Level3Action messager) throws Exception {
        super.init(serviceContext, weightCategories, messager);

        // compute the global compositionModel

        LengthCompositionAggregateModel.buildCompositionModel(
                allSetSpeciesFrequenciesBySpecies,
                compositionModel,
                weightCategories,
                conversionHelper
        );
    }

    public float getCatchStratumTotalWeight() {
        return catchStratumTotalWeight;
    }

    public float getSampleStratumTotalWeight() {
        return sampleStratumTotalWeight;
    }

    public float getSpeciesTotalCount(Species species) {
        checkInitMethodInvoked(speciesCount);
        float result = speciesCount.get(species);
        return result;
    }

    public Collection<Species> getSampleSpecies() {
        checkInitMethodInvoked(speciesCount);
        return speciesCount.keySet();
    }

    public Map<Species, LengthCompositionAggregateModel> getCompositionModel() {
        checkInitMethodInvoked(compositionModel);
        return compositionModel;
    }

    @Override
    public String logSampleStratumLevel(int substitutionLevel,
                                        Level3Action messager) {

        Locale l = messager.getLocale();

        String title = l_(l, "t3.level3.sampleStratum.resume.for.level",
                          substitutionLevel,
                          // Do not use #getNbActivites() since it is perharp not yet setted.
                          // Use this internal state instead of the #getNbActivities() since activites can still not be setted in the stratum
                          // Fix http://forge.codelutin.com/issues/1907
                          getNbMergedActivities(),
                          getSampleStratumTotalWeight()
        );

        Decorator<Species> decorator = messager.getDecoratorService().getDecorator(l, Species.class, null);

        Set<Species> speciesToFix = getSpeciesToFix();
        StringBuilder sb = new StringBuilder(title);
        for (Map.Entry<Species, Float> entry : speciesCount.entrySet()) {
            Species species = entry.getKey();
            if (speciesToFix.contains(species)) {
                sb.append(l_(l, "t3.level3.sampleStratum.resume.for.level.and.species", decorator.toString(species), entry.getValue()));
            }
        }
        return sb.toString();
    }
}
