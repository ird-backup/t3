/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.data.level0;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.web_tomorrow.utils.suntimes.SunTimes;
import fr.ird.t3.entities.data.Activity;
import fr.ird.t3.entities.data.ActivityDAO;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.VesselActivity;
import fr.ird.t3.entities.type.T3Point;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.DateUtil;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.l_;

/**
 * Compute for each given trip the efforts of it.
 * <p/>
 * <strong>Note:</strong> to make this action, all trips must have a computed
 * rf2.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ComputeTripEffortsAction extends AbstractLevel0Action<ComputeTripEffortsConfiguration> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeTripEffortsAction.class);

    /**
     * Les codes des activités surlequel on ne calcule pas d'effort de pêche.
     *
     * @since 2.0
     */
    protected static final ImmutableSet<Integer> VESSEL_ACTIVITY_CODES_TO_SKIP = ImmutableSet.of(
            4,  // route sans veille
            7,  // avarie
            8,  // à la cape
            10, // en attente
            15  // au port
    );

    protected float totalTimeAtSeaN0;

    protected float totalFishingTimeN0;

    protected float totalSearchTimeN0;

    public ComputeTripEffortsAction() {
        super(Level0Step.COMPUTE_TRIP_EFFORTS);
    }

    public float getTotalTimeAtSeaN0() {
        return totalTimeAtSeaN0;
    }

    public float getTotalFishingTimeN0() {
        return totalFishingTimeN0;
    }

    public float getTotalSearchTimeN0() {
        return totalSearchTimeN0;
    }

    @Override
    protected void prepareAction() throws Exception {

        super.prepareAction();

        List<Trip> tripList = getUsableTrips(null, true);
        setTrips(tripList);
    }

    @Override
    protected void deletePreviousData() {
        // do not delete data here (done for all level 0 at first step)
    }

    @Override
    protected boolean executeAction() throws Exception {

        boolean result = false;

        if (CollectionUtils.isNotEmpty(trips)) {

            setNbSteps(3 * trips.size());

            // do action for each data using the prepared action context

            for (Trip trip : trips) {

                result |= executeForTrip(trip);
            }
        }
        return result;
    }

    protected boolean executeForTrip(Trip trip) throws TopiaException {

        // compute time at sea
        float timeAtSea = computeTimeAtSea(trip);
        totalTimeAtSeaN0 += timeAtSea;

        trip.setComputedTimeAtSeaN0(timeAtSea);
        incrementsProgression();

        // compute fishing time
        float fishingTime = computeFishingTime(trip);
        totalFishingTimeN0 += fishingTime;
        trip.setComputedFishingTimeN0(fishingTime);
        incrementsProgression();

        // compute search time
        float searchTime = computeTripSearchTime(trip, fishingTime);
        totalSearchTimeN0 += searchTime;
        trip.setComputedSearchTimeN0(searchTime);

        incrementsProgression();

        String message = l_(locale, "t3.level0.computeTripEffort",
                decorate(trip),
                trip.getComputedTimeAtSeaN0(),
                trip.getComputedFishingTimeN0(),
                trip.getComputedSearchTimeN0()
        );
        addInfoMessage(message);
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        markTripAsTreated(trip);
        return true;
    }

    protected float computeTimeAtSea(Trip trip) {

        float setTimeAtSea;

        // date depart - date dpq = nb jours
        // result = nb heures entre le départ et l'arrivée
        Calendar calendarDep = DateUtils.toCalendar(trip.getDepartureDate());
        calendarDep.set(Calendar.MINUTE, 0);
        calendarDep.set(Calendar.SECOND, 0);
        calendarDep.set(Calendar.MILLISECOND, 0);
        Calendar calendarLanding = DateUtils.toCalendar(trip.getLandingDate());
        calendarLanding.set(Calendar.MINUTE, 0);
        calendarLanding.set(Calendar.SECOND, 0);
        calendarLanding.set(Calendar.MILLISECOND, 0);

        int months = DateUtil.getDifferenceInHours(calendarDep.getTime(), calendarLanding.getTime());
        setTimeAtSea = months;

        if (log.isDebugEnabled()) {
            log.debug(decorate(trip) + " : time at sea = " + setTimeAtSea);
        }
        return setTimeAtSea;
    }

    protected float computeFishingTime(Trip trip) throws TopiaException {

        float setFishingTime = 0;

        if (!trip.isActivityEmpty()) {

            Multimap<Long, Activity> activitiesByDay = ActivityDAO.groupActivitiesByDay(trip.getActivity());

            List<Long> daysIds = Lists.newArrayList(activitiesByDay.keySet());
            Collections.sort(daysIds);

            long firstDay = daysIds.get(0);
            long lastDay = Iterables.getLast(daysIds);

            for (Long dayId : daysIds) {

                Collection<Activity> activities = activitiesByDay.get(dayId);

                if (dayId == firstDay || lastDay == dayId) {
                    float currentFishingTime = computeSetFishingTimeForFirstOrLayDay(activities);
                    setFishingTime += currentFishingTime;
                    continue;
                }

                Date date = new Date(dayId);
                if (!canComputeSetFishingTime(activities)) {

                    String message = l_(locale, "t3.level0.computeTripEffortSkipForDay", decorate(trip), date);
                    addInfoMessage(message);
                    if (log.isInfoEnabled()) {
                        log.info(message);
                    }

                    continue;
                }


                T3Point point;

                if (activities.size() == 1) {

                    // only one activity for this day,
                    Activity activity = activities.iterator().next();
                    point = activity.toPoint();

                    if (log.isDebugEnabled()) {
                        log.debug("Day [" + date + "] , single activity,  point : " + point);
                    }
                } else {

                    // compute barycenter for all activities of the day
                    point = tripDAO.getBarycenterForActivitiesOfADay(trip, date);

                    if (log.isDebugEnabled()) {
                        log.debug("Day [" + date + "] Computed barycenter point " + point);
                    }
                }

                // compute day duration for this point and day (in minutes)
                double duration = SunTimes.getDayDurationInMonths(date, point.getX(), point.getY());

                if (log.isInfoEnabled()) {
                    log.info("Day [" + date + "] duration = " + duration);
                }
                // add it to setTime
                setFishingTime += duration;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(decorate(trip) + " - fishing time = " + setFishingTime);
        }

        return setFishingTime;
    }

    protected float computeTripSearchTime(Trip trip, float fishingTime) {

        // compute sum of catches time
        float catchesTime = trip.getTotalSetsDuration();

        if (log.isDebugEnabled()) {
            log.debug("Total catches time = " + catchesTime);
        }
        float result = fishingTime - catchesTime;

        if (log.isDebugEnabled()) {
            log.debug(decorate(trip) + " - trip search time = " + result);
        }
        return result;
    }

    protected float computeSetFishingTimeForFirstOrLayDay(Collection<Activity> activities) {

        float result = 0f;
        for (Activity activity : activities) {
            result += activity.getFishingTime();
        }
        return result;

    }

    protected boolean canComputeSetFishingTime(Collection<Activity> activities) {
        boolean result = false;
        for (Activity activity : activities) {
            VesselActivity vesselActivity = activity.getVesselActivity();
            if (!VESSEL_ACTIVITY_CODES_TO_SKIP.contains(vesselActivity.getCode())) {
                result = true;
                break;
            }
        }
        return result;
    }

}
