/*
 * #%L
 * T3 :: Actions
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.actions.io.input;

import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3Action;
import fr.ird.t3.entities.T3DAOHelper;
import fr.ird.t3.entities.data.T3DataEntity;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Vessel;
import fr.ird.t3.entities.reference.VesselDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

import java.util.Date;
import java.util.Set;
import java.util.Stack;

import static org.nuiton.i18n.I18n.l_;

/**
 * Action to import incoming trips (which coming from input source) into a
 * real T3 database.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ImportInputSourceAction extends T3Action<ImportInputSourceConfiguration> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImportInputSourceAction.class);

    public static final String PARAM_TRIPS_TO_IMPORT = "tripsToImport";

    public static final String PARAM_TRIPS_TO_DELETE = "tripsToDelete";

    protected Set<Trip> trips;

    protected Set<Trip> tripToDelete;

    @InjectDAO(entityType = Trip.class)
    protected TripDAO tripDAO;

    @InjectDAO(entityType = Vessel.class)
    protected VesselDAO vesselDAO;

    @Override
    protected void prepareAction() throws Exception {
        super.prepareAction();

        trips = getConfiguration().getTripsToImport();
        tripToDelete = getConfiguration().getTripsToDelete();
    }

    @Override
    protected void deletePreviousData() {
        //TODO Remove data
    }

    @Override
    protected boolean executeAction() throws Exception {

        if (CollectionUtils.isEmpty(trips)) {

            // no trips at all to import
            // no commit to do, skip the action
            return false;
        }

        int nbSteps = 0;
        if (CollectionUtils.isNotEmpty(trips)) {
            nbSteps += trips.size();
        }
        if (CollectionUtils.isNotEmpty(tripToDelete)) {
            nbSteps += tripToDelete.size();
        }

        setNbSteps(nbSteps);
        if (log.isInfoEnabled()) {
            log.info("Nb steps : " + getNbSteps());
        }
        if (CollectionUtils.isNotEmpty(tripToDelete)) {

            // delete trips which will be replaced

            for (Trip trip : tripToDelete) {

                incrementsProgression();

                String tripLabel = trip.getVessel().getLibelle() + " - " +
                                   trip.getLandingDate();

                if (log.isInfoEnabled()) {
                    String message = "Delete old trip " + tripLabel;
                    log.info(message);
                }

                // get trip to delete from this session
                Trip aTripToDelete = tripDAO.findByTopiaId(trip.getTopiaId());

                tripDAO.delete(aTripToDelete);

                String message = l_(locale, "t3.message.trip.was.deleted",
                                    tripLabel);
                addInfoMessage(message);
            }
        }

        boolean createVirtualVessel =
                getConfiguration().isCreateVirtualVessel();

        BuildVisitor builder = new BuildVisitor(getTransaction());

        for (Trip trip : trips) {

            incrementsProgression();

            Vessel vessel = trip.getVessel();

            String tripLabel = vessel.getLibelle() + " - " +
                               trip.getLandingDate();

            if (log.isInfoEnabled()) {
                String message = "Import trip " + tripLabel;
                log.info(message);
            }

            if (!vesselDAO.existByProperties(Vessel.PROPERTY_CODE, vessel.getCode())) {

                // will create the vessel

                String message = l_(locale, "t3.message.create.new.vessel",
                                    decorate(vessel));
                if (log.isInfoEnabled()) {
                    log.info(message);
                }
                addInfoMessage(message);

                vessel.setVesselVirtual(createVirtualVessel);

                Vessel createdVessel = vesselDAO.create(vessel);
                trip.setVessel(createdVessel);
            }
            builder.acceptEntity(trip);

            String message = l_(locale, "t3.message.trip.was.imported",
                                tripLabel);
            addInfoMessage(message);

            // clean the builder after each entity (since each entity is
            // standalone and does not share data with other trips)
            builder.clear();
        }

        // some trips were imported must commit them
        return true;
    }

    protected static class BuildVisitor implements EntityVisitor {

        protected Set<String> ids = Sets.newHashSet();

        protected Stack<TopiaEntity> stack = new Stack<TopiaEntity>();

        protected TopiaEntity current;

        protected final TopiaContext tx;

        public BuildVisitor(TopiaContext tx) {
            this.tx = tx;
        }

        protected void acceptEntity(TopiaEntity entity) {

            if (StringUtils.isEmpty(entity.getTopiaId()) ||
                !ids.contains(entity.getTopiaId())) {

                // first time we want to accept the entity
                try {
                    entity.accept(this);
                } catch (TopiaException e) {
                    throw new IllegalStateException(
                            "Could not visit entity " + entity, e);
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Entity already visited " + entity.getTopiaId());
                }
            }
        }

        @Override
        public void start(TopiaEntity entity) {

            String id = entity.getTopiaId();

            if (StringUtils.isEmpty(id)) {
                Class<? extends TopiaEntity> type =
                        T3DAOHelper.getContractClass(entity.getClass());

                // generate a new id for the given entity

                id = TopiaId.create(type);
                entity.setTopiaId(id);
            }
            // register the entity (to not come back to it twice)
            ids.add(id);

            entity.setTopiaCreateDate(new Date());
            if (log.isDebugEnabled()) {
                log.debug("start entity " + id);
            }
            stack.push(entity);
        }

        @Override
        public void end(TopiaEntity entity) {

            String id = entity.getTopiaId();
            if (log.isDebugEnabled()) {
                log.debug("end  entity " + id);
            }

            // can now persist the entity
            try {
                TopiaDAO<TopiaEntity> dao =
                        T3DAOHelper.<TopiaEntity, TopiaDAO<TopiaEntity>>getDAO(tx, entity);
                dao.update(entity);

            } catch (TopiaException e) {
                throw new IllegalStateException(
                        "Could not get dao for entity " + entity, e);
            }

            stack.pop();

            if (!stack.isEmpty()) {

                // still some entity at the top
                current = stack.peek();
            }
        }

        @Override
        public void visit(TopiaEntity entity,
                          String propertyName,
                          Class<?> type,
                          Object value) {
            if (value != null && T3DataEntity.class.isAssignableFrom(type)) {

                // ok try to accept this entity
                acceptEntity((TopiaEntity) value);
            }
        }

        @Override
        public void visit(TopiaEntity entity,
                          String propertyName,
                          Class<?> collectionType,
                          Class<?> type,
                          Object value) {
            if (value != null && T3DataEntity.class.isAssignableFrom(type)) {
                Iterable<?> col = (Iterable<?>) value;

                for (Object t3DataEntity : col) {

                    acceptEntity((TopiaEntity) t3DataEntity);
                }
            }
        }

        @Override
        public void visit(TopiaEntity entity,
                          String propertyName,
                          Class<?> collectionType,
                          Class<?> type,
                          int index,
                          Object value) {
        }

        @Override
        public void clear() {
            ids.clear();
        }
    }

}
