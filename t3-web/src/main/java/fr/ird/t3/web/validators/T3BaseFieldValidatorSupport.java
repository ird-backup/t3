/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.validators;

import org.nuiton.validator.xwork2.field.NuitonFieldValidatorSupport;

import java.util.Arrays;

/**
 * Base T3 validator.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class T3BaseFieldValidatorSupport extends NuitonFieldValidatorSupport {

    /**
     * Add directly a field error with the given sentence wihtout any translation from the xml validator file.
     *
     * @param propertyName name of the property where to push the error
     * @param error        the error
     */
    protected void addFieldError(String propertyName, String error) {
        getValidatorContext().addFieldError(propertyName, error);
    }

    /**
     * Translate the given i18n key with his optional arguments.
     * <p/>
     * <strong>Note:</strong> This method name is fixed to be detected via the
     * nuiton i18n system, do NOT change this method name.
     *
     * @param key  the i18n key to translate
     * @param args the optional arguments of the sentence
     * @return the translated sentence
     */
    protected String _(String key, Object... args) {
        String text = getValidatorContext().getText(key, Arrays.asList(args));
        return text;
    }
}
