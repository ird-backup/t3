/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.entities.T3Functions;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.LengthWeightConversion;
import fr.ird.t3.entities.reference.LengthWeightConversionDAO;
import fr.ird.t3.entities.reference.LengthWeightConversionHelper;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.entities.reference.SpeciesDAO;
import fr.ird.t3.entities.reference.zone.ZoneStratumAwareMeta;
import fr.ird.t3.entities.reference.zone.ZoneVersion;
import fr.ird.t3.entities.type.T3Date;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Manage level 3 action configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConfigureLevel3Step1Action extends AbstractConfigureAction<Level3Configuration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ConfigureLevel3Step1Action.class);

    @InjectDAO(entityType = Species.class)
    protected transient SpeciesDAO specieDAO;

    @InjectDAO(entityType = Ocean.class)
    protected transient OceanDAO oceanDAO;

    @InjectDAO(entityType = Country.class)
    protected transient CountryDAO countryDAO;

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    @InjectDAO(entityType = LengthWeightConversion.class)
    protected transient LengthWeightConversionDAO lengthWeightConversionDAO;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> catchFleets;

    @InjectDecoratedBeans(beanType = ZoneStratumAwareMeta.class)
    protected Map<String, String> zoneTypes;

    @InjectDecoratedBeans(beanType = ZoneVersion.class)
    protected Map<String, String> zoneVersions;

    @InjectDecoratedBeans(beanType = Species.class)
    protected Map<String, String> species;

    @InjectDecoratedBeans(beanType = Ocean.class)
    protected Map<String, String> oceans;

    protected final Map<String, String> timeSteps = createTimeSteps();

    /** when validating configuration */
    protected boolean validating;

    /**
     * Flag to know if some data are missings.
     * <p/>
     * This flag is setted in the {@link #prepare()} method while
     * loading possibles data.
     */
    protected boolean missingDatas;

    public ConfigureLevel3Step1Action() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {

        boolean configurationInSession = isConfigurationInSession();

        Level3Configuration conf = getConfiguration();

        injectExcept(InjectDecoratedBeans.class);

        // assume data are available
        missingDatas = false;

        // invalidate step 1 configuration
        conf.setValidStep1(false);

        // invalidate step 2 configuration
        conf.setValidStep2(false);

        if (!configurationInSession) {

            conf.setCatchFleets(sortToList(countryDAO.findAllFleetUsedInCatch()));
            conf.setOceans(sortToList(oceanDAO.findAllUsedInActivity()));
            conf.setSpecies(sortToList(specieDAO.findAllSpeciesUsedInCatch()));
            conf.setZoneTypes(sortToList(getZoneStratumService().getZoneStratumAwareMetas()));
            conf.setUseWeightCategories(true);

        }
        if (StringUtils.isNotEmpty(conf.getZoneTypeId())) {

            // let's fill the zone versions for the selected zone type

            ZoneStratumAwareMeta zoneType =
                    getZoneStratumService().getZoneMetaById(conf.getZoneTypeId());

            conf.setZoneVersions(sortToList(zoneType.getAllZoneVersions(getTransaction())));

            if (CollectionUtils.isEmpty(conf.getZoneVersions())) {
                addFieldError("configuration.zoneVersionId",
                              _("t3.error.no.zoneVersion.found"));
                // need some data
                missingDatas = true;
            }
        }

        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);

        if (CollectionUtils.isEmpty(conf.getCatchFleets())) {
            addFieldError("configuration.catchFleetId",
                          _("t3.error.no.catch.fleet.found"));
            missingDatas = true;
        }

        if (CollectionUtils.isEmpty(conf.getOceans())) {
            addFieldError("configuration.oceanId",
                          _("t3.error.no.ocean.found"));
            missingDatas = true;
        }
        if (CollectionUtils.isEmpty(conf.getSpecies())) {
            addFieldError("configuration.speciesIds",
                          _("t3.error.no.species.found"));
            missingDatas = true;
        }
        if (CollectionUtils.isEmpty(conf.getZoneTypes())) {
            addFieldError("configuration.zoneTypeId",
                          _("t3.error.no.zoneType.found"));
            missingDatas = true;
        }

        if (!isValidating() && !configurationInSession) {

            List<Integer> level3DefaultSpecies =
                    getApplicationConfig().getLevel3DefaultSpecies();

            // keep default species of level 3
            for (Species aSpecies : conf.getSpecies()) {

                if (level3DefaultSpecies.contains(aSpecies.getCode())) {
                    conf.getSpeciesIds().add(aSpecies.getTopiaId());
                }
            }

            // by default time step is 3 months
            conf.setTimeStep(3);

            // use first and last landing date
            T3Date firstLandingDate = tripDAO.getFirstLandingDate();
            T3Date lastLandingDate = tripDAO.getLastLandingDate();
            conf.setMinDate(firstLandingDate);
            conf.setBeginDate(firstLandingDate);
            conf.setMaxDate(lastLandingDate);
            conf.setEndDate(lastLandingDate);

            // let's put the configuration is session, we don't want to redo
            // all queries for validation...
            getT3Session().setActionConfiguration(conf);
        }

        if (log.isInfoEnabled()) {
            log.info("Selected species                 : " + conf.getSpeciesIds());
            log.info("Selected catch fleet country     : " + conf.getCatchFleetId());
            log.info("Selected ocean                   : " + conf.getOceanId());
            log.info("Selected begin date              : " + conf.getBeginDate());
            log.info("Selected end date                : " + conf.getEndDate());
            log.info("Selected time step               : " + conf.getTimeStep());
            log.info("Selected zone type               : " + conf.getZoneTypeId());
            log.info("Selected zone version         : " + conf.getZoneVersionId());
        }
    }

    @Override
    public void validate() {

        Level3Configuration config = getConfiguration();

        boolean oceanOk = true;

        if (StringUtils.isEmpty(config.getOceanId())) {
            addFieldError("configuration.oceanId",
                          _("t3.error.no.ocean.selected"));
            oceanOk = false;
        }

        if (StringUtils.isEmpty(config.getZoneTypeId())) {
            addFieldError("configuration.zoneTypeId",
                          _("t3.error.no.zoneType.selected"));
        }

        if (StringUtils.isEmpty(config.getZoneVersionId())) {
            addFieldError("zoneVersionId",
                          _("t3.error.no.zoneVersion.selected"));
        }

        if (StringUtils.isEmpty(config.getCatchFleetId())) {
            addFieldError("configuration.catchFleetId",
                          _("t3.error.no.catch.fleet.selected"));
        }

        T3Date beginDate = config.getBeginDate();

        Set<String> speciesIds = config.getSpeciesIds();

        boolean speciesOk = true;
        if (CollectionUtils.isEmpty(speciesIds)) {
            addFieldError("configuration.speciesIds",
                          _("t3.error.no.species.selected"));
            speciesOk = false;
        }

        boolean beginDateOk = true;
        if (beginDate == null) {
            addFieldError("configuration.beginDate",
                          _("t3.error.no.beginDate.selected"));
            beginDateOk = false;
        }

        T3Date endDate = config.getEndDate();
        if (endDate == null) {
            addFieldError("configuration.endDate",
                          _("t3.error.no.endDate.selected"));
        }

        if (beginDate != null && endDate != null) {

            if (beginDate.equals(endDate) || beginDate.after(endDate)) {

                // begin date must be strictly before end date
                addFieldError("configuration.beginDate",
                              _("t3.error.beginDate.equals.or.after.endDate"));
            } else {

                // check number of mouths are a multiple of timeStep
                int timeStep = config.getTimeStep();
                if (!beginDate.isModuloMonths(endDate, timeStep)) {
                    addFieldError("configuration.endDate",
                                  _("t3.error.endDate.no.modulo.timestep"));

                }
            }
        }

        if (oceanOk && beginDateOk && speciesOk) {

            // check that for each species we have what we need

            List<Species> allSpecies = config.getSpecies();
            Map<String, Species> speciesById =
                    Maps.uniqueIndex(allSpecies, T3Functions.TO_TOPIA_ID);

            for (String speciesId : speciesIds) {
                Species aSpecies = speciesById.get(speciesId);

                if (aSpecies.getThresholdNumberLevel3FreeSchoolType() == null) {

                    // no threshold for free school type
                    addFieldError("configuration.speciesIds",
                                  _("t3.error.no.thresholdNumberLevel3FreeSchoolType.for.species",
                                    decorate(aSpecies)));
                }
                if (aSpecies.getThresholdNumberLevel3ObjectSchoolType() == null) {

                    // no threshold for object school type
                    addFieldError("configuration.speciesIds",
                                  _("t3.error.no.thresholdNumberLevel3ObjectSchoolType.for.species",
                                    decorate(aSpecies)));
                }

                Ocean ocean;
                try {
                    ocean = oceanDAO.findByTopiaId(config.getOceanId());
                } catch (TopiaException e) {
                    throw new TopiaRuntimeException("Can not obtain ocean", e);
                }

                LengthWeightConversionHelper conversionHelper =
                        lengthWeightConversionDAO.newConversionHelper(
                                ocean,
                                0,
                                configuration.getBeginDate().toBeginDate()
                        );

                try {
                    LengthWeightConversion conversions =
                            conversionHelper.getConversions(aSpecies);
                    if (conversions == null) {
                        addFieldError("configuration.speciesIds",
                                      _("t3.error.no.convertor.for.species",
                                        decorate(aSpecies)));
                    }
                } catch (TopiaException e) {
                    throw new TopiaRuntimeException("Can not obtain converter", e);
                }
            }
        }

    }

    @Override
    public String execute() throws Exception {
        Level3Configuration config = getConfiguration();
        config.setValidStep1(true);
        storeActionConfiguration(config);
        return SUCCESS;
    }

    public String getZoneVersionId() {
        return getConfiguration().getZoneVersionId();
    }

    public void setZoneVersionId(String zoneVersionId) {
        getConfiguration().setZoneVersionId(zoneVersionId);
    }

    public Map<String, String> getCatchFleets() {
        return catchFleets;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getZoneTypes() {
        return zoneTypes;
    }

    public Map<String, String> getZoneVersions() {
        return zoneVersions;
    }

    public Map<String, String> getSpecies() {
        return species;
    }

    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    public boolean isValidating() {
        return validating;
    }

    public void setValidating(boolean validating) {
        this.validating = validating;
    }

    public boolean isMissingDatas() {
        return missingDatas;
    }

    public void setMissingDatas(boolean missingDatas) {
        this.missingDatas = missingDatas;
    }
}
