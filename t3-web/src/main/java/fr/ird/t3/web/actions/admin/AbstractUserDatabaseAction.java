/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.T3UserImpl;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.util.Collection;

/**
 * Operations of {@link UserDatabase}
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractUserDatabaseAction<T extends UserDatabase> extends T3ActionSupport implements Preparable {

    private static final Log log =
            LogFactory.getLog(AbstractUserDatabaseAction.class);

    private static final long serialVersionUID = -2894789101651139566L;

    public static final String T3_DATABASE = "t3Database";

    public static final String OUPUT_DATABASE = "outputDatabase";

    protected T databaseConfiguration;

    protected T3User user;

    protected String databaseEditAction;

    protected String userEditAction;

    protected abstract T getDatabase(String id) throws TopiaException;

    public abstract T getDatabaseConfiguration();

    protected abstract Collection<T> getDatabases(T3User user);

    public abstract String getDatabaseType();

    protected abstract void create(String userId, T database) throws Exception;

    protected abstract void update(T database) throws Exception;

    protected abstract void delete(String userId, T database) throws Exception;

    @Override
    public void prepare() throws Exception {

        String id = getDatabaseConfiguration().getTopiaId();
        if (StringUtils.isNotEmpty(id)) {

            // load configuration from db
            databaseConfiguration = getDatabase(id);
        }
    }

    public String doCreate() throws Exception {

        T configuration = getDatabaseConfiguration();

        if (log.isInfoEnabled()) {
            log.info("will create database configuration  " +
                     configuration.getDescription());
        }

        String userId = getUser().getTopiaId();

        create(userId, configuration);

        return SUCCESS;
    }

    public String doUpdate() throws Exception {
        T configuration = getDatabaseConfiguration();

        if (log.isInfoEnabled()) {
            log.info("will update user  " + configuration.getDescription());
        }

        update(configuration);

        return SUCCESS;
    }

    public String doDelete() throws Exception {
        T configuration = getDatabaseConfiguration();

        if (log.isInfoEnabled()) {
            log.info("will update user  " + configuration.getDescription());
        }
        String userId = getUser().getTopiaId();

        delete(userId, configuration);

        return SUCCESS;
    }

    @Override
    public void validate() {

        EditActionEnum action = getEditActionEnum();

        log.info("Edit action : " + action);

        if (action == null) {

            // no validation (no edit action)
            return;
        }

        UserDatabase configuration = getDatabaseConfiguration();
        String userId = getUser().getTopiaId();
        String description = configuration.getDescription();
        String login = configuration.getLogin();
        String url = configuration.getUrl();
        String id = configuration.getTopiaId();

        boolean noError = true;

        switch (action) {

            case CREATE:

                // url + description + login required

                if (StringUtils.isEmpty(description)) {

                    // empty description
                    addFieldError("databaseConfiguration.description",
                                  _("t3.error.required.description"));
                    noError = false;
                }

                if (StringUtils.isEmpty(url)) {

                    // empty url
                    addFieldError("databaseConfiguration.url",
                                  _("t3.error.required.url"));
                    noError = false;
                }

                if (StringUtils.isEmpty(login)) {

                    // empty user login
                    addFieldError("databaseConfiguration.login",
                                  _("t3.error.required.login"));
                    noError = false;
                }

                if (noError) {

                    // check now the configuration does not already exist with
                    // same description for this user

                    T3User t3User;
                    try {
                        t3User = getUserService().getUserById(userId);
                    } catch (Exception e) {

                        // could not get user
                        throw new IllegalStateException(
                                "Could not obtain user " + userId, e);
                    }

                    Collection<? extends UserDatabase> databases =
                            getDatabases(t3User);

                    UserDatabase existingDatabase = null;
                    if (CollectionUtils.isNotEmpty(databases)) {

                        // look for an existing database with same description

                        for (UserDatabase database : databases) {
                            if (description.equals(database.getDescription())) {
                                existingDatabase = database;
                                break;
                            }
                        }
                    }

                    if (existingDatabase != null) {
                        addFieldError("databaseConfiguration.description",
                                      _("t3.error.configuration.description.already.used"));
                    }
                }

                break;
            case EDIT:

                // url + description + login required

                if (StringUtils.isEmpty(description)) {

                    // empty description
                    addFieldError("databaseConfiguration.description",
                                  _("t3.error.required.description"));
                    noError = false;
                }

                if (StringUtils.isEmpty(url)) {

                    // empty url
                    addFieldError("databaseConfiguration.url",
                                  _("t3.error.required.url"));
                    noError = false;
                }

                if (StringUtils.isEmpty(login)) {

                    // empty user login
                    addFieldError("databaseConfiguration.login",
                                  _("t3.error.required.login"));
                    noError = false;
                }

                if (noError) {

                    // check now the configuration does not already exist with
                    // same description for this user
                    T3User t3User;
                    try {
                        t3User = getUserService().getUserById(userId);
                    } catch (Exception e) {

                        // could not get user
                        throw new IllegalStateException(
                                "Could not obtain user " + userId, e);
                    }

                    Collection<? extends UserDatabase> databases =
                            getDatabases(t3User);

                    UserDatabase existingDatabase = null;
                    if (CollectionUtils.isNotEmpty(databases)) {

                        // look for an existing database with same description

                        for (UserDatabase database : databases) {
                            if (description.equals(database.getDescription()) &&
                                !id.equals(database.getTopiaId())) {

                                existingDatabase = database;
                                break;
                            }
                        }
                    }
                    if (existingDatabase != null) {
                        addFieldError("databaseConfiguration.description",
                                      _("t3.error.configuration.description.already.used"));
                    }
                }
                break;
            case DELETE:

                // nothing to validate

            default:
                // nothing to validate
        }
    }

    public String getUserEditAction() {
        return userEditAction;
    }

    public void setUserEditAction(String userEditAction) {
        this.userEditAction = userEditAction;
    }

    public String getDatabaseEditAction() {
        return databaseEditAction;
    }

    public void setDatabaseEditAction(String databaseEditAction) {
        this.databaseEditAction = databaseEditAction;
    }

    public T3User getUser() {
        if (user == null) {
            user = new T3UserImpl();
        }
        return user;
    }

    protected EditActionEnum getEditActionEnum() {
        if (databaseEditAction == null) {
            return null;
        }
        return EditActionEnum.valueOf(databaseEditAction.toUpperCase());
    }
}
