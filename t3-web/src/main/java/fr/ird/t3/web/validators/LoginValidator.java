/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.validators;

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.services.UserService;

/**
 * Check user login.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class LoginValidator extends T3BaseFieldValidatorSupport {

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        UserService userService =
                (UserService) getFieldValue("userService", object);

        String login = (String) getFieldValue("login", object);
        String password = (String) getFieldValue("password", object);

        if (log.isInfoEnabled()) {
            log.info("try to log for user " + login);
        }

        try {
            // check in db that user is ok
            T3User user = userService.getUserByLogin(login);

            if (user == null) {

                // user not found
                addFieldError("login", _("t3.error.login.unknown"));
                return;
            }

            boolean passwordOk = userService.checkPassword(user, password);

            if (!passwordOk) {
                addFieldError("password", _("t3.error.bad.password"));
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not validate login", e);
            }
            throw new ValidationException("Could not validate login : " +
                                          e.getMessage());
        }
    }

    @Override
    public String getValidatorType() {
        return "login";
    }
}
