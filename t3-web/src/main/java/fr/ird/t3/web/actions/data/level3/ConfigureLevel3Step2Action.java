/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.data.level3;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ird.t3.actions.stratum.StratumMinimumSampleCount;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.CountryDAO;
import fr.ird.t3.entities.reference.Species;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.interceptor.ParameterAware;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To manager the step 2 of a level 3 treatment configuration.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConfigureLevel3Step2Action extends AbstractConfigureAction<Level3Configuration> implements ParameterAware {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ConfigureLevel3Step2Action.class);

    @InjectDAO(entityType = Country.class)
    protected transient CountryDAO countryDAO;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> sampleFleets;

    @InjectDecoratedBeans(beanType = Country.class)
    protected Map<String, String> sampleFlags;

    @InjectDecoratedBeans(beanType = Species.class, filterById = true, pathIds = "speciesIds")
    protected Map<String, String> species;

    protected final Map<String, String> timeSteps = createTimeSteps();

    protected Map<String, String> useSamplesOrNot;

    protected Map<String, String> useWeightCategoriesOrNot;

    /**
     * Flag to know if some data are missings.
     * <p/>
     * This flag is setted in the {@link #prepare()} method while
     * loading possibles data.
     */
    protected boolean missingDatas;

    public ConfigureLevel3Step2Action() {
        super(Level3Configuration.class);
    }

    @Override
    public void prepare() throws Exception {

        useSamplesOrNot = createLevel3UseSamplesOrNotMap();
        useWeightCategoriesOrNot = createLevel3UseWeightCategoriesOrNotMap();

        Level3Configuration conf = getConfiguration();
        if (log.isInfoEnabled()) {
            log.info("Prepare with configuration " + conf);
        }
        injectExcept(InjectDecoratedBeans.class);

        missingDatas = false;

        List<Country> sampleFleetCountries = Lists.newArrayList();
        List<Country> sampleFlagCountries = Lists.newArrayList();

        String oceanId = conf.getOceanId();
        sampleFleetCountries.addAll(countryDAO.findAllFleetUsedInSample(oceanId));
        sampleFlagCountries.addAll(countryDAO.findAllFlagUsedInSample(oceanId));
        conf.setSampleFlags(sampleFlagCountries);
        conf.setSampleFleets(sampleFleetCountries);

        if (CollectionUtils.isEmpty(sampleFleetCountries)) {
            addFieldError("configuration.sampleFleetIds",
                          _("t3.error.no.sample.fleet.found"));
            // need some data
            missingDatas = true;
        }
        if (CollectionUtils.isEmpty(sampleFlagCountries)) {
            addFieldError("configuration.sampleFlagIds",
                          _("t3.error.no.sample.flag.found"));
            // need some data
            missingDatas = true;
        }

        // let's inject decorated values
        injectOnly(InjectDecoratedBeans.class);

        // prepapre stratum minimum sample count for each species selected

        for (Species aSpecies : conf.getSpecies()) {
            String specieId = aSpecies.getTopiaId();
            StratumMinimumSampleCount sampleCount =
                    getStratumMinimumSampleCount(specieId);

            if (sampleCount == null) {
                sampleCount = new StratumMinimumSampleCount();
                Integer thresholdNumberLevel3FreeSchoolType = aSpecies.getThresholdNumberLevel3FreeSchoolType();
                Integer thresholdNumberLevel3ObjectSchoolType = aSpecies.getThresholdNumberLevel3ObjectSchoolType();
                sampleCount.setMinimumCountForFreeSchool(thresholdNumberLevel3FreeSchoolType);
                sampleCount.setMinimumCountForObjectSchool(thresholdNumberLevel3ObjectSchoolType);
                getStratumMinimumSampleCount().put(specieId, sampleCount);
            }
        }

        // prepapre stratum weight ratio value
        float stratumWeightRatio = conf.getStratumWeightRatio();
        if (stratumWeightRatio == 0) {

            // get the default value from application configuration
            stratumWeightRatio = getApplicationConfig().getStratumWeightRatio();

            // store it back in configuration
            conf.setStratumWeightRatio(stratumWeightRatio);
        }

        if (log.isInfoEnabled()) {
            log.info("Selected sample fleet countries  : " + conf.getSampleFleetIds());
            log.info("Selected sample flag  countries  : " + conf.getSampleFlagIds());
            log.info("Selected weight ratio            : " + conf.getStratumWeightRatio());
            for (String specieId : conf.getSpeciesIds()) {
                String specieLabel = getSpecies().get(specieId);
                log.info("[" + specieLabel + "] min sample count BL     : " + getStratumMinimumSampleCountFreeSchoolType(specieId));
                log.info("[" + specieLabel + "] min sample count BO     : " + getStratumMinimumSampleCountObjectSchoolType(specieId));
            }
        }
    }

    public Map<String, StratumMinimumSampleCount> getStratumMinimumSampleCount() {
        Map<String, StratumMinimumSampleCount> stratumMinimumSampleCount =
                getConfiguration().getStratumMinimumSampleCount();
        if (stratumMinimumSampleCount == null) {
            stratumMinimumSampleCount = Maps.newTreeMap();
            getConfiguration().setStratumMinimumSampleCount(
                    stratumMinimumSampleCount);
        }
        return stratumMinimumSampleCount;
    }

    public StratumMinimumSampleCount getStratumMinimumSampleCount(String specie) {
        return getStratumMinimumSampleCount().get(specie);
    }

    @Override
    public void validate() {

        Level3Configuration conf = getConfiguration();

        if (CollectionUtils.isEmpty(conf.getSampleFlagIds())) {
            addFieldError("configuration.sampleFlagIds",
                          _("t3.error.no.sample.flag.selected"));
        }

        if (CollectionUtils.isEmpty(conf.getSampleFleetIds())) {
            addFieldError("configuration.sampleFleetIds",
                          _("t3.error.no.sample.fleet.selected"));
        }
    }

    @Override
    public String execute() throws Exception {

        Level3Configuration config = getConfiguration();

        // as the level 2 configuration is now fully valid, store it
        config.setValidStep2(true);

        return SUCCESS;
    }

    public Map<String, String> getSampleFleets() {
        return sampleFleets;
    }

    public Map<String, String> getSampleFlags() {
        return sampleFlags;
    }

    public Map<String, String> getTimeSteps() {
        return timeSteps;
    }

    public Integer getStratumMinimumSampleCountObjectSchoolType(String specieId) {
        StratumMinimumSampleCount sampleCount =
                getStratumMinimumSampleCount(specieId);
        return sampleCount.getMinimumCountForObjectSchool();
    }

    public void setStratumMinimumSampleCountObjectSchoolType(
            String specieId,
            Integer stratumMinimumSampleCountObjectSchoolType) {
        StratumMinimumSampleCount sampleCount =
                getStratumMinimumSampleCount(specieId);
        sampleCount.setMinimumCountForObjectSchool(
                stratumMinimumSampleCountObjectSchoolType);
    }

    public Integer getStratumMinimumSampleCountFreeSchoolType(String specieId) {
        StratumMinimumSampleCount sampleCount =
                getStratumMinimumSampleCount(specieId);
        return sampleCount.getMinimumCountForFreeSchool();
    }

    public void setStratumMinimumSampleCountFreeSchoolType(
            String specieId,
            Integer stratumMinimumSampleCountFreeSchoolType) {
        StratumMinimumSampleCount sampleCount =
                getStratumMinimumSampleCount(specieId);
        sampleCount.setMinimumCountForFreeSchool(
                stratumMinimumSampleCountFreeSchoolType);
    }

    public boolean isMissingDatas() {
        return missingDatas;
    }

    public static final Pattern BO_STRATUM_MINIMUM_COUNT_PATTERN = Pattern.compile("BO:(.*)?");

    public static final Pattern BL_STRATUM_MINIMUM_COUNT_PATTERN = Pattern.compile("BL:(.*)?");

    @Override
    public void setParameters(Map<String, String[]> parameters) {

        // to obtain back values for stratumMinimumCount...
        for (Map.Entry<String, String[]> e : parameters.entrySet()) {
            String name = e.getKey();
            String[] values = e.getValue();

            Matcher matcher;
            matcher = BO_STRATUM_MINIMUM_COUNT_PATTERN.matcher(name);
            if (matcher.matches()) {

                // specieId is second part
                String specieId = matcher.group(1);
                StratumMinimumSampleCount sampleCount =
                        getStratumMinimumSampleCount(specieId);

                Integer realValue = null;
                if (values.length > 0) {

                    try {
                        realValue = Integer.valueOf(values[0]);

                    } catch (NumberFormatException e1) {
                        addFieldError(name, _("t3.error.invalid.integer"));
                    }
                }
                sampleCount.setMinimumCountForObjectSchool(realValue);

                continue;
            }

            matcher = BL_STRATUM_MINIMUM_COUNT_PATTERN.matcher(name);
            if (matcher.matches()) {

                // specieId is second part
                String specieId = matcher.group(1);

                StratumMinimumSampleCount sampleCount =
                        getStratumMinimumSampleCount(specieId);

                Integer realValue = null;
                if (values.length > 0) {

                    try {
                        realValue = Integer.valueOf(values[0]);

                    } catch (NumberFormatException e1) {
                        addFieldError(name, _("t3.error.invalid.integer"));
                    }
                }
                sampleCount.setMinimumCountForFreeSchool(realValue);
            }
        }
    }

    public Map<String, String> getSpecies() {
        return species;
    }

    public Map<String, String> getUseSamplesOrNot() {
        return useSamplesOrNot;
    }

    public Map<String, String> getUseWeightCategoriesOrNot() {
        return useWeightCategoriesOrNot;
    }
}
