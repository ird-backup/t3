/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.input;

import com.google.common.collect.Sets;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceAction;
import fr.ird.t3.actions.io.input.ImportInputSourceConfiguration;
import fr.ird.t3.actions.io.input.InputSourceConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.web.actions.AbstractRunAction;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Analyze and import data from input source.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ImportDataAction extends AbstractRunAction<ImportInputSourceConfiguration, ImportInputSourceAction> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportDataAction.class);

    protected boolean replaceTrip;

    protected boolean valid;

    private int nbImportedTrips;

    private int nbDeletedTrips;

    /** List of all known input providers. */
    private List<T3InputProvider> inputProviders;

    public ImportDataAction() {
        super(ImportInputSourceAction.class);
    }

    @Override
    public void prepare() throws Exception {

        super.prepare();

        inputProviders = Arrays.asList(getIncomingConfiguration().getInputProvider());
    }

    public final List<T3InputProvider> getInputProviders() {
        return inputProviders;
    }

    public InputSourceConfiguration getIncomingConfiguration() {
        Object configuration = getT3ActionContext().getConfiguration();
        return (InputSourceConfiguration) configuration;
    }

    public String prepareImport() throws Exception {

        // get the action context from analyze

        T3ActionContext<ImportInputSourceConfiguration> actionContext =
                getT3ActionContext();

        Set<Trip> safeTrips = actionContext.getResultAsSet(
                AnalyzeInputSourceAction.RESULT_SAFE_TRIPS,
                Trip.class);

        Map<Trip, Trip> tripToReplace = actionContext.getResultAsMap(
                AnalyzeInputSourceAction.RESULT_TRIPS_TO_REPLACE
        );

        if (log.isInfoEnabled()) {
            log.info("--> Prepare import");
            log.info("Input provider   : " + getIncomingConfiguration().getInputProvider());
            log.info("Input file       : " + getIncomingConfiguration().getInputFile());
            log.info("Use well         : " + getIncomingConfiguration().isUseWells());
            log.info("Samples Only     : " + getIncomingConfiguration().isSamplesOnly());
            log.info("Can Create vessel: " + getIncomingConfiguration().isCanCreateVessel());
            if (getIncomingConfiguration().isCanCreateVessel()) {
                log.info("Create virtual vessel: " + getIncomingConfiguration().isCreateVirtualVessel());
            }
            log.info("Trips to import  : " + safeTrips.size());
            log.info("Trips to replace : " + tripToReplace.size());
        }

        Set<Trip> toImportTrips = Sets.newHashSet();
        Set<Trip> toDeleteTrips = Sets.newHashSet();

        if (isReplaceTrip()) {

            // will replace existing trips
            // so need to delete before all old trips
            toDeleteTrips.addAll(tripToReplace.keySet());

            // can import all safe trips
            toImportTrips.addAll(safeTrips);

        } else {

            // do NOT replace old trip

            // get all importable trips
            toImportTrips.addAll(safeTrips);

            if (MapUtils.isNotEmpty(tripToReplace)) {

                // remove the one which reflect existing trips
                toImportTrips.removeAll(tripToReplace.values());
            }
        }

        // creates the new configuration

        ImportInputSourceConfiguration configuration =
                ImportInputSourceConfiguration.newConfiguration(getIncomingConfiguration());

        configuration.setTripsToImport(toImportTrips);

        configuration.setTripsToDelete(toDeleteTrips);

        t3ActionContext = getServiceFactory().newT3ActionContext(
                configuration, getServiceContext()
        );
        if (log.isInfoEnabled()) {
            log.info("Created action context " + t3ActionContext);
        }
        getT3Session().setActionContext(t3ActionContext);

        return SUCCESS;
    }

    public String prepareResult() throws Exception {

        Set<Trip> importedTrips = getConfiguration().getTripsToImport();

        nbImportedTrips = importedTrips.size();

        Set<Trip> deletedeTrips = getConfiguration().getTripsToDelete();

        nbDeletedTrips = deletedeTrips.size();

        return INPUT;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getNbImportedTrips() {
        return nbImportedTrips;
    }

    public int getNbDeletedTrips() {
        return nbDeletedTrips;
    }

    public boolean isReplaceTrip() {
        return replaceTrip;
    }

    public void setReplaceTrip(boolean replaceTrip) {
        this.replaceTrip = replaceTrip;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(ImportInputSourceAction action,
                                                          Exception error,
                                                          Date startDate,
                                                          Date endDate) {

        Map<String, Object> map = super.prepareResumeParameters(action,
                                                                error,
                                                                startDate,
                                                                endDate
        );

        map.put("tripDecorator",
                getDecorator(Trip.class, DecoratorService.WITH_ID));

        return map;
    }
}
