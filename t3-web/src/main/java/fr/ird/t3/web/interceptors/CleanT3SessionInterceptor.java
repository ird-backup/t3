/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.util.TextParseUtil;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.web.T3Session;

import java.util.Collections;
import java.util.Set;

/**
 * Interceptor to remove some stuff from user session.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see T3Session
 * @since 1.0
 */
public class CleanT3SessionInterceptor extends AbstractInterceptor {
    private static final long serialVersionUID = 1L;

    private boolean removeConfiguration = true;

    private Set<String> parameters = Collections.emptySet();

    /**
     * <code>parameters</code> to remove from session as comma-separated-values (csv).
     *
     * @param parameters the parameters to set
     */
    public void setParameters(String parameters) {
        this.parameters =
                TextParseUtil.commaDelimitedStringToSet(parameters);
    }

    public void setRemoveConfiguration(boolean removeConfiguration) {
        this.removeConfiguration = removeConfiguration;
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        T3Session t3Session = T3Session.getT3Session(invocation.getInvocationContext());
        if (t3Session != null) {

            for (String paramName : parameters) {
                Object value = t3Session.remove(paramName);

                if (removeConfiguration &&
                    T3Session.PROPERTY_ACTION_CONTEXT.equals(paramName) &&
                    value instanceof T3ActionContext<?>) {

                    // remove also the configuration of the action

                    T3ActionContext<?> context =
                            (T3ActionContext<?>) value;
                    Object configuration = context.getConfiguration();


                    t3Session.removeActionConfiguration(configuration.getClass());
                }

            }
        }
        return invocation.invoke();
    }
}
