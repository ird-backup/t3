/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import com.google.common.collect.Maps;
import fr.ird.t3.actions.T3ActionContext;
import fr.ird.t3.actions.admin.DeleteTripConfiguration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.decorator.Decorator;

import java.util.List;
import java.util.Map;

/**
 * To delete trips (or just data of trips).
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1
 */
public class ConfirmTripDeleteAction extends T3ActionSupport {

    protected static final Log log =
            LogFactory.getLog(ConfirmTripDeleteAction.class);

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    protected List<String> tripIds;

    protected Map<String, String> tripsToDelete;

    public Map<String, String> getTripsToDelete() {
        return tripsToDelete;
    }

    public void setTripIds(List<String> tripIds) {
        this.tripIds = tripIds;
    }

    public String input() throws Exception {

        injectOnly(InjectDAO.class);

        tripsToDelete = Maps.newTreeMap();

        if (CollectionUtils.isNotEmpty(tripIds)) {

            Decorator<Trip> decorator = getDecorator(Trip.class);
            // load trips
            for (String tripId : tripIds) {
                Trip aTrip = tripDAO.findByTopiaId(tripId);
                String s = decorator.toString(aTrip);
                tripsToDelete.put(tripId, s);
            }
        }
        return INPUT;
    }

    public String doDelete() throws Exception {
        return startDeleteData(true);
    }

    public String doDeleteComputedData() throws Exception {
        return startDeleteData(false);
    }

    protected String startDeleteData(boolean deleteTrips) {
        List<String> ids = tripIds;

        DeleteTripConfiguration conf = new DeleteTripConfiguration();
        conf.setTripIds(ids);
        conf.setDeleteTrip(deleteTrips);

        T3ActionContext<DeleteTripConfiguration> context =
                getServiceFactory().newT3ActionContext(conf, getServiceContext());
        getT3Session().setActionContext(context);
        return SUCCESS;
    }
}
