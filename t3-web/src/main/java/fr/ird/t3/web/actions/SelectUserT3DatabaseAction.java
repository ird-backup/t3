/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.google.common.collect.Maps;
import com.opensymphony.xwork2.Preparable;
import fr.ird.t3.entities.user.JdbcConfiguration;
import fr.ird.t3.entities.user.JdbcConfigurationImpl;
import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserT3Database;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Map;

/**
 * Database to select and connect to a t3 database.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class SelectUserT3DatabaseAction extends T3ActionSupport implements Preparable {

    protected static final Log log =
            LogFactory.getLog(SelectUserT3DatabaseAction.class);

    private static final long serialVersionUID = 1L;

    protected String databaseId;

    protected JdbcConfiguration database;

    protected Map<String, String> databases;

    public String getDatabaseId() {
        return databaseId;
    }

    public JdbcConfiguration getDatabase() {
        if (database == null) {
            database = new JdbcConfigurationImpl();
        }
        return database;
    }

    public Map<String, String> getDatabases() {
        return databases;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    @Override
    public void prepare() throws Exception {

        T3User user = getT3Session().getUser();

        user = getUserService().getUserById(user.getTopiaId());

        Collection<UserT3Database> dbs = user.getUserT3Database();

        if (CollectionUtils.isEmpty(dbs)) {
            databases = Maps.newHashMap();
            addActionMessage(_("t3.message.no.t3.database.registred"));
        } else {

            databases = sortAndDecorate(dbs);
        }
    }

    @Override
    public String execute() throws Exception {

        // set the selected database in session
        getT3Session().initDatabaseConfiguration(getDatabase());

        return SUCCESS;
    }
}
