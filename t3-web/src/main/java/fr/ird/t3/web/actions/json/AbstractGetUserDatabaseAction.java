/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.json;

import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.entities.user.UserDatabaseDTO;
import fr.ird.t3.entities.user.UserDatabaseDTOImpl;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

/**
 * Obtains a database configuration from his id.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractGetUserDatabaseAction<T extends UserDatabase> extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractGetUserDatabaseAction.class);

    /** Selected database id. */
    protected String databaseId;

    /** Loaded database. */
    protected UserDatabaseDTO database;

    /**
     * <strong>Note:</strong> As this is a json action, we need to have at
     * least a property on the concrete class...
     *
     * @return the selected database.
     */
    public abstract UserDatabaseDTO getDatabase();

    /**
     * Get the database given his id.
     *
     * @param databaseId the database id
     * @return the found database
     * @throws TopiaException if any pb while
     */
    protected abstract T getUserDatabase(String databaseId) throws TopiaException;

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    @Override
    public String execute() throws Exception {

        if (log.isInfoEnabled()) {
            log.info("databaseId = " + databaseId);
        }

        if (StringUtils.isEmpty(databaseId)) {

            // no database selected, use a fake database
            database = new UserDatabaseDTOImpl();
        } else {
            T userDatabase = getUserDatabase(databaseId);
            database = userDatabase.toDTO();
        }


        return SUCCESS;
    }

}
