/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import fr.ird.t3.actions.data.level1.Level1Configuration;
import fr.ird.t3.actions.data.level2.Level2Configuration;
import fr.ird.t3.actions.data.level3.Level3Configuration;
import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.services.ioc.InjectFromDAO;

/**
 * To go to the home page.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class HomeAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    @InjectFromDAO(entityType = Ocean.class, method = "count")
    protected long nbOceans;

    @InjectFromDAO(entityType = Trip.class, method = "count")
    protected long nbTrips;

    protected boolean withTrips;

    protected boolean withReferences;

    protected boolean containsLevel1Configuration;

    @Override
    public String execute() throws Exception {

        getT3Session().removeTripListModel();

        injectOnly(InjectFromDAO.class);

        withReferences = nbOceans > 0;

        if (!withReferences) {
            addActionMessage(_("t3.message.no.references.in.db"));
        }

        withTrips = nbTrips > 0;

        if (!withTrips) {
            addActionMessage(_("t3.message.no.trips.in.db"));
        }

        Level1Configuration actionConfiguration =
                getActionConfiguration(Level1Configuration.class);
        containsLevel1Configuration = actionConfiguration != null;

        // clean level 2 and level 3 configurations
        getT3Session().removeActionConfiguration(Level2Configuration.class);
        getT3Session().removeActionConfiguration(Level3Configuration.class);

        return SUCCESS;
    }

    public boolean isWithTrips() {
        return withTrips;
    }

    public boolean isWithReferences() {
        return withReferences;
    }

    public boolean isContainsLevel1Configuration() {
        return containsLevel1Configuration;
    }
}
