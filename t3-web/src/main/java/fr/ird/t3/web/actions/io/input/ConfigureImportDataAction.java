/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.input;

import com.opensymphony.xwork2.inject.Inject;
import fr.ird.t3.actions.io.input.AnalyzeInputSourceConfiguration;
import fr.ird.t3.io.input.T3InputProvider;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractConfigureAction;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsConstants;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Action to import data fro input sources.
 *
 * @author chemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ConfigureImportDataAction extends AbstractConfigureAction<AnalyzeInputSourceConfiguration> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ConfigureImportDataAction.class);

    public static final String RELOAD_ACTION = "reload";

    /** List of all known input providers. */
    private List<T3InputProvider> inputProviders;

    /** Input file that was just uploaded on server */
    protected File sourceToLoad;

    /** Name of file to upload */
    protected String sourceToLoadFileName;

    /** flag to use strict mode with wells. */
    protected boolean useWells;

    /**
     * flag to use a db with samplesOnly (says with no logbook nor catches).
     *
     * @since 1.2
     */
    protected boolean samplesOnly;

    /**
     * flag to authorize creation of missing vessels.
     *
     * @since 1.3.1
     */
    protected boolean canCreateVessel;

    /**
     * flag to create or not virtual vessels.
     *
     * @since 1.3.1
     */
    protected boolean createVirtualVessel;


    /** Where to keep data for treatment */
    private String treatmentDirectoryPath;

    /** Name of uploaded source */
    private String loadedSource;

    /** Id of selected InputProvider */
    private String inputProviderId;

    protected long maxSize;

    @Inject(StrutsConstants.STRUTS_MULTIPART_MAXSIZE)
    public void setMaxSize(String maxSize) {
        this.maxSize = Long.parseLong(maxSize);
    }

    public String getMaxSize() {
        return StringUtil.convertMemory(maxSize);
    }

    public ConfigureImportDataAction() {
        super(AnalyzeInputSourceConfiguration.class);
    }

    @Override
    public void prepare() throws Exception {

        injectOnly(InjectDAO.class);

        if (!isConfigurationInSession()) {

            // no yet configuration, can use all input providers
            inputProviders = Arrays.asList(getT3InputService().getProviders());

        } else {
            // as soon as there is a configuration, can only use the
            // inputProvider from it

            inputProviders = Arrays.asList(getConfiguration().getInputProvider());
        }

        injectOnly(InjectDecoratedBeans.class);

        if (getTreatmentDirectoryPath() == null) {

            // first time coming here get a new treatment directory path

            File workingDirectory =
                    getApplicationConfig().getTreatmentWorkingDirectory();

            long l = System.nanoTime();

            File treatmentDirectory = new File(workingDirectory,
                                               "importData-" + l);

            FileUtil.createDirectoryIfNecessary(treatmentDirectory);

            if (log.isInfoEnabled()) {
                log.info("Create a new treatment directory path : " +
                         treatmentDirectory);
            }

            setTreatmentDirectoryPath(treatmentDirectory.getAbsolutePath());
        } else {

            if (log.isInfoEnabled()) {
                log.info("Use existing treatment directory " +
                         getTreatmentDirectoryPath());
            }
        }
    }

    public String doAddSource() throws Exception {

        // this action execution purpose is to add a new source to the
        // treatment configuration
        // It will upload the file and place it the correct place (says the
        // temporary directory of the treatment configuration)

        // file uploaded on server
        File upload = getSourceToLoad();

        if (upload == null) {

            String message = _("t3.error.required.file.to.upload");
            addFieldError("sourceToLoad", message);
            log.error(message);
            return ERROR;
        }

        // treatment directory
        File targetDirectory = getTreatmentDirectory();

        String filename = getSourceToLoadFileName();

        if (ZipUtil.isZipFile(upload)) {

            // let's decompress input stream it
            ZipFile zipfile = new ZipFile(upload);

            Enumeration<? extends ZipEntry> entries = zipfile.entries();

            if (!entries.hasMoreElements()) {
                String message = _("t3.error.required.one.entry.in.zip.to.upload");
                addFieldError("sourceToLoad", message);
                log.error(message);
                return ERROR;
            }

            // get first entry
            ZipEntry zipEntry = entries.nextElement();

            // keep the filename of the zip entry
            filename = zipEntry.getName();

            File target = new File(targetDirectory, filename);

            if (log.isInfoEnabled()) {
                log.info("Will copy loaded zipped entry file " + filename +
                         " to treatment configuration directory " + target);
            }
            InputStream in = zipfile.getInputStream(zipEntry);
            try {
                FileUtils.copyInputStreamToFile(in, target);
                in.close();
            } finally {
                IOUtils.closeQuietly(in);
            }
        } else {

            // target file
            File target = new File(targetDirectory, filename);

            if (log.isInfoEnabled()) {
                log.info("Will copy loaded file " + upload +
                         " to treatment configuration directory " + target);
            }

            // just copy file
            FileUtils.copyFile(upload, target);
        }

        // add the new file to the sources
        setLoadedSource(filename);

        // go back to the configuration definition
        return INPUT;
    }

    public String doDeleteSource() throws Exception {

        String filename = getLoadedSource();

        File targetDirectory = getTreatmentDirectory();

        if (log.isInfoEnabled()) {
            log.info("Will delete loaded file " + filename +
                     " from " + targetDirectory);
        }
        File f = new File(targetDirectory, filename);
        if (f.exists()) {
            boolean delete = f.delete();
            if (!delete) {
                throw new IOException("Could not delete file " + f
                                      + " on server.");
            }
        }

        setLoadedSource(null);

        // go back to the configuration definition
        return INPUT;
    }

    public String doPrepareAnalyze() throws Exception {

        T3InputProvider inputProvider =
                getT3InputService().getProvider(getInputProviderId());

        File path = getTreatmentDirectory();
        String name = getLoadedSource();

        File inputFile = new File(path, name);

        if (log.isInfoEnabled()) {
            log.info("Will use input provider : " + inputProvider);
            log.info("Will use input file     : " + inputFile);
        }

        // prepare a new action configuration
        configuration = AnalyzeInputSourceConfiguration.newConfiguration(
                inputProvider,
                inputFile,
                useWells,
                samplesOnly,
                canCreateVessel,
                createVirtualVessel
        );

        prepareActionContext();
        return SUCCESS;
    }

    public File getSourceToLoad() {
        return sourceToLoad;
    }

    public String getSourceToLoadFileName() {
        return sourceToLoadFileName;
    }

    public void setSourceToLoad(File sourceToLoad) {
        this.sourceToLoad = sourceToLoad;
    }

    public void setSourceToLoadFileName(String sourceToLoadFileName) {
        this.sourceToLoadFileName = sourceToLoadFileName;
    }

    public boolean isUseWells() {
        return useWells;
    }

    public void setUseWells(boolean useWells) {
        this.useWells = useWells;
    }

    public boolean isSamplesOnly() {
        return samplesOnly;
    }

    public void setSamplesOnly(boolean samplesOnly) {
        this.samplesOnly = samplesOnly;
    }

    public boolean isCanCreateVessel() {
        return canCreateVessel;
    }

    public void setCanCreateVessel(boolean canCreateVessel) {
        this.canCreateVessel = canCreateVessel;
    }

    public boolean isCreateVirtualVessel() {
        return createVirtualVessel;
    }

    public void setCreateVirtualVessel(boolean createVirtualVessel) {
        this.createVirtualVessel = createVirtualVessel;
    }

    public String getInputProviderId() {
        return inputProviderId;
    }

    public void setInputProviderId(String inputProviderId) {
        this.inputProviderId = inputProviderId;
    }

    public String getLoadedSource() {
        return loadedSource;
    }

    public void setLoadedSource(String loadedSource) {
        this.loadedSource = loadedSource;
    }

    public String getTreatmentDirectoryPath() {
        return treatmentDirectoryPath;
    }

    public void setTreatmentDirectoryPath(String treatmentDirectoryPath) {
        this.treatmentDirectoryPath = treatmentDirectoryPath;
    }

    public final List<T3InputProvider> getInputProviders() {
        return inputProviders;
    }

    protected File getTreatmentDirectory() {
        String path = getTreatmentDirectoryPath();
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        return new File(path);
    }

}
