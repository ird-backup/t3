/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.opensymphony.xwork2.ActionContext;
import fr.ird.t3.T3Configuration;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.entities.reference.Idable;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.entities.reference.OceanImpl;
import fr.ird.t3.services.DecoratorService;
import fr.ird.t3.services.DefaultT3ServiceContext;
import fr.ird.t3.services.FreeMarkerService;
import fr.ird.t3.services.IOCService;
import fr.ird.t3.services.T3InputService;
import fr.ird.t3.services.T3OutputService;
import fr.ird.t3.services.T3Service;
import fr.ird.t3.services.T3ServiceContext;
import fr.ird.t3.services.T3ServiceFactory;
import fr.ird.t3.services.UserService;
import fr.ird.t3.services.ZoneStratumService;
import fr.ird.t3.web.T3ApplicationContext;
import fr.ird.t3.web.T3InternalTransactionFilter;
import fr.ird.t3.web.T3Session;
import fr.ird.t3.web.T3UserTransactionFilter;
import fr.ird.t3.web.actions.admin.TripListModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.StrutsStatics;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaTransactionAware;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.TimeLog;
import org.nuiton.util.decorator.Decorator;
import org.nuiton.web.struts2.BaseAction;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Action support for any t3 action.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3ActionSupport extends BaseAction implements TopiaTransactionAware {

    private static final long serialVersionUID = 1L;

    public static final String LOG_LINE =
            "--------------------------------------------------------------------------------";

    /** Logger. */
    private static final Log log = LogFactory.getLog(T3ActionSupport.class);

    private static final TimeLog TIME_LOG = new TimeLog(T3ActionSupport.class);

    private SimpleDateFormat dateFormat;

    private SimpleDateFormat monthFormat;

    protected transient TopiaContext transaction;

    protected transient TopiaContext internalTransaction;

    /**
     * Provides a way to get a service.
     * <p/>
     * Actions may <strong>not</strong> call it directly by use
     * {@link #newService(Class)} instead.
     */
    protected transient T3ServiceFactory serviceFactory;

    private transient T3ServiceContext serviceContext;

    public static T3ApplicationContext getT3ApplicationContext() {
        ActionContext actionContext = getActionContext();
        T3ApplicationContext applicationContext =
                T3ApplicationContext.getT3ApplicationContext(actionContext);
        return applicationContext;
    }

    public T3ServiceFactory getServiceFactory() {
        if (serviceFactory == null) {
            serviceFactory = new T3ServiceFactory();
        }
        return serviceFactory;
    }

    /**
     * Fabrique pour récupérer le ServiceContext tel qu'il devrait être fourni
     * à la fabrication d'un service.
     *
     * @return service context
     */
    protected T3ServiceContext getServiceContext() {
        if (serviceContext == null) {
            serviceContext = DefaultT3ServiceContext.newContext(
                    getLocale(),
                    getInternalTransaction(),
                    getTransaction(),
                    getApplicationConfig(),
                    getServiceFactory()
            );
        }
        return serviceContext;
    }

    protected T3Session getT3Session() {
        return T3Session.getT3Session(getActionContext());
    }

    protected <C> C getActionConfiguration(Class<C> configurationClass) {
        return getT3Session().getActionConfiguration(configurationClass);
    }

    public static String getApplicationVersion() {
        return getApplicationConfig().getApplicationVersion();
    }

    public static T3Configuration getApplicationConfig() {
        return getT3ApplicationContext().getConfiguration();
    }

    public final DecoratorService getDecoratorService() {
        return newService(DecoratorService.class);
    }

    public final FreeMarkerService getFreeMarkerService() {
        return newService(FreeMarkerService.class);
    }

    public final IOCService getIocService() {
        return newService(IOCService.class);
    }

    public final ZoneStratumService getZoneStratumService() {
        return newService(ZoneStratumService.class);
    }

    public final T3InputService getT3InputService() {
        return newService(T3InputService.class);
    }

    public final T3OutputService getT3OutputService() {
        return newService(T3OutputService.class);
    }

    public final UserService getUserService() {
        return newService(UserService.class);
    }

    public <O> Decorator<O> getDecorator(Class<O> type) {
        Decorator<O> decorator = getDecorator(type, null);
        return decorator;
    }

    public String decorate(Object o) {
        Decorator<?> decorator = getDecorator(o.getClass());
        String result = decorator.toString(o);
        return result;
    }

    public <O> Collection<String> decorate(Class<O> type,
                                           Collection<O> objects) {
        Decorator<O> decorator = getDecorator(type);
        Collection<String> result = Lists.newArrayList();
        for (O object : objects) {
            String s = decorator.toString(object);
            result.add(s);
        }
        return result;
    }

    public <O> Decorator<O> getDecorator(Class<O> type, String context) {
        Decorator<O> decorator = getDecoratorService().getDecorator(
                getLocale(), type, context);
        return decorator;
    }

    public String formatDate(Date date) {
        String result = getDateFormat().format(date);
        return result;
    }

    public String formatMonth(Date date) {
        String result = getMonthFormat().format(date);
        return result;
    }

    public final TopiaContext getInternalTransaction() {
        if (internalTransaction == null) {
            HttpServletRequest request = (HttpServletRequest)
                    getActionContext().get(StrutsStatics.HTTP_REQUEST);

            internalTransaction =
                    T3InternalTransactionFilter.getTransaction(request);
        }
        return internalTransaction;
    }

    public final TopiaContext getTransaction() {
        if (transaction == null) {
            HttpServletRequest request = (HttpServletRequest)
                    getActionContext().get(StrutsStatics.HTTP_REQUEST);

            transaction = T3UserTransactionFilter.getTransaction(request);
        }
        return transaction;
    }

    @Override
    public final void setTransaction(TopiaContext transaction) {
        // never use this one
        throw new UnsupportedOperationException();
    }

    protected void injectExcept(Class<?>... annotations) throws Exception {
        getIocService().injectExcept(this, annotations);
    }

    protected void injectOnly(Class<?>... annotations) throws Exception {
        getIocService().injectOnly(this, annotations);
    }

    protected <E extends T3Service> E newService(Class<E> clazz) {
        return getServiceContext().newService(clazz);
    }

    protected void addContentInFile(T3Session session, String content) {
        File userLogfile = session.getUserLogFile();
        try {
            FileWriter writer = new FileWriter(userLogfile, true);
            try {
                writer.append(content);
            } finally {
                writer.close();
            }
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Could not write content in user log file " + userLogfile,
                    e);
        }
    }

    protected <E extends TopiaEntity> Map<String, String> sortAndDecorate(Collection<E> beans) {
        Map<String, String> result = sortAndDecorate(beans, null);
        return result;
    }

    protected <E extends TopiaEntity> Map<String, String> sortAndDecorate(Collection<E> beans, String context) {
        Map<String, String> result =
                getDecoratorService().sortAndDecorate(getLocale(), beans, context);
        return result;
    }

    protected <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Collection<E> beans) {
        Map<String, String> result = sortAndDecorateIdAbles(beans, null);
        return result;
    }

    protected <E extends Idable> Map<String, String> sortAndDecorateIdAbles(Collection<E> beans, String context) {
        Map<String, String> result =
                getDecoratorService().sortAndDecorateIdAbles(getLocale(), beans, context);
        return result;
    }

    protected <E> List<E> sortToList(Collection<E> beans) {
        List<E> list = sortToList(beans, null);
        return list;
    }

    protected <E> List<E> sortToList(Collection<E> beans, String context) {
        List<E> list = getDecoratorService().sortToList(getLocale(), beans, context);
        return list;
    }

    protected Map<String, String> createTimeSteps() {
        Map<String, String> timeSteps = Maps.newLinkedHashMap();
        for (int i = 1; i < 13; i++) {
            timeSteps.put("" + i, "" + i);
        }
        return timeSteps;
    }

    protected SimpleDateFormat getDateFormat() {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        }
        return dateFormat;
    }

    protected SimpleDateFormat getMonthFormat() {
        if (monthFormat == null) {
            monthFormat = new SimpleDateFormat("mm-yyyy");
        }
        return monthFormat;
    }

    protected static ActionContext getActionContext() {
        return ActionContext.getContext();
    }

    protected TripListModel loadTripListModel(TripDAO tripDAO) throws TopiaException {

        long t0 = TimeLog.getTime();
        if (log.isInfoEnabled()) {
            log.info("Loading tripListModel...");
        }
        TripListModel result = new TripListModel();


        Multimap<Ocean, String> allByOcean = tripDAO.findAllIdsByOcean();

        result.setTripIdsByOcean(allByOcean);
        getT3Session().setTripListModel(result);

        List<Ocean> allOceans = Lists.newArrayList(allByOcean.keySet());

        if (allOceans.contains(null)) {

            // there is some trips with no oceans
            Ocean nullOcean = new OceanImpl();
            nullOcean.setLibelle(_("t3.common.nullOcean"));
            nullOcean.setTopiaId("null");

            if (log.isInfoEnabled()) {
                log.info("Add a nullOcean : " + nullOcean.getLibelle() +
                         " for " + allByOcean.get(null).size() +
                         " trip(s).");
            }
            allOceans.remove(null);
            allOceans.add(nullOcean);

            Collection<String> ids = allByOcean.get(null);
            allByOcean.putAll(nullOcean, ids);
            allByOcean.removeAll(null);
        }
        TIME_LOG.log(t0, "loadTripListModel for " + result.getNbTrips() +
                         " trips.");

        allOceans = sortToList(allOceans);
        result.setOceans(allOceans);

        getT3Session().setTripListModel(result);

        return result;
    }

    protected ImmutableMap<String, String> createLevel2UseSamplesOrNotMap() {
        return ImmutableMap.of(
                "false",_("t3.label.data.level2.configuration.useCatchSamples"),
                "true",_("t3.label.data.level2.configuration.useAllSamples")
        );
    }

    protected ImmutableMap<String, String> createLevel3UseSamplesOrNotMap() {
        return ImmutableMap.of(
                "false",_("t3.label.data.level3.configuration.useCatchSamples"),
                "true",_("t3.label.data.level3.configuration.useAllSamples")
        );
    }

    protected ImmutableMap<String, String> createLevel3UseWeightCategoriesOrNotMap() {
        return ImmutableMap.of(
                "true",_("t3.label.data.level3.configuration.useWeightCategories"),
                "false",_("t3.label.data.level3.configuration.ignoreWeightCategories")
        );
    }

    protected ImmutableMap<String, String> createUseRfMinus10AndRfPlus10OrNot() {
        return ImmutableMap.of(
                "true",_("t3.label.data.level1.configuration.useRfMinus10AndRfPlus10"),
                "false",_("t3.label.data.level1.configuration.useOnlyRfTot")
        );
    }

}