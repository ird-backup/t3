/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web;

import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaRuntimeException;
import org.nuiton.web.filter.TopiaTransactionFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;

/**
 * Deliver for each http request an internal db transaction.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.1.1
 */
public class T3InternalTransactionFilter extends TopiaTransactionFilter {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(T3InternalTransactionFilter.class);

    public static final String INTERNAL_TRANSACTION = "internalTransaction";

    public static TopiaContext getTransaction(ServletRequest request) {
        TopiaContext topiaContext = (TopiaContext)
                request.getAttribute(INTERNAL_TRANSACTION);
        return topiaContext;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        setRequestAttributeName(INTERNAL_TRANSACTION);
    }

    @Override
    protected TopiaContext beginTransaction(ServletRequest request) throws TopiaRuntimeException {
        T3ApplicationContext applicationContext =
                T3ActionSupport.getT3ApplicationContext();

        TopiaContext rootContext =
                applicationContext.getInternalRootContext();
        try {
            TopiaContext transaction = rootContext.beginTransaction();
            if (log.isDebugEnabled()) {
                log.debug("Starts a new internal transaction " + transaction);
            }
            return transaction;
        } catch (TopiaException eee) {
            throw new TopiaRuntimeException("Could not start transaction", eee);
        }
    }
}
