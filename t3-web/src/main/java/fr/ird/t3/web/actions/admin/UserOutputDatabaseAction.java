/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import fr.ird.t3.entities.user.T3User;
import fr.ird.t3.entities.user.UserDatabase;
import fr.ird.t3.entities.user.UserOutputDatabase;
import fr.ird.t3.entities.user.UserOutputDatabaseImpl;
import org.nuiton.topia.TopiaException;

import java.util.Collection;

/**
 * Operations of {@link UserDatabase}
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class UserOutputDatabaseAction extends AbstractUserDatabaseAction<UserOutputDatabase> {

    private static final long serialVersionUID = -2894789101651139566L;

    @Override
    public String getDatabaseType() {
        return OUPUT_DATABASE;
    }

    @Override
    protected UserOutputDatabase getDatabase(String id) throws TopiaException {
        return getUserService().getUserOutputDatabase(id);
    }

    @Override
    protected void create(String userId, UserOutputDatabase database) throws Exception {
        getUserService().addOutputDatabase(userId, database);
    }

    @Override
    protected void update(UserOutputDatabase database) throws Exception {
        getUserService().updateUserOuputDatabase(database);
    }

    @Override
    protected void delete(String userId, UserOutputDatabase database) throws Exception {
        getUserService().removeUserT3Database(userId, database.getTopiaId());
    }

    @Override
    public UserOutputDatabase getDatabaseConfiguration() {
        if (databaseConfiguration == null) {
            databaseConfiguration = new UserOutputDatabaseImpl();
        }
        return databaseConfiguration;
    }

    @Override
    protected Collection<UserOutputDatabase> getDatabases(T3User user) {
        return user.getUserOutputDatabase();
    }
}
