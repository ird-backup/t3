/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.admin;

import fr.ird.t3.entities.data.Trip;
import fr.ird.t3.entities.data.TripDAO;
import fr.ird.t3.services.ioc.InjectDAO;
import fr.ird.t3.web.actions.T3ActionSupport;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * To show all details of selected trips.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class TripDetailAction extends T3ActionSupport {

    private static final long serialVersionUID = 1L;

    @InjectDAO(entityType = Trip.class)
    protected transient TripDAO tripDAO;

    /** Selected trip ids. */
    protected String back;

    /** Selected trip ids. */
    protected List<String> tripIds;

    /** Selected loaded trips. */
    protected List<Trip> trips;

    public void setTripIds(List<String> tripIds) {
        this.tripIds = tripIds;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    @Override
    public String input() throws Exception {
        injectOnly(InjectDAO.class);

        if (CollectionUtils.isNotEmpty(tripIds)) {
            trips = tripDAO.findAllByIds(tripIds);
        }
        return INPUT;
    }

    public String getBoolean(boolean bool) {
        String result;
        if (bool) {
            result = _("t3.common.true");
        } else {
            result = _("t3.common.false");
        }
        return result;
    }
}
