/*
 * #%L
 * T3 :: Web
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package fr.ird.t3.web.actions.io.output;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ird.t3.actions.io.output.ExportAction;
import fr.ird.t3.actions.io.output.ExportConfiguration;
import fr.ird.t3.entities.reference.Country;
import fr.ird.t3.entities.reference.Ocean;
import fr.ird.t3.io.output.T3OutputOperation;
import fr.ird.t3.io.output.T3OutputProvider;
import fr.ird.t3.services.ioc.InjectDecoratedBeans;
import fr.ird.t3.web.actions.AbstractRunAction;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * To execute the {@link ExportRunAction}
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class ExportRunAction extends AbstractRunAction<ExportConfiguration, ExportAction> {

    private static final long serialVersionUID = 1L;

    protected List<T3OutputProvider<?, ?>> outputProviders;

    protected Map<String, String> operations;

    @InjectDecoratedBeans(beanType = Country.class, filterById = true, filterBySingleId = true)
    protected Map<String, String> fleets;

    @InjectDecoratedBeans(beanType = Ocean.class, filterById = true, filterBySingleId = true)
    protected Map<String, String> oceans;

    public ExportRunAction() {
        super(ExportAction.class);
    }

    @Override
    public void prepare() throws Exception {

        super.prepare();

        ExportConfiguration configuration = getConfiguration();

        outputProviders = Lists.newArrayList();
        T3OutputProvider<?, ?> outputProvider = configuration.getOutputProvider();
        outputProviders.add(outputProvider);

        operations = Maps.newLinkedHashMap();
        for (String operationId : configuration.getOperationIds()) {
            T3OutputOperation operation = outputProvider.getOperation(operationId);
            operations.put(operation.getId(), operation.getLibelle(getLocale()));
        }
    }

    public final List<T3OutputProvider<?, ?>> getOutputProviders() {
        return outputProviders;
    }

    public Map<String, String> getFleets() {
        return fleets;
    }

    public Map<String, String> getOceans() {
        return oceans;
    }

    public Map<String, String> getOperations() {
        return operations;
    }

    @Override
    protected Map<String, Object> prepareResumeParameters(ExportAction action,
                                                          Exception error,
                                                          Date startDate,
                                                          Date endDate) {
        Map<String, Object> map = super.prepareResumeParameters(action,
                                                                error,
                                                                startDate,
                                                                endDate
        );
        map.put("oceans", oceans);
        map.put("fleets", fleets);
        return map;
    }
}
