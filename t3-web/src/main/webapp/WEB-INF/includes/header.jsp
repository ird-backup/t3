<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript">

  jQuery(document).ready(function () {

    // apply common style
    $('fieldset').attr('class', 'ui-widget-content ui-corner-all')
  });
</script>
<div class='displayBlock'>

  <div class='floatLeft'>
    <s:a action="home" namespace="/">T3+</s:a>
  </div>

  <div id='headerRight'>
    <div>
      <s:text name="t3.label.user.login">
        <s:param>
          <s:property value="#session.t3Session.user.login"/>
        </s:param>
      </s:text>
      <ul>
        <li>
          <s:a action="logout" namespace="/user">
            <s:text name="t3.action.logout"/>
          </s:a>
        </li>
      </ul>
    </div>
    <br/>
    <%@ include file="i18n.jsp" %>
    <br/>

    <div>
      <s:text name="t3.label.user.actions"/>

      <ul>
        <s:if
          test="#session.t3Session !=null && #session.t3Session.userLogFile.exists()">
          <li>
            <s:a action="getUserLog" namespace="/user">
              <s:text name="t3.action.getUserLog"/>
            </s:a>
          </li>
        </s:if>
        <li>
          <s:a action="userForm" namespace="/user">
            <s:text name="t3.menu.admin.editUser"/>
            <s:param name="user.topiaId"
                     value="#session.t3Session.user.topiaId"/>
            <s:param name="userEditAction" value="'edit'"/>
          </s:a>
        </li>
      </ul>
    </div>
    <br/>
    <div>
      <ul>
        <li>
          <s:if
            test="!#session.t3Session.t3DatabaseSelected">
            <strong>
              <s:text name="t3.message.missingT3DatabaseSelected"/>
            </strong>
          </s:if>
          <s:else>
            <s:text name="t3.label.selected.t3Database">
              <s:param>
                <s:property value="#session.t3Session.t3DatabaseUrl"/>
              </s:param>
            </s:text>
          </s:else>
        </li>
        <li>
          <s:a action="selectUserT3Database!input" namespace="/user">
            <s:text name="t3.menu.select.t3Database"/>
          </s:a>
        </li>
      </ul>
    </div>
  </div>
</div>
<hr/>
