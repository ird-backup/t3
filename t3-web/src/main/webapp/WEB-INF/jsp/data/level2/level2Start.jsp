<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style type="text/css">
  .wwlbl {
    width: 500px;
  }
</style>
<title><s:text name="t3.label.data.treatment.level2"/></title>

<h2><s:text name="t3.label.data.treatment.level2"/></h2>

<s:form namespace="/level2">

  <jsp:include page="level2ConfigurationResume.jsp"/>

  <br/>

  <%-- back to step 1 --%>
  <s:submit action="configureLevel2Step1!input"
            key="t3.action.back.to.configuration.step1" align="right"/>

  <%-- back to step 2 --%>
  <s:submit action="configureLevel2Step2!input"
            key="t3.action.back.to.configuration.step2" align="right"/>

  <%-- launch action --%>
  <s:submit action="configureLevel2Resume" key="t3.action.runAction"
            align="right"/>

</s:form>
