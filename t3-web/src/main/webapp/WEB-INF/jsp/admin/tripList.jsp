<%--
  #%L
  T3 :: Web
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript"
        src="<s:url value='/js/gridHelper.js' />"></script>
<title><s:text name="t3.label.admin.trip.list"/></title>
<s:set var="userIsAdmin" value="%{#session.t3Session.user.admin}"/>

<script type="text/javascript">

  jQuery(document).ready(function () {

    $.subscribe('resetFilter', function (event) {
      // reselect all
      $('#filterZone :checkbox').attr('checked', false);
      $('#filterZone .filterClass').change();
      // re-apply filter
      $('#applyFilter').click();
    });
    var prefix = "filterForm_tripListModel_";
    $.useFilter(prefix + 'yearFilter', 'noYearFilter', prefix + 'years');
    $.useFilter(prefix + 'oceanFilter', 'noOceanFilter', prefix + 'oceanIds');
    $.useFilter(prefix + 'fleetFilter', 'noFleetFilter', prefix + 'fleetIds');
    $.useFilter(prefix + 'flagFilter', 'noflagFilter', prefix + 'flagIds');
    $.useFilter(prefix + 'vesselFilter', 'noVesselFilter', prefix + 'vesselIds');
  });
</script>

<h2><s:text name="t3.label.admin.trip.list"/></h2>

<s:form id='filterForm' namespace="/trip" action="tripListGrid" method="POST">

  <fieldset id='filterZone'>
    <legend>
      <s:text name="t3.label.tripFilter"/>
    </legend>
    <table class="fontsize11">
      <tr>
        <th><s:checkbox name="tripListModel.yearFilter" value="false"
                        cssClass="filterClass"
                        label="%{getText('t3.common.yearFilter')}"/></th>
        <th><s:checkbox name="tripListModel.oceanFilter" value="false"
                        cssClass="filterClass"
                        label="%{getText('t3.common.oceanFilter')}"/></th>
        <th><s:checkbox name="tripListModel.fleetFilter" value="false"
                        cssClass="filterClass"
                        label="%{getText('t3.common.fleetFilter')}"/></th>
        <th><s:checkbox name="tripListModel.flagFilter" value="false"
                        cssClass="filterClass"
                        label="%{getText('t3.common.flagFilter')}"/></th>
        <th><s:checkbox name="tripListModel.vesselFilter" value="false"
                        cssClass="filterClass"
                        label="%{getText('t3.common.vesselFilter')}"/></th>
      </tr>
      <tr>
        <td class='verticalAlignTop'>
          <div id='noYearFilter'>
            <s:text name="t3.common.notFiltered"/>
          </div>

          <s:checkboxlist key="tripListModel.years" list="years"
                          template="mycheckboxlist"
                          label='' labelSeparator="" value="false"
                          cssClass="hide"/>
        </td>
        <td class='verticalAlignTop'>
          <div id='noOceanFilter'>
            <s:text name="t3.common.notFiltered"/>
          </div>
          <s:checkboxlist key="tripListModel.oceanIds" list="oceans"
                          label='' labelSeparator="" value="false"
                          template="mycheckboxlist" cssClass="hide"/>

        </td>
        <td class='verticalAlignTop'>
          <div id='noFleetFilter'>
            <s:text name="t3.common.notFiltered"/>
          </div>
          <s:checkboxlist key="tripListModel.fleetIds" list="fleets"
                          label='' labelSeparator="" value="false"
                          template="mycheckboxlist" cssClass="hide"/>
        </td>
        <td class='verticalAlignTop'>
          <div id='noflagFilter'>
            <s:text name="t3.common.notFiltered"/>
          </div>
          <s:checkboxlist key="tripListModel.flagIds" list="flags"
                          label='' labelSeparator="" value="false"
                          template="mycheckboxlist" cssClass="hide"/>

        </td>
        <td class='verticalAlignTop'>
          <div id='noVesselFilter'>
            <s:text name="t3.common.notFiltered"/>
          </div>
          <s:checkboxlist key="tripListModel.vesselIds" list="vessels"
                          label='' labelSeparator="" value="false"
                          template="mycheckboxlist" cssClass="hide"/>
        </td>
      </tr>
    </table>


    <div class="cleanBoth floatRight">

      <s:url id="loadUrl" action="tripListGrid" namespace="/trip" escapeAmp="false"/>

      <sj:submit onClickTopics="resetFilter" key="t3.action.resetFilter"/>
      <sj:submit id='applyFilter' value="%{getText('t3.action.applyFilter')}"
                 targets="tripsGrid" />

      <%--href='%{loadUrl}'--%>

    </div>

  </fieldset>
</s:form>

<br/>

<sj:div id="tripsGrid">
  <fieldset>
  Appliquer pour visualiser les marées...
  </fieldset>
</sj:div>

