/*
 * #%L
 * T3 :: Web
 * *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

( function ($) {

    $.fn.extend(
            {
                prepareMonthPickers:function (options) {
                    $('[class~="hasDatepicker"]').each(function () {

                        $.prepareMonthPicker($(this), options);
                    });
                },

                prepareMonthPicker:function (picker, options) {
                    var inputValue = picker.val();

                    // let's use default options
                    var pickerOptions = {

                        // input field date format
                        dateFormat:'mm-yy',

                        // select months
                        changeMonth:true,

                        // select years
                        changeYear:true,

                        // authorize only caracters from dateformat
                        constrainInput:true,

                        // when user change month or year
                        onChangeMonthYear:function (year, month, inst) {

                            var newValue = (month < 10 ? "0" : "") +
                                           month + "-" + year;

                            var newDate = $.getDateFromInputValue(newValue);
                            var maxDate = inst.settings.maxDate;
                            var minDate = inst.settings.minDate;
                            if (maxDate.getTime() < newDate.getTime()) {

                                //console.warn("Too big date : " + newDate + " : max = " + maxDate);
                                newValue = $.datepicker.formatDate(inst.settings.dateFormat, maxDate);
                            } else if (minDate.getTime() > newDate.getTime()) {

                                //console.warn("Too low date : " + newDate + " : min = " + minDate);
                                newValue = $.datepicker.formatDate(inst.settings.dateFormat, minDate);
                            }


                            $(this).val(newValue);
                        },

                        // before showing date picker (to fix a nasty bug)
                        // the input date value is not push to picker ui
                        beforeShow:function (input, inst) {

                            // get input value
                            var inputValue = $(input).val();

                            // get date from date picker input field
                            var date = $.getDateFromInputValue(inputValue);

                            // set as default date (otherwise the date will not be used in ui)
                            inst.settings.defaultDate = date;
                        }
                    };

                    if (options) {

                        if (options.minDateAsMonth) {

                            // format incoming minDate with mm-yy format
                            options.minDate = $.getDateFromInputValue(options.minDateAsMonth);
                            options.minDateAsMonth = undefined;
                        }

                        if (options.maxDateAsMonth) {

                            // format incoming maxDate with mm-yy format
                            options.maxDate = $.getDateFromInputValue(options.maxDateAsMonth);
                            options.maxDateAsMonth = undefined;
                        }

                        if (options.minDate && options.maxDate) {

                            // can compute year Range
                            var yearRange = options.minDate.getFullYear() + ':' + options.maxDate.getFullYear();

                            options.yearRange = yearRange;
                        }

                        // add all incoming options
                        $.extend(pickerOptions, options);
                    }

                    // push options to date picker
                    picker.datepicker('option', pickerOptions);

                    // push back incoming value, otherwise value is empty
                    picker.val(inputValue);
                },

                getDateFromInputValue:function (inputValue) {
                    var date = null;
                    if (inputValue) {
                        date = $.datepicker.parseDate('dd-mm-yy', '01-' + inputValue);
                    }
                    return date;
                }
            });

    $.extend({

                 prepareMonthPickers:function (options) {
                     return $(document).prepareMonthPickers(options);
                 },
                 prepareMonthPicker:function (picker, options) {
                     return $(document).prepareMonthPicker(picker, options);
                 },
                 getDateFromInputValue:function (inputValue) {
                     return $(document).getDateFromInputValue(inputValue);
                 }
             });
})(jQuery);

